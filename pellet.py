import os, sys, logging, traceback, datetime
from aug_sfutils import read_imas
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.pellet')
logger.setLevel(logging.DEBUG)

try:
    import urllib3
except:
    try:
        import urllib2
    except:
        try:
            import urllib
        except:
            logger.error('No urllib(2, 3) found')
try:
    import imas
except:
    pass

import numpy as np
import aug_sfutils as sf
from trview import str_byt


def between_str(txt, str1, str2):

    txt  = str_byt.to_byt(txt)
    str1 = str_byt.to_byt(str1)
    str2 = str_byt.to_byt(str2)
    return txt.split(str1)[1].split(str2)[0].strip()


def pellet_penetration(m_pellet, Te, Bt, kappa, amin, v_pellet=565.):

    '''Input:
        m_pellet [10**19 particles]
        Te       [keV] (line-averaged)
        |Bt|     [T]
        kappa    [m]   (elongation)
        amin     [m]   (minor radius)
        v_pellet [m/s]

    Reference:
        http://iopscience.iop.org/article/10.1088/0029-5515/48/6/065009/meta
        E. Belonohy et al, Nuclear Fusion 48 (2008) 065009, eq 3.3 + Table 3
        Units: see table 1'''

    v_pellet /= 420. # (see paper, page 4, just before equation 3.3)
    fac = 0.25*np.power(m_pellet, 0.22) * np.power(v_pellet, 0.24 - 0.34*np.log(v_pellet))
    depth = fac * amin * np.power(Te, -0.67) * np.power(Bt, -0.41) * np.power(kappa, 1.28)

    return depth


class PELLET:


    Rpel_path = [1.259,  2.804]  # [m]
    Zpel_path = [1.000, -3.755]  # [m]
    alpha = np.arctan2(Zpel_path[1] - Zpel_path[0], Rpel_path[1] - Rpel_path[0])
    efficiency = 0.85   # Pellet efficiency: 85%


    def __init__(self, nshot, readSF=True, imasRun=None, trig=0.2, tbeg=0., tend=10., equ=None, equ_diag='EQI'):

        self.shot = nshot
        self.tbeg = tbeg
        self.tend = tend

        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.read_log()
            if hasattr(self, 'mass'):
                self.pellet_times(trig=trig)
                self.penetration_depth()
                if equ is None:
                    self.eqm = sf.EQU(self.shot, diag=equ_diag)
                else:
                    self.eqm = equ
                self.ablation()


    def read_inj(self):

        diag = 'INJ'
        inj = sf.SFREAD(self.shot, diag)
        if inj.status:
            self.tinj  = inj.gettimebase('pellFFMd')
            self.mass1 = inj.getobject('pellFFMd')
            self.mass2 = inj.getobject('pellFBMd')


    def pellet_parameters(self, gas, size0):

        if gas in ('D', 'D2'):
            Agas = 2
            Zgas = 1
            self.density = 0.6*1e20 # e/mm**3
        elif gas == 'ArD':
            Agas = 40
            Zgas = 18
            self.density = 0.6*1e20 # e/mm**3
        elif gas in('H', 'H2'):
            Agas = 1
            Zgas = 1
            self.density = 0.522*1e20 # e/mm**3
        else:
            logger.error(gas)

        if size0 == 1.9:
            self.sizes = [1.9, 1.9, 2.0]
        if size0 == 1.4:
            self.sizes = [1.4, 1.4, 1.5]
        if size0 == 5:
            self.sizes = [5., 5., 5.1]

        self.vol = self.sizes[0]*self.sizes[1]*self.sizes[2] # mm**3
        self.mass = self.vol*self.density*self.efficiency


    def read_log(self):

        f_log = '%s/pellet_logs.dat' %os.path.dirname(os.path.realpath(__file__))
        with open(f_log, 'r') as f:
            for line in f.readlines():
                sshot = line[:5]
                if int(sshot) == self.shot:
                    self.gas = line[7: 19].strip()
                    svel, ssize = line[20:].split()
                    self.vel = float(svel)
                    size = float(ssize)
                    self.pellet_parameters(self.gas, size)
                    break


    def read_jou(self):

        url = 'http://www.aug.ipp.mpg.de/cgibin/local_or_pass/journal.cgi?shot=%d' %self.shot
        logger.info('Parsing %s' %url)
        url_not_found = 'URLs %s not found' %url
        if 'urllib3' in sys.modules:
            http = urllib3.PoolManager()
            tmp = http.request('GET', url)
            txt = tmp.data
        elif 'urllib2' in sys.modules:
            txt = urllib2.urlopen(url).read()
        elif 'urllib' in sys.modules:
            txt = request.urlopen(url).read()

        try:
            pell_txt = between_str(txt, 'Pellets:', 'Heating')
        except:
            return

        self.gas = between_str(pell_txt, 'Gas:</i>', '\n')

        try:
            tmp = between_str(pell_txt, 'Size:</i>', 'mm')
            pell_size = tmp.split(b'x')
            size0 = float(pell_size[0])
        except:
            return

        try:
            self.rate = float(between_str(pell_txt, 'Rate:</i>', 'Hz'))
        except:
            self.rate = None
        self.vel  = float(between_str(pell_txt, 'Velocity:</i>', 'm/s').replace(',', '.'))

        tmp3 = between_str(pell_txt, 'Time:</i>', '</div>')
        pell_time = tmp3.split(b'-')
        try:
            self.time_jou = [float(x) for x in pell_time]
        except:
            self.time_jou = None
        self.pellet_parameters(self.gas, size0)


    def pellet_times(self, trig=0.2):

        sig = '5Co'
        self.trigger = trig
        pid = sf.SFREAD(self.shot, 'PID')
        if pid.status:
            self.pel_diag = 'PID'
        else:
            pid = sf.SFREAD(self.shot, 'PEL')
            if pid.status:
                self.pel_diag = 'PEL'
            else:
                logger.error('Neither PID nor PEL shotfiles found')
                return

        self.c5o  = pid.getobject(sig, cal=True)
        self.tpid = pid.gettimebase(sig)
        (ind_offset, ) = np.where(self.tpid > 11.)
        if len(ind_offset) > 0:
            offset = np.mean(self.c5o[ind_offset])
            self.c5o -= offset
        logger.info('Read PID/PEL 5co, signal length: %d', len(self.c5o))
        (ind_trig1, ) = np.where(self.c5o > trig)
# Remove consecutive indices, belonging to the same trigger
        if len(ind_trig1) < 1:
            logger.error('No pellet times found')
            return
        a = ind_trig1[1:]
        (ind, ) = np.where(np.diff(ind_trig1) > 1)
        ind_trig = np.append(ind_trig1[0], a[ind])
        n_trig = len(ind_trig)
        logger.debug('Pellets threshold: %8.4f', trig)
        logger.info('Pellets above threshold: %d', n_trig)
        ind_del = []
        for jt in range(n_trig-1, 0, -1):
            if np.min(self.c5o[ind_trig[jt-1]: ind_trig[jt]]) > 0.1*trig: #Remove points belonging to the same trigger
                ind_del.append(jt)
        ind_del = np.unique(ind_del)
        if len(ind_del) > 0:
            ind_trig = np.delete(ind_trig, ind_del)
        self.times = self.tpid[ind_trig]
        logger.debug('#Pellet times %d', len(self.times))


    def pellet_flux(self):

        dcs = sf.SFREAD(self.shot, 'DCS')
        self.t_dcs = dcs.gettimebase('PelFlo')
        self.pellet_flux = dcs('PelFlo') # e/s


    def penetration_depth(self,):
        '''Fetch signals for pellet_penetration and map them to a common timebase'''
# use self.times as time base, even though irregular

        eqscal = sf.SFREAD(self.shot, 'IDG')
        if not eqscal.status:
            eqscal = sf.SFREAD(self.shot, 'GQH')
            if not eqscal.status:
                eqscal = sf.SFREAD(self.shot, 'FPG')
                if not eqscal.status:
                    logger.error('Neither IDG nor GQH nor FPG found')
                    return

        fpc = sf.SFREAD(self.shot, 'FPC')
        ida = sf.SFREAD(self.shot, 'IDA')
        if ida.status:
            te_av = 1e-3*np.average(ida('Te'), axis=0) # [keV] Simplified, not exactly line average
            t_te = ida.gettimebase('Te')
        else:
            vta = sf.SFREAD(self.shot, 'VTA')
            if not vta.status:
                return
            else:
                te_vta = vta('Te_c')
                t_te = vta.gettimebase('Te_c')
                te_av = 1e-3*np.average(te_vta, axis=1) # [keV] Simplified, not exactly line average

        ahor = 0.5*(eqscal('Raus') - eqscal('Rin'))
        kapp = 0.5*(eqscal('Zoben') - eqscal('Zunt'))/ahor
        t_eq  = eqscal.gettimebase('Raus')
        bt    = np.abs(fpc('BTF'))
        t_fpc = fpc.gettimebase('BTF')

        amin  = np.interp(self.times, t_eq, ahor)
        kappa = np.interp(self.times, t_eq, kapp)
        btor  = np.interp(self.times, t_fpc, bt)
        teav  = np.interp(self.times, t_te, te_av)

        self.pen_depth = pellet_penetration(1e-19*self.mass, teav, btor, kappa, amin, v_pellet=self.vel)
        self.ablation_time = self.pen_depth/self.vel


    def ablation(self, coord_out='rho_tor'):
        '''Calculates the rho and time of the pellet ablation'''

        if not hasattr(self, 'pen_depth'):
            self.penetration_depth()
        if self.eqm.sf.status:
            R_sep_chord, Z_sep_chord = sf.cross_surf(self.eqm, t_in=self.times, \
                r_in=self.Rpel_path[0], z_in=self.Zpel_path[0], theta_in=self.alpha) # (n_t, n_rho, n_crossings)

            R_pel_stop = R_sep_chord[:, 0] + self.pen_depth*np.cos(self.alpha) # n_t
            Z_pel_stop = Z_sep_chord[:, 0] + self.pen_depth*np.sin(self.alpha) # n_t

            self.rho_dep = np.zeros(len(self.times))
            for jt, tim in enumerate(self.times):
                self.rho_dep[jt] = sf.rz2rho(self.eqm, R_pel_stop[jt], Z_pel_stop[jt], t_in=tim, coord_out=coord_out)


    def fromIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('pellets')

        pel = ids.pellets
        self.times = pel.time
        self.ablation_time = np.zeros_like(self.times)
        nt_pel = len(self.times)
        pelt = pel.time_slice

        if len(pelt) > 0:
            self.Rpel_path = np.zeros(2)
            self.Zpel_path = np.zeros(2)
            self.Rpel_path[0] = pelt[0].pellet[0].path_geometry.first_point.r
            self.Zpel_path[0] = pelt[0].pellet[0].path_geometry.first_point.z
            self.Rpel_path[1] = pelt[0].pellet[0].path_geometry.second_point.r
            self.Zpel_path[1] = pelt[0].pellet[0].path_geometry.second_point.z
            self.sizes = pelt[0].pellet[0].shape.size
            self.vel   = pelt[0].pellet[0].velocity_initial
            self.gas = pelt[0].pellet[0].species[0].label
            self.density = 1e-9*pelt[0].pellet[0].species[0].density # 1/m**3 -> 1/mm**3
            for itim in range(nt_pel):
                self.ablation_time[itim] = pelt[itim].time
            self.ablation_time -= self.times


    def fromH5(self):
        
        pel_ids = read_imas.read_imas_h5(self.shot, self.run, branch='pellets')
        pel = pel_ids['pellets']
        self.times = pel['time'][:]
        nt_pel = len(self.times)
        self.ablation_time = pel['time_slice[]&time'][:]
        if len(self.ablation_time) > 0:
            self.Rpel_path = np.zeros(2)
            self.Zpel_path = np.zeros(2)
            self.Rpel_path[0] = pel['time_slice[]&pellet[]&path_geometry&first_point&r' ][0, 0]
            self.Zpel_path[0] = pel['time_slice[]&pellet[]&path_geometry&first_point&z' ][0, 0]
            self.Rpel_path[1] = pel['time_slice[]&pellet[]&path_geometry&second_point&r'][0, 0]
            self.Zpel_path[1] = pel['time_slice[]&pellet[]&path_geometry&second_point&z'][0, 0]
            self.sizes = pel['time_slice[]&pellet[]&shape&size'][0, 0]
            self.vel   = pel['time_slice[]&pellet[]&velocity_initial'][0, 0]
            self.gas   = pel['time_slice[]&pellet[]&species[]&label'][0, 0, 0]
            self.density = 1e-9*pel['time_slice[]&pellet[]&species[]&density'][0, 0, 0] # 1/m**3 -> 1/mm**3


    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        pel = imas.pellets()
        pel.ids_properties.provider = os.getenv('USER')
        pel.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
        pel.ids_properties.homogeneous_time = 1

        pel.time = np.array(self.times, dtype=np.float32)
        nt_pel = len(self.times)
        pelt = pel.time_slice
        pelt.resize(nt_pel)
        sizes = np.array(self.sizes, dtype=np.float32)
        dens_m3 = 1e9*self.density # 1/mm**3 -> 1/m**3 

        for itim in range(nt_pel):
            pelt[itim].time = self.times[itim] + self.ablation_time[itim]
            pelt[itim].pellet.resize(1)
            pelt[itim].pellet[0].path_geometry.first_point.r = self.Rpel_path[0]
            pelt[itim].pellet[0].path_geometry.first_point.z = self.Zpel_path[0]
            pelt[itim].pellet[0].path_geometry.first_point.phi = 0.
            pelt[itim].pellet[0].path_geometry.second_point.r = self.Rpel_path[1]
            pelt[itim].pellet[0].path_geometry.second_point.z = self.Zpel_path[1]
            pelt[itim].pellet[0].path_geometry.second_point.phi = 0.

            pelt[itim].pellet[0].shape.type.index = 3
            pelt[itim].pellet[0].shape.size = np.array(sizes, dtype=np.float32)
            pelt[itim].pellet[0].species.resize(1)
            pelt[itim].pellet[0].velocity_initial = self.vel
#            pelt[itim].pellet[0].species[0].velocity_initial = self.vel
            pelt[itim].pellet[0].species[0].label = self.gas
            pelt[itim].pellet[0].species[0].density = dens_m3 # 1/mm**3 -> 1/m**3
            if self.gas in ('D', 'D2'):
                pelt[itim].pellet[0].species[0].a   = float(2)
                pelt[itim].pellet[0].species[0].z_n = float(1)
            elif self.gas in ('H', 'H2'):
                pelt[itim].pellet[0].species[0].a   = float(1)
                pelt[itim].pellet[0].species[0].z_n = float(1)

        return pel


if __name__ == '__main__':

    import matplotlib.pylab as plt

    nshot = 29554
    nshot = 34182
    nshot = 40498
#    nshot = 39649 # no pellets
    nshot = 34041
    nshot = 38684

    pel = PELLET(nshot)

    if hasattr(pel, 'gas'):
        print('Pellet gas:', pel.gas)
    if hasattr(pel, 'size'):
        print('Pellet size:', pel.size)
        print('Pellet mass:', pel.mass)
        print('Pellet velocity:', pel.vel)

    if hasattr(pel, 'mass1'):
        if pel.mass1 is not None:
            plt.figure(1, (13,8))
            plt.plot(pel.tinj, pel.mass1, label='mass1')
            plt.plot(pel.tinj, pel.mass2, label='mass2')
            plt.legend()

    if hasattr(pel, 'tpid'):
        plt.figure(2, (13,8))
        plt.plot(pel.tpid, pel.c5o, label='c50')
        if hasattr(pel, 'rho_dep'):
            plt.plot(pel.times, pel.rho_dep, label='rho_dep')
        plt.title('Ablation position for #%d' %pel.shot)
        plt.xlabel('Time [s]')
        plt.ylabel(r'$\rho_{tor}$')
        plt.ylim([0, 1])
        plt.legend()

    if hasattr(pel, 'ablation_time'):
        plt.figure(3, (13,8))
        plt.plot(pel.times, pel.ablation_time)
        plt.title('Ablation duration for #%d' %pel.shot)
        plt.xlabel('Time [s]')
        plt.ylabel('Ablation time [s]')

    plt.show()
