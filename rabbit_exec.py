import os, sys, logging
import numpy as np
from scipy.interpolate import CubicSpline
from trview import code_io, write_rabbit
from trview.rabbit import rabbit_wrapper as rbwrap

logger = logging.getLogger('trview.exe')
logger.setLevel(logging.INFO)
    

rbhome = '%s/rabbit' %os.getenv('SHARES_USER_DIR')
if not os.path.exists(rbhome):
    os.mkdir(rbhome)

locdir = os.path.dirname(os.path.realpath(__file__))


class RUN_RABBIT:


    def __init__(self, rb_input):

        self.rb_input = rb_input

        logger.info('Initialising RABBIT')

        with open(self.rb_input.rb_nml, 'w') as f:
            f.write(self.rb_input.namelist)
        rbwrap.n_nbi, rbwrap.nspc = self.rb_input.part_mix.shape
        rbwrap.nrho_in  = len(self.rb_input.rhot_in)
        rbwrap.n_leg = 2
        rbwrap.rb_nml = self.rb_input.rb_nml.ljust(120)

# Assuming Carbon imputiy. RABBIT has ADAS data only for Z=5,6,28 

        for key in ('output_timing', 'dt_in', 'nrho_out', \
            'aplasma', 'zplasma', 'aimp', 'zimp', 'a_beam', 'z_beam', \
            'rrect', 'zrect', 'part_mix', 'rhot_in'):
            setattr(rbwrap, key, self.rb_input.__dict__[key])

        for key in ('start_pos', 'unit_vec', 'width_poly'):
            setattr(rbwrap, key, self.rb_input.__dict__[key].T)

        rbwrap.nrrect = len(rbwrap.rrect)
        rbwrap.nzrect = len(rbwrap.zrect)
        rbwrap.ierr   = np.zeros(rbwrap.n_nbi, dtype=np.int32)

# Free memory
        rbwrap.dealloc_profiles()

# RABBIT initialisation (allocates)
        rbwrap.rabbit_init()

# RABBIT output profiles

        for key in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'porbloss', 'pcxloss', 'inbcd'):
            setattr(rbwrap, key, np.zeros(rbwrap.n_nbi, dtype=np.float32))

        for key in ('rhot_out', 'bdens_in', 'nrate', 'dvol', 'darea'):
            setattr(rbwrap, key, np.zeros(rbwrap.nrho_out, dtype=np.float32))

        for key in ('powe', 'powi', 'press', 'bdep', 'bdens', 'jfi', 'jnbcd', \
            'torqe', 'torqi', 'torqjxb', 'torqth', 'torqdepo', 'torqthcxloss', \
            'wfi_par', 'wfi_perp', 'wfi_par_lab'):
            setattr(rbwrap, key, np.zeros((rbwrap.nrho_out, rbwrap.n_nbi), dtype=np.float32))

        for key in ('bdep3', 'einj3', 'vinj3'):
            setattr(rbwrap, key, np.zeros((rbwrap.nrho_out, rbwrap.nspc, rbwrap.n_nbi), dtype=np.float32))

        rbwrap.bdep_legmoms = np.zeros((rbwrap.nrho_out, rbwrap.nspc, rbwrap.n_leg+1, rbwrap.n_nbi), dtype=np.float32)


    def step(self, jt_rb):

# rabbit_lib_step input

        rbwrap.einj = self.rb_input.einj
        for key in ('pnbi', 'rmag', 'zmag', 'psi_axis', 'psi_sep', \
            'ne_in', 'te_in', 'ti_in', 'omg_in', 'zef_in', \
            'ffp', 'vol', 'iota', 'area', 'psi1d_n', 'psi_rect'):
            setattr(rbwrap, key, self.rb_input.__dict__[key][jt_rb])

# RABBIT run

        rbwrap.rabbit_step()

        rbwrap.bdens_in = np.sum(rbwrap.bdens, axis=1) # For next call


class RABBIT:


    def __init__(self, rb_in, grab):

# Get input from shotfile
        logger.info('Getting RABBIT input from shotfile')

        self.time = np.atleast_1d(rb_in.profiles.time)
        nt = int(len(self.time))

        rb_in.equ.to_coco(cocos_out=5)

        self.INPUT  = type('', (), {})()

        self.shot = rb_in.gpar.shot

        self.INPUT.time  = self.time
        self.INPUT.shot  = self.shot

        self.INPUT.output_timing = 0.5
        self.INPUT.dt_in    = rb_in.gpar.dt_in
        self.INPUT.rhot_in  = rb_in.profiles.rho
        self.INPUT.nrho_out = grab.nrho_out_rb
        nrho_in = len(self.INPUT.rhot_in)
        self.INPUT.rb_nml   = '%s/rabbit.nml' %rbhome
#        self.INPUT.options = code_io.parse_rb_namelist(fnml=self.INPUT.rb_nml)
        self.INPUT.namelist =  grab.nml
        self.INPUT.options = code_io.parse_rb_namelist(nml=self.INPUT.namelist)
        flim = self.INPUT.options.physics.limiter_file.data
        self.INPUT.rlim, self.INPUT.zlim = np.loadtxt(flim, skiprows=1, unpack=True)

        self.INPUT.shot = rb_in.gpar.shot
        self.INPUT.width_poly = rb_in.nbi.beamwidthpoly
        self.INPUT.start_pos  = rb_in.nbi.start_pos
        self.INPUT.unit_vec   = rb_in.nbi.unit_vec
        self.INPUT.part_mix   = rb_in.nbi.part_mix

        self.INPUT.rrect   = rb_in.equ.Rmesh
        self.INPUT.zrect   = rb_in.equ.Zmesh
        self.INPUT.aplasma = rb_in.ion_d['Amain']
        self.INPUT.zplasma = rb_in.ion_d['Zmain']
        self.INPUT.aimp    = 12.
        self.INPUT.zimp    = 6.
        self.INPUT.a_beam  = rb_in.nbi.abeam
        self.INPUT.z_beam  = rb_in.nbi.zbeam
        self.INPUT.einj    = 1e3*rb_in.nbi.einj_kev

# Time dependent

        self.INPUT.psi_axis = rb_in.equ.psi0
        self.INPUT.psi_sep  = rb_in.equ.psix
        self.INPUT.rmag     = rb_in.equ.Rmag
        self.INPUT.zmag     = rb_in.equ.Zmag
        self.INPUT.pnbi     = rb_in.nbi.power_ds

# Profiles

        self.INPUT.ne_in  = rb_in.profiles.Ne.yfit_rhot
        self.INPUT.te_in  = rb_in.profiles.Te.yfit_rhot
        self.INPUT.ti_in  = rb_in.profiles.Ti.yfit_rhot
        self.INPUT.zef_in = np.repeat(rb_in.zef.data_ds, nrho_in).reshape((nt, nrho_in))
        self.INPUT.omg_in = rb_in.profiles.Angf.yfit_rhot

# Time-dependent equ input

        for key in ('ffp', 'vol', 'iota', 'area', 'psi1d_n'):
            self.INPUT.__dict__[key] = np.zeros((nt, nrho_in), dtype=np.float32)

        nr = len(self.INPUT.rrect)
        nz = len(self.INPUT.zrect)
        self.INPUT.psi_rect = np.transpose(rb_in.equ.pfm, axes=(2, 0, 1))

        for jt in range(nt):
            rho_eq = rb_in.equ.rhotor_trapz[jt]
            interp_pfl  = CubicSpline(rho_eq, rb_in.equ.pfl[jt], bc_type='natural')
            interp_ffp  = CubicSpline(rho_eq, 2.e-7*rb_in.equ.jpol[jt], bc_type='natural')
            interp_vol  = CubicSpline(rho_eq, rb_in.equ.vol[jt], bc_type=((1, 0.), 'natural'))
            interp_iota = CubicSpline(rho_eq, 1./rb_in.equ.q[jt], bc_type='natural')
            pfl                 = interp_pfl( self.INPUT.rhot_in)
            self.INPUT.ffp[jt]  = interp_ffp( self.INPUT.rhot_in)
            self.INPUT.vol[jt]  = interp_vol( self.INPUT.rhot_in)
            self.INPUT.iota[jt] = interp_iota(self.INPUT.rhot_in)
            self.INPUT.psi1d_n[jt] = (pfl - pfl[0])/(pfl[-1] - pfl[0])

        self.INPUT.area = self.INPUT.vol/(2.*np.pi*rb_in.equ.R0)

# Grids for NetCDF
        n_nbi, n_v = self.INPUT.part_mix.shape
        n_nbi, n_space = self.INPUT.start_pos.shape
        n_nbi, n_poly  = self.INPUT.width_poly.shape
        n_lim = len(self.INPUT.rlim)

        self.INPUT.cdf_dims = {'time': nt, 'rhot_in': nrho_in, 'rrect': nr, 'zrect': nz, \
            'spec': n_v, 'space': n_space, 'coeff': n_poly, 'n_nbi': n_nbi, 'n_lim': n_lim}

# Put attributes to plasma and geomertry parameters

        self.INPUT = code_io.rb_input_attr(self.INPUT)

# Dump input NetCDF file for debugging (GUI option)

        if grab.dump_rb_cdf:
            code_io.code2cdf(self, f_cdf='%s/ncdf/%d_in.cdf' %(rbhome, self.INPUT.shot))

# Dump RABBIT input files for debugging (GUI option)

        if grab.dump_rb_input:
            write_rabbit.write_rabbit(rb_in)

# Initialise Fortran-RABBIT

        rrb = RUN_RABBIT(self.INPUT)

# Class for time-dependent output

        self.OUTPUT = type('', (), {})()
        self.OUTPUT.time = self.time
        self.OUTPUT.shot = self.shot

        for key in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'porbloss', 'pcxloss', 'inbcd'):
            setattr(self.OUTPUT, key, np.zeros((nt, rbwrap.n_nbi), dtype=np.float32))
        for key in ('nrate', 'dvol', 'darea'):
            setattr(self.OUTPUT, key, np.zeros((nt, rbwrap.nrho_out), dtype=np.float32))
        for key in ('powe', 'powi', 'press', 'bdep', 'bdens', 'jfi', 'jnbcd', \
            'torqe', 'torqi', 'torqjxb', 'torqth', 'torqdepo', 'torqthcxloss', \
            'wfi_par', 'wfi_perp', 'wfi_par_lab'):
            setattr(self.OUTPUT, key, np.zeros((nt, rbwrap.nrho_out, rbwrap.n_nbi), dtype=np.float32))
        for key in ('bdep3', 'einj3', 'vinj3'):
            setattr(self.OUTPUT, key, np.zeros((nt, rbwrap.nrho_out, rbwrap.nspc, rbwrap.n_nbi), dtype=np.float32))
        self.OUTPUT.rhot_out = np.zeros(rbwrap.nrho_out, dtype=np.float32)

        for jt, tim in enumerate(self.time):

# Execute RABBIT

            logger.info('RABBIT calculation, time %5.3f', tim)

            rrb.step(jt)

# Collect output in time-dependent arrays

            if jt == 0:
                self.OUTPUT.rhot_out[:] = getattr(rbwrap, 'rhot_out') # time-independent rho-grid

            for key in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'porbloss', 'pcxloss', 'inbcd', \
                'nrate', 'dvol', 'darea', 'powe', 'powi', 'press', 'bdep', 'bdens', 'jfi', 'jnbcd', \
                'torqe', 'torqi', 'torqjxb', 'torqth', 'torqdepo', 'torqthcxloss', \
                'wfi_par', 'wfi_perp', 'wfi_par_lab', \
                'bdep3', 'einj3', 'vinj3'):
                self.OUTPUT.__dict__[key][jt] = getattr(rbwrap, key)

        self.OUTPUT.cdf_dims = {'rhot_out': rbwrap.nrho_out}
        self.OUTPUT = code_io.rb_output_attr(self.OUTPUT)
