import os, shutil, logging
import numpy as np
from scipy.interpolate import interp1d
from trview import config, locate_dir, write_u, rabbit_pos

logger = logging.getLogger('trview.astra_nml_u')

awd = os.getenv('AWD')
if awd is None:
    awd = locate_dir.locate('pyparse')
#if awd is None:
#    awd = locate_dir.locate_rec('pyparse')
tlbl = 'Time'.ljust(20) + 'Seconds'


def split_line(arr, dims, lbl, fmt='%10.4e'):

    tmp = np.cumsum( np.append([0], np.array(dims)) )
    nml = ''
    for jdim in range(len(dims)):
        nml += '   %s(%d) = ' %(lbl, tmp[jdim]+1)
        for jch in range(tmp[jdim], tmp[jdim+1]):
            nml += fmt %arr[jch]
            if jch < tmp[jdim+1] - 1:
                nml += ', '
            else:
                nml += '\n'

    return nml


def spider():

    sp_nml  = """
&spider

   kprs = 0
   k_grids = 0
   epsros = 1.d-9
   enelss  = 1.d-7
   key_plcs = 1
   toric_fourc = 11
   strahl_fourc = 4
   write_coils_diagn = 0
   k_filessss = 0
   psplexavg = 0.01
   psplexavgexp = 0. !exponent of Ip in psplexavg
   fixadapgrid = 0 !0 for standard spider adaptive grid, 1 for EF variation
/

&equil_settings

   time_fix_eqpff = 0.0021
   fix_eqpf_eqff = 0
   cheb_degree = 5
   spidat_yes = 1
   iter_one_only_fbe = 0
   advanced_methods = 3
   i3method = 3
   diagnostic_gsef = 0
   do_adcmp = 0
   urelax = 0.5
   urelax2 = 0.1
   murelax2 = 0.2
   ydiff = 1.e2
   ydiff2 = 1.
   max_iter = 10000
   miter_ext = 1
   interp_routine = 2
   interp_method_rect = 4
   epsf_tol = 1.e-9
   epss_tol = 1.e-5
   epsv_tol = 1.e-5
   epsg_tol = 1.e-8
   key_no_startz = 0
   key_no_refits = 0

/
"""

    return sp_nml
    

def rabbit_nml(nbi, tav=True):

    if not hasattr(nbi, 'power_mix'):
        return

    nshot = nbi.shot

# Namelist

    n_box1 = nbi.n_box[0]
    n_box2 = nbi.n_box[1]
    n_nbi = np.sum(nbi.n_box)
    einj_ev = 1e3*nbi.einj_kev

    xyz_src_m, xyz_vec = rabbit_pos.rabbit_pos(nbi)

    if tav:
        ext = 'NBI_AVG'
    else:
        ext = 'NBI'

# RABBIT

    rb_nml = '&rabbit_beam_geo\n\n'

    for jnb in range(n_nbi):
        rb_nml += '   start_pos(1:3, %d) =' %(jnb+1)
        rb_nml += '%11.7f,%11.7f,%11.7f\n' %(xyz_src_m[jnb, 0], xyz_src_m[jnb, 1], xyz_src_m[jnb, 2])
    rb_nml += '\n'
    for jnb in range(n_nbi):
        rb_nml += '   unit_vec(1:3, %d) =' %(jnb+1)
        rb_nml += '%11.7f,%11.7f,%11.7f\n' %(xyz_vec[jnb, 0], xyz_vec[jnb, 1], xyz_vec[jnb, 2])
    rb_nml += '\n'

    rb_nml += """
   width_poly(1:3, 1) = 0.0000000, 0.0098729131, 0.0000000
   width_poly(1:3, 2) = 0.0000000, 0.0098729131, 0.0000000
   width_poly(1:3, 3) = 0.0000000, 0.0098729131, 0.0000000
   width_poly(1:3, 4) = 0.0000000, 0.0098729131, 0.0000000
   width_poly(1:3, 5) = 0.0000000, 0.0116500000, 0.0000000
   width_poly(1:3, 6) = 0.0000000, 0.0116500000, 0.0000000
   width_poly(1:3, 7) = 0.0000000, 0.0116500000, 0.0000000
   width_poly(1:3, 8) = 0.0000000, 0.0116500000, 0.0000000

/

&partmix

"""

    for jnb in range(n_nbi):
        rb_nml += '   part_mix(1:3, %d) =' %(jnb+1)
        rb_nml += '%11.7f,%11.7f,%11.7f\n' %(nbi.part_mix[jnb, 0], nbi.part_mix[jnb, 1], nbi.part_mix[jnb, 2])
    rb_nml += '\n/\n\n'

    rb_nml += '&nbi_par\n\n'
    rb_nml += '   n_nbi = %d\n' %n_nbi
    rb_nml += '   a_beam = '
    for jnb in range(n_nbi-1):
        rb_nml += ' %2.1f,' %nbi.abeam[jnb]
    rb_nml += ' %2.1f\n'    %nbi.abeam[-1]
    rb_nml += '   z_beam = '
    for jnb in range(n_nbi-1):
        rb_nml += ' %2.1f,' %nbi.zbeam[jnb]
    rb_nml += ' %2.1f\n'    %nbi.zbeam[-1]
    rb_nml += "   pinj_file = 'udb/%d/P%d.%s'\n" %(nshot, nshot, ext)
    rb_nml += split_line(einj_ev, [n_box1, n_box2], 'einj', fmt='%10.4e')
    rb_nml += '\n/\n\n'

    rb_nml += """
&species
   Aimp = 200.00
   Zimp = 100.00
/
&output_settings
   it_orbout = -1
/
&physics
   jumpcor = 2
   table_path   = '%s'
   limiter_file = '%s'
   Rlim = 2.20 !AUG defaults, overridden if limiter_file exists
   zlim = 0.20
   Rmax = 2.26 !AUG defaults
   Rmin = 1.08
   torqjxb_model = 3
/

&numerics
   norbits=20
   orbit_dt_fac = 1.56
   distfun_nv = 200
   distfun_vmax = 4.5e6
/
""" %(config.rabbitTable, config.rabbitLimiter)

# NBINJ

    nb_nml = '&nbinj_par\n\n'
    for jnb in range(n_nbi):
        nb_nml += '   power_mix(1:3, %d) =' %(jnb+1)
        nb_nml += '%11.7f,%11.7f,%11.7f\n' %(nbi.power_mix[jnb, 0], nbi.power_mix[jnb, 1], nbi.power_mix[jnb, 2])
    nb_nml += '\n'
    nb_nml += split_line(n_nbi*[0] , [n_box1, n_box2], 'CONTR', fmt='%5.2f')
    nb_nml += split_line(n_nbi*[1] , [n_box1, n_box2], 'CBMS1', fmt='%5.2f') # orb_av
    nb_nml += split_line(n_nbi*[81], [n_box1, n_box2], 'CBMS2', fmt='%5.2f') # penc_num

    if not hasattr(nbi, 'hbeam'):
        nbi.tr2as()
    nb_nml += split_line(nbi.hbeam , [n_box1, n_box2], 'HBEAM' , fmt='%10.7f')
    nb_nml += split_line(nbi.rbtmax, [n_box1, n_box2], 'rb_max', fmt='%10.7f')
    nb_nml += split_line(nbi.rbtmin, [n_box1, n_box2], 'rb_min', fmt='%10.7f')
    nb_nml += split_line(nbi.tgA   , [n_box1, n_box2], 'CBMS3' , fmt='%10.7f')    # tan(alpha)
    nb_nml += split_line(n_nbi*[nbi.aspect], [n_box1, n_box2], 'CBMS4', fmt='%10.7f') # Aspect
    nb_nml += split_line(n_nbi*[4], [n_box1, n_box2], 'CBMH1', fmt='%5.2f')
    nb_nml += split_line(n_nbi*[2], [n_box1, n_box2], 'CBMH2', fmt='%5.2f')
    nb_nml += split_line(n_nbi*[4], [n_box1, n_box2], 'CBMR1', fmt='%5.2f')
    nb_nml += split_line(n_nbi*[2], [n_box1, n_box2], 'CBMR2', fmt='%5.2f')

    nb_nml += '\n/\n'

    return rb_nml, nb_nml


def torbeam_nml(ech, tav=True):

# ECN signals for theta, phi, freq from shot 23187
# ECRH3 from shot 33725

    if ech is None:
        return
    if not hasattr(ech, 'p_ecs'):
        return ''
    nshot = ech.shot
    ngy = len(ech.freq)

# Parameter output: TORBEAM piece of namelist

    if hasattr(ech, 'freq'):

        nmod = -1                 # -1 X-mode, 1 O-mode

        if tav:
            ext = 'ECH_AVG'
        else:
            ext = 'ECH'
        tb_nml  = '&torbeam\n\n'
        tb_nml += '   n_gyro = %d\n' %ngy
        tb_nml += "   pecr_file  = 'udb/%d/P%d.%s'\n"   %(nshot, nshot, ext)
        tb_nml += "   theta_file = 'udb/%d/THEAS%d.%s'\n" %(nshot, nshot, ext)
        tb_nml += "   phi_file   = 'udb/%d/PHIAS%d.%s'\n" %(nshot, nshot, ext)
        tb_nml += '   beam_on = '
        for jgy in range(ngy):
            tb_nml += str(ech.avail[jgy])[0] # 'T' or 'F'
            if jgy < ngy-1:
                tb_nml += ', '
            else:
                tb_nml += '\n'

        nmod_str = '%d, ' %nmod 
        tb_nml += '   nmod = ' + (ngy - 1)*nmod_str + ' %d\n' %nmod

        tb_nml += split_line(ech.freq        , ech.n_gy, 'freq_n' , fmt='%10.4e')
        tb_nml += split_line(ech.phi_ps      , ech.n_gy, 'phi_n'  , fmt='%10.4e')
        tb_nml += split_line(ech.the_ps      , ech.n_gy, 'theta_n', fmt='%10.4e')
        tb_nml += split_line(100.*ech.R      , ech.n_gy, 'RR_n'   , fmt='%10.4e')
        tb_nml += split_line(100.*ech.z      , ech.n_gy, 'ZZ_n'   , fmt='%10.4e')
        tb_nml += split_line(100.*ech.width_y, ech.n_gy, 'xwyyb'  , fmt='%10.4e')
        tb_nml += split_line(100.*ech.width_z, ech.n_gy, 'xwzzb'  , fmt='%10.4e')
        tb_nml += split_line(100.*ech.curv_y , ech.n_gy, 'xryyb'  , fmt='%10.4e')
        tb_nml += split_line(100.*ech.curv_z , ech.n_gy, 'xrzzb'  , fmt='%10.4e')
        tb_nml += '\n/\n'

    return tb_nml


def pellet_nml(pellet):

    nshot = pellet.shot

    pel_nml = '&pellet\n\n'
    pel_nml += "   rho_abl_file  = 'udb/%d/PEL%d.RHO_ABL'\n" %(nshot, nshot)
    pel_nml += "   time_abl_file = 'udb/%d/PEL%d.TIM_ABL'\n" %(nshot, nshot)
    pel_nml += '   mass = %8.4f\n' %(1e-19*pellet.mass)
    pel_nml += '\n/\n'

    return pel_nml


def astra_nml_u(plasma, wr_nbi2=False, wr_spider=True, tav=False):

# Boundary u-files for SPIDER, NBI and ECH u-files
    nshot = plasma.gpar.shot
    udir = '%s/udb/%d' %(awd, nshot)
    write_u.as_ubnd(plasma.equ.sep, udir=udir)
    write_u.write_u1d(plasma, code='as', udir=udir)

# Initial q, write first in $HOME/udb/<nshot>

    write_u.write_q2d(plasma.equ)

# Namelist and ECH, NBI u-files
    nml_dir = '%s/exp/nml' %awd
    os.system('mkdir -p %s' %nml_dir)

    fnml = '%s/aug%d' %(nml_dir, nshot)
    if os.path.isfile(fnml):
        backup = fnml+ '~'
        logger.info('mv %s %s', fnml, backup)
        shutil.move(fnml, backup)

    f = open(fnml, 'w')
    if plasma.nbi.status:
        rbnml, nbnml = rabbit_nml(plasma.nbi, tav=tav)
        f.write(rbnml)
        f.write('\n')
        if wr_nbi2:
            f.write(nbnml)
            f.write('\n')

    f.write(torbeam_nml(plasma.ech, tav=tav))

    if hasattr(plasma.pellet, 'rho_dep'):
        f.write('\n')
        f.write(pellet_nml(plasma.pellet))

    if wr_spider:
        f.write('\n')
        f.write(spider())

    f.close()

    logger.info('Stored %s', fnml)


if __name__ == '__main__':


    import argparse
    import aug_sfutils as sf
    import ech, nbi, signals, fit_sep

    parser = argparse.ArgumentParser(description='ASTRA input generator')
    parser.add_argument('-x', '--shot', type=int, help='Shotnr.', required=True)
    parser.add_argument('-n', '--nthe', type=int, help='Nr of BND points', required=False, default=90)
    parser.add_argument('-s', '--tbeg', type=float, help='Start time', required=False, default=0.)
    parser.add_argument('-e', '--tend', type=float, help='End time', required=False, default=10.)
    parser.add_argument('-equ', '--equ_dia', help='Equ diag', required=False, default='EQI')
    parser.add_argument('-tav', action='store_true', help='Time av. time traces', required=False)
    parser.add_argument('-nbi2', action='store_true', help='Write nml for NBI routine', required=False)
    parser.add_argument('-spider', action='store_true', help='Write nml for SPIDER', required=False)

    args = parser.parse_args()

    nshot = args.shot
    tbeg  = args.tbeg
    tend  = args.tend

    if tend <= tbeg:
        logger.error('tend must be larget than tbeg')
        logger.error('tbeg=%8.4f, tend=%8.4f', tbeg, tend)
        logger.error('Exiting')
    else:
# ASTRA namelist
        logger.info('%d %6.3f %6.3f', nshot, tbeg, tend)
        nmom = 6
        n_the += 1
        nbi = nbi.NBI(nshot, tbeg=tbeg, tend=tend)
        ech = ech.gyros(nshot, tbeg=tbeg, tend=tend)
        equ = sf.EQU(nshot, tbeg=tbeg, tend=tend, diag=equ_dia)
        sig = signals.SIGNALS(equ, tbeg=tbeg, tend=tend)
        pel = pellet.PELLET(nshot, equ=equ, tbeg=tbeg, tend=tend)
        geofit = fit_sep.geofit(equ, nmom=nmom, n_the=n_the)
        astra_nml_u(geofit, sig, nbi, ech, pel, wr_nbi2=args.nbi2, wr_spider=args.spider, tav=args.tav)
