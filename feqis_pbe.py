import sys, logging, time
import numpy as np
from scipy.linalg.lapack import dgels
from trview import fit_sep
import aug_sfutils as sf
from jacobian_inversion import jacobInv

fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')
logger = logging.getLogger('feqis')
hnd = logging.StreamHandler()
hnd.setFormatter(fmt)
logger.addHandler(hnd)
logger.setLevel(logging.INFO)

flt = np.float64
max_iter = 500

def feqis_run(feqisInput):

    jt, time_in, Rax_in, iplasma, rbphi, psib, Rb, Zb, ffprime, pprime = feqisInput

    nrho   = len(ffprime)
    ntheta = len(Rb)
    Zax_in = 0.

    muvac = 4.e-7*np.pi
    gpi4  = (2.*np.pi)**2

    psiax = 0. # psib has to be consistent with psi_axis=0; it is constant throughout all iterations
    psin_grid_in = np.linspace(0, 1, nrho, endpoint=True, dtype=flt)
    psin_grid = psin_grid_in # f(rho)
    epprime  = -gpi4*muvac*pprime
    effprime = -gpi4*ffprime

    psi = np.repeat(psin_grid_in, ntheta).reshape((nrho, ntheta))
    fpol = np.zeros_like(psin_grid)
    lambda2d  = np.repeat(psin_grid_in, ntheta).reshape((nrho, ntheta))
    lambda2dp = lambda2d + 0.5/(nrho - 1.)
    psin_gridp = 0.5*(psin_grid[1:] + psin_grid[:-1])

    X0  = Rax_in
    Y0  = Zax_in
    X0o = X0
    Y0o = Y0

    jrho_axis = 0
    jthe_axis = 0
    relambda_flag = False
    tsum = np.zeros(3)
    tim = np.zeros(4)
    matrix = np.ones((2*ntheta + 1, 6), dtype=flt)
    for jiter in range(max_iter):
        tim[0] = time.time()
        if relambda_flag:
            fpol[-1] = rbphi # not used!
            fpol[-2] = np.sqrt(rbphi**2 - ffprime[-1] * (psin_grid[-1] - psin_grid[-2]) * (psib - psiax))
            for jrho in range(nrho-3, -1, -1):
                fpol[jrho] = np.sqrt(fpol[jrho+1]**2 - ffprime[jrho+1] * (psin_grid[jrho+2] - psin_grid[jrho]) * (psib - psiax))
            conv = (2.*np.arange(nrho-1) + 1)*np.diff(psin_grid)/(fpol[:-1]*dArea2_X2)
            fpol2 = np.append(0, np.cumsum(conv))
            psin_grid = 0.5*psin_grid + 0.5*fpol2/fpol2[-1]
            psin_gridp = 0.5*(psin_grid[1:] + psin_grid[:-1])

        psin2d = (psi - psiax)/(psib - psiax)
        tim[1] = time.time()
        jac = jacobInv(Rb, Zb, X0, Y0, lambda2d, lambda2dp, psin_grid, psin_gridp, psin2d, iplasma, psib, effprime, epprime, relambda_flag)
        tim[2] = time.time()
        jrho_axis, jthe_axis = np.unravel_index(jac.psi.argmin(), jac.psi.shape)

# Lapack DGELS: m=2*ntheta+1, n=6, nrhs=1; a=matrix(m, n), b=gpsi(MAX(m, n), nrhs)

        if jrho_axis == 0:
            gpsi = np.hstack((jac.psi[0, 0], jac.psi[1, :], jac.psi[2, :]))
            matrix[:, 1] = np.hstack((jac.X[0, 0], jac.X[1, :], jac.X[2, :]))
            matrix[:, 4] = np.hstack((jac.Y[0, 0], jac.Y[1, :], jac.Y[2, :]))
            matrix[:, 0] = matrix[:, 1]**2
            matrix[:, 3] = matrix[:, 4]**2
            matrix[:, 5] = matrix[:, 1]*matrix[:, 4]
            _, xpsi, _ = dgels(matrix, gpsi)
            denom = 4.*xpsi[0]*xpsi[3] - xpsi[5]**2
            X0 = (xpsi[5]*xpsi[4] - 2.*xpsi[3]*xpsi[1])/denom
            Y0 = (xpsi[5]*xpsi[1] - 2.*xpsi[0]*xpsi[4])/denom
            psiax = xpsi[0]*X0**2 + xpsi[1]*X0 + xpsi[2] + xpsi[3]*Y0**2 + xpsi[4]*Y0 + xpsi[5]*X0*Y0
        else:
            X0 = jac.X[jrho_axis, jthe_axis]
            Y0 = jac.Y[jrho_axis, jthe_axis]
            psiax = jac.psi[jrho_axis, jthe_axis]

        relambda_flag = (jrho_axis == 0 and jthe_axis == 0)
        if abs(X0) > 20 and abs(Y0) > 20:
            logger.error('ERROR: x0, y0 too large!')
            sys.exit()

        axis_change = abs(X0 - X0o)/X0 + abs(Y0 - Y0o)/X0
        if axis_change < 1.e-6:
            logger.info('jt=%d FEQIS converged, step #%d', jt, jiter)
            break # Exit iteration loop
        X0o = X0
        Y0o = Y0
        lambda2d  = jac.lambda2d
        lambda2dp = jac.lambda2dp
        dArea2_X2 = jac.dArea2_X2
        psi       = jac.psi
        tim[3] = time.time()
        tsum += np.diff(tim)

    print(tsum)
    return jac.X, jac.Y, jac.psi


class FEQIS_PBE:


    def __init__(self, feq_d, eqm, t_in=None, parallel=False):

        logger.info('Starting FEQIS')
        if t_in is None:
            t_in = eqm.time
        self.time = np.atleast_1d(t_in)
        self.shot = eqm.shot

        tim_ind = []
        for tim in self.time:
            j_t = np.argmin(np.abs(eqm.time - tim))
            tim_ind.append(j_t)

        psi_sign = -1. #  Input is COCO 5, not 17; output is 13?
        fac_psi  = psi_sign #* (2.*np.pi)
        nrho = feq_d['nrho']
        ntheta = feq_d['ntheta']
        
        psin_grid_in = np.linspace(0, 1, nrho)
        nt = len(tim_ind)
        self.pprime_in  = np.zeros((nt, nrho), dtype=np.float32)
        self.ffprime_in = np.zeros_like(self.pprime_in)

        self.R0 = eqm.R0
        self.ipl_in = 1.e-6*eqm.ip[tim_ind]
        self.rbphi = np.abs(eqm.B0[tim_ind])*eqm.R0
        self.psib_in = (eqm.psix[tim_ind] - eqm.psi0[tim_ind])* fac_psi # psiax_in = 0
        for jt, jtim in enumerate(tim_ind):
            psin = eqm.psiN[jt]
# Interpolate profiles on regular psin_grid_in
            self.pprime_in[jt]  = np.interp(psin_grid_in, psin, eqm.dpres[jtim])
            self.ffprime_in[jt] = np.interp(psin_grid_in, psin, eqm.ffp[jtim])
        self.pprime_in  *= 1./fac_psi
        self.ffprime_in *= 1./fac_psi

# Take regular R,z at separatrix with a Fourier-moments fit (DESCUR)
        sep = fit_sep.geofit(equ=eqm, nmom=feq_d['m_moms'], n_the=ntheta+1)
        self.rfit = sep.rfit[:, :-1]
        self.zfit = sep.zfit[:, :-1]

        if parallel:
            self.multi()
        else:
            self.serial()


    def multi(self):

        from multiprocessing import Pool, cpu_count

        timeout_pool = 200
        print('CPUs', cpu_count())
        pool = Pool(cpu_count())

        nt, ntheta = self.rfit.shape
        nrho = len(self.ffprime_in[0])
# Execute FEQIS
        out = pool.map_async(feqis_run, [(jt, self.time[jt], self.R0, self.ipl_in[jt], self.rbphi[jt], \
            self.psib_in[jt], self.rfit[jt, :], self.zfit[jt, :], self.ffprime_in[jt], self.pprime_in[jt]) \
            for jt in range(nt)]).get(timeout_pool)
        pool.close()
        pool.join()

        out = np.array(out)
        self.rsurf   = out[:, 0, :, :]
        self.zsurf   = out[:, 1, :, :]
        self.psi_out = out[:, 2, :, :]

        logger.info('FEQIS done') 


    def serial(self):

# Time-dependent output
        nt, ntheta = self.rfit.shape
        nrho = len(self.ffprime_in[0])

        self.rsurf   = np.zeros((nt, nrho, ntheta), dtype=flt)
        self.zsurf   = np.zeros_like(self.rsurf)
        self.psi_out = np.zeros_like(self.rsurf)
        for jt in range(nt):
            feqis_in = (jt, self.time[jt], self.R0, self.ipl_in[jt], self.rbphi[jt], self.psib_in[jt], \
                self.rfit[jt, :], self.zfit[jt, :], self.ffprime_in[jt], self.pprime_in[jt])
            self.rsurf[jt], self.zsurf[jt], self.psi_out[jt] = feqis_run(feqis_in)

        logger.info('FEQIS done') 


if __name__ == '__main__':

    import matplotlib.pylab as plt

    shot = 38384
    tim = np.linspace(2.0, 3.4, 3)
    feq_d = {'nrho': 31, 'm_moms': 7, 'ntheta': 101}
    eqm = sf.EQU(shot, diag='EQH')
    eqm.timeDownsample(tim)
    eqm.to_coco(cocos_out=5)
    feq = FEQIS_PBE(feq_d, eqm, t_in=tim, parallel=False)

# Plot

    plt.figure(1, (9, 12))
    plt.subplot(1, 1, 1, aspect='equal')
    nrho, ntheta = feq.rsurf.shape[-2:]
    ind_the = np.append(np.arange(ntheta), 0)
    for jrho in range(nrho):
        plt.plot(feq.rsurf[-1, jrho, ind_the], feq.zsurf[-1, jrho, ind_the])
    plt.show()
