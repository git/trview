import logging, datetime
import numpy as np
import aug_sfutils as sf
from aug_sfutils import read_imas
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.icrf')


class ICRF:


    def __init__(self, nshot, readSF=True, imasRun=None, tbeg=0., tend=10., icr_min=1e4):


        self.shot = nshot
        self.icr_min  = icr_min
        self.tbeg    = tbeg
        self.tend    = tend
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('ic_antennas')
        ica = ids.ic_antennas.antenna
        nt   = len(ica[0].power_launched.time)
        nant = len(ica)

        self.freq  = np.zeros(nant, dtype=np.float64)
        self.phase = np.zeros(nant, dtype=np.float64)
        self.t_ds = ica[0].power_launched.time
        self.power_ds = np.zeros((nt, nant), dtype=np.float64)
        for jant in range(nant):
            self.freq[jant]  = np.squeeze(ica[jant].frequency.data)
            self.phase[jant] = np.degrees(np.squeeze(ica[jant].module[0].strap[0].phase.data))
            self.power_ds[:, jant] = ica[jant].power_launched.data


    def fromH5(self):

        icr_ids = read_imas.read_imas_h5(self.shot, self.run, branch='ic_antennas')
        ica = icr_ids['ic_antennas']
        self.freq     = ica['antenna[]&frequency&data'][:]
        self.phase    = np.degrees(ica['antenna[]&module[]&strap[]&phase&data'][:, 0, 0])
        self.t_ds     = ica['antenna[]&power_launched&time'][:]
        self.power_ds = ica['antenna[]&power_launched&data'][:]


    def fromShotfile(self):

        nant = 4 # AUG hardcoded
        self.freq  = np.zeros(nant)
        self.phase = np.zeros(nant)
        diag = 'ICP'
        icp = sf.SFREAD(self.shot, diag)
        if icp.status:
            pset_fr = icp.getparset('Frequenz')
            pset_ph = icp.getparset('AntPhase')
            for jant in range(nant):
                tbas = diag + '.'.ljust(9)
                sig1 = 'pnet%d'  %(jant + 1)
                sig2 = 'ploss%d' %(jant + 1)
                self.freq [jant] = pset_fr['Freq%d'  %(jant+1)]
                self.phase[jant] = pset_ph['Phase%d' %(jant+1)]

                logger.info('Requested signal %s', sig1)

                tim = icp.gettimebase(sig1)
                tbeg = max(self.tbeg, 1e-6)
                index = (tim >= tbeg) & (tim <= self.tend)
                self.time = tim[index]
                ntim = len(self.time)
                if jant == 0:
                    self.power = np.zeros((ntim, nant))

                dat1 = icp.getobject(sig1)[index]
                tmp2 = icp.getobject(sig2)
                if tmp2 is None:
                    self.power[:, jant] = dat1
                else:
                    self.power[:, jant] = dat1 - tmp2[index]

            self.power = np.maximum(self.power, 0.)

            for jant in range(nant):
                if np.max(self.power[:, jant]) <= self.icr_min:
                    self.power[:, jant] = 0.

# Minority concentration

        diag = 'CXF'
        cxf = sf.SFREAD(self.shot, diag)
        if cxf.status:
            sig = 'cHavR'
            if sig in cxf.sf.SFobjects:
                self.t_cxf = cxf.gettimebase(sig)
                self.minr = cxf.getobject(sig)
                self.minl = cxf.getobject('cHavL')


    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        ic = imas.ic_antennas()

        if not hasattr(self, 't_ds'):
            return

        strap_coord_r = np.array([2.161, 2.189, 2.208, 2.200, 2.183, 2.139, 2.094, 2.047])
        strap_coord_z = np.array([0.449, 0.307, 0.134, 0, -0.123, -0.262, -0.373, -0.454])
        ic.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
        ic.ids_properties.homogeneous_time = 0
#    ic.ids_properties.source = os.path.abspath(__file__)
        ic.ids_properties.comment = "Still iterating with EWE-2 on correctness; more work should go into the antenna description"

        nant = self.power_ds.shape[1]
        ica = ic.antenna
        ica.resize(nant)
        tim = np.atleast_1d(np.average(self.t_ds))
        for jant in range(nant):
            ica[jant].name = str(jant+1)
            ica[jant].identifier = str(jant+1)
            ica[jant].frequency.data = np.atleast_1d(self.freq[jant])
            ica[jant].frequency.time = tim
            ica[jant].power_launched.data = self.power_ds[:, jant]
            ica[jant].power_launched.time = self.t_ds
            ica[jant].module.resize(1)
            ica[jant].module[0].name = '%s/%s' % (jant+1, 1)
            ica[jant].module[0].identifier = '%s/%s' % (jant+1, 1)
            ica[jant].module[0].frequency.data = np.atleast_1d(self.freq[jant])
            ica[jant].module[0].frequency.time = tim
            ica[jant].module[0].power_launched.data = self.power_ds[:, jant]
            ica[jant].module[0].power_launched.time = self.t_ds
            ica[jant].module[0].strap.resize(2)

            ica[jant].module[0].strap[0].width_tor = 0.18
            ica[jant].module[0].strap[0].distance_to_conductor =  0.21
            ica[jant].module[0].strap[0].phase.data = np.atleast_1d(0.0)
            ica[jant].module[0].strap[0].phase.time = tim
            ica[jant].module[0].strap[0].outline.r = strap_coord_r
            ica[jant].module[0].strap[0].outline.z = strap_coord_z
            ica[jant].module[0].strap[0].outline.phi = np.repeat(0.0, len(strap_coord_r))

            ica[jant].module[0].strap[1].width_tor = 0.18
            ica[jant].module[0].strap[1].distance_to_conductor =  0.21
            ica[jant].module[0].strap[1].phase.data = np.atleast_1d(np.radians(self.phase[jant]))
            ica[jant].module[0].strap[1].phase.time = tim
            ica[jant].module[0].strap[1].outline.r = strap_coord_r
            ica[jant].module[0].strap[1].outline.z = strap_coord_z
            ica[jant].module[0].strap[1].outline.phi = np.repeat(0.0 + 0.095, len(strap_coord_r))

        return ic
        
if __name__ == '__main__':

    ICRF(29795)
