TRV_HOME=$(dir $(abspath $(firstword $(MAKEFILE_LIST))))

# module load nag_flib/mk25 for NEMEC

RB_SRC=$(TRV_HOME)/src/rabbit_wrapper.f90
RB_PYF=$(TRV_HOME)/rabbit.pyf
RB_HOME=/shares/departments/AUG/users/git/ASTRA_LIBRARIES_EXT/rabbit/jul22
RB_LIB=$(RB_HOME)/lib/libRabbitMore.so

TB_SRC=$(TRV_HOME)/src/torbeam_wrapper.f90
TB_PYF=$(TRV_HOME)/torbeam.pyf
TB_HOME=/shares/departments/AUG/users/git/ASTRA_LIBRARIES_EXT/torbeam/jul21
TB_LIB=$(TB_HOME)/lib/libtorbeamB.so

VM_SRC=$(TRV_HOME)/src/nemec_wrapper.f90
VM_PYF=$(TRV_HOME)/nemec.pyf
VM_HOME=/shares/departments/AUG/users/git/nemec
VM_LIB=$(VM_HOME)/amd64_sles15/lib/libnemec.so

FC=ifort
RB_PYLIB=$(TRV_HOME)/rabbit.cpython-310-x86_64-linux-gnu.so
TB_PYLIB=$(TRV_HOME)/torbeam.cpython-310-x86_64-linux-gnu.so
VM_PYLIB=$(TRV_HOME)/nemec.cpython-310-x86_64-linux-gnu.so

all: $(RB_PYLIB) $(TB_PYLIB) $(VM_PYLIB)

$(RB_PYF):	$(RB_SRC)
	f2py --overwrite-signature $(RB_SRC) -h $(RB_PYF) -m rabbit

$(TB_PYF):	$(TB_SRC)
	f2py --overwrite-signature $(TB_SRC) -h $(TB_PYF) -m torbeam

$(VM_PYF):	$(VM_SRC)
	f2py --overwrite-signature $(VM_SRC) -h $(VM_PYF) -m nemec

$(RB_PYLIB):	$(RB_LIB) $(RB_PYF)
	f2py -c $(RB_PYF) --verbose --debug --f90exec=$(FC) --f77exec=$(FC) -I$(RB_HOME)/inc $(RB_LIB) $(RB_SRC)

$(TB_PYLIB):	$(TB_LIB) $(TB_PYF)
	f2py -c $(TB_PYF) --verbose --debug --f90exec=$(FC) --f77exec=$(FC) $(TB_LIB) $(TB_SRC) -L/mpcdf/soft/SLE_15/packages/x86_64/intel_oneapi/2023.1/compiler/2023.1.0/linux/compiler/lib/intel64_lin -lirc

$(VM_PYLIB):	$(VM_LIB) $(VM_PYF)
	f2py -c $(VM_PYF) --verbose --debug --f90exec=$(FC) --f77exec=$(FC) -I$(VM_HOME)/amd64_sles15/inc $(VM_LIB) $(NAGFLIB) $(NAGMKLLIB) $(VM_SRC)

clean:
	rm $(RB_PYF)
	rm $(TB_PYF)
	rm $(RB_PYLIB)
	rm $(TB_PYLIB)
	rm $(VM_PYLIB)
