import logging
import aug_sfutils as sf

logger = logging.getLogger('trview.main_spec')


def to_str(str_or_byt):
    """
    Converts a plain string to a byte string (python3 compatibility)
    """

    str_out = str_or_byt.decode('utf8') if isinstance(str_or_byt, bytes) else str_or_byt
    return str_out.strip()


def  extract_main(gasv_str):

    main_spec = 'D'
    if '(1)' in gasv_str:
        main_spec = gasv_str.split('(1)')[0].split(':')[-2].split(',')[-1].strip()
    elif gasv_str.count(':') == 1:
        main_spec = gasv_str.split(':')[0].strip()
    return main_spec


def spec_jou_sf(nshot):

    jou = sf.SFREAD(nshot, 'JOU')

    if jou.status:
        fill = jou.getparset('FILLING')
        tmp = fill['GasVent']
        gasv_str = ''
        for y in tmp:
            gasv_str += to_str(y)

        return extract_main(gasv_str)
    else:
        return None


if __name__ == '__main__':

     gas = spec_jou_sf(28053)
     if gas is not None:
         logger.info(gas)
