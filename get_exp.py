import os, logging, traceback
import numpy as np
from scipy.interpolate import interp1d
import aug_sfutils as sf
from trview import prof_names, rhop2rhot, read_prof, main_spec, \
    signals, nbi, ece, ech, zeff, icrf, pellet, \
    getAUGcoilCurrents, rabbit_pos, fit_sep, \
    imas2core_profiles
from trview.fit_prof import fit_prof

logger = logging.getLogger('trview.get_exp')


class INPUT:


    def __init__(self, gpar, gprof, readSF=True):

        self.gpar = gpar
        self.gprof   = gprof
        if readSF:
            self.fromShotfile()
        else:
            self.fromIMAS()


    def fromIMAS(self):

        run  = self.gpar.ids_run
        shot = self.gpar.shot
        self.sig = signals.SIGNALS(shot, imasRun=run, readSF=False)
        self.nbi = nbi.NBI(shot, imasRun=run, readSF=False)
        self.ech = ech.gyros(shot, imasRun=run, readSF=False)
        self.icr = icrf.ICRF(shot, imasRun=run, readSF=False)
        self.pellet = pellet.PELLET(shot, imasRun=run, readSF=False)
        self.sig.fromIMAS()
        self.nbi.fromIMAS()
        if hasattr(self.nbi, 't_ds'):
            self.nbi.start_pos, self.nbi.unit_vec = rabbit_pos.rabbit_pos(self.nbi)
        self.ech.fromIMAS()
        self.icr.fromIMAS()
        self.pellet.fromIMAS()
        self.equ = sf.EQU(shot, readSF=False, imasRun=run)
        if not (np.any(self.equ.Br) and np.any(self.equ.Bz) and np.any(self.equ.Bt)):
            self.equ.B_mesh()
        self.zef = zeff.ZEF(self.equ, imasRun=run, readSF=False)
        self.zef.fromIMAS()
        self.profiles = imas2core_profiles.profiles(shot, run)
        self.profiles = rhop2rhot.rhot2rhop(self.equ, self.profiles)
        self.ion_d = self.profiles.ion_d


    def fromShotfile(self):

        gpar = self.gpar
        self.nbi = nbi.NBI(gpar.shot)
        self.ech = ech.gyros(gpar.shot)
        self.icr = icrf.ICRF(gpar.shot)
        self.sig = signals.SIGNALS(gpar.shot)
        self.ece = ece.ECE(gpar.shot)
        self.equ = sf.EQU(gpar.shot, tbeg=0., tend=10., diag=gpar.eq_diag, exp=gpar.eq_exp, ed=gpar.eq_ed)
        if not hasattr(self.equ, 'time'):
            return
        self.pellet = pellet.PELLET(gpar.shot, tbeg=gpar.tbeg, tend=gpar.tend, equ=self.equ, trig=gpar.pel_trig)
        self.zef = zeff.ZEF(self.equ)
        try:
            self.coilCur = getAUGcoilCurrents.coils_currents(gpar.shot, t_offset=None)
            if self.coilCur.status:
                self.coilCur.timAvg = 0.5*(gpar.tbeg + gpar.tend)
                dt = 0.005
                (ind, ) = np.where(abs(self.coilCur.timeCurr - self.coilCur.timAvg) < dt)
                self.coilCur.astraCurAvg = np.mean(self.coilCur.astra_curr[ind, :], axis=0)
        except:
            traceback.print_exc()

        self.load_2d_profiles(gpar, self.equ, self.gprof)
        t1d = self.profiles.time

# Equilibrium with downsampled time grid
        self.equ.timeDownsample(t1d, noELMs=gpar.no_elm, dt_out=self.dt_out)

        if hasattr(self.equ, 'time'):
            self.equ.to_coco(cocos_out=5)       # Converting COCO of equil
            self.equ.B_mesh()
            self.equ.sep = fit_sep.geofit(self.equ, nmom=gpar.nmom, n_the=gpar.nthe_in, noELMs=gpar.no_elm) # Separatrix fit
        else:
            logger.error('Missing equilibrium!')
            return

        if hasattr(self.nbi, 'time'):
            self.nbi.start_pos, self.nbi.unit_vec = rabbit_pos.rabbit_pos(self.nbi)
        self.sig.t_ds = t1d
        if hasattr(self.sig.bt, 'time'):
            for attr in signals.diagsigs.keys():
                if hasattr(self.sig, attr):
                    val = getattr(self.sig, attr)
                    if hasattr(val, 'time'):
                        val.data_ds, _, _ = sf.time_slice.map2tgrid(t1d, val.time, val.data, dt_out=self.dt_out)

# Downscaling to refrence time grid
        if hasattr(self.nbi, 'time'):
            self.nbi.t_ds = t1d
            self.nbi.power_ds, _, _  = sf.time_slice.map2tgrid(t1d, self.nbi.time, self.nbi.power, dt_out=self.dt_out)
            self.nbi.aug_tr()
        if hasattr(self.ech, 'time'):
            self.ech.t_ds = t1d
            self.ech.power_ds, _, indbin = sf.time_slice.map2tgrid(t1d, self.ech.time, self.ech.power, dt_out=self.dt_out)
            self.ech.theta_ds, _, _      = sf.time_slice.map2tgrid(t1d, self.ech.time, self.ech.theta, indbin=indbin, dt_out=self.dt_out)
            self.ech.phi_ds  , _, _      = sf.time_slice.map2tgrid(t1d, self.ech.time, self.ech.phi  , indbin=indbin, dt_out=self.dt_out)
        if hasattr(self.icr, 'time'):
            self.icr.t_ds = t1d
            self.icr.power_ds, _, _  = sf.time_slice.map2tgrid(t1d, self.icr.time, self.icr.power, dt_out=self.dt_out)

        self.zef.t_ds = t1d
        if hasattr(self.zef, 'time'):
            self.zef.data_ds, _, _ = sf.time_slice.map2tgrid(t1d, self.zef.time, self.zef.data, dt_out=self.dt_out)
        else:
            self.zef.data_ds = 1.4 + np.zeros_like(self.zef.t_ds)

        self.time = t1d

        ion_spec = main_spec.spec_jou_sf(gpar.shot)
        logger.info('Main ion species is %s', ion_spec)
        self.ion_d = {'Aimp': 12, 'Zimp': 6}
        if ion_spec == 'D':
            self.ion_d['Amain'] = 2
            self.ion_d['Zmain'] = 1
        elif ion_spec == 'H':
            self.ion_d['Amain'] = 1 
            self.ion_d['Zmain'] = 1
        elif ion_spec == 'He':
            self.ion_d['Amain'] = 4 
            self.ion_d['Zmain'] = 2 


    def load_2d_profiles(self, gpar, equ, gprof):

        diags, sigs, pres, err_sigs = \
            np.genfromtxt('%s/diags.dat' %os.path.dirname(os.path.realpath(__file__)), dtype='str', skip_header=2, unpack=True, usecols=(0, 1, 2, 6))

        raw_data = {}
        x_fit = np.linspace(0, 1, gpar.nrho_in)

# Fetch exp data

        raw_data = {}
        sfl_d = {}
        for prefix in prof_names:
            sfl_d[prefix] = []
            diag_sigs = gprof[prefix].sflist
            for sflist in diag_sigs.split(): #'CEZ:Ti_c CMZ:Ti_c'
                rdd = read_prof.dd2read(sflist, equ, gpar)
                if rdd is not None:
# Replace edition 0 with actual editoin number
                    sf_list = sflist.split(':')
                    sfl = ':'.join(sf_list[:-1])
                    sfl += ':%d' %rdd['ed']
                    raw_data[sfl] = rdd
                    sfl_d[prefix].append(sfl)

        if len(sfl_d['Te']) == 0 or len(sfl_d['Ne']) == 0:
            raise Exception('Missing Te or ne profile, no simulation possible')

# Create wish time grid

        tbeg_e = 12.
        tbeg_n = 12.
        tend_e = 0.
        tend_n = 0.
        for lbl in sfl_d['Te']:
            rdd = raw_data[lbl]
            tbeg_e = min(tbeg_e, rdd['tgrid'][0])
            tend_e = max(tend_e, rdd['tgrid'][-1])
        for lbl in sfl_d['Ne']:
            rdd = raw_data[lbl]
            tbeg_n = min(tbeg_n, rdd['tgrid'][0])
            tend_n = max(tend_n, rdd['tgrid'][-1])
        tbeg = max([gpar.tbeg, tbeg_e, tbeg_n])
        tend = min([gpar.tend, tend_e, tend_n])

        dt_ds = gpar.dt_in
        nt = int((tend - tbeg + 1e-3*dt_ds)/ dt_ds) + 1
        if nt > 1:
            tgrid = tbeg + dt_ds*np.arange(nt)
            self.dt_out = None
        else:
            tgrid = np.atleast_1d(0.5*(tend + tbeg))
            self.dt_out = 0.5*(tend - tbeg)

# Profile object: start with parameter settings

        self.profiles = type('', (), {})()
        for key, val in gpar.__dict__.items():
            setattr(self.profiles, key, val)
        n_spl = self.profiles.nrho_in
        self.profiles.time = tgrid
        self.profiles.rho  = np.linspace(0, 1, n_spl)

# slice all kinetic profiles

        for prefix in prof_names:
            red_data = {}
            for sflist in sfl_d[prefix]:
                red_data[sflist] = {}
                x_red, y_red, err_red = sf.time_slice.map2tgrid_rho_err( \
                raw_data[sflist], tgrid, nshot=gpar.shot, noELMs=gpar.no_elm, dt_out=self.dt_out)
                red_data[sflist]['rhop']     = x_red
                red_data[sflist]['data']     = y_red
                red_data[sflist]['data_err'] = err_red
                red_data[sflist]['tgrid']    = tgrid
                red_data[sflist]['unit']     = raw_data[sflist]['unit']

# Combine diagnostics

            xexp = None
            for sflist in sfl_d[prefix]:
                if xexp is None:
                    xexp = np.array(red_data[sflist]['rhop'])
                    yexp = np.array(red_data[sflist]['data'])
                    yerr = np.array(red_data[sflist]['data_err'])
                else:
                    xexp = np.append(xexp, red_data[sflist]['rhop']    , axis=1)
                    yexp = np.append(yexp, red_data[sflist]['data']    , axis=1)
                    yerr = np.append(yerr, red_data[sflist]['data_err'], axis=1)

            logger.info('prefix: %s, method: %s, sigmas: %5.2f', prefix, gprof[prefix].method, gprof[prefix].sigma)

            if xexp is not None:

                logger.info(str(xexp.shape))
                ind_sort = np.argsort(xexp, axis=1)
                for jt in range(xexp.shape[0]):
                    xexp[jt, :] = xexp[jt, ind_sort[jt]]
                    yexp[jt, :] = yexp[jt, ind_sort[jt]]
                    yerr[jt, :] = yerr[jt, ind_sort[jt]]

                probj = type('', (), {})()
                probj.sflists  = sorted(red_data.keys())
                probj.xexp_rhop = xexp
                probj.yexp_rhop = yexp
                probj.yerr_rhop = yerr
                probj.fit_method = gprof[prefix].method
                probj.fit_tol    = gprof[prefix].fit_tol
                probj.sigmas     = gprof[prefix].sigma
                probj.tnot = []
                probj.units = red_data[probj.sflists[0]]['unit']

# Fit
   
                if gprof[prefix].method == 'Rec. spline':
                    timeout_pool = 10
                else:
                    timeout_pool = 300
                
                for sflist in sfl_d[prefix]:
                    if 'IDA' in sflist:
                        fit_tol = 0.
                        break

                fit_prof(probj, tgrid, x_fit)
                self.profiles.__dict__[prefix] = probj

            else:

                logger.warning('Prefix %s not selected', prefix)

                if prefix == 'Angf':
                    probj = type('', (), {})()
                    probj.sflists  = sorted(red_data.keys())
                    probj.fit_method = gprof[prefix].method
                    probj.tnot = []
                    probj.units = 'rad/s'
                    probj.failed_fit = True
                    self.profiles.__dict__[prefix] = probj

        if self.profiles.Angf.failed_fit:
            logger.warning('No experimental data for Angf, setting Angf = 0')
            self.profiles.Angf.yfit_rhop = np.zeros((nt, n_spl), dtype=np.float32)
            self.profiles.Angf.confid    = np.infty + np.zeros((nt, n_spl), dtype=np.float32)
            sfl_d['Angf'] = ['dummy:ZERO:angf']

        if not hasattr(self.profiles, 'Ti'):
            logger.warning('No experimental data for Ti, setting Ti = Te')
            self.profiles.Ti = self.profiles.Te
            sfl_d['Ti'] = sfl_d['Te']
        self.profiles = rhop2rhot.rhop2rhot(equ, self.profiles) # rho_tor !!

        for prefix, sfls in sfl_d.items():
            self.profiles.__dict__[prefix].diags = ''
            self.profiles.__dict__[prefix].sflist = sfls
            for sfl in sfls:
                diag = sfl.split(':')[1]
                self.profiles.__dict__[prefix].diags += diag
