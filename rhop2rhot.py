import logging
import numpy as np
import aug_sfutils as sf
from trview import prof_names

logger = logging.getLogger('trview.rhop2rhot')


def rhop2rhot(equ, pr_obj, eq_exp='AUGD', eq_diag='EQH', eq_ed=0):

    logger.info('Mapping fit data from regular rho_pol to regular rho_tor')

    for prefix in prof_names:

        yfit = pr_obj.__dict__[prefix].yfit_rhop
        yerr = pr_obj.__dict__[prefix].confid
        nt, nrho = yfit.shape
        rho_grid = pr_obj.rho # pol in input, tor in output

        rho_tor = sf.rho2rho(equ, rho_grid, t_in=pr_obj.time, coord_in='rho_pol', coord_out='rho_tor')

        pr_obj.__dict__[prefix].yfit_rhot = np.zeros_like(yfit)
        pr_obj.__dict__[prefix].yerr_rhot = np.zeros_like(yfit)

        for jt in range(nt):
            pr_obj.__dict__[prefix].yfit_rhot[jt, :] = np.interp(rho_grid, rho_tor[jt, :], yfit[jt, :])
            pr_obj.__dict__[prefix].yerr_rhot[jt, :] = np.interp(rho_grid, rho_tor[jt, :], yerr[jt, :])

        pr_obj.psi = sf.rho2rho(equ, rho_grid, t_in=pr_obj.time, coord_in='rho_tor', coord_out='Psi')

        pr_obj.xlbl = 'rho_tor'

    return pr_obj


def rhot2rhop(equ, pr_obj, eq_exp='AUGD', eq_diag='EQH', eq_ed=0):

    logger.info('Mapping fit data from regular rho_pol to regular rho_tor')

    for prefix in prof_names:

        yfit = pr_obj.__dict__[prefix].yfit_rhot
        yerr = pr_obj.__dict__[prefix].yerr_rhot
        nt, nrho = yfit.shape
        rho_grid = pr_obj.rho # pol in input, tor in output

        rho_pol = sf.rho2rho(equ, rho_grid, t_in=pr_obj.time, coord_in='rho_tor', coord_out='rho_pol')

        pr_obj.__dict__[prefix].yfit_rhop = np.zeros_like(yfit)
        pr_obj.__dict__[prefix].yerr_rhop = np.zeros_like(yfit)

        for jt in range(nt):
            pr_obj.__dict__[prefix].yfit_rhop[jt, :] = np.interp(rho_grid, rho_pol[jt, :], yfit[jt, :])
            pr_obj.__dict__[prefix].yerr_rhop[jt, :] = np.interp(rho_grid, rho_pol[jt, :], yerr[jt, :])

        pr_obj.psi = sf.rho2rho(equ, rho_grid, t_in=pr_obj.time, coord_in='rho_pol', coord_out='Psi')

        pr_obj.xlbl = 'rho_pol'

    return pr_obj
