import logging, traceback
import numpy as np
from aug_sfutils import time_slice, read_imas
from trview import CalcZef
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.zef')


class ZEF:


    def __init__(self, equ, readSF=True, imasRun=None, tbeg=0., tend=10.):

        self.equ  = equ
        self.shot = equ.shot
        self.tbeg    = tbeg
        self.tend    = tend
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromShotfile(self):

        self.unit = None
        self.diagsig = 'CAR:onesix'
        self.edition = 0

        try:
            zbrs = CalcZef.Zeff(self.equ)

            self.time = np.atleast_1d(zbrs.timeZeff)
            self.data = np.atleast_1d(zbrs.Zeff0)
            if np.max(np.abs(self.data)) == 0:
                self.data = zbrs.Zeff

        except:
            traceback.print_exc()
            logger.error('CalcZef Bremsstrahlung unsuccesful')

        if not hasattr(self, 'data') and self.shot < 21482:
            try:
                zfml = CalcZef.Zeff_fml(self.shot) # From formula
                self.time = np.atleast_1d(zfml.time)
                self.data = np.atleast_1d(zfml.zeff)
            except:
                traceback.print_exc()
                logger.exception('CalcZef formula unsuccesful')


    def fromIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('core_profiles')
        cp = ids.core_profiles
        self.t_ds = cp.time
        nt = len(cp.time)
        self.data_ds = np.zeros(nt)
        for jt in range(nt):
            self.data_ds[jt] = cp.profiles_1d[jt].zeff[0] # Assuming flat Zeff profile


    def fromH5(self):

        ids = read_imas.read_imas_h5(self.shot, self.run, branch='nbi')
        cp = ids['core_profiles']
        self.t_ds = cp['time']
        nt = len(cp['time'])
        self.data_ds = np.zeros(nt)
        for jt in range(nt):
            self.data_ds[jt] = cp['profiles_1d[]&zeff[]'][jt, 0] # Assuming flat Zeff profile


if __name__ == '__main__':

    import aug_sfutils as sf
    import matplotlib.pylab as plt
    shot = 38384
    equ = sf.EQU(shot)
    zef = ZEF(equ, tbeg=3, tend=4)
    plt.plot(zef.time, zef.data)
    plt.ylim(ymin=0)
    plt.show()
    
