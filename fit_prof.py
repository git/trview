import os, logging, traceback
import numpy as np
from multiprocessing import Pool, cpu_count
from trview import spline_csaps, gauss
from trview.grid_map import map2grid

logger = logging.getLogger('trview.vprof')
logger.setLevel(logging.INFO)
#logger.setLevel(logging.DEBUG)


def fit_prof(profObject, tgrid, xfit):

    xexp = profObject.xexp_rhop.copy()
    yexp = profObject.yexp_rhop.copy()
    yerr = profObject.yerr_rhop.copy()
    nt       = len(tgrid)
    nrho_exp = xexp.shape[1]
    nrho_fit = len(xfit)

    if profObject.fit_method in ('Rec. spline', 'Gaussian fit'):

        if profObject.fit_method == 'Rec. spline':
            timeout_pool = 25
        else:
            timeout_pool = 300
            profObject.sigmas = 1e12
        if profObject.fit_tol == 0:
            profObject.sigmas = 1e12

        profObject.failed_fit = False

        try:
            pool = Pool(cpu_count())
            out = pool.map_async(fit1d, [(xexp[jt], yexp[jt], yerr[jt], \
                xfit, tgrid[jt], profObject.fit_tol, profObject.sigmas, profObject.fit_method) \
                for jt in range(nt)]).get(timeout_pool)
            pool.close()
            pool.join()
            logger.debug('After pool')
        except:
            traceback.print_exc()
            logger.error('Failed spline')
            try: # fall-back: pure interpolation
                pool = Pool(cpu_count())
                out = pool.map_async(fit1d, [(xexp[jt], yexp[jt], yerr[jt], \
                    xfit, tgrid[jt], 0., profObject.sigmas, profObject.fit_method) \
                    for jt in range(nt)]).get(timeout_pool)
                pool.close()
                pool.join()
                logger.debug('After pool')
            except:
                traceback.print_exc()
                profObject.failed_fit = True

        if not profObject.failed_fit:
            out_np = np.array(out)
            profObject.yfit_rhop = out_np[:, : nrho_fit]
            profObject.dat_ok    = out_np[:, nrho_fit: nrho_fit+nrho_exp]
            if profObject.fit_method in ('Rec. spline', 'Gaussian fit'):
                profObject.confid = out_np[:, nrho_fit+nrho_exp: ]

    else: # 2d regularisation, Odstrcil-way

        profObject.yfit_rhop, x_out = fit_reg2d(xexp, yexp, yerr, xfit, tgrid, eta=profObject.fit_tol, lam=profObject.sigmas)
        profObject.confid = np.zeros_like(profObject.yfit_rhop)
        for jt in range(nt):
            profObject.confid[jt, :] = np.interp(x_out, xexp[jt], yerr[jt])
        profObject.dat_ok = yexp


def fit1d(tup_in):

    x, y_in, err_in, rho, time, fit_tol, sigmas, method = tup_in
    y = y_in.copy()
    err = err_in.copy()
    nx = len(x)
    nfit = len(rho)
    if np.isnan(y).all():
        logger.debug('fit1d, nans only at t=%.3f s', time) 
        return np.zeros(nfit + nx + nfit)

    drho = rho[1] - rho[0]
    rho_ext = rho
    while rho_ext[-1] < x[-1]: # include further points in the fit, out of sep
        rho_ext = np.append(rho_ext, rho_ext[-1] + drho)
    if np.max(np.abs(y)) == 0:
        return np.zeros(nfit + nx + nfit)

# Discard zeros
    indzero = (y == 0)
    y[indzero]   = np.nan
    err[indzero] = np.infty
    if fit_tol == 0:
        ind = np.isfinite(y)
        yfit = np.interp(rho, x[ind], y[ind])
        confid = np.zeros(nfit)
    else:
        if method == 'Rec. spline':
            try:
                yfit_ext, y = spline_csaps.rec_spline(x, y, rho_ext, \
                     y_err=err, fit_tol=fit_tol, sigmas=sigmas)
                confid = np.interp(rho, x, err_in)
            except:
                traceback.print_exc()
                logger.error('Failed Rec. spline')
                yfit_ext = np.zeros(nfit)
        elif method == 'Gaussian fit':
            ind = np.isfinite(err)
            yfit_ext, confid = gauss.gauss(x[ind], y[ind], err[ind], rho_ext, fit_tol=fit_tol)
            confid = confid[:nfit]
        yfit = yfit_ext[:nfit]
# Outcome are lists
    out = np.hstack((yfit, y, confid))

    return out


def fit_reg2d(xexp, yexp, yerr, xfit, tgrid, eta=0., lam=2., f0=0., edge=False):

    tvec = np.repeat(tgrid, xexp.shape[1])
    nr_fit = len(xfit)
    nt     = len(tgrid)

    invalid = ~np.isfinite(yexp)| ~np.isfinite(yerr)
    yerr[invalid] = np.inf
    yexp[invalid] = -1

    m2g = map2grid(xexp, tvec, yexp, yerr, nr_fit, nt)
    m2g.r_max = 1.0
    m2g.PrepareCalculation(eta=-eta, perturb_freq=f0, zero_edge=edge)
    m2g.Calculate(lam)

    x_out = m2g.r_new[0, :]
    yfit_rhop = m2g.g

    return yfit_rhop, x_out
