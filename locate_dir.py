import os, logging

logger = logging.getLogger('trview.locate_dir')

trv_wd = os.getenv('SHARES_USER_DIR')


def locate(dname, path_in=trv_wd):

    if not os.path.isdir(path_in):
        return None
    for item in os.listdir(path_in):
        if os.path.isdir('%s/%s/%s' %(path_in, item, dname)):
            return os.path.join(path_in, item)
    return None


def locate_rec(dname, path_in=trv_wd):
# Recursive

    for root, dirs, files in os.walk(path_in):
        if dname in dirs:
            return root

    return None

def locate_rec2(dname, path_in=trv_wd):
# Recursive
    import pathlib
    logger.info('Locating AWD recursively 2')
    fil = pathlib.Path(path_in).rglob(dname)
    return sorted(fil)[0].parent


if __name__ == '__main__':

    path = locate('pyparse')
    logger.info(path)

    path = locate_rec('pyparse')
    logger.info(path)

    path = locate_rec2('pyparse')
    logger.info(path)
