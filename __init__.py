#!/usr/bin/env python

"""TRVIEW

https://gitlab.mpcdf.mpg.de/git/trview/-/wikis/TRVIEW-manual
"""

__author__  = 'Giovanni Tardini (Tel. 1898)'
__version__ = '1.0.6'
__date__    = '26.06.2024'

import os, sys, logging, traceback

fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')

logger = logging.getLogger('trview')

if len(logger.handlers) == 0:
    hnd = logging.StreamHandler()
    hnd.setFormatter(fmt)
    logger.addHandler(hnd)

#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

logger.info('Using version %s', __version__)

prof_names = ['Ne', 'Te', 'Ti', 'Angf']
rb_home = os.path.dirname(os.path.realpath(__file__))
logger.info('TRVIEW home %s', rb_home)
