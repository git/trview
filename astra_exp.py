import os, shutil, logging
from trview import ufiles, locate_dir

logger = logging.getLogger('trview.astra_exp')


def coil(time, coil_curr):

    coil_txt = '''!Coil currents
NAMEXP CCOIL NTIMES 1  POINTS 12  
 %8.4f
%8.4f %8.4f %8.4f %8.4f
%8.4f %8.4f %8.4f %8.4f
%8.4f %8.4f %8.4f %8.4f
!Coil voltages 
NAMEXP VCOIL NTIMES 1  POINTS 12
 %8.4f
0. 0. 0. 0.
0. 0. 0. 0.
0. 0. 0. 0.
''' %(time, *coil_curr, time)

    return coil_txt


def astra_exp(plasma):

    nshot = plasma.gpar.shot
    f_exp = 'aug%d' %nshot
    udb = '%s/udb/%d' %(os.getenv('SHARES_USER_DIR'), nshot) 
    awd = os.getenv('AWD')
    if awd is None:
        awd = locate_dir.locate('pyparse')
    if awd is None:
        awd = locate_dir.locate_rec('pyparse')
    as_udb = '%s/udb/%d' %(awd, nshot)
    as_exp = '%s/exp' %awd
    as_nbi = '%s/nbi' %as_exp
    os.system('mkdir -p %s' %as_udb)
    os.system('mkdir -p %s' %as_nbi)

    atxt1 =  'aug #%d Name	 Time	 Value	 Error\n' %nshot
    atxt1 += """
|____|--|____|--|____|--|____|----------------
NA1             91
RTOR            1.65
AWALL           0.7              Reserved minor radius in the mid-plane
ELONM           1.70             Chamber elongation
TRICH           0.15             Chamber traingularity
AB              0.6              Chamber minor radius in the mid-plane
"""

    atxt1 += 'ZMJ             %3.1f              Majority Z\n' %float(plasma.ion_d['Zmain'])
    atxt1 += 'AMJ             %3.1f              Majority mass/m_p\n' %float(plasma.ion_d['Amain'])

    atxt1 += 'IPL      U-file:%d/MAG%d.IPL\n' %(nshot, nshot)
    atxt1 += 'BTOR     U-file:%d/MAG%d.BTOR\n'   %(nshot, nshot)

    atxt2 = '\nFILTER   0.002\n\n'
    uf_d = {'Te': ('TE', '1.e-3'), 'Ti': ('TI', '1.e-3'), 'Ne': ('NE', '1.e-13'), \
            'V': ('VTOR', '1.e-2') , 'Angf': ('CAR1', '1.'), 'Q': ('CAR2', '1.') }

    for prefix in ['Q'] + ['Te', 'Ti', 'Ne', 'Angf']:
        if prefix == 'Q':
            ext = 'QPR'
        else:
            ext = plasma.profiles.__dict__[prefix].diags
        fudb = '%s%d.%s' %(prefix.upper(), nshot, ext)
        source = '%s/%s' %(udb, fudb)
        logger.debug('%s', fudb)
        if os.path.isfile(source):
            target = '%s/%s' %(as_udb, fudb)
            uf = ufiles.UFILE(fin=source)
            rho_lbl = uf.Y['label'].strip()
            if rho_lbl == 'rho_tor':
                logger.info('cp %s %s', source, target)
                shutil.copy2(source, target)
                atxt2 += '%-4s     U-file:%d/%s:%s\n'  %(uf_d[prefix][0], nshot, fudb, uf_d[prefix][1])
            else:
                logger.error('u-file label %s in %s is not supported (only rho_tor)', rho_lbl, source)
        else:
            logger.error('File %s not found', source)

    atxt2 += '\n'

    coil_txt = ''
    if hasattr(plasma, 'coilCur'):
        if plasma.coilCur.status:
            coil_txt += coil(plasma.coilCur.timAvg, plasma.coilCur.astraCurAvg)

# Create ASTRA exp file

    fexp = '%s/%s' %(as_exp, f_exp)
    if os.path.isfile(fexp):
        fbak = '%s~' %fexp
        logger.info('Creating backup file %s' %fbak)
        shutil.move(fexp, fbak)
    f = open(fexp, 'w')
    f.write(atxt1)
    f.write(atxt2)
    f.write(coil_txt)

    f.write('NAMEXP BNDU\n')
    f.write('../udb/%d/%d.bnd.txt\n' %(nshot, nshot))

    f.close()
    logger.info('Written %s' %fexp)

# Create exp/nbi file

    if plasma.nbi.status:
        plasma.nbi.tr2as()
        f_nbi = '%s/%s' %(as_nbi, f_exp)
        logger.info('Writing %s' %f_nbi)
        if os.path.isfile(f_nbi):
            shutil.copy2(f_nbi, '%s~' %f_nbi)
        fmt = ' %11.4e'
        as_nbi = '!\n'
        for jx, einj in enumerate(plasma.nbi.einj_kev):
            jnb = jx + 1
            as_nbi += '  %d\n' %jnb
            as_nbi += 'ZRD%d        ' %jnb + 3*fmt %(0, 2, 1)
            en = 1.e-3*plasma.nbi.einja[jx]
            as_nbi += fmt %einj
            as_nbi += '\n'
            for spec in plasma.nbi.spec_arr[:, jx]:
                as_nbi += fmt %spec
            as_nbi += '  1.0000E+00  8.1000E+01'
            as_nbi += '\n'
            as_nbi += fmt  %plasma.nbi.hbeam[jx]
            as_nbi += fmt  %plasma.nbi.rbtmax[jx]
            as_nbi += fmt  %plasma.nbi.rbtmin[jx]
            as_nbi += fmt  %plasma.nbi.tgA[jx]
            as_nbi += fmt  %plasma.nbi.aspect
            as_nbi += '\n'
            as_nbi += 5*fmt %(4, 2, 4, 2, 0)
            as_nbi += '\n'

        fnbi = open(f_nbi, 'w')
        fnbi.write(as_nbi)
        fnbi.close()
