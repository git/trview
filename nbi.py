import logging, datetime
import numpy as np
import aug_sfutils as sf
from aug_sfutils import read_imas
from trview import rot_matrix
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.nbi')

rot = rot_matrix.ROT_MATRIX

kv_def = {'INJ1': 60, 'INJ2': 93}
spec_lbl = {(1, 1): 'H', (1, 2): 'D', (2, 4): 'He', (6, 12): 'C'}
div_fac = {}
div_fac['aug'] = np.array([1., 1., 1., 1., 1.18, 1.14, 1.14, 1.18])


def dd2xy(inj_tor_box, src_slit, op, rabsca, phi_box):

    R_src = src_slit + op
    tmp   = rot(-inj_tor_box, R_src, rabsca, x_cen=op)
    alpha = np.arctan2(tmp.y, tmp.x)
    beta  = np.arctan2(tmp.y, tmp.x - op)
    rm = rot(phi_box, tmp.x, tmp.y)
    phi = alpha - beta
    return rm.x, rm.y, phi


def tr2as(rtcena, xybsca, xlbtna, xybapa, xlbapa, deltaR):

    rtcena    = np.atleast_1d(rtcena)
    xybsca    = np.atleast_1d(xybsca)
    xlbtna    = np.atleast_1d(xlbtna)
    xybapa    = np.atleast_1d(xybapa)
    xlbapa    = np.atleast_1d(xlbapa)

    vert_incl = -np.arcsin((xybapa - xybsca)/xlbapa)

    deltaR = np.atleast_1d(deltaR)
    rbtmin = 0.01*rtcena - 0.5*deltaR
    rbtmax = 0.01*rtcena + 0.5*deltaR
    hbeam  = 0.01*(xybsca - xlbtna*np.sin(vert_incl))
    tgA    = np.tan(vert_incl)

    return np.squeeze(rbtmin), np.squeeze(rbtmax), np.squeeze(hbeam), np.squeeze(tgA)


class NBI:


    def __init__(self, nshot, readSF=True, imasRun=None, tbeg=0., tend=10., nbi_min=3e4):

        self.shot    = nshot
        self.nbi_min = nbi_min
        self.tbeg    = tbeg
        self.tend    = tend
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromIMAS(self, tok='aug'):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('nbi')
        pini = ids.nbi.unit
        nnbi = len(pini)

        self.status = True
        self.t_ds = pini[0].power_launched.time
        nt = len(self.t_ds)

        n_spc = 3
        self.abeam = np.zeros(nnbi)
        self.zbeam = np.zeros(nnbi)
        self.theta_los = np.zeros(nnbi)
        self.einj_kev  = np.zeros(nnbi)
        self.part_mix  = np.zeros((nnbi, n_spc))
        self.power_mix = np.zeros((nnbi, n_spc))
        self.beamwidthpoly = np.zeros((nnbi, n_spc))
        self.power_ds = np.zeros((nt, nnbi))

        labels = []
        for jnb in range(nnbi):
            labels.append(pini[jnb].name.split('-')[0]) # AUG related
            self.abeam[jnb] = pini[jnb].species.a
            self.zbeam[jnb] = pini[jnb].species.z_n
            self.theta_los[jnb] = pini[jnb].beamlets_group[0].angle
            self.einj_kev[jnb] = 1e-3*np.squeeze(pini[jnb].energy.data)
            self.part_mix [jnb, :] = np.squeeze(pini[jnb].beam_current_fraction.data)
            self.power_mix[jnb, :] = np.squeeze(pini[jnb].beam_power_fraction.data)
            self.power_ds[:, jnb] = pini[jnb].power_launched.data
            self.beamwidthpoly[jnb, 1] = pini[jnb].beamlets_group[0].divergence_component[0].vertical/np.sqrt(2)

        self.beamwidthpoly[:, 1] /= div_fac[tok]
        self.beta_sf_deg = np.degrees(np.abs(self.theta_los))
        labels = np.array(labels)
        self.n_box = []
        for lbl in np.unique(labels):
            self.n_box.append(np.count_nonzero(labels == lbl))


    def fromH5(self):

        nbi_ids = read_imas.read_imas_h5(self.shot, self.run, branch='nbi')
        pini = nbi_ids['nbi']

        self.status = True
        self.t_ds = pini['unit[]&power_launched&time'][0, :]
        nt = len(self.t_ds)

        self.abeam = pini['unit[]&species&a'][:]
        self.zbeam = pini['unit[]&species&z_n'][:]
        self.einj_kev  = 1e-3*np.squeeze(pini['unit[]&energy&data'][:])
        self.part_mix  = np.squeeze(pini['unit[]&beam_current_fraction&data'][:])
        self.power_mix = np.squeeze(pini['unit[]&beam_power_fraction&data'][:])
        self.power_ds = pini['unit[]&power_launched&data'][:]

        nnbi = len(self.abeam)
        labels = []
        for jnb in range(nnbi):
            lbl = pini['unit[]&name'][jnb].decode()
            labels.append(lbl.split('-')[0]) # AUG related
        labels = np.array(labels)
        self.n_box = []
        for lbl in np.unique(labels):
            self.n_box.append(np.count_nonzero(labels == lbl))


    def fromShotfile(self):

        diag = 'NIS'
        sig = 'PNIQ'

        beta_deg = {}
        einj_nis = {}
        mass_amu = {}
        pow_mix  = {}
        nis = sf.SFREAD(self.shot, diag)
        self.status = nis.status
        if nis.status:
            tim = nis.gettimebase(sig)
            dat = nis.getobject(sig)
            for box in ('INJ1', 'INJ2'):
                nips = nis.getparset(box)
                beta_deg[box] = nips['BETA']
                mass_amu[box] = nips['M']
                einj_nis[box] = nips['UEXQ']
                pow_mix [box] = nips['SPEC']
                ind = (einj_nis[box] <= 0)
                einj_nis[box][ind] = kv_def[box]
        else:
            logger.error('No NIS shotfile found for shot %d', self.shot)
            return

# Parameters

        self.n_box = np.array( [ len(einj_nis['INJ1']), len(einj_nis['INJ2']) ] )
        n_nbi = np.sum(self.n_box)
        n_spc = 3
        self.nbi_on = np.ones(n_nbi, dtype=bool)

        if self.shot > 14050:
            self.beta_sf_deg = np.append(beta_deg['INJ1'], beta_deg['INJ2'])
        else:
            self.beta_sf_deg = np.array(8*[4.8891] )
        self.abeam = np.append(np.repeat(mass_amu['INJ1'], self.n_box[0]), \
                               np.repeat(mass_amu['INJ2'], self.n_box[1]) )
        self.abeam[self.abeam == 0] = 2
        self.zbeam = np.maximum(self.abeam/2, 1)
        self.einj_kev = np.append(einj_nis['INJ1'], einj_nis['INJ2'])

        self.power_mix = np.zeros((n_nbi, n_spc))
        self.part_mix  = np.zeros((n_nbi, n_spc))

        for jnb in range(n_nbi):
            if jnb == 0:
                powe  = pow_mix['INJ1'][::-1]
            elif jnb == self.n_box[0]:
                powe = pow_mix['INJ2'][::-1]
            pow_sum = np.sum(powe)
            if np.isnan(powe[0]) or pow_sum == 0:
                self.power_mix[jnb, :] = [1, 0, 0]
                self.part_mix [jnb, :] = [1, 0, 0]
                self.nbi_on[jnb] = False
            else:
                part = np.array([powe[0], 2.*powe[1], 3.*powe[2]])
                self.power_mix[jnb, :] = powe/pow_sum
                self.part_mix [jnb, :] = part/np.sum(part)

# Beam divergence from machine description, TRANSP definition w.r.t sqrt(2)

        self.divR = 0.0099 + np.zeros(n_nbi)
        self.divZ = 0.0099 + np.zeros(n_nbi)
        self.beamwidthpoly = np.zeros((n_nbi, n_spc))
        self.beamwidthpoly[:, 1] = self.divZ*div_fac['aug']

# Power

        index = (tim >= self.tbeg) & (tim <= self.tend)
        self.time = tim[index]
        dat = dat[index]
        ntim = len(self.time)
        self.power = np.zeros((ntim, n_nbi))

        jnb = 0
        for jbox, box_size in enumerate(self.n_box):
            for jsrc in range(box_size):
                self.power[:, jnb] = dat[:, jsrc, jbox]
                if np.max(self.power[:, jnb]) <= self.nbi_min:
                    self.nbi_on[jnb] = False
                    self.power[:, jnb] = 0.
                jnb += 1


    def aug_tr(self):

# distances: cm; angles: rad
# op: distance box-torus axis
# phi_box: absolute angle of NBI box (does not matter in poloidal cross section)
# inj_tor_box: toroidal angle between mid source and torus axis
# inj: toroidal angle between source and box aligned
# src_slit: distance ion source - aperture, for the center of the beamlines
# src_hw: horizontal semi-displacement of sources
# xybsca: source vertical elevation w.r.t. midplane
# rabsca: source horizontal half width w.r.t. mid-point
# RTCENA: R_tang
# vert_incl: vertical inclination
# XLBAPA: length = midpl1 / COS(vert_incl)
# midpl2: Grid - P(RTCENA)   projected on horizont. midplane
# XLBTNA: length = midpl2 / COS(vert_incl)
# XYBAPA: elevation at slit

# Rectangular half aperture in point P
      self.rapedga = 16
      self.xzpedga = 19

      self.phi_box = np.array(4*[np.radians(33.75)] + 4*[np.radians(209)])
      self.op = np.array(4*[284.20] + 4*[329.63])
      self.inj_tor_box = np.array(4*[np.radians(15)] + 4*[np.radians(18.9)])
      src_slit = 650.
      self.src_slit = np.array(8*[src_slit])
      self.src_hw  = 47.
      mag_ang = np.arctan2(self.src_hw, src_slit)
      self.midpl1  = np.hypot(self.src_slit, self.src_hw)

      self.magic_angle = np.array(2*[-mag_ang, mag_ang, mag_ang, -mag_ang])

      self.xybsca    = np.array([60, 60, -60, -60, 60, 70, -70, -60])
      self.rabsca    = self.src_hw*np.array(2*[1, -1, -1, 1])
      self.vert_incl = np.radians(self.beta_sf_deg)
      self.theta_los = np.array(2*[-1, -1, 1, 1])*self.vert_incl
      self.alpha     = self.inj_tor_box + self.magic_angle
      self.rtcena    = self.op*np.sin(self.alpha)
      self.midpl2    = self.midpl1 + np.sqrt(self.op**2 - self.rtcena**2)
      self.xlbtna    = self.midpl2/np.cos(self.vert_incl)
      self.xlbapa    = self.midpl1/np.cos(self.vert_incl)
      self.xybapa    = self.xybsca - np.sign(self.xybsca)*self.src_slit*np.tan(self.vert_incl)

      self.xsrc, self.ysrc, self.phi_los = dd2xy(self.inj_tor_box, self.src_slit, self.op, \
                            self.rabsca, self.phi_box)
      tmp = np.degrees(np.arctan2(self.ysrc, self.xsrc))
      ind = (self.phi_box > np.pi)
      tmp[ind] += 180
      self.xbzeta = tmp

# Species mix

      self.ffulla = self.part_mix[:, 0]
      self.fhalfa = self.part_mix[:, 1]
      self.ffulla[np.isnan(self.ffulla)] = 1.
      self.fhalfa[np.isnan(self.fhalfa)] = 0.
      self.einja  = 1e3*self.einj_kev


    def tr2as(self):

        if not hasattr(self, 'rtcena'):
            self.aug_tr()
        deltaR = [0.25, 0.35, 0.35, 0.25, 0.62, 0.58, 0.58, 0.62]
        self.aspect = 1
        self.rbtmin, self.rbtmax, self.hbeam, self.tgA = \
            tr2as(self.rtcena, self.xybsca, self.xlbtna, self.xybapa, self.xlbapa, deltaR)
        self.spec_arr = np.array([self.ffulla, 0.5*self.fhalfa, (1 - self.ffulla - self.fhalfa)/3.])
        self.spec_arr /= np.sum(self.spec_arr, axis=0)


    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        nb = imas.nbi()
        nb.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
        nb.ids_properties.homogeneous_time = 0
#    nb.ids_properties.source = os.path.abspath(__file__)

        n_nbi = np.sum(self.n_box)
        nbu = nb.unit
        nbu.resize(n_nbi)
        tim = np.atleast_1d(np.average(self.t_ds))
        Rsrc = np.hypot(self.xsrc, self.ysrc)
        zsrc = self.xybsca
        phi_src = np.arctan2(self.ysrc, self.xsrc)
        logger.info('Rsrc1 = %8.4f m, zsrc1 = %8.4f m, phisrc1 = %8.4f rad', Rsrc[0], zsrc[0], phi_src[0])
        for jnb in range(n_nbi):
            pow_frac = self.power_mix[jnb, :].reshape((3, 1)) # energy_mix, t
            part_frac = self.part_mix[jnb, :].reshape((3, 1)) # energy_mix, t
            if jnb < 4:
                nbu[jnb].name = 'INJ1-%s' % (jnb+1)
            else:
                nbu[jnb].name = 'INJ2-%s' % (jnb-3)
            nbu[jnb].species.a   = np.float64(self.abeam[jnb])
            nbu[jnb].species.z_n = np.float64(self.zbeam[jnb])
            nbu[jnb].species.label = spec_lbl[(self.zbeam[jnb], self.abeam[jnb])]
            nbu[jnb].beam_current_fraction.time = tim
            nbu[jnb].beam_current_fraction.data = part_frac
            nbu[jnb].beam_power_fraction.time = tim
            nbu[jnb].beam_power_fraction.data = pow_frac
            nbu[jnb].energy.time = tim
            nbu[jnb].energy.data = np.atleast_1d(1e3*self.einj_kev[jnb]) #keV->eV
# Power: time dependent
            nbu[jnb].power_launched.time = self.t_ds
            nbu[jnb].power_launched.data = self.power_ds[:, jnb]
            nbu[jnb].beamlets_group.resize(1)
            nbu[jnb].beamlets_group[0].tangency_radius = 1e-2*self.rtcena[jnb]
            nbu[jnb].beamlets_group[0].position.r   = 1e-2*Rsrc[jnb]
            nbu[jnb].beamlets_group[0].position.z   = 1e-2*zsrc[jnb]
            nbu[jnb].beamlets_group[0].position.phi = phi_src[jnb]
            nbu[jnb].beamlets_group[0].direction = 1
            nbu[jnb].beamlets_group[0].angle = self.theta_los[jnb]
            if jnb < self.n_box[0]:
                nbu[jnb].beamlets_group[0].focus.focal_length_horizontal = 6.5
                nbu[jnb].beamlets_group[0].focus.focal_length_vertical = 8.5
            else:
                nbu[jnb].beamlets_group[0].focus.focal_length_horizontal = 7.23
                nbu[jnb].beamlets_group[0].focus.focal_length_vertical = 11.94
            nbu[jnb].beamlets_group[0].divergence_component.resize(1)
# hhe following added by DPC 2019-10-24 based on data from MD
            nbu[jnb].beamlets_group[0].divergence_component[0].particles_fraction = 1.0
            nbu[jnb].beamlets_group[0].divergence_component[0].vertical   = np.sqrt(2.)*self.divZ[jnb]
            nbu[jnb].beamlets_group[0].divergence_component[0].horizontal = np.sqrt(2.)*self.divR[jnb]
# end of addition
            nbu[jnb].beamlets_group[0].beamlets.tangency_radii.resize(1)
            nbu[jnb].beamlets_group[0].beamlets.tangency_radii[0] = 1e-2*self.rtcena[jnb]

        return nb
    
        
if __name__ == '__main__':

    NBI(28053)
