import numpy as np

def mom2rz(rcos, rsin, zcos, zsin, nthe=101, endpoint=False):

# Fourier moments -> {R,z} of magnetic surfaces
# rcos [jt, jrho, jmom] or [jrho, jmom] or [jt, jmom] or [jmom]

    theta = np.linspace(0, 2*np.pi, nthe, endpoint=endpoint) 

    nmom  = np.size(rcos, -1)
    angle = np.outer(np.arange(nmom), theta)
    cosm  = np.cos(angle)
    sinm  = np.sin(angle)
    r_plot = np.dot(rcos, cosm) + np.dot(rsin, sinm)
    z_plot = np.dot(zcos, cosm) + np.dot(zsin, sinm)

    return r_plot, z_plot
