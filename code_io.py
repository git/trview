"""RABBIT I/O
Classes for parsing RABBIT input and output
"""

import os, sys, json, shutil, datetime, logging
import numpy as np
from scipy.io import netcdf
import aug_sfutils as sf
from aug_sfutils import sfhmod
from trview.sfarr import SFARR

logger = logging.getLogger('trview.IO')
logger.setLevel(logging.INFO)

rbhome = '%s/rabbit' %os.getenv('SHARES_USER_DIR')
if not os.path.exists(rbhome):
    os.mkdir(rbhome)

locdir = os.path.dirname(os.path.realpath(__file__))

rb2sf_d = {'rhot_in': 'rho_in', 'rhot_out': 'rho_out', 'ffp': 'F', \
    'ne_in': 'ne', 'te_in': 'te', 'ti_in': 'ti', 'omg_in': 'wtor', \
    'powi_tot': 'pheatIQ', 'powe_tot': 'pheatEQ', 'prot': 'protQ', \
    'pcxloss': 'pcxQ', 'porbloss': 'plossQ', 'pshine': 'pshineQ', \
    'dvol': 'dV', 'darea': 'dArea', \
    'wfi_par_lab': 'wfi_parL', \
    'torqthcxloss': 'torqthcx', \
    'bdep3': 'bdep', 'einj3': 'EinjQ', 'vinj3': 'vinjQ'}

for lbl in ('vol', 'area', 'torqe', 'torqi', 'torqdepo', 'torqjxb', 'torqth', 'nrate', \
    'wfi_par', 'wfi_perp', 'powe', 'powi', 'jfi', 'jnbcd', 'bdens', 'press'):
    rb2sf_d[lbl] = lbl
 
sf2rb_d = {val: key for key, val in rb2sf_d.items()}


def parse_rb_namelist(fnml=None, nml=None):

    """
    Parsing settings for RABBIT run
    """

    options = type('', (), {})()

    if nml is None:
        fnml = fnml.strip()

        if os.path.isfile(fnml):
            logger.info('Parsing option namelist %s', fnml)
        else:
            logger.error('File %s not found', fnml)
            return

        with open(fnml, 'r') as f:
            lines = f.readlines()
    else:
        lines = nml.splitlines()

    for jlin in range(len(lines)):
        sline = lines[jlin].strip()
        if sline == '':
            continue
        if sline[0] == '!':
            continue
        if sline[0] == '&':
            node = sline[1:].lower()
            options.__dict__[node] = type('', (), {})()
            while True:
                jlin += 1
                sline = lines[jlin].strip().split('!')[0]
                if len(sline) == 0:
                    break
                if sline[0] == '/':
                    break
                try:
                    key, val1 = sline.split('=')
                except:
                    logger.warning('Missing delimiter before line %s', sline)
                key = key.strip().lower()
                logger.debug('parse nml %s %s', key, val1)
                try:
                    val = np.int32(val1)
                except:
                    try: 
                        val = np.float32(val1)
                    except:
                        val = val1.strip().replace("'", '')
                tmp = type('', (), {})()
                tmp.data = val
                options.__dict__[node].__dict__[key] = tmp

    return options


def sfh_mod(heat_class, fsfh_in=None, fsfh_out=None):

    sfh = sfhmod.SFHMOD(fin=fsfh_in)
    sfo = sfh.sffull

    sfh_d = {}

    tbases  = [ lbl for lbl in sfo.objects if sfo[lbl].SFOtypeLabel == 'TimeBase']
    abases  = [ lbl for lbl in sfo.objects if sfo[lbl].SFOtypeLabel == 'AreaBase']

    nt = len(heat_class.OUTPUT.time)
    sfh.modtimeall('time', nt)
    sfh_d['time'] = heat_class.OUTPUT.time[:]

    for obj in abases:
        rbobj = sf2rb_d[obj]
        if hasattr(heat_class.INPUT, rbobj):
            nx = heat_class.INPUT.__dict__[rbobj].shape[-1]
            sfh.modareaall(obj, nx)
        elif hasattr(heat_class.OUTPUT, rbobj):
            nx = heat_class.OUTPUT.__dict__[rbobj].shape[-1]
            sfh.modareaall(obj, nx)

    for obj in sfo.parsets:
        parset = sfo[obj].data
        sfh_d[obj] = {}
        for pn, val in parset.items():
            for pset in heat_class.INPUT.options.__dict__.values():
                if hasattr(pset, pn):
                    sfh_d[obj][pn] = pset.__dict__[pn].data
                    break

    for obj in sfo.objects:
        if obj in sf2rb_d.keys():
            rbobj = sf2rb_d[obj]
        else:
            rbobj = 'None'
        if hasattr(heat_class.INPUT, rbobj):
            sfh_d[obj] = heat_class.INPUT.__dict__[rbobj]
        elif hasattr(heat_class.OUTPUT, rbobj):
            sfh_d[obj] = heat_class.OUTPUT.__dict__[rbobj]

    logger.debug('GIT %d', len(sfh_d['time']))
    sfh.write(fout=fsfh_out)

    return sfh_d


def rb2sf(heat_class, diag='RBT'):
    """
    Converting RABBIT output to shotfile
    """

# Paths

    source = '%s/%s00000.sfh.temp' %(locdir, diag)
    fsfh = '%s/%s00000.sfh' %(rbhome, diag)

    sfh_dic = sfh_mod(heat_class, fsfh_in=source, fsfh_out=fsfh)
    sf.write_sf(heat_class.shot, sfh_dic, rbhome, diag, exp=os.getenv('USER'))


def code2cdf(heat_class, code='rb', f_cdf=None):
    """
    Converting RABBIT input/output to NetCDF
    """

    if f_cdf is None:
        cdf_dir = '%s/ncdf' %rbhome
        if not os.path.exists(cdf_dir):
            os.mkdir(cdf_dir)
        f_cdf = '%s/%d%s.cdf' %(cdf_dir, heat_class.shot, code)
    f = netcdf.netcdf_file(f_cdf, 'w', mmap=False)

    f.history = "Created " + datetime.datetime.today().strftime("%d/%m/%y")

    for key, val in heat_class.INPUT.cdf_dims.items():
        f.createDimension(key, val)

    if hasattr(heat_class, 'OUTPUT'):
        for key, val in heat_class.OUTPUT.cdf_dims.items():
            f.createDimension(key, val)

# Scalars, from options namelist, excluding strings

    if hasattr(heat_class.INPUT, 'options'):
        for node_key, node in heat_class.INPUT.options.__dict__.items():
            for key, val in node.__dict__.items():
                logger.debug('%s:%s', node_key, key)
                if not isinstance(val.data, str):
                    scal = f.createVariable(key, val.data.dtype, () )
                    scal.assignValue(val.data)
                    if hasattr(val, 'units'):
                        scal.units = val.units
                    else:
                        scal.units = ''
                    scal.long_name = val.long_name

# Code input quantities

    for sig in heat_class.INPUT.__dict__.keys():
        logger.debug(sig)
        sf_arr = getattr(heat_class.INPUT, sig)
        if hasattr(sf_arr, 'ncdims'):
            tmp = f.createVariable(sig, np.float32, sf_arr.ncdims)
            tmp[:] = sf_arr
            tmp.units = sf_arr.units
            tmp.long_name = sf_arr.long_name

# Code output

    if hasattr(heat_class, 'OUTPUT'):
        for sig in heat_class.OUTPUT.__dict__.keys():
            logger.debug(sig)
            sf_arr = getattr(heat_class.OUTPUT, sig)
            if hasattr(sf_arr, 'ncdims'):
                tmp = f.createVariable(sig, np.float32, sf_arr.ncdims)
                tmp[:] = sf_arr
                tmp.units = sf_arr.units
                tmp.long_name = sf_arr.long_name

    f.close()
    logger.info('Stored %s', f_cdf)
    return f_cdf


def rb_input_attr(INPUT):

    for key in ('time', 'rhot_in', \
        'start_pos', 'unit_vec', 'width_poly', 'einj', 'pnbi', \
        'a_beam', 'z_beam', 'part_mix', 'rlim', 'zlim', \
        'ne_in', 'te_in', 'ti_in', 'zef_in', 'omg_in', \
        'rrect', 'zrect', 'psi_rect', 'rmag', 'zmag', 'psi_axis', 'psi_sep', \
        'ffp', 'vol', 'iota', 'area', 'psi1d_n'):
        INPUT.__dict__[key] = SFARR(getattr(INPUT, key))

    INPUT.time.units  = '(s)'
    INPUT.time.ncdims = ('time', )
    INPUT.time.long_name = 'Output time grid'

# Input settings from namelist
    INPUT.options.species.aimp.units = '(AMU)'
    INPUT.options.physics.rlim.units = '(m)'
    INPUT.options.physics.zlim.units = '(m)'
    INPUT.options.physics.rmin.units = '(m)'
    INPUT.options.physics.rmax.units = '(m)'

    INPUT.options.species.aimp.long_name = 'Atomic number of impurity'
    INPUT.options.species.zimp.long_name = 'Charge number of impurity'

    INPUT.options.output_settings.it_orbout.long_name = 'Some param'

    INPUT.options.physics.jumpcor.long_name = ''
    INPUT.options.physics.table_path.long_name    = 'Path to ADAS tables'
    INPUT.options.physics.limiter_file.long_name  = 'Path to limiter file'
    INPUT.options.physics.rlim.long_name          = 'Default limiter R array'
    INPUT.options.physics.zlim.long_name          = 'Default limiter Z array'
    INPUT.options.physics.rmin.long_name          = 'Default R min for grids'
    INPUT.options.physics.rmax.long_name          = 'Default R max for grids'
    INPUT.options.physics.torqjxb_model.long_name = 'Model for JxB torque'

    INPUT.options.numerics.norbits.long_name      = '# of orbits considered'
    INPUT.options.numerics.orbit_dt_fac.long_name = 'dt_fac'
    INPUT.options.numerics.distfun_nv.long_name   = 'v-dim for distribution functions'
    INPUT.options.numerics.distfun_vmax.long_name = 'Max v-dim for distribution functions'

# Geometry

    INPUT.start_pos.units  = '(m)'
    INPUT.unit_vec.units   = ''
    INPUT.width_poly.units = ''
    INPUT.einj.units       = '(eV)'
    INPUT.pnbi.units       = '(W)'
    INPUT.a_beam.units     = '(AMU)'
    INPUT.part_mix.units   = ''
    INPUT.rlim.unit = '(m)'
    INPUT.zlim.unit = '(m)'

    INPUT.start_pos.long_name  = 'Position of NBI source in real space'
    INPUT.unit_vec.long_name   = 'Versor of direction of central beam'
    INPUT.width_poly.long_name = 'Polynomial coefficients for beam-width'
    INPUT.einj.long_name       = 'Injection energy'
    INPUT.pnbi.long_name       = 'NBI injected power'
    INPUT.a_beam.long_name     = 'Mass number of NBI ions'
    INPUT.part_mix.long_name   = 'Particle fraction of full/half/third energy'
    INPUT.rlim.long_name       = 'R of limiter contour'
    INPUT.zlim.long_name       = 'Z of limiter contour'

    INPUT.start_pos.ncdims  = ('n_nbi', 'space')
    INPUT.unit_vec.ncdims   = ('n_nbi', 'space')
    INPUT.width_poly.ncdims = ('n_nbi', 'coeff')
    INPUT.einj.ncdims       = ('time', 'n_nbi', )
    INPUT.pnbi.ncdims       = ('time', 'n_nbi')
    INPUT.a_beam.ncdims     = ('n_nbi', )
    INPUT.part_mix.ncdims   = ('n_nbi', 'spec')
    INPUT.rlim.ncdims       = ('n_lim',)
    INPUT.zlim.ncdims       = ('n_lim',)

# Profile input

    INPUT.rhot_in.units = ''
    INPUT.te_in.units   = '(eV)'
    INPUT.ti_in.units   = '(eV)'
    INPUT.ne_in.units   = '(1/cm**3)'
    INPUT.omg_in.units  = '(rad/s)'
    INPUT.zef_in.units  = ''

    INPUT.rhot_in.long_name = 'Normalised rho toroidal'
    INPUT.te_in.long_name   = 'Electron temperature profile'
    INPUT.ti_in.long_name   = 'Ion temperature profile'
    INPUT.ne_in.long_name   = 'Electron density profile'
    INPUT.omg_in.long_name  = 'Angular frequency profile'
    INPUT.zef_in.long_name  = 'Effective charge profile'

    INPUT.rhot_in.ncdims = ('rhot_in', )
    for attr in ('te_in', 'ne_in', 'ti_in', 'omg_in', 'zef_in'):
        INPUT.__dict__[attr].ncdims = ('time', 'rhot_in')

# Equil input

    for attr in ('rrect', 'zrect', 'rmag', 'zmag'):
        INPUT.__dict__[attr].units = '(m)'
    for attr in ('psi_rect', 'psi_sep', 'psi_axis'):
        INPUT.__dict__[attr].units = '(Wb/rad)'
    for attr in ('iota', 'psi1d_n'):
        INPUT.__dict__[attr].units = ''
    INPUT.vol.units     = '(m**3)'
    INPUT.area.units    = '(m**2)'
    INPUT.ffp.units     = '(T m)'

    INPUT.rrect.long_name    = 'R-mesh for equilibrium matrices'
    INPUT.zrect.long_name    = 'Z-mesh for equilibrium matrices'
    INPUT.rmag.long_name     = 'R-coord of magnetic axis'
    INPUT.zmag.long_name     = 'Z-coord of magnetic axis'
    INPUT.psi_rect.long_name = 'Psi matrix on cartesian R,z grid'
    INPUT.psi1d_n.long_name  = 'Psi profile'
    INPUT.vol.long_name      = 'Volume within flux surfaces'
    INPUT.area.long_name     = 'Area within flux surfaces'
    INPUT.iota.long_name     = 'Rotational transform (1/q)'
    INPUT.ffp.long_name      = 'F function profile'
    INPUT.psi_axis.long_name = 'Psi at axis'
    INPUT.psi_sep.long_name  = 'Psi at separatrix'

    INPUT.rrect.ncdims = ('rrect', )
    INPUT.zrect.ncdims = ('zrect', )
    INPUT.psi_rect.ncdims = ('time', 'rrect', 'zrect')
    for attr in ('rmag', 'zmag', 'psi_sep', 'psi_axis'):
        INPUT.__dict__[attr].ncdims = ('time',)
    for attr in ('psi1d_n', 'vol', 'area', 'iota', 'ffp'):
        INPUT.__dict__[attr].ncdims = ('time', 'rhot_in')

    return INPUT


def rb_output_attr(OUTPUT):

    for key in ('time', 'rhot_out', \
        'prot', 'powe_tot', 'powi_tot', 'pshine', 'porbloss', 'pcxloss', 'inbcd', \
        'nrate', 'dvol', 'darea', 'powe', 'powi', 'press', 'bdep', 'bdens', 'jfi', 'jnbcd', \
        'torqe', 'torqi', 'torqjxb', 'torqth', 'torqdepo', 'torqthcxloss', \
        'wfi_par', 'wfi_perp', 'wfi_par_lab', \
        'bdep3', 'einj3', 'vinj3'):
        OUTPUT.__dict__[key] = SFARR(getattr(OUTPUT, key))
    
    OUTPUT.time.units  = '(s)'
    OUTPUT.time.ncdims = ('time', )
    OUTPUT.time.long_name = 'Output time grid'

    OUTPUT.rhot_out.units = ''
    for attr in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'pcxloss'):
        OUTPUT.__dict__[attr].units = '(W)'
    OUTPUT.inbcd.units = '(A)'
    for attr in ('torqe', 'torqi', 'torqjxb', 'torqth', 'torqdepo', 'torqthcxloss'):
        OUTPUT.__dict__[attr].units = '(N/m**2)'
    for attr in ('wfi_par', 'wfi_perp', 'wfi_par_lab'):
        OUTPUT.__dict__[attr].units = '(J/m**3)'
    OUTPUT.powe.units  = '(W/m**3)'
    OUTPUT.powi.units  = '(W/m**3)'
    OUTPUT.dvol.units  = '(m**3)'
    OUTPUT.darea.units = '(m**2)'
    OUTPUT.bdens.units = '(1/m**3)'
    OUTPUT.nrate.units = '(1/m**3/s)'
    OUTPUT.bdep.units  = '(1/m**3/s)'
    OUTPUT.bdep3.units = '(1/m**3/s)'
    OUTPUT.einj3.units = '(eV)'
    OUTPUT.vinj3.units = '(m/s)'
    OUTPUT.jfi.units   = '(A/m**2)'
    OUTPUT.jnbcd.units = '(A/m**2)'
    OUTPUT.press.units = '(Pa)'

    OUTPUT.rhot_out.long_name = 'Output rhot'

    OUTPUT.rhot_out.ncdims = ('rhot_out', )  
    for attr in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'pcxloss', 'inbcd'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'n_nbi')
    for attr in ('nrate', 'dvol', 'darea'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'rhot_out')
    for attr in ('powe', 'powi', 'press', 'bdep', 'bdens', 'jfi', 'jnbcd', \
        'wfi_par', 'wfi_perp', 'wfi_par_lab', \
        'torqe', 'torqi', 'torqjxb', 'torqth', 'torqthcxloss', 'torqdepo'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'rhot_out', 'n_nbi')
    for attr in ('bdep3', 'einj3', 'vinj3'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'rhot_out', 'spec', 'n_nbi')

    OUTPUT.prot.long_name      = 'Power to Rotation by source'
    OUTPUT.powe_tot.long_name  = 'Electron Heating Power by source'
    OUTPUT.powi_tot.long_name  = 'Ion Heating Power by source'
    OUTPUT.powe.long_name      = 'Electron Heating Power Density by source'
    OUTPUT.powi.long_name      = 'Ion Heating Power Density by source'
    OUTPUT.press.long_name     = 'Fast-ion pressure'
    OUTPUT.bdep.long_name      = 'Particle Source Density by source'
    OUTPUT.bdep3.long_name     = 'Particle Source Density by source and Energy Component'
    OUTPUT.bdens.long_name     = 'Fast-ion density'
    OUTPUT.nrate.long_name     = 'Neutron yield profile per unit volume'
    OUTPUT.dvol.long_name      = 'Volume of Radial Cell'
    OUTPUT.darea.long_name     = 'Poloidal Cross-Sectional Area of Radial Cell'
    OUTPUT.einj3.long_name     = 'Injection Energy Profile (per Energy Component and source)'
    OUTPUT.vinj3.long_name     = 'Injection Velocity Profile (per Energy Component and source)'
    OUTPUT.jfi.long_name       = 'Fast-ion Current Density'
    OUTPUT.jnbcd.long_name     = 'Driven Current Density'
    OUTPUT.wfi_par.long_name   = 'Stored Fast-Ion Energy Density (parallel)'
    OUTPUT.wfi_perp.long_name  = 'Stored Fast-Ion Energy Density (perpendicular)'
    OUTPUT.wfi_par_lab.long_name  = 'Stored Fast-Ion Energy Density (parallel, lab frame)'
    OUTPUT.torqthcxloss.long_name = 'CX losses Torque Density'
    OUTPUT.torqe.long_name    = 'Collisional Torque Density to Electrons'
    OUTPUT.torqi.long_name    = 'Collisional Torque Density to Ions'
    OUTPUT.torqjxb.long_name  = 'JxB Torque Density'
    OUTPUT.torqth.long_name   = 'Thermalisation Torque Density'
    OUTPUT.torqdepo.long_name = 'Deposited Torque Density'

    return OUTPUT


def tb_input_attr(INPUT):

    for key in ('time', 'rhop_in', 'freq', \
        'ne_in', 'te_in', 'ti_in', 'zef_in', 'omg_in', \
        'rrect', 'zrect', 'psi_rect', 'psi_sep', \
        'Br', 'Bz', 'Bt'):
        INPUT.__dict__[key] = SFARR(getattr(INPUT, key))

    INPUT.time.units  = '(s)'
    INPUT.time.ncdims = ('time', )
    INPUT.time.long_name = 'Output time grid'

# Input settings from namelist
#    INPUT.options.rmaj.units = '(cm)'
#    INPUT.options.rmaj.long_name = 'Major radius'

# Time-independent parameters

    INPUT.freq.unit = '(Hz)'
    INPUT.freq.long_name  = 'Gyrotron frequency'
    INPUT.freq.ncdims  = ('n_ech', )

# Profile input

    INPUT.rhop_in.units = ''
    INPUT.te_in.units   = '(eV)'
    INPUT.ti_in.units   = '(eV)'
    INPUT.ne_in.units   = '(1/m**3)'
    INPUT.omg_in.units  = '(rad/s)'
    INPUT.zef_in.units  = ''

    INPUT.rhop_in.long_name = 'Normalised rho toroidal'
    INPUT.te_in.long_name   = 'Electron temperature profile'
    INPUT.ti_in.long_name   = 'Ion temperature profile'
    INPUT.ne_in.long_name   = 'Electron density profile'
    INPUT.omg_in.long_name  = 'Angular frequency profile'
    INPUT.zef_in.long_name  = 'Effective charge profile'

    INPUT.rhop_in.ncdims = ('rhop_in', )
    for attr in ('te_in', 'ne_in', 'ti_in', 'omg_in', 'zef_in'):
        INPUT.__dict__[attr].ncdims = ('time', 'rhop_in')

# Equil input

    for attr in ('rrect', 'zrect'):
        INPUT.__dict__[attr].units = '(m)'
    for attr in ('psi_rect', 'psi_sep'):
        INPUT.__dict__[attr].units = '(Wb/rad)'

    INPUT.rrect.long_name    = 'R-mesh for equilibrium matrices'
    INPUT.zrect.long_name    = 'Z-mesh for equilibrium matrices'
    INPUT.psi_rect.long_name = 'Psi matrix on cartesian R,z grid'
    INPUT.psi_sep.long_name  = 'Psi at separatrix'

    INPUT.rrect.ncdims = ('rrect', )
    INPUT.zrect.ncdims = ('zrect', )
    for attr in ('psi_rect', 'Br', 'Bz', 'Bt'):
        INPUT.__dict__[attr].ncdims = ('time', 'rrect', 'zrect')
    for attr in ('psi_sep', ):
        INPUT.__dict__[attr].ncdims = ('time',)

# Take var descriptions for intinbeam, floatinbeam from tooltips
    f_json = '%s/settings/default.json' %locdir
    with open(f_json) as fjson:
        tb_in_d = json.load(fjson)['torbeam']
    for key, val in tb_in_d.items():
        if hasattr(INPUT.options.intinbeam, key):
            INPUT.options.intinbeam.__dict__[key].long_name = val['toolTip']
        elif hasattr(INPUT.options.floatinbeam, key):
            INPUT.options.floatinbeam.__dict__[key].long_name = val['toolTip']

    return INPUT


def tb_output_attr(OUTPUT):

    for key in ('time', 'rhop_out', 'pecrh', 'ieccd', \
        'power_out', 'jeccd_out'):
        OUTPUT.__dict__[key] = SFARR(getattr(OUTPUT, key))
    
    OUTPUT.time.units  = '(s)'
    OUTPUT.time.ncdims = ('time', )
    OUTPUT.time.long_name = 'Output time grid'

    OUTPUT.rhop_out.units  = ''
    OUTPUT.pecrh.units = '(W)'
    OUTPUT.ieccd.units = '(A)'
    OUTPUT.power_out.units = '(W/m**3)'
    OUTPUT.jeccd_out.units = '(A/m**2)'

    OUTPUT.rhop_out.long_name = 'Output rhop'

    OUTPUT.rhop_out.ncdims = ('rhop_out', )  
    for attr in ('pecrh', 'ieccd'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'n_ech')
    for attr in ('power_out', 'jeccd_out'):
        OUTPUT.__dict__[attr].ncdims = ('time', 'rhop_out', 'n_ech')

    OUTPUT.pecrh.long_name = 'Electron heating power by gyrotron'
    OUTPUT.ieccd.long_name = 'Driven current by gyrotron'
    OUTPUT.power_out.long_name = 'Electron heating power density by gyrotron'
    OUTPUT.jeccd_out.long_name = 'Driven current density by gyrotron'

    return OUTPUT
