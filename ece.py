import logging, datetime
import numpy as np
import aug_sfutils as sf
from aug_sfutils import read_imas
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.ece')
logger.setLevel(logging.DEBUG)

if len(logger.handlers) == 0:
    fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')
    hnd = logging.StreamHandler()
    hnd.setFormatter(fmt)
    logger.addHandler(hnd)

# /shares/departments/AUG/users/eced/Software/CEC/libece/geo_los.c
phi_src = 191.25 # degrees, between sector 1/16 is 0


def geo_los(nshot, nowg, z_lens, nLOS):

    R_max = 2.35
    R_min = 1.00
    Rlos = np.linspace(R_max, R_min, nLOS, endpoint=True) # array(nLOS)
    if nshot <= 24202:
        R_lens   = 3.993
        R_wguide = 5.796
        z_wguide = [0.044, 0.044, 0.075, 0.075, 0.075, 0.044, 0.075, 0.044, 0.106, 0.106, 0.106, 0.106]
        m  = (z_wguide[nowg-1] - z_lens)/(R_wguide - R_lens)
        b  = z_lens - m*R_lens
        Zlos = m*Rlos + b # array(nLOS)
    else:
        R1 = Rlos - 0.133 # array(nLOS)
        Z1 = z_lens - 0.075
        if nowg in (9, 10, 11, 12):
            Zlos = Z1*1.4 - 0.01534 - Z1*R1*0.372 + R1*0.0254
        elif nowg in (3, 4, 5, 7):
            Zlos = Z1*1.4 + 0.075 - Z1*R1*0.372
        elif nowg in (1, 2, 6, 8):
            Zlos = Z1*1.4 + 0.16534 - Z1*R1*0.372 - R1*0.0254
    return Rlos, Zlos

    
class ECE:


    def __init__(self, nshot, readSF=True, imasRun=None, tbeg=0., tend=10.):

        self.shot = nshot
        self.tbeg = tbeg
        self.tend = tend
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromIMAS(self, tok='aug'):

        logger.debug('Reading from IMAS')
        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('ece')

        ec = ids.ece
        n_ch = len(ec.channel)
        nt = len(ec.time)
        self.freq_time = np.squeeze(ec.time)

        self.freq = np.zeros(n_ch, dtype=np.float64)
        self.ifBandWidth = np.zeros_like(self.freq)
        self.rLOS1 = np.zeros_like(self.freq)
        self.rLOS2 = np.zeros_like(self.freq)
        self.zLOS1 = np.zeros_like(self.freq)
        self.zLOS2 = np.zeros_like(self.freq)
        self.phiLOS1 = np.zeros_like(self.freq)
        self.phiLOS2 = np.zeros_like(self.freq)
        for jch in range(n_ch):
            los = ec.channel[jch].line_of_sight
            self.rLOS1[jch]   = los.first_point.r
            self.zLOS1[jch]   = los.first_point.z
            self.phiLOS1[jch] = los.first_point.phi
            self.rLOS2[jch]   = los.second_point.r
            self.zLOS2[jch]   = los.second_point.z
            self.phiLOS2[jch] = los.second_point.phi
            self.freq[jch] = np.squeeze(ec.channel[jch].frequency.data)
            self.ifBandWidth[jch] = ec.channel[jch].if_bandwidth


    def fromH5(self, tok='aug'):

        logger.debug('Reading from HDF5')
        ece_ids = read_imas.read_imas_h5(self.shot, self.run, branch='ece')
        ece = ece_ids['ece']
        self.freq = np.squeeze(ece['channel[]&frequency&data'][:])
        self.freq_time = np.squeeze(ece['channel[]&frequency&time'][:, 0]) # equal for all channels
        self.ifBandWidth = ece['channel[]&if_bandwidth'][:]
        self.rLOS1   = ece['channel[]&line_of_sight&first_point&r']
        self.zLOS1   = ece['channel[]&line_of_sight&first_point&z']
        self.phiLOS1 = ece['channel[]&line_of_sight&first_point&phi']
        self.rLOS2   = ece['channel[]&line_of_sight&second_point&r']
        self.zLOS2   = ece['channel[]&line_of_sight&second_point&z']
        self.phiLOS2 = ece['channel[]&line_of_sight&second_point&phi']


    def fromShotfile(self, diag='CEC'):

        logger.debug('Reading from ShotFile')
        ecsf = sf.SFREAD(self.shot, diag)
        parm = ecsf('parms-A')
        self.freq        = parm['f']
        self.ifBandWidth = parm['df']
        ifgroup = parm['IFGROUP'] - 1 # python indexing
        methods = ecsf('METHODS')
        z_lens    = methods['ZLENS']*1.e-2 # cm -> m
        waveguide = methods['WAVEGUID']
        n_wguides = waveguide[ifgroup]
        n_channels = len(n_wguides)
        nLOS = 2
        nowg = n_wguides[0]
        nowg = n_wguides[-1]
        self.rLOS1 = np.zeros(n_channels, dtype=np.float32)
        self.rLOS2 = np.zeros_like(self.rLOS1)
        self.zLOS1 = np.zeros_like(self.rLOS1)
        self.zLOS2 = np.zeros_like(self.rLOS1)
        for jch, nowg in enumerate(n_wguides):
            Rlos, Zlos = geo_los(self.shot, nowg, z_lens, nLOS)
            self.rLOS1[jch] = Rlos[0]
            self.zLOS1[jch] = Zlos[0]
            self.rLOS2[jch] = Rlos[-1]
            self.zLOS2[jch] = Zlos[-1]

        self.freq_time = np.zeros(1, dtype=np.float64)
        self.phiLOS1 = np.radians(phi_src)
        self.phiLOS2 = np.radians(phi_src)


    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        ddic_vers = [int(x) for x in imas.__package__.split('ual_')[1].split('_')]
        new_ece_los = ddic_vers[0] >= 4 and ddic_vers[1] >= 11 and ddic_vers[2] >= 10
        n_ch = len(self.freq)
        ece = imas.ece()
        ece.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
        ece.ids_properties.homogeneous_time = 1
        ece.time = self.freq_time

        ece.channel.resize(n_ch)
        for jch in range(n_ch):
            ece.channel[jch].frequency.data = np.atleast_1d(self.freq[jch])
            ece.channel[jch].if_bandwidth   = np.float64(self.ifBandWidth[jch])
            if new_ece_los:
                los = ece.channel[jch].line_of_sight
                los.first_point.r    = np.float64(self.rLOS1[jch])
                los.first_point.z    = np.float64(self.zLOS1[jch])
                los.first_point.phi  = np.float64(self.phiLOS1)
                los.second_point.r   = np.float64(self.rLOS2[jch])
                los.second_point.z   = np.float64(self.zLOS2[jch])
                los.second_point.phi = np.float64(self.phiLOS2)

        return ece
    

if __name__ == '__main__':

#    ec = ECE(34954)
    ec = ECE(38384, imasRun=6, readSF=False)
    print(ec.freq)
    print(ec.ifBandWidth)
    print(ec.rLOS1, ec.rLOS2, ec.zLOS1, ec.zLOS2, ec.phiLOS1, ec.phiLOS2)
#    ec_ids = ec.toIMAS()
