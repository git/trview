import os, logging
import numpy as np
import ctypes as ct
import datetime
from trview import config
from aug_sfutils.config import sfBasepath

logger = logging.getLogger('trview.libech_wrapper')

libECRH =  ct.cdll.LoadLibrary(config.ecalib)


class libECRH_wrapper:


    def __init__(self, nshot):

        sf_file = '%s/augd/ECN/%d/%d' %(sfBasepath, nshot//1000, nshot)
        if not os.path.isfile(sf_file):
            logger.error('File %s not found', sf_file)
            return
        date_obj = datetime.datetime.fromtimestamp(os.path.getmtime(sf_file))
        date = ct.c_double(date_obj.date().year * 1.e4 + date_obj.date().month * 1.e2 + date_obj.date().day + 0.0)
        self._date = ct.byref(date)


    def setval2tp(self, gynum, a, beta):

        ct_gy = ct.c_int32(gynum)
        c_error = ct.c_int32(0)
        ct_theta_pol = ct.c_double(a)
        ct_phi_tor   = ct.c_double(beta)
        libECRH.setval2tp_(ct.byref(c_error), ct.byref(ct_gy), ct.byref(ct_theta_pol), ct.byref(ct_phi_tor), self._date)
        error = 0
        if(c_error.value != 0):
            logger.error('Encountered error %d', c_error.value)
            if(np.abs(c_error.value) == 102 or np.abs(c_error.value) == 2):
                error = -1
        return error, ct_theta_pol.value, ct_phi_tor.value


    def tp2setval(self, gynum, theta, phi):

        ct_gy = ct.c_int32(gynum)
        c_error = ct.c_int32(0)
        c_error.value = 0
        ct_a_pol = ct.c_double(theta)
        ct_beta_tor = ct.c_double(phi)
        error = 0
        libECRH.tp2setval_(ct.byref(c_error), ct.byref(ct_gy), ct.byref(ct_a_pol), ct.byref(ct_beta_tor), self._date)
        if(c_error.value != 0):
            logger.error('Encountered error %d', c_error.value)
            if(np.abs(c_error.value) == 102 or np.abs(c_error.value) == 2):
                error = -1
        return error, ct_a_pol.value, ct_beta_tor.value
