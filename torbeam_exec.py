import os, sys, logging
import numpy as np
from scipy.interpolate import CubicSpline, interp1d
from trview import code_io, rhop2rhot
from trview.torbeam import torbeam_wrapper as tbwrap

fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')

logger = logging.getLogger('pyTORBEAM')

if len(logger.handlers) == 0:
    hnd = logging.StreamHandler()
    hnd.setFormatter(fmt)
    logger.addHandler(hnd)

#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

tbhome = '%s/torbeam' %os.getenv('SHARES_USER_DIR')
if not os.path.exists(tbhome):
    os.mkdir(tbhome)

locdir = os.path.dirname(os.path.realpath(__file__))

int_in_list = ['ndns', 'nte', 'nmod', 'npow', 'ncd', 'ianexp', 'ncdroutine', \
    'nprofv', 'noout', 'nrela', 'nmaxh', 'nabsroutine', 'nastra', 'nprofcalc']

flt_in_list = ['xrtol', 'xatol', 'xstep']

err_code = { \
    0: 'Normal exit (non-zero absorption, i.e. y(19) < 1)', \
    1: 'No intersection with the plasma (iswtch = 0)', \
    2: 'Plasma crossed, no absorption (Rout < Rmag )', \
    3: 'Plasma intersected, cutoff (Rout > Rmag )', \
    4: 'Integrator failure (istate < 0)', \
    5: 'Unphysical (negative) ne or Te on input', \
    6: 'Magnetic axis not found (powell failure, returns xma= −', \
    7: 'Maximum number of integration steps (ndat) exceeded', \
    8: 'Cutoff encountered at the vacuum-plasma boundary'}


class RUN_TORBEAM:


    def __init__(self, tb_input):

        self.tb_input = tb_input

        logger.info('Initialising TORBEAM')

        nt, self.n_gyro = self.tb_input.pech.shape
        tbwrap.nrho_in = len(self.tb_input.rhop_in)

# Assuming Carbon imputiy. TORBEAM has ADAS data only for Z=5,6,28 

        for key in ('rrect', 'zrect', 'rhop_in'):
            setattr(tbwrap, key, self.tb_input.__dict__[key])

# Dimensions

        tbwrap.mmax = 150
        tbwrap.nmax = tbwrap.mmax
        tbwrap.prdim = 2*tbwrap.mmax + 2*tbwrap.nmax
        tbwrap.maxdim = 1 + tbwrap.mmax + tbwrap.nmax + 4*tbwrap.mmax*tbwrap.nmax
        tbwrap.npnt = 5000
        tbwrap.ndat = 100000
 
# Choose some in GUI

        tbwrap.rhoresult = np.zeros(20, dtype=np.float32)
        tbwrap.prdata    = np.zeros(tbwrap.prdim, dtype=np.float32)
        tbwrap.eqdata    = np.zeros(tbwrap.maxdim, dtype=np.float32)

        tbwrap.nrrect = len(tbwrap.rrect)
        tbwrap.nzrect = len(tbwrap.zrect)


    def call_torbeam(self, jt_tb):


        pech_min = 1e3 # W

# TORBEAm input

        tbwrap.prdata[: 4*tbwrap.nrho_in] = np.hstack(( \
            self.tb_input.rhop_in, 1e-19*self.tb_input.ne_in[jt_tb], \
            self.tb_input.rhop_in, 1e-3 *self.tb_input.te_in[jt_tb] ))

        neq = 1 + tbwrap.nrrect + tbwrap.nzrect + 4*tbwrap.nrrect*tbwrap.nzrect
        tbwrap.eqdata[: neq] = np.hstack(( self.tb_input.psi_sep[jt_tb], \
            self.tb_input.rrect, self.tb_input.zrect, \
            self.tb_input.Br[jt_tb].T.ravel(), \
	    self.tb_input.Bt[jt_tb].T.ravel(), \
            self.tb_input.Bz[jt_tb].T.ravel(), \
            self.tb_input.psi_rect[jt_tb].T.ravel() ))

        tbwrap.prof_out = np.zeros(3*tbwrap.npnt, dtype=np.float32)
        self.power_out = np.zeros((tbwrap.npnt, self.n_gyro), dtype=np.float32)
        self.jeccd_out = np.zeros_like(self.power_out)
        self.pecrh = np.zeros((self.n_gyro), dtype=np.float32)
        self.ieccd = np.zeros_like(self.pecrh)
        self.gyro_on = np.zeros(self.n_gyro, dtype=bool)

        self.beam_pol_c_r = []
        self.beam_pol_c_z = []
        self.beam_pol_u_r = []
        self.beam_pol_u_z = []
        self.beam_pol_l_r = []
        self.beam_pol_l_z = []

        self.beam_hor_c_r = []
        self.beam_hor_c_z = []
        self.beam_hor_u_r = []
        self.beam_hor_u_z = []
        self.beam_hor_l_r = []
        self.beam_hor_l_z = []

        nan_arr = np.nan*np.ones(1)
        tbwrap.nprofvw = self.tb_input.options.intinbeam.nprofv.data

# TORBEAM run

        for jgy in range(self.n_gyro):
            logger.info('Gyrotron %d', jgy+1)
            tbwrap.intinbeam = np.zeros(50, dtype=np.int32)
            tbwrap.intinbeam[0]  = self.tb_input.options.intinbeam.ndns.data
            tbwrap.intinbeam[1]  = self.tb_input.options.intinbeam.nte.data
            tbwrap.intinbeam[2]  = self.tb_input.options.intinbeam.nmod.data
            tbwrap.intinbeam[3]  = self.tb_input.options.intinbeam.npow.data
            tbwrap.intinbeam[4]  = self.tb_input.options.intinbeam.ncd.data
            tbwrap.intinbeam[5]  = self.tb_input.options.intinbeam.ianexp.data
            tbwrap.intinbeam[6]  = self.tb_input.options.intinbeam.ncdroutine.data
            tbwrap.intinbeam[7]  = self.tb_input.options.intinbeam.nprofv.data
            tbwrap.intinbeam[8]  = self.tb_input.options.intinbeam.noout.data
            tbwrap.intinbeam[9]  = self.tb_input.options.intinbeam.nrela.data
            tbwrap.intinbeam[10] = self.tb_input.options.intinbeam.nmaxh.data
            tbwrap.intinbeam[11] = self.tb_input.options.intinbeam.nabsroutine.data
            tbwrap.intinbeam[12] = self.tb_input.options.intinbeam.nastra.data
            tbwrap.intinbeam[13] = self.tb_input.options.intinbeam.nprofcalc.data

            tbwrap.floatinbeam = np.zeros(50, dtype=np.float32)

            tbwrap.floatinbeam[0]  = self.tb_input.freq[jgy]
            tbwrap.floatinbeam[1]  = self.tb_input.phi[  jt_tb, jgy]
            tbwrap.floatinbeam[2]  = self.tb_input.theta[jt_tb, jgy]
            tbwrap.floatinbeam[3]  = 100.*self.tb_input.rr[jgy] # beam launching R
            tbwrap.floatinbeam[5]  = 100.*self.tb_input.zz[jgy] # beam launching Z
            tbwrap.floatinbeam[14] = self.tb_input.options.floatinbeam.xrtol.data
            tbwrap.floatinbeam[15] = self.tb_input.options.floatinbeam.xatol.data
            tbwrap.floatinbeam[16] = self.tb_input.options.floatinbeam.xstep.data
            tbwrap.floatinbeam[19] = 100.*self.tb_input.curv_y[jgy]
            tbwrap.floatinbeam[20] = 100.*self.tb_input.curv_z[jgy]
            tbwrap.floatinbeam[21] = 100.*self.tb_input.width_y[jgy]
            tbwrap.floatinbeam[22] = 100.*self.tb_input.width_z[jgy]
            tbwrap.floatinbeam[23] = 1.      # initial beam power [MW]
            tbwrap.floatinbeam[24] = 100.*self.tb_input.R0    # Major radius [cm]
# Hardcoded values, replace with self.tb_input attributes
            if hasattr(self.tb_input, 'ahor'):
                aminor = self.tb_input.ahor[jt_tb]
            else:
                aminor = self.tb_input.amin[jt_tb]
            aminor *= 100.
            tbwrap.floatinbeam[25] = aminor     # Minor radius [cm]
            tbwrap.floatinbeam[26] = np.abs(self.tb_input.bt[jt_tb])    # |Bt(0)| [T]
            if self.tb_input.psi_axis[jt_tb] < self.tb_input.psi_sep[jt_tb]:
                tbwrap.floatinbeam[33] = 1.
            else:
                tbwrap.floatinbeam[33] = -1.
            tbwrap.floatinbeam[34] = self.tb_input.zef_in[jt_tb, 0] # zef0

            power_gyro = self.tb_input.pech[jt_tb, jgy]

            if power_gyro > pech_min:
                tbwrap.calc_torbeam()
                if tbwrap.rhoresult[19] > 0:
                    logger.error(err_code[tbwrap.rhoresult[19]])
                self.rhop_out, self.power_out[:, jgy], self.jeccd_out[:, jgy] = np.split(tbwrap.prof_out, 3)

                pol_c_r, pol_c_z, pol_u_r, pol_u_z, pol_l_r, pol_l_z = np.split(tbwrap.beam_proj_pol, 6)
                hor_c_r, hor_c_z, hor_u_r, hor_u_z, hor_l_r, hor_l_z = np.split(tbwrap.beam_proj_hor, 6)

                self.gyro_on[jgy] = True
                self.power_out[:, jgy] *= power_gyro
                self.jeccd_out[:, jgy] *= power_gyro
                self.pecrh[jgy] = tbwrap.rhoresult[13]*power_gyro
                self.ieccd[jgy] = tbwrap.rhoresult[12]*power_gyro
            else:
                self.rhop_out = np.linspace(0, 1, tbwrap.npnt)
                pol_c_r = nan_arr
                pol_c_z = nan_arr
                pol_u_r = nan_arr
                pol_u_z = nan_arr
                pol_l_r = nan_arr
                pol_l_z = nan_arr

                hor_c_r = nan_arr
                hor_c_z = nan_arr
                hor_u_r = nan_arr
                hor_u_z = nan_arr
                hor_l_r = nan_arr
                hor_l_z = nan_arr
# Need a list, as dimensions are not constant for each gyrotron nor time point
            self.beam_pol_c_r.append(pol_c_r.copy())
            self.beam_pol_c_z.append(pol_c_z.copy())
            self.beam_pol_u_r.append(pol_u_r.copy())
            self.beam_pol_u_z.append(pol_u_z.copy())
            self.beam_pol_l_r.append(pol_l_r.copy())
            self.beam_pol_l_z.append(pol_l_z.copy())

            self.beam_hor_c_r.append(hor_c_r.copy())
            self.beam_hor_c_z.append(hor_c_z.copy())
            self.beam_hor_u_r.append(hor_u_r.copy())
            self.beam_hor_u_z.append(hor_u_z.copy())
            self.beam_hor_l_r.append(hor_l_r.copy())
            self.beam_hor_l_z.append(hor_l_z.copy())


class TORBEAM:


    def __init__(self, tb_in, gtbm):

# Get input from shotfile
        logger.info('Getting TORBEAM input from shotfile')

        self.time = np.atleast_1d(tb_in.profiles.time)

        self.INPUT = type('', (), {})()

        self.shot = tb_in.gpar.shot

        self.INPUT.options = type('', (), {})()
        self.INPUT.options.intinbeam = type('', (), {})()
        self.INPUT.options.floatinbeam = type('', (), {})()
        for key in int_in_list:
            self.INPUT.options.intinbeam.__dict__[key] = type('', (), {})()
            self.INPUT.options.intinbeam.__dict__[key].data = np.int32(getattr(gtbm, key))
        for key in flt_in_list:
            self.INPUT.options.floatinbeam.__dict__[key] = type('', (), {})()
            self.INPUT.options.floatinbeam.__dict__[key].data = np.float32(getattr(gtbm, key))
        
        self.INPUT.time  = self.time
        self.INPUT.shot  = self.shot
        self.INPUT.rhop_in = tb_in.profiles.rho
        self.INPUT.rrect   = tb_in.equ.Rmesh
        self.INPUT.zrect   = tb_in.equ.Zmesh
        self.INPUT.aplasma = tb_in.ion_d['Amain']
        self.INPUT.zplasma = tb_in.ion_d['Zmain']
        self.INPUT.aimp    = np.atleast_1d(12.)
        self.INPUT.zimp    = np.atleast_1d(6.)
        self.INPUT.R0      = tb_in.equ.R0
        self.INPUT.freq    = tb_in.ech.freq
        self.INPUT.curv_y  = tb_in.ech.curv_y
        self.INPUT.curv_z  = tb_in.ech.curv_z
        self.INPUT.width_y = tb_in.ech.width_y
        self.INPUT.width_z = tb_in.ech.width_z
        self.INPUT.rr      = tb_in.ech.R # git, remove 100
        self.INPUT.zz      = tb_in.ech.z

# Time dependent

        self.INPUT.psi_axis = tb_in.equ.psi0
        self.INPUT.psi_sep  = tb_in.equ.psix
        self.INPUT.rmag     = tb_in.equ.Rmag
        self.INPUT.zmag     = tb_in.equ.Zmag
        if hasattr(tb_in.equ, 'ahor'):
            self.INPUT.ahor = tb_in.equ.ahor
        if hasattr(tb_in.equ, 'amin'):
            self.INPUT.amin = tb_in.equ.amin
        self.INPUT.pech     = tb_in.ech.power_ds
        self.INPUT.theta    = tb_in.ech.theta_ds
        self.INPUT.phi      = tb_in.ech.phi_ds
        self.INPUT.bt       = tb_in.sig.bt.data_ds

# Dimensions

        nt      = len(self.time)
        nrho_in = len(self.INPUT.rhop_in)
        n_gyro  = len(self.INPUT.freq)
        nr      = len(self.INPUT.rrect)
        nz      = len(self.INPUT.zrect)
        nr_eq   = tb_in.equ.q.shape[-1]
        nrho_out = int(gtbm.nrho_out_tb)

# Profiles
        self.INPUT.ne_in  = tb_in.profiles.Ne.yfit_rhop
        self.INPUT.te_in  = tb_in.profiles.Te.yfit_rhop
        self.INPUT.ti_in  = tb_in.profiles.Ti.yfit_rhop
        self.INPUT.zef_in = np.repeat(tb_in.zef.data_ds, nrho_in).reshape((nt, nrho_in))
        self.INPUT.omg_in = tb_in.profiles.Angf.yfit_rhop

# Time-dependent equ input

        self.INPUT.q = tb_in.equ.q
        self.INPUT.psi_rect = np.transpose(tb_in.equ.pfm, axes=(2, 0, 1))
        self.INPUT.Br = tb_in.equ.Br
        self.INPUT.Bz = tb_in.equ.Bz
        self.INPUT.Bt = tb_in.equ.Bt

        self.INPUT.cdf_dims = {'time': nt, 'rhop_in': nrho_in, 'n_ech': n_gyro, \
            'rrect': nr, 'zrect': nz}

# Put attributes to plasma and geomertry parameters

        self.INPUT = code_io.tb_input_attr(self.INPUT)

# Dump input NetCDF file for debugging (GUI option)

        if gtbm.dump_tb_cdf:
            code_io.tb2cdf(self, f_cdf='%s/ncdf/%d_in.cdf' %(tbhome, self.INPUT.shot))

# Initialise Fortran-TORBEAM

        rtb = RUN_TORBEAM(self.INPUT)

# Class for time-dependent output

        self.OUTPUT = type('', (), {})()
        self.OUTPUT.time = self.time
        self.OUTPUT.shot = self.shot

        self.OUTPUT.power_out = np.zeros((nt, nrho_out, n_gyro), dtype=np.float32)
        self.OUTPUT.jeccd_out = np.zeros_like(self.OUTPUT.power_out)
        self.OUTPUT.pecrh = np.zeros((nt, n_gyro), dtype=np.float32)
        self.OUTPUT.gyro_on = np.zeros((nt, n_gyro), dtype=bool)
        self.OUTPUT.ieccd = np.zeros_like(self.OUTPUT.pecrh)
        self.OUTPUT.rhop_out = np.linspace(0, 1, nrho_out, dtype=np.float32)

        self.OUTPUT.beam_pol_c_r = []
        self.OUTPUT.beam_pol_c_z = []
        self.OUTPUT.beam_pol_u_r = []
        self.OUTPUT.beam_pol_u_z = []
        self.OUTPUT.beam_pol_l_r = []
        self.OUTPUT.beam_pol_l_z = []

        self.OUTPUT.beam_hor_c_r = [] # list(t) of lists(jgy) of np.arrays(space)
        self.OUTPUT.beam_hor_c_z = []
        self.OUTPUT.beam_hor_u_r = []
        self.OUTPUT.beam_hor_u_z = []
        self.OUTPUT.beam_hor_l_r = []
        self.OUTPUT.beam_hor_l_z = []

        for jt, tim in enumerate(self.time):

# Execute TORBEAM

            logger.info('Calling TORBEAM, time %5.3f', tim)

            rtb.call_torbeam(jt)

# Collect output in time-dependent arrays
            
            pinterpol = interp1d(rtb.rhop_out, rtb.power_out, axis=0, fill_value='extrapolate')
            jinterpol = interp1d(rtb.rhop_out, rtb.jeccd_out, axis=0, fill_value='extrapolate')
            self.OUTPUT.power_out[jt] = pinterpol(self.OUTPUT.rhop_out)
            self.OUTPUT.jeccd_out[jt] = jinterpol(self.OUTPUT.rhop_out)
            self.OUTPUT.pecrh[jt] = rtb.pecrh
            self.OUTPUT.ieccd[jt] = rtb.ieccd
            self.OUTPUT.gyro_on[jt] = rtb.gyro_on

# Beam projections
                
            self.OUTPUT.beam_pol_c_r.append(rtb.beam_pol_c_r)
            self.OUTPUT.beam_pol_c_z.append(rtb.beam_pol_c_z)
            self.OUTPUT.beam_pol_u_r.append(rtb.beam_pol_u_r)
            self.OUTPUT.beam_pol_u_z.append(rtb.beam_pol_u_z)
            self.OUTPUT.beam_pol_l_r.append(rtb.beam_pol_l_r)
            self.OUTPUT.beam_pol_l_z.append(rtb.beam_pol_l_z)

            self.OUTPUT.beam_hor_c_r.append(rtb.beam_hor_c_r)
            self.OUTPUT.beam_hor_c_z.append(rtb.beam_hor_c_z)
            self.OUTPUT.beam_hor_u_r.append(rtb.beam_hor_u_r)
            self.OUTPUT.beam_hor_u_z.append(rtb.beam_hor_u_z)
            self.OUTPUT.beam_hor_l_r.append(rtb.beam_hor_l_r)
            self.OUTPUT.beam_hor_l_z.append(rtb.beam_hor_l_z)

        self.OUTPUT.cdf_dims = {'rhop_out': nrho_out}
        self.OUTPUT = code_io.tb_output_attr(self.OUTPUT)
