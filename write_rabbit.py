import os, logging
import numpy as np
from scipy.interpolate import interp1d
from aug_sfutils import time_slice
from trview.rw_for import wr_for
from trview import config, rabbit_pos

logger = logging.getLogger('trview.write_rabbit')
logger.setLevel(logging.INFO)

rbhome = '%s/rabbit' %os.getenv('SHARES_USER_DIR')

def write_rabbit(rb_in):

    logger.info('Writing RABBIT input')
    if not rb_in.nbi.status:
        logger.error('No NIS found, no RABBIT run possible')
        return
    rbpath = '%s/%d' %(rbhome, rb_in.equ.shot)
    os.system('rm -r %s' %rbpath)
    os.system('mkdir -p %s' %rbpath)
    write_timetraces(rb_in.profiles, rb_in.equ, rb_in.zef, rb_in.nbi)
    write_optionsnml(rb_in.nbi, rb_in.ion_d)
    write_beamsdat(rb_in.nbi)
    write_equdat(rb_in.equ, rb_in.profiles.time)
    write_lim(rb_in.equ.shot)


def write_timetraces(profiles, equ, Zeff, nbi):
    
    '''
    writes the timetraces.dat file
    the time and rho parameters are read by gpar from the GUI
    the profiles are taken from profiles.py: in trgui, call as self.profiles
    the equilibrium is called in trgui as self.equ
    The Zeff is called in trgui as self.sig.zef
    The NBI power is called in trgui as self.nbi and it's used for nbi.data_ds and nbi.time_ds
    '''

    #time array:
    output_time = profiles.time
    nt = output_time.size
    
    #radial array
    output_rho = profiles.rho #check if this is correct
    nrho = output_rho.size

    output_Te = 1.e-3*profiles.Te.yfit_rhot # Needs to be in keV
    output_Ti = 1.e-3*profiles.Ti.yfit_rhot # Needs to be in keV
    output_ne = 1.e-6*profiles.Ne.yfit_rhot # Needs to be in cm^-3
    output_vrot = profiles.Angf.yfit_rhot

    mapped_Zeff, _, _ = time_slice.map2tgrid(output_time, Zeff.time, Zeff.data)
    output_zeff = np.repeat(mapped_Zeff, nrho).reshape(nt, nrho)

    pnbi, _, _ = time_slice.map2tgrid(output_time, nbi.time, nbi.power)

    out_dir = '%s/%d' %(rbhome, equ.shot)
    
    filename = '%s/timetraces.dat' %out_dir

    logger.info('Writing RABBIT time traces and kinetic profiles %s', filename)

    with open(filename, 'w') as f:
    
        f.write('%12i\n' %nt)
        f.write('%12i\n' %nrho)
    
        f.write('rho_tor' + '\n') #just write rho_tor

# time and radial grids
        f.write(wr_for(output_time, fmt = '%13.5f')) # time grid
        f.write(wr_for(output_rho, fmt = '%16.8f', n_lin = 5)) # radial grid
    
# profiles
# Te
        for i in range(nrho):
            f.write(wr_for(output_Te[: , i], fmt = '%16.8f', n_lin = 5))

# Ti
        for i in range(nrho):
            f.write(wr_for(output_Ti[: , i], fmt = '%16.7f', n_lin = 5))
    
# ne
        for i in range(nrho):
            f.write(wr_for(output_ne[: , i], fmt = '%16.7e', n_lin = 5))
    
# ang. velocity   
        for i in range(nrho):
            f.write(wr_for(output_vrot[: , i], fmt = '%16.3f', n_lin = 5))
        
# Zeff
        for i in range(nrho):
            f.write(wr_for(output_zeff[: , i], fmt = '%16.7f', n_lin = 5))
            
# P_NBI
        for i in range(pnbi.shape[1]):
            f.write(wr_for(pnbi[:, i], fmt = '%13.5e', n_lin = 6))


def write_beamsdat(nbi):
    
    '''
    Writes the beams.dat file to out_dir
    '''
    
    nshot = nbi.shot
    nv = 3
    out_dir = '%s/%d/' %(rbhome, nshot)
    filename = out_dir + 'beams.dat' #file to create and save in out_dir

# Namelist

    n_box1 = nbi.n_box[0]
    n_box2 = nbi.n_box[1]
    
    n_nbi = np.sum(nbi.n_box) #number of sources
    
    einj_ev = 1e3*nbi.einj_kev #energies in eV
    einj_ev[~nbi.nbi_on] = 0.1

    xyz_src_m, xyz_vec = rabbit_pos.rabbit_pos(nbi)

    start_pos = ''
    for jnb in range(n_nbi):
        start_pos += '%16.7f%16.7f%16.7f\n' %(xyz_src_m[jnb, 0], xyz_src_m[jnb, 1], xyz_src_m[jnb, 2])
  
    beam_unit_vec = ''
    for jnb in range(n_nbi):
        beam_unit_vec += '%16.7f%16.7f%16.7f\n' %(xyz_vec[jnb, 0], xyz_vec[jnb, 1], xyz_vec[jnb, 2])

    #width_pol:
        
    width_pol_op = 3
     
    if width_pol_op == 1:
         
         width_pol = \
'''       0.0000000     0.010999778       0.0000000
       0.0000000     0.010999778       0.0000000
       0.0000000    0.0099998331       0.0000000
       0.0000000     0.010999778       0.0000000
       0.0000000     0.010999778       0.0000000
       0.0000000     0.010999778       0.0000000
       0.0000000     0.010999778       0.0000000
       0.0000000     0.010999778       0.0000000'''
 
            
    elif width_pol_op == 2:
         
         width_pol = \
'''       0.0000000     0.0098729131       0.0000000
       0.0000000     0.0098729131       0.0000000
       0.0000000     0.0098729131       0.0000000
       0.0000000     0.0098729131       0.0000000
       0.0000000     0.0116500000       0.0000000
       0.0000000     0.0116500000       0.0000000
       0.0000000     0.0116500000       0.0000000
       0.0000000     0.0116500000       0.0000000'''
        
         
    elif width_pol_op == 3:
        
        width_pol = \
'''       0.0000000    0.0098999999       0.0000000
       0.0000000    0.0098999999       0.0000000
       0.0000000    0.0098999999       0.0000000
       0.0000000    0.0098999999       0.0000000
       0.0000000     0.011650000       0.0000000
       0.0000000     0.011000000       0.0000000
       0.0000000     0.011000000       0.0000000
       0.0000000     0.011650000       0.0000000'''

    width_pol += '\n'
    
# injection energy
    
    inj_energy = wr_for(einj_ev, fmt = '%16.3f', n_lin = 1)
    
# particle fraction

    part_frac = ''
    
    for jnb in range(n_nbi):
        part_frac += '%16.7f%16.7f%16.7f\n' %(nbi.part_mix[jnb, 0], nbi.part_mix[jnb, 1], nbi.part_mix[jnb, 2])
    
    a_beam = wr_for(nbi.abeam, fmt = '%16.7f', n_lin = 1)
    
# write the data into the file:
    
    with open(filename, 'w') as f:
        
        f.write('# no. of sources:' + '\n')
        f.write('%12i\n' %n_nbi)
        
        f.write('# nv:' + '\n')
        f.write('%8i\n' %nv)
        
        f.write('# start pos: [m]' + '\n')
        f.write(start_pos)
        
        f.write('# beam unit vector:' + '\n')
        f.write(beam_unit_vec)

        f.write('# beam-width-polynomial coefficients:' + '\n')
        f.write(width_pol)

        f.write('# Injection energy [eV]:' + '\n')
        f.write(inj_energy)
        
        
        f.write('# Particle fraction of full/half/third energy:' + '\n')
        f.write(part_frac)
        
        
        f.write('# A beam [u]' + '\n')
        f.write(a_beam + '\n')
        

def write_optionsnml(nbi, ion_directory):
    
    '''
    Writes the options.nml file to out_dir
    '''
    
    nshot = nbi.shot
    
    out_dir = '%s/%d' %(rbhome, nshot)
    filename = '%s/options.nml' %out_dir

    out_set = '   it_orbout = -1'

    phys_nums = \
"""
&physics
   jumpcor = 2
   table_path   = '%s'
   limiter_file = '%s'
   Rlim = 2.20 !AUG defaults, overridden if limiter_file exists
   zlim = 0.20
   Rmax = 2.26 !AUG defaults
   Rmin = 1.08
/

&numerics
   norbits=20
   orbit_dt_fac = 1.56
   distfun_nv = 200
   distfun_vmax = 4.5e6
/
""" %(config.rabbitTable, config.rabbitLimiter)

    with open(filename, 'w') as f:

        #species
        f.write('&species' + '\n')
        f.write('   Aimp=' + '%.2f' %ion_directory['Aimp'] + '\n')
        f.write('   Zimp=' + '%.2f' %ion_directory['Zimp'] + '\n')
        f.write('   Aplasma=' + '%.2f' %ion_directory['Amain'] + '\n')
        f.write('   Zplasma=' + '%.2f' %ion_directory['Zmain'] + '\n' +'/' +'\n')

        f.write('&output_settings' + '\n')
        f.write(out_set + '\n')
        f.write('/' + '\n')

        f.write(phys_nums)
        
#--------------------------write equ_XX.dat files--------------------------------

def getTime(time_traces, time_equ):
    
    '''
    in trgui.py call time_traces as self.profiles.time and time_equ as self.equ.time
    '''
    
    ind = []
    for tim in time_traces:
        jclose = np.argmin(np.abs(time_equ - tim))
        ind.append(jclose)
        
    return ind

def write_equdat(equ, time_traces):
    
    '''
    Writes the equ_XX.dat files for each time step and save them to out_dir
    '''
    
    nshot = equ.shot

    out_dir = '%s/%d/equ' %(rbhome, nshot) #files are written to this directory
    os.system('mkdir -p %s' %out_dir)

    logger.info('Settings: ')
    logger.info('Shot Number = %d'  , nshot)
    logger.info('Starting Time = %.3f', equ.time[0])
    logger.info('Final Time = %.3f'   , equ.time[-1])

# Retrieve Variables
    logger.info('Reading equilibrium variables')

# AUGD: COCOS17, RABBIT: COCOS5
    equ.to_coco(cocos_out=5)
    indt = getTime(time_traces, equ.time)

    nt = len(indt)
    nR = equ.Rmesh.size
    nZ = equ.Zmesh.size
    logger.debug('%d, %d, %d', nt, nR, nZ)

    output_time = equ.time[indt]
    output_Rmag = equ.Rmag[indt]
    output_Zmag = equ.Zmag[indt]
    output_psi   = equ.pfm[:, :, indt].transpose([2, 0, 1])
    output_psi1d = equ.pfl[indt, :]
    output_psia  = equ.psi0[indt] #psi axis (?)
    output_psis  = equ.psix[indt] #psi separatrix (?)
    output_Q     = equ.q[indt, :]
    #output_dpres = equ.dpres[indt, :]
    output_F     = 2.e-7*equ.jpol[indt, :]
    output_Vol   = equ.vol [indt, :]
    output_Area  = equ.area[indt, :]

    output_rhot1d = equ.rhotor_trapz[indt, :]

    '''
    Email from Markus:
        
    Ok, so this should be fine. I think the issue is the rhotor 2D array, 
    if I remember correctly it is set to zero in your scripts...
    There are two solutions: Either you set it to meaningful values, 
    or I extend the executable to do the calculation itself...

    But I think it is better to do it correctly in the input file.
    Since rhotor is only defined inside of the separatrix, 
    I recommend to use rho_tor inside, and rho_pol outside...

    The IDL code for this goes something like this:
      rhopol2d = sqrt( (equ.psi-equ.psi_axis)/(equ.psi_sep-equ.psi_axis) )
     iii = where(rhopol2d lt 1)
     equ.rhotor = rhopol2d
     rhopol1d = sqrt( (equ.psi1d-equ.psi1d[0])/(equ.psi1d[-1]-equ.psi1d[0]) )
     spl = spl_init(rhopol1d, equ.rhotor1d)
     equ.rhotor[iii] = spl_interp(rhopol1d, equ.rhotor1d, spl, rhopol2d[iii])

    So it is doing a spline interpolation of the 1D profiles to get the 2D values of rhotor (i.e. on the R,z grid)
    
    '''
    
    #calculate rhot matrix correctly: it has to have a shape of (nt, nR, nZ)
    output_rhot = np.zeros_like(output_psi) #auxiliary matrix of (nt, nR, nZ) of zeros
    
    rhopol_aux = np.sqrt((output_psi - output_psia[:, None, None])/(output_psis[:, None, None] - output_psia[:, None, None]))

    rhopol2d = np.sqrt((output_psi - np.repeat(output_psia, nR*nZ).reshape(nt,nR,nZ))/np.repeat(output_psis - output_psia, nR*nZ).reshape(nt, nR, nZ))
 
    output_rhot[np.where(rhopol2d >= 1)] = rhopol2d[np.where(rhopol2d >= 1)] #fill with rhopol outside the separatrix

    for jt in range(nt):
        rhopol1d_aux = np.sqrt((output_psi1d[jt, :] - output_psi1d[jt, 0])/(output_psi1d[jt, -1] - output_psi1d[jt, 0]))
# approximate the relation between rhot(rhop), then evaluate it in rhop2d_i > 1        
        try:
            from scipy.interpolate import CubicSpline
            finterp = CubicSpline(rhopol1d_aux, output_rhot1d[jt], bc_type='natural') 
        except:
            logger.warning('time frame %d, t=%.3f . Problems with CubicSpline', jt, equ.time[jt])
            finterp = interp1d(rhopol1d_aux, output_rhot1d[jt], kind = 'cubic', fill_value = 'extrapolate')

        inside_sep = np.where(rhopol2d[jt] < 1)
        output_rhot[jt][inside_sep] = finterp(rhopol2d[jt][inside_sep])

    sI = np.sign(np.average(equ.ipipsi))

# Create equilibrium files

    logger.info('Writing equilibrium files')

    for jt in range(nt):
        filename = '%s/equ_%d.dat' %(out_dir, jt+1)
        if jt in (0, 1, nt-2, nt-1):
            logger.info('Writing : %s for time point %.3f', filename, output_time[jt])
        if jt == 3:
            logger.info('...')
        with open(filename, 'w') as f:
            f.write('%12i\n' %nR)
            f.write('%12i\n' %nZ)

            f.write(wr_for(equ.Rmesh, fmt = '%13.6f'))
            f.write(wr_for(equ.Zmesh, fmt = '%13.6f'))

            for j in range(nZ):
                f.write(wr_for(output_psi[jt, : , j], fmt = '%13.6f'))
            for j in range(nZ):
                f.write(wr_for(output_rhot[jt, : , j], fmt = '%13.6f'))

            f.write('%12i\n' %output_rhot1d.shape[1] )

            f.write(wr_for(output_psi1d [jt, :], fmt = '%13.6f'))
            f.write(wr_for(output_Vol   [jt, :], fmt = '%13.6f'))
            f.write(wr_for(output_Area  [jt, :], fmt = '%13.6f'))
            f.write(wr_for(output_rhot1d[jt, :], fmt = '%13.6f'))
            f.write(wr_for(output_Q     [jt, :], fmt = '%13.6f'))
            f.write(wr_for(output_F     [jt, :], fmt = '%13.6f'))

            f.write('%13.6g' %output_psis[jt])
            f.write('%13.6g' %output_psia[jt])
            f.write('%12i'   %int(sI) )
            f.write('%13.6g' %output_Rmag[jt])
            f.write('%13.6g' %output_Zmag[jt])
            f.write( '\n' )
#            f.write('%13.4f' %output_time[jt])
#            f.write('\n')


def write_lim(nshot):

    lim = \
"""           2          31
      1.03500      0.00000
      1.05000     0.250000
      1.09000     0.500000
      1.14000     0.700000
      1.23500     0.965000
      1.34000      1.10000
      1.43000      1.15000
      1.50500      1.19000
      1.63000      1.15000
      1.77000      1.08500
      1.97000     0.780000
      2.11000     0.540000
      2.17000     0.390000
      2.20500     0.210000
      2.21000      0.00000
      2.17500    -0.150000
      2.13000    -0.280000
      2.01000    -0.500000
      1.89000    -0.680000
      1.70500    -0.860000
      1.64500    -0.966000
      1.58000     -1.20800
      1.45800     -1.06000
      1.32500     -1.06000
      1.23500     -1.12600
      1.28600    -0.976000
      1.28000    -0.892000
      1.24500    -0.820000
      1.12500    -0.634000
      1.06000    -0.300000
      1.03500      0.00000
"""

    out_dir = '%s/%d' %(rbhome, nshot)
    filename = '%s/limiter_aug_git.dat' %out_dir

    with open(filename, 'w') as f:
        f.write(lim)
