# Description: function for mapping of the irregulary sampled noise radial profiles
#              the regular grid. mapping is done by inversion of the grid projection 
#              matrix. Inversion is regularized by tikhonov regularization with 
#              an assumption of the minimal diffusion in a cylindrical geometry 
#		('power' density is expected as constant guess).  Moreover by 'zero_edge' 
#		can by profile forced to by zero at the edge,
#              Robusthes against outliers is improved by two pass calculation and 
    #               error correction by  Winsorizing estimator. 
# Author: Tomas Odstrcil
# mail:tomas.odstrcil@gmail.com

import os, sys, time, logging
import numpy as np
import scipy.sparse as sp
from scipy.sparse import linalg as splinalg
from numpy.fft import fft, fftfreq, ifft
from scipy.interpolate import interp1d

logger = logging.getLogger('trview.grid_map')


class map2grid():


    def __init__(self, R, T, Y, dy, nr_new, nt_new, eta=0, name=''):


        logger.info('grid map: %s', name)

        self.nr_new = nr_new
        self.nt_new = nt_new
        self.eta = eta
        self.name = name

        self.dtype='double'
        self.R = R.ravel()
        self.T = T.ravel()

        self.norm = np.linalg.norm(Y/np.amax(Y)) / np.sqrt(np.size(Y)) * np.amax(Y)

#        Y  /= self.norm    
#        dy /= self.norm
        
        self.t_min = np.amin(self.T)
        self.t_max = np.amax(self.T)
        self.r_min = 0.
        self.r_max = max(min(np.amax(self.R), 1.1), 1.01)

        self.n_points = np.size(Y)
        self.Y  =  Y.ravel()/self.norm
        self.dy = dy.ravel()/self.norm
        self.dy[self.dy<=0] = np.infty

        self.g = np.zeros((nt_new, nr_new)) * np.nan
        self.missing_data = np.ones(self.nt_new, dtype=bool)
        self.K      = np.zeros((nr_new, nt_new))
        self.Kerr_u = np.zeros((nr_new, nt_new)) * np.nan
        self.Kerr_d = np.zeros((nr_new, nt_new)) * np.nan

        self.retro_f = np.zeros_like(self.Y) * np.nan
        
        self.chi2 = np.nan
        self.g1   = np.nan
        
        self.corrected = False
        self.prepared  = False

        rgrid = np.linspace(self.r_min, self.r_max, self.nr_new)
        tgrid = np.linspace(self.t_min, self.t_max, self.nt_new)
        self.r_new, self.t_new = np.meshgrid(rgrid, tgrid)


    def FourierMatrix(self, n):
        #create matrix of the unitary fourier operator

        A = np.identity(n)
        F = fft(A)
        F /= np.sqrt(n/2)
        
        return F


    def G_filter(self, f, f0, s):
        #filter  of the higher orders of signal with frequency f0, width s

        f_resp = np.ones_like(f)
        if f0 is not None and f0 != 0:
            for i in range(1, int(np.amax(f)/f0) + 2):
                f_resp *= 1 - np.exp(-(f - i*f0*sign(f))**2/(2*(s/i**0.0)**2))/np.sqrt(1 + ((i - 1)/5.)**2)
            
        #supress also higher harmonics 
        #higher  orders has a narrower window to force a higher stability 

        return f_resp


    def FilteredDerivative(self, n, f0, dt, missing_data):
       
        n_ext = 2**int(np.ceil(np.log2(n*2)))
        f = fftfreq(n_ext, d=dt)
        
        #speed of the nonperiodic changes
        s = 0.05
        GF = self.G_filter(f, f0, s)
        FT_D = 4*np.sin(np.pi*f*dt)**2

        F = self.FourierMatrix(n_ext)#fast
        
        spectrum = -FT_D*GF*np.sqrt(n_ext/2)

        D = ifft((spectrum[:, np.newaxis]*F).T)        
        D = np.real(D[:n, :n])  #use only a cut (D was 2x larger in order to avoid a edge effects)
         #imag part are only numerical errors
     
        D[missing_data, :] = (np.eye(n, k=1) - np.eye(n))[missing_data, :]

        D -= np.diag(np.sum(D, axis=1))
        D[abs(D) < 1e-5] = 0

        return D


    def _sparseness(self, x):
        """Hoyer's measure of sparsity for a vector"""

        sqrt_n = np.sqrt(len(x))
        return (sqrt_n - np.linalg.norm(x, 1) / np.linalg.norm(x)) / (sqrt_n - 1)


    def PrepareCalculation(self, eta=0, perturb_freq=0, zero_edge=False):

        logger.info('\nPrepareCalculation')
        
        #create a geometry matrix 
        dt = (self.t_max - self.t_min)/(self.nt_new - 1)
        dr = (self.r_max - self.r_min)/(self.nr_new - 1)
        it = (self.T - self.t_min)/dt
        it[it < 0] = 0
        it[it > self.nt_new-1] = self.nt_new - 1
        
        ir = (self.R - self.r_min)/dr
        ir[ir < 0] = 0
        ir[ir > self.nr_new-1] = self.nr_new - 1

        weight  = np.empty((self.n_points, 4))
        index_p = np.tile(np.arange(self.n_points, dtype=int), (4, 1)).T
        index_t = np.empty((self.n_points, 4), dtype=int)
        index_r = np.empty((self.n_points, 4), dtype=int)

        index_t[:, 0] = np.floor(it)
        index_t[:, 1] = np.ceil(it)
        index_t[:, 2] = index_t[:, 0]
        index_t[:, 3] = index_t[:, 1]

        index_r[:, 0] = np.floor(ir)
        index_r[:, 1] = index_r[:, 0]
        index_r[:, 2] = np.ceil(ir)
        index_r[:, 3] = index_r[:, 2]

        weight[:, 0] = (np.floor(it) + 1 - it)*(np.floor(ir) + 1 - ir)
        weight[:, 1] = (it - np.floor(it))*(np.floor(ir) + 1 - ir)
        weight[:, 2] = (np.floor(it) + 1 - it)*(ir - np.floor(ir))
        weight[:, 3] = (it - np.floor(it))*(ir - np.floor(ir))
        
        #regions which are not covered by any measurements
        self.missing_data[index_t[:, 0]] = False
        self.missing_data[index_t[:, 1]] = False

        weight  = weight.ravel()
        index_p = index_p.ravel()
        index_r = index_r.ravel()
        index_t = index_t.ravel()
        index_rt = index_r*self.nt_new + index_t
        npix = self.nr_new*self.nt_new
        
        self.vogel = sp.csc_matrix((np.copy(weight), (np.copy(index_p), np.copy(index_rt))), \
            shape=(self.n_points, npix), dtype=self.dtype)
    
        #prepare smoothing matrix         
        #calculate (1+c)*d/dr(1/r*dF/dr) + (1-c)*d^2F/dt^2
        ct = np.exp(-eta)   #(0, 1)
        cx = np.exp( eta)
        rvec = np.linspace(self.r_min, self.r_max, self.nr_new)
        rvec = (rvec[1:]+rvec[:-1])/2

        fun_r2 = (rvec*np.arctan(np.pi*rvec) - np.log((np.pi*rvec)**2+1)/(2*np.pi))/rvec  #alternative
        self.ifun = 1/np.sqrt(fun_r2) + 1/(fun_r2)/4
        self.ifun = 1/fun_r2
        
        #time domain
        #dt = (t_max-t_min)/nt_new
        FD = self.FilteredDerivative(self.nt_new, perturb_freq, dt, self.missing_data)
        FD = sp.csc_matrix(FD, dtype=self.dtype)
    
        weight = np.r_[self.ifun.max(), self.ifun]*ct
    
        #radial domain
        DR = np.zeros((3, self.nr_new), dtype=self.dtype)
        
        DR[0, :-2]  =  self.ifun[:-1]
        DR[1, 1:-1] = -self.ifun[:-1] - self.ifun[1:]
        DR[2, 2:]   =  self.ifun[1: ]
        if zero_edge:
            DR[1, -1] = 10

        DR = sp.spdiags(DR*cx, (-1, 0, 1), self.nr_new, self.nr_new)
    
        #effective way to calculate (DR+DT).T*(DR+DT)
        W = sp.diags(weight, 0)
        
        DDT  = sp.kron(W**2, FD.T*FD)
        DRDT = sp.kron(DR.T*W, FD) + sp.kron(W*DR, FD.T)
        DRDR = sp.kron(DR.T*DR, np.eye(self.nt_new), format='csc')
        
        DD = DDT + DRDT
        del DRDT, DDT
        DD = DD.tocsc()
        
        self.DD = DD + DRDR
        self.DT = sp.kron(sp.diags(weight, 0), FD)
        self.DR = sp.kron(DR, np.eye(self.nt_new))
        del DRDR, FD

        self.prepared = True
            
        
    def PreCalculate(self):
        
        if not self.prepared:
            self.PrepareCalculation()
        
        logger.debug('First Pass')
        #first pass - decrease the weight of the outliers
        npix = self.nr_new*self.nt_new

        f = np.copy(self.Y)
        lam = 5

        f /= self.dy
        
        self.V = sp.spdiags(1./self.dy, 0, self.n_points, self.n_points, format='csr')*self.vogel

        self.VV = self.V.T*self.V
            
        try:
            self.Factor = splinalg.factorized(self.VV + 10**lam * self.DD)   
            g = np.squeeze(self.Factor(self.V.T*f))
        except :
            lam += 2
            self.Factor = splinalg.factorized(self.VV + 10**lam * self.DD)
            g = np.squeeze(self.Factor(self.V.T*f))
       
        #make a more robust fit in one iteration
        dist = (self.V*g - self.Y/self.dy)  
        
        #Winsorizing M estimator
        dy_corr = ((dist/3)**2 + 1)**(0.25)  #BUG oprava

        self.f = f/dy_corr
        self.V = sp.spdiags(1/dy_corr, 0, self.n_points, self.n_points, format='csr')*self.V
        self.VV = self.V.T*self.V
        
        self.corrected = True


    def Calculate(self, lam):

        if not self.corrected:
            self.PreCalculate()

        #noise vector for estimation of uncertainty
        n_noise_vec = 20
        noise = np.random.randn(self.n_points, n_noise_vec)
        noise /= ((noise/3)**2 + 1)**(0.25)
        
        try:
            self.Factor.cholesky_inplace(self.VV + 10**lam*self.DD)
        except:
            self.Factor = splinalg.factorized(self.VV + 10**lam*self.DD)

        g = np.squeeze(self.Factor(self.V.T*self.f))
        fg1 = np.log10(np.linalg.norm((self.vogel*g - self.Y)/self.dy)**2/sum(np.isfinite(self.dy)))

        map_noise = np.empty((self.nr_new*self.nt_new, n_noise_vec))

        #BUG
        for i in range(n_noise_vec):
            map_noise[:, i] = np.squeeze(self.Factor(self.V.T*noise[:, i]))

        self.chi2 = 10**fg1
        self.g1 = lam
        logger.debug('\nchi2: %.2f reg:%.2f' %(self.chi2, self.g1))

        map_noise = np.reshape(map_noise, (self.nr_new, self.nt_new, n_noise_vec)).T*self.norm*np.sqrt(self.chi2)
    
        retro_f = self.vogel*g
    
        self.retro_f = np.reshape(retro_f, np.shape(self.T))*self.norm    
        self.g       = np.reshape(g, (self.nr_new, self.nt_new)).T*self.norm
        
        if any(self.missing_data):
           self.g=interp1d(self.t_new[~self.missing_data, 0], self.g[~self.missing_data, :], 
                    axis=0, kind='linear', bounds_error=False, fill_value=np.nan)(self.t_new[:, 0])

        K = -np.diff(self.g)*self.ifun[np.newaxis, :]*self.nr_new

        Kerr = np.std(np.diff(map_noise)*self.ifun[np.newaxis, np.newaxis, :], axis=0)*self.nr_new

        K_noise = np.diff(map_noise)*self.ifun[None, None, :]*self.nr_new
        K_noise.sort(0)
        K_noise-= np.mean(K_noise, axis=0)[np.newaxis, ...]

        Kerr_u = np.abs(np.mean(K_noise[n_noise_vec//2:, ...], axis=0))/0.76 #84% quantil
        Kerr_d = np.abs(np.mean(K_noise[:n_noise_vec//2, ...], axis=0))/0.76 #16% quantil

        K0 = np.zeros((1, self.nt_new))
        self.K, self.Kerr_u, self.Kerr_d = \
            np.r_[K.T, K0*np.nan], np.r_[Kerr_u.T, K0], np.r_[Kerr_d.T, K0]
        self.Kerr = np.r_[Kerr.T, K0]


if __name__ == '__main__':

    xexp = np.array([ \
       [0.02613815, 0.02831471, 0.07475223, 0.15851343, 0.20333683,
        0.2483771 , 0.34301147, 0.38284367, 0.4259361 , 0.46657872,
        0.50643128, 0.54616326, 0.58822131, 0.66128641, 0.69763637,
        0.73712957, 0.77163374, 0.80492008, 0.84105086, 0.87215638,
        0.90401107, 0.93555695, 0.96310282, 0.99516529, 1.00832903,
        1.01443207, 1.01936615, 1.0225172 , 1.02879715, 1.03122127,
        1.0360992 , 1.03880501, 1.04340196, 1.04572964, 1.05314028,
        1.05320656, 1.05659831, 1.0606904 , 1.06445825, 1.06901133,
        1.07200205, 1.07580602, 1.07840037, 1.08285308, 1.08707023],
       [0.02594825, 0.03348567, 0.07798146, 0.16136427, 0.20622554,
        0.25136578, 0.34622929, 0.38616705, 0.42936388, 0.47011322,
        0.51005828, 0.54988402, 0.59203601, 0.66525155, 0.7016781 ,
        0.74125379, 0.77582771, 0.80918932, 0.84542447, 0.87661105,
        0.90855438, 0.94032609, 0.96825808, 1.00072849, 1.00795805,
        1.01405823, 1.01901114, 1.02215707, 1.02846134, 1.03088248,
        1.03578579, 1.0384922 , 1.04311216, 1.04543674, 1.05287814,
        1.05294836, 1.05634212, 1.06045341, 1.06422341, 1.06879532,
        1.07178652, 1.07560563, 1.07819831, 1.08266652, 1.0868839 ],
       [0.02378045, 0.03445116, 0.06639244, 0.15014778, 0.19525009,
        0.24060224, 0.33598328, 0.37613583, 0.41958657, 0.46055427,
        0.50073701, 0.54079288, 0.58319992, 0.65689701, 0.69357437,
        0.73344946, 0.76831383, 0.80198121, 0.83856803, 0.87012362,
        0.90250129, 0.93466449, 0.96284944, 0.99560672, 0.99753428,
        1.00370431, 1.00871682, 1.0119065 , 1.01829636, 1.02075708,
        1.02573264, 1.02848637, 1.0331775 , 1.03554165, 1.04309547,
        1.04316652, 1.04661155, 1.05078351, 1.05460727, 1.05924487,
        1.06227612, 1.06614888, 1.0687741 , 1.07330287, 1.07757103],
       [0.02665367, 0.03036049, 0.07365358, 0.1573097 , 0.20241667,
        0.24771374, 0.34307596, 0.38319761, 0.42663264, 0.46754134,
        0.5076834 , 0.54767233, 0.5899992 , 0.66352856, 0.70008779,
        0.73981172, 0.77452457, 0.80801225, 0.84434617, 0.87564701,
        0.90768814, 0.93953556, 0.96753252, 0.99807942, 1.00014997,
        1.00431967, 1.00938058, 1.01261437, 1.01907456, 1.02157366,
        1.02660477, 1.02940345, 1.03414893, 1.03655422, 1.04419768,
        1.04426599, 1.04776263, 1.05197978, 1.05585873, 1.06054699,
        1.0636214 , 1.06753552, 1.07019877, 1.07477641, 1.07910168],
       [0.02343244, 0.0467472 , 0.05365399, 0.13502517, 0.17936602,
        0.22417054, 0.31846792, 0.35828474, 0.40141517, 0.4421939 ,
        0.48223329, 0.52223599, 0.56466925, 0.63861406, 0.67553365,
        0.71575528, 0.75099266, 0.78508919, 0.82223111, 0.85433054,
        0.88735998, 0.92008257, 0.94856232, 0.98172605, 0.99028343,
        0.99658287, 1.00170732, 1.00497556, 1.01152456, 1.01405549,
        1.01916468, 1.02200341, 1.02682483, 1.0292604 , 1.03702211,
        1.03709435, 1.04063511, 1.04491961, 1.04884493, 1.05360544,
        1.05671394, 1.0606879 , 1.06337714, 1.06802142, 1.07239079],
       [0.01886628, 0.04340296, 0.05394003, 0.13672191, 0.1815961 ,
        0.22671959, 0.32178843, 0.36184078, 0.40523753, 0.44615301,
        0.48633951, 0.52641571, 0.56888592, 0.64279747, 0.67962128,
        0.71969926, 0.75478166, 0.78868794, 0.82555598, 0.85741436,
        0.89015418, 0.92259711, 0.95088035, 0.9838624 , 0.98746628,
        0.99378437, 0.99887818, 1.0021764 , 1.00870097, 1.01126206,
        1.01634419, 1.01921439, 1.0240159 , 1.0264883 , 1.03423035,
        1.03429115, 1.0378592 , 1.04212177, 1.04607427, 1.05081439,
        1.05394447, 1.05790031, 1.06061208, 1.06523943, 1.06963241]])

    tvec = np.array( \
      [3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.00000024, 3.00000024, 3.00000024, 3.00000024, 3.00000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.05000024, 3.05000024, 3.05000024, 3.05000024, 3.05000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.10000024, 3.10000024, 3.10000024, 3.10000024, 3.10000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.15000024, 3.15000024, 3.15000024, 3.15000024, 3.15000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.20000024, 3.20000024, 3.20000024, 3.20000024, 3.20000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024,
       3.25000024, 3.25000024, 3.25000024, 3.25000024, 3.25000024])

    yexp = np.array([
       [6.77515527e+03, 5.84646387e+03, 6.58492578e+03, 5.51977637e+03,
        4.84544727e+03, 4.68130957e+03, 3.60344116e+03, 3.09464233e+03,
        3.04515137e+03, 2.67596826e+03, 2.60937061e+03, 2.36469385e+03,
        2.24040869e+03, 2.04131824e+03, 1.88199280e+03, 1.79167761e+03,
        1.68589404e+03, 1.61752222e+03, 1.57553247e+03, 1.44871887e+03,
        1.32889465e+03, 1.28144592e+03, 1.22912000e+03, 1.28212756e+03,
        2.46758863e-01,         np.nan,         np.nan,         np.nan,
                np.nan,         np.nan,         np.nan,         np.nan,
                np.nan,         np.nan,         np.nan,         np.nan,
        1.70114563e+02, 5.27724624e-02, 8.02574692e+01, 5.00650330e+01,
                np.nan,         np.nan,         np.nan,         np.nan,
                np.nan],
       [6.67220166e+03, 6.66262354e+03, 6.89806738e+03, 5.78296826e+03,
        5.22541309e+03, 4.66700684e+03, 3.66824341e+03, 3.28460352e+03,
        3.09835156e+03, 2.75850244e+03, 2.69449365e+03, 2.41621826e+03,
        2.28118604e+03, 2.11190942e+03, 1.93769165e+03, 1.85075854e+03,
        1.70705786e+03, 1.66394666e+03, 1.50347498e+03, 1.43506079e+03,
        1.32627820e+03, 1.21832727e+03, 1.16075781e+03, 1.23878333e+03,
        1.13801018e+02,         np.nan, 2.84301117e+02,         np.nan,
                np.nan, 2.79570129e+02, 1.10638863e+02,         np.nan,
                np.nan,         np.nan, 1.64821396e+01, 1.94792877e+02,
        8.08802338e+01, 7.58089066e+01, 1.53061401e+02, 1.60801300e+02,
        1.09093254e+02,         np.nan,         np.nan,         np.nan,
                np.nan],
       [7.02882666e+03, 7.21617773e+03, 7.48199219e+03, 6.48650146e+03,
        5.54736377e+03, 5.22698193e+03, 3.82286035e+03, 3.43302612e+03,
        3.16757910e+03, 2.90228027e+03, 2.71415063e+03, 2.45750171e+03,
        2.27590894e+03, 2.08022632e+03, 1.92550439e+03, 1.83084424e+03,
        1.70230798e+03, 1.59586609e+03, 1.50158850e+03, 1.42074377e+03,
        1.29638013e+03, 1.21603210e+03, 1.15263928e+03, 1.05593176e+03,
        2.95421600e+02, 2.39724304e+02, 1.40050522e+02,         np.nan,
        2.26624329e+02, 2.53867401e+02, 2.06163986e+02,         np.nan,
        1.95339783e+02, 4.64627991e+01, 1.85491882e+02, 1.32099045e+02,
        1.51972748e+02, 9.40957489e+01, 9.90029526e+01, 1.05931908e+02,
        1.64232559e+02, 1.29431244e+02,         np.nan,         np.nan,
                np.nan],
       [7.02294043e+03, 6.83986523e+03, 7.38761816e+03, 6.50884863e+03,
        5.68891113e+03, 5.15932617e+03, 3.71523901e+03, 3.36398145e+03,
        3.16482031e+03, 2.91604102e+03, 2.79809766e+03, 2.49805591e+03,
        2.39427905e+03, 2.18354712e+03, 2.02914819e+03, 1.87755908e+03,
        1.75570874e+03, 1.71045850e+03, 1.59024231e+03, 1.48613086e+03,
        1.38554956e+03, 1.30610425e+03, 1.19363330e+03, 1.61128510e+02,
        1.08368823e+03, 1.99634705e+02,         np.nan, 1.94557816e+02,
                np.nan, 2.19807037e+02,         np.nan, 1.24266869e+02,
        2.78605003e+01, 1.75933167e+02, 4.62999649e+01, 1.52128494e+02,
        1.70129639e+02, 4.88435516e+01, 6.97053604e+01, 8.02070694e+01,
        7.37862549e+01, 1.24025398e+02, 8.43922272e+01,         np.nan,
                np.nan],
       [7.02500488e+03, 7.34447119e+03, 7.06675781e+03, 6.52354150e+03,
        5.73140771e+03, 5.26547705e+03, 3.95946289e+03, 3.49450244e+03,
        3.20918701e+03, 2.94169849e+03, 2.94509961e+03, 2.58334326e+03,
        2.44839966e+03, 2.26006421e+03, 2.10993213e+03, 1.92263025e+03,
        1.81551929e+03, 1.74306604e+03, 1.66498169e+03, 1.55703613e+03,
        1.41798657e+03, 1.38264905e+03, 1.32259253e+03, 1.30579907e+03,
                np.nan,         np.nan,         np.nan,         np.nan,
                np.nan, 2.89331268e+02, 3.34231201e+02, 2.15581284e+02,
        2.14918961e+02, 2.79433613e+01, 1.63109802e+02, 1.81769821e+02,
        2.12061890e+02, 1.17316689e+02, 1.45318878e+02, 8.56968689e+01,
        2.50045670e+02, 8.76986084e+01, 1.28367950e+02, 1.54771332e+02,
        3.18573341e+01],
       [6.76987402e+03, 6.69432666e+03, 6.99266553e+03, 6.55820020e+03,
        5.64856152e+03, 5.38820557e+03, 4.12288867e+03, 3.55222119e+03,
        3.35006177e+03, 3.15262817e+03, 2.95283984e+03, 2.65522070e+03,
        2.51257104e+03, 2.31169214e+03, 2.13618213e+03, 1.97485156e+03,
        1.85527075e+03, 1.77119019e+03, 1.63660767e+03, 1.59981433e+03,
        1.43989954e+03, 1.35936621e+03, 1.24815454e+03, 1.20383716e+03,
                np.nan,         np.nan, 1.81569534e+02, 4.29031769e+02,
        1.90100449e+02, 2.09420288e+02, 3.04401489e+02,         np.nan,
        3.14351635e+01,         np.nan, 1.48484680e+02, 2.01908630e+02,
        1.58801102e+02, 1.41694275e+02, 1.64180542e+02, 1.51253479e+02,
        1.78643051e+02, 1.76801575e+02, 4.72931709e+01, 2.18987549e+02,
                np.nan]])

    yerr = np.array([ \
       [3.56216644e+02, 2.74616486e+02, 3.01008209e+02, 2.13045746e+02,
        1.74618805e+02, 1.43907349e+02, 1.02544334e+02, 1.20714890e+02,
        8.71819763e+01, 9.89424133e+01, 1.03811012e+02, 7.18103333e+01,
        6.38912392e+01, 6.28537636e+01, 4.44711609e+01, 3.89341469e+01,
        4.23198090e+01, 4.07726898e+01, 4.66978416e+01, 3.73095779e+01,
        3.52688599e+01, 3.73095779e+01, 6.30143547e+01, 1.29394073e+02,
        1.10513706e+01, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        5.12500343e+01, 4.70825081e+01, 2.61863747e+01, 2.01893349e+01,
        6.10269653e+05, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05],
       [3.31519104e+02, 2.98938080e+02, 2.97766937e+02, 2.38115280e+02,
        1.79298676e+02, 1.55507843e+02, 1.07458351e+02, 1.27348572e+02,
        1.05540367e+02, 9.51498413e+01, 1.19172951e+02, 7.71809158e+01,
        7.37574310e+01, 6.79982758e+01, 5.14849167e+01, 4.65293884e+01,
        4.95024948e+01, 4.17319450e+01, 3.95327950e+01, 3.89032669e+01,
        3.49542007e+01, 7.17314301e+01, 6.61270370e+01, 1.24859879e+02,
        7.96826172e+01, 6.10269653e+05, 7.70762711e+01, 6.10269653e+05,
        6.10269653e+05, 6.42629547e+01, 5.49334145e+01, 6.10269653e+05,
        6.10269653e+05, 6.10269653e+05, 2.44398651e+01, 6.32466354e+01,
        4.34798508e+01, 4.19943390e+01, 5.63344955e+01, 4.75340042e+01,
        4.68196945e+01, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05],
       [3.03117432e+02, 3.77685425e+02, 3.24647430e+02, 2.69420380e+02,
        2.01856735e+02, 1.76187988e+02, 1.17202393e+02, 1.41968857e+02,
        1.06944489e+02, 1.04197052e+02, 1.28460220e+02, 8.18183594e+01,
        7.31609421e+01, 6.53637619e+01, 5.05878143e+01, 4.35907898e+01,
        4.63224678e+01, 4.13176003e+01, 4.21141014e+01, 3.88898354e+01,
        3.33077278e+01, 3.71440926e+01, 5.05862465e+01, 8.13511047e+01,
        6.76108704e+01, 6.33901901e+01, 7.56828995e+01, 6.10269653e+05,
        7.83359146e+01, 6.44340668e+01, 4.78936119e+01, 6.10269653e+05,
        7.72800217e+01, 7.35095901e+01, 5.64958038e+01, 3.14030609e+01,
        5.61001244e+01, 4.97222786e+01, 3.51975784e+01, 4.13153343e+01,
        6.67941208e+01, 4.85838966e+01, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05],
       [3.27258514e+02, 2.95693695e+02, 2.88796814e+02, 2.55393280e+02,
        2.13490845e+02, 1.71194260e+02, 1.24316193e+02, 1.33202423e+02,
        1.06924995e+02, 1.06103088e+02, 1.40704788e+02, 9.40087280e+01,
        7.15973511e+01, 7.07209854e+01, 5.18091583e+01, 4.69508018e+01,
        5.03662834e+01, 4.69060097e+01, 4.59805717e+01, 4.14463234e+01,
        3.65438995e+01, 4.05666008e+01, 5.27712326e+01, 8.46879044e+01,
        8.86244888e+01, 9.07742844e+01, 6.10269653e+05, 6.72265091e+01,
        6.10269653e+05, 6.44742966e+01, 6.10269653e+05, 5.72350311e+01,
        2.58879490e+01, 8.08095322e+01, 3.86002579e+01, 3.83383408e+01,
        4.30758324e+01, 5.21119843e+01, 4.39119110e+01, 2.81601963e+01,
        7.06015854e+01, 7.62545547e+01, 2.75681782e+01, 6.10269653e+05,
        6.10269653e+05],
       [2.85598785e+02, 3.33258728e+02, 2.80712097e+02, 2.77394836e+02,
        2.05544235e+02, 1.65923126e+02, 1.21452271e+02, 1.46015320e+02,
        1.13927078e+02, 1.08385460e+02, 1.36875900e+02, 8.57543564e+01,
        8.10285873e+01, 6.65325928e+01, 5.36610947e+01, 4.95303917e+01,
        4.82497025e+01, 4.59657516e+01, 4.89238052e+01, 4.34468079e+01,
        3.60878181e+01, 4.02433624e+01, 5.58328247e+01, 9.99682236e+01,
        6.10269653e+05, 6.10269653e+05, 6.10269653e+05, 6.10269653e+05,
        6.10269653e+05, 7.48933258e+01, 6.56627579e+01, 7.13872299e+01,
        9.13952026e+01, 6.65858612e+01, 4.28168716e+01, 5.23832207e+01,
        5.50491219e+01, 4.93664169e+01, 4.19164963e+01, 3.48858986e+01,
        7.88220291e+01, 4.44882355e+01, 2.83867664e+01, 4.84202499e+01,
        4.22969398e+01],
       [2.82299927e+02, 3.24664551e+02, 2.80935059e+02, 2.73199554e+02,
        2.22980133e+02, 1.83071304e+02, 1.37543121e+02, 1.51597549e+02,
        1.17113136e+02, 1.26489342e+02, 1.41604507e+02, 9.16101227e+01,
        9.08832169e+01, 7.46000214e+01, 5.70433846e+01, 4.80311279e+01,
        5.36407356e+01, 4.87373238e+01, 4.90421562e+01, 4.30819397e+01,
        3.67044945e+01, 3.70242844e+01, 4.73468018e+01, 7.84939423e+01,
        6.10269653e+05, 6.10269653e+05, 5.89567146e+01, 8.40873566e+01,
        3.82506943e+01, 5.90891342e+01, 7.10567245e+01, 6.10269653e+05,
        9.65363503e+00, 6.10269653e+05, 6.28859901e+01, 5.16398697e+01,
        5.45869331e+01, 6.00451889e+01, 5.29138718e+01, 5.04527168e+01,
        5.65899467e+01, 5.87707634e+01, 2.15409985e+01, 7.16840515e+01,
        6.10269653e+05]])

    nr_fit = 51
    nt = 5
    eta = 0
    lam = 4.
    edge = False
    f0 = 0.
    invalid = ~np.isfinite(yexp)| ~np.isfinite(yerr)
    yerr[invalid] = np.inf
    yexp[invalid] = -1

    m2g = map2grid(xexp, tvec, yexp, yerr, nr_fit, nt)
    m2g.PrepareCalculation(eta=-eta, perturb_freq=f0, zero_edge=edge)
    m2g.Calculate(lam)

