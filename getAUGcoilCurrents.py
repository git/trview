import os
import numpy as np
from aug_sfutils import SFREAD

signals = ['IOH', 'dIOH2s', 'dIOH2u', 'IV1o', 'IV1u', 'IV2o', 'IV2u', 'IV3o', 'ICoIo', 'ICoIu', 'Ipslon', 'Ipslun']


class coils_currents:


    def __init__(self, nshot, diag='DDS', dt=0.005, t_offset=-9.5):

        self.shot = nshot
        self.coil_curr = {}
        self.get_currents(diag=diag)
        if t_offset is not None:
            self.subtractOffset(t_offset, dt)
        self.astraCurrents()

    def get_currents(self, diag='DDS'):

        mag = SFREAD(self.shot, diag)
        if mag.status:
            self.timeCurr = mag.gettimebase('IV1o')
            for sfname in signals:
                curr = mag.getobject(sfname)
                if curr is not None:
                    self.coil_curr[sfname] = curr.copy()
            self.status = True
        else:
            self.status = False        

    def subtractOffset(self, t_offset, dt):

        if self.status:
            (indCurrOff, ) = np.where(abs(self.timeCurr - t_offset) < dt)
            (indPslOff , ) = np.where(abs(self.timePsl  - t_offset) < dt)
            for lbl, val in self.coil_curr.items():
                val -= np.mean(val[indCurrOff])

    def astraCurrents(self):

        if self.status:
            nt = len(self.timeCurr)
            self.astra_curr = np.zeros((nt, 12), dtype=np.float64)
            self.astra_curr[:, 0]  = self.coil_curr['IOH']
            self.astra_curr[:, 1]  = self.coil_curr['IOH'] + self.coil_curr['dIOH2s']
            self.astra_curr[:, 2]  = self.coil_curr['IOH'] + self.coil_curr['dIOH2s'] + self.coil_curr['dIOH2u']
            self.astra_curr[:, 3]  = self.coil_curr['IV1o']
            self.astra_curr[:, 4]  = self.coil_curr['IV1u']
            self.astra_curr[:, 5]  = self.coil_curr['IV2o']
            self.astra_curr[:, 6]  = self.coil_curr['IV2u']
            self.astra_curr[:, 7]  = self.coil_curr['IV3o']
            self.astra_curr[:, 8]  = self.coil_curr['ICoIo']
            self.astra_curr[:, 9]  = self.coil_curr['ICoIu']
            if 'Ipslon' in self.coil_curr.keys():
                self.astra_curr[:, 10] = self.coil_curr['Ipslon']
            if 'Ipslun' in self.coil_curr.keys():
                self.astra_curr[:, 11] = self.coil_curr['Ipslun']
            self.astra_curr *= 1.e-3
