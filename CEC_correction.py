import os, logging
from scipy.interpolate import interp1d 
from scipy.optimize import fmin_powell
from scipy.linalg import lstsq
import numpy as np
from multiprocessing import Pool, cpu_count

logger = logging.getLogger('trview.cec_corr')
logger.setLevel(logging.DEBUG)


def L1_alternative(P, q):
# Solve ||P*x - q||_1 without linear programing 
    x0, _, _, _ = lstsq(P, q)

    optim = lambda x: sum(abs(np.dot(P, x) - q))
    
    x = fmin_powell(optim, x0,  ftol=0.1, maxiter=1e3, maxfun=1e4, full_output=0, disp=0, retall=0) 
    
    return x, optim(x) 


def decomposeMulti_inner(XAind):
    X = XAind[0]
    A = XAind[1]
    ind_bad = XAind[2]
    ind = ~ind_bad
    if sum(ind) <= np.size(A, 1):
        return np.ones(np.size(A, 1))*np.nan
    q = X[ind]
    P = A[ind, :]
    c, r = L1_alternative(P, q)
    return np.asarray(c)


def decomposeMulti(A, X, ind_bad):

    pool = Pool(cpu_count())
    out = pool.map(decomposeMulti_inner,[(X[i, :], A, ind_bad[i, :]) for i in range(np.size(X, 0))])
    pool.close()
    pool.join()
    B = np.vstack(out).T

    return B

def correction(Te, rho, Bt, R, name):

    logger.debug('Start')
    Te_ = np.copy(Te)
    rho_ = np.copy(rho)

    name = str(name)

    R = np.atleast_2d(R)
    r = np.arange(60)

    for j in range(np.size(R, 0)):

        wrongind = (R[j, :] == 0)
        if all(wrongind):
            R[j, :] = np.linspace(1, 2, 60)
        else:
            R[j, wrongind] = np.interp(r[wrongind], r[~wrongind], 
                R[j, ~wrongind], left=R[j, 0], right=R[j, -1])

    ind_sort = np.argsort(np.median(abs(R - 1.65), axis=0))
    ind_bad = (np.isnan(Te))

    R = R[:, ind_sort]
    Te =  Te[:, ind_sort] 
    if rho is not None:
        rho = rho[:, ind_sort]
    inv_order = np.argsort(ind_sort)

    path = os.path.dirname(os.path.abspath(__file__))
    if abs(Bt) < 2.4:
        A = np.loadtxt(path + '/A_low.dat')
    if abs(Bt) > 2.4:
        A = np.loadtxt(path + '/A_high.dat')

    ind_bad[Te <= 20] = True

    B = decomposeMulti(A, Te, ind_bad)
#    B = decompose(A, Te, ind_bad)

    Te_corr = np.dot(A, B).T
    Te_corr[ind_bad] = np.nan
    logger.debug('End')

    return Te_corr[:, inv_order]
