import os, logging
import numpy as np
from multiprocessing import Pool, cpu_count
import aug_sfutils as sf
from trview import descur2d, mom2rz

logger = logging.getLogger('trview.fit_sep')


class geofit:


    def __init__(self, equ, nmom=6, n_the=51, rho_sep=0.999, noELMs=False):

        descu = descur2d.scrunch

        self.rho_sep = rho_sep
        self.shot = equ.shot
        self.time = equ.time
        self.exp  = equ.exp
        self.diag = equ.diag
        self.ed   = equ.ed

        if hasattr(equ, 'lpfp'): # from shotfile
            self.rscat, self.zscat = sf.rho2rz(equ, self.rho_sep)
        else:                    # from IMAS
            self.rscat = equ.sep.rscat
            self.zscat = equ.sep.zscat

        nt_in = len(equ.time)
        self.error = np.zeros(nt_in, dtype=np.int32)
        if noELMs:
            ind_elm = sf.time_no_elm.time_no_elm(equ.shot, equ.time)
        else:
            ind_elm = []
        self.error[ind_elm] = 1
        for jt in range(nt_in):
            if (max(self.rscat[jt][0]) > 2.2):
                self.error[jt] = 2
            elif (min(self.zscat[jt][0]) < -1.1):
                self.error[jt] = 3
            elif (max(self.zscat[jt][0]) > 1.2):
                self.error[jt] = 4

        logger.info('Fitting separatrix')

        timeout_pool = 90
        pool = Pool(cpu_count())
        out = pool.map_async(descu, [(self.rscat[jt][0], self.zscat[jt][0], nmom, equ.time[jt], self.error[jt]) for jt in range(nt_in)]).get(timeout_pool)
        pool.close()
        pool.join()

        logger.info('Done fitting separatrix')

        self.moments = np.array(out)
        logger.debug(self.moments.shape)

# Discard "bad" time points

        for jt in range(nt_in):
            if self.error[jt] == 0 and np.max(self.moments[jt, :, 0]) <= 0:
                self.error[jt] = 5

        self.rfit, self.zfit = mom2rz.mom2rz( \
            self.moments[:, :, 0], self.moments[:, :, 1], \
            self.moments[:, :, 2], self.moments[:, :, 3], \
            nthe=n_the, endpoint=True)
        for jt in range(nt_in):
            if min(self.zfit[jt]) < -1.1:
                self.error[jt] = 6
                self.rfit[jt, :] = 0
                self.zfit[jt, :] = 0
        ind_bad = (self.error > 0)
        self.moments[ind_bad, :, :] = 0
        self.ind_ok = ~ind_bad
