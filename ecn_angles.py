import os, logging
import numpy as np
import aug_sfutils as sf
from trview import libech_wrapper

logger = logging.getLogger('trview.ecn_angles')


gy_name = ['ECRH1_1', 'ECRH1_2', 'ECRH1_3', 'ECRH1_4', \
          'ECRH2_1', 'ECRH2_2', 'ECRH2_3', 'ECRH2_4']


def ecn_angles(nshot, alpha, beta, avail, t_ds, n_gy=[4, 4], tbeg=0., tend=10.):

    if nshot < 23187: # Before ECRH2 installed
        return None

    ecn = sf.SFREAD(nshot, 'ECN')
    if not ecn.status:
        return None

    ngy = np.sum(n_gy)

    ech_obj = libech_wrapper.libECRH_wrapper(nshot)
    if not hasattr(ech_obj, '_date'):
        return None # ECN not existing

# Check spindel position for drifts

    try:
        sucomdat = ecn.getobject('D-SIMPRE')
        if sucomdat is None:
            logger.warning('Falling back to SUCOMDAT')
            sucomdat = ecn.getobject('SUCOMDAT')
        logger.debug('SUCOMDAT shape', sucomdat.shape)
        sucomdat = np.float32(sucomdat)/10.
        flag_sucomdat = True
    except:
        flag_sucomdat = False

    if flag_sucomdat:
        a_ref    = np.zeros(ngy)
        beta_ref = np.zeros(ngy)
        beta_ref[n_gy[0]: ngy] = sucomdat[[26, 71, 116, 161]]
        a_ref   [n_gy[0]: ngy] = sucomdat[[27, 72, 117, 162]]

        for jgy in range(n_gy[0], ngy):
            if not avail[jgy]: continue
            if np.abs(a_ref[jgy] - alpha[jgy]) > 0.1:
                logger.warning('Discrepancy in spindle position alpha')
                logger.warning('ECS, SUCOMDAT, %s %8.4f %8.4f' %(gy_name[jgy], a_ref[jgy], alpha[jgy]))
            if np.abs(beta_ref[jgy] - beta[jgy]) > 0.1:
                logger.warning('Discrepancy in spindle position beta')
                logger.warning('ECS, SUCOMDAT, %s %8.4f %8.4f' %(gy_name[jgy], beta_ref[jgy], beta[jgy]))

    tim = ecn.getobject('T-Base')
    logger.debug(tim[:40])
    tbeg = max(tbeg, 1e-6)
    (index, ) = np.where((tim >= tbeg) & (tim <= tend))
    t_ecn = tim[index]
    nt_ecn = len(t_ecn)
    alpha_t = np.zeros((nt_ecn, ngy))
    for jgy in range(n_gy[0], ngy):
        N_2 = jgy - n_gy[0] + 1
        alpha_t[:, jgy] = ecn.getobject('G%dPOL' %N_2, cal=True)[index] * 10.0
        offset = alpha[jgy] - alpha_t[0, jgy]
        logger.info('Offset correction: %8.4f' %offset)
        alpha_t[:, jgy] += offset  # Offset correction from ECS PSet

# Downsample to 1ms. t_ecn is not regular!

    alpha_ds, _, _ = sf.time_slice.map2tgrid(t_ds, t_ecn, alpha_t)
    the_ds   = np.zeros_like(alpha_ds)
    phi_ds   = np.zeros_like(alpha_ds)

    nt_ds = len(t_ds)
    logger.info('#nt: %d, after downsampling: %d' %(nt_ecn, nt_ds))

    for jgy in range(n_gy[0], ngy):
        N_2 = jgy - n_gy[0] + 1
        gynum = 200 + N_2
        if(not avail[jgy]):
            logger.warning('Gyrotron %s not active' %gy_name[jgy])
        else:
            for jt in range(nt_ds):
                errpr, the_ds[jt, jgy], phi_ds[jt, jgy] = \
                    ech_obj.setval2tp(gynum, alpha_ds[jt, jgy], beta[jgy])

    the_ds *= -1 #TORBEAM
    phi_ds *= -1

    return the_ds, phi_ds
