import os, logging
import numpy as np
from trview import ufiles

logger = logging.getLogger('trview.write_u')
#logger.setLevel(logging.DEBUG)
logger.setLevel(logging.INFO)

lbl_d = { \
    'ip'    : ['SM', 'CUR'   , 'Plasma Current'.ljust(20) + 'Amps'], \
    'bt'    : ['S' , 'RBZ'   , 'Rp*Bt'.ljust(20)          + 'T.cm'], \
    'uloop' : ['U' , 'VSF'   , 'Vloop'.ljust(20)          + 'V'   ], \
    'prad'  : ['P' , 'RAD'   , 'Prad'.ljust(20)           + 'W'   ], \
    'wmhd'  : ['W' , 'MHD'   , 'Plasma energy'.ljust(20)  + 'J'   ], \
    'betpol': ['B' , 'BETPOL', 'Beta poloidal'.ljust(20)          ]}

tlbl = 'Time'.ljust(20) + 'Seconds'


def as_ubnd(fit_sep, udir=None):

    if hasattr(fit_sep, 'time'):
        ind_t = fit_sep.ind_ok
        t_u  = fit_sep.time[fit_sep.ind_ok]
    else: #eqdsk
        ind_t = [0]
        t_u = np.array([1.])
    Rbnd = np.atleast_2d(fit_sep.rfit)[ind_t, :-1]
    Zbnd = np.atleast_2d(fit_sep.zfit)[ind_t, :-1]

    nt, n_the = Rbnd.shape

    uf = ufiles.UFILE()

    uf.pre = ''
    uf.ext = 'bnd.txt_r'
    uf.shot = fit_sep.shot
    uf.X = {'label': tlbl  , 'data': t_u}
    uf.Y = {'label': 'POSITION', 'data': 1 + np.arange(n_the)}
    uf.f = {'label': 'R BOUNDARY    [M]', 'data': Rbnd}
    uf.write(udir=udir)

    uf.ext = 'bnd.txt_z'
    uf.f = {'label': 'Z BOUNDARY    [M]', 'data': Zbnd}
    uf.write(udir=udir)
    

def tr_u_geo(fit_sep):

    t_u = fit_sep.time[fit_sep.ind_ok]
    moments_u = 100.*fit_sep.moments[fit_sep.ind_ok, :, :]

    nt, mom_order, mom_type = moments_u.shape

    uf = ufiles.UFILE()

    uf.pre = 'M'
    uf.ext = 'MRY'
    uf.shot = fit_sep.shot
    uf.X = {'label': tlbl, 'data': t_u}
    uf.Y = {'label': 'MOMENT INDEX', 'data': 1 + np.arange(mom_order)}
    uf.Z = {'label': 'MOMENT TYPE' , 'data': 1 + np.arange(mom_type) }
    uf.f = {'label': 'FOURIER MOMENTS'.ljust(20) + 'CM', 'data': moments_u}
    uf.comment = 'exp=%s\n diag=%s\n ed=%d' %(fit_sep.exp, fit_sep.diag, fit_sep.ed)

    uf.write()


def write_q(equ, udir=None):

    time = equ.time[0]

    uf = ufiles.UFILE()

    uf.pre = 'Q'
    uf.ext = 'QPR'
    uf.shot = equ.shot
    uf.X = {'label': 'rho_tor'      , 'data': equ.rho_tor_n[0, :] }
    uf.f = {'label': 'Safety factor', 'data': np.abs(equ.q[0, :]) }

    uf.comment  = 'time=%8.4f s\n' %equ.time[0]
    uf.comment += ' exp=%s\n diag=%s\n ed=%d' %(equ.exp, equ.diag, equ.ed)
    uf.write(udir=udir)


def write_q2d(equ, udir=None):

    rholbl = 'rho_tor'
    rho = equ.rho_tor_n[0, :-1]
    q   = np.atleast_2d(np.abs(equ.q[0, :-1]))
    time = np.atleast_1d(equ.time[0])

    uf = ufiles.UFILE()

    uf.pre = 'Q'
    uf.ext = 'QPR'
    uf.shot = equ.shot
    uf.X = {'label': tlbl   , 'data': time}
    uf.Y = {'label': 'rho_tor', 'data': rho}
    uf.f = {'label': 'Safety factor', 'data': q }

    uf.comment = ' exp=%s\n diag=%s\n ed=%d' %(equ.exp, equ.diag, equ.ed)
    uf.write(udir=udir)


def write_u1d(plasma, code='tr', udir=None):

    R0_cm = 100*plasma.equ.R0
    if not hasattr(plasma.sig, 'ip'):
        logger.error('Missing current, no 1D u-files written')
        return

    for key, data in plasma.sig.__dict__.items():
        if key in lbl_d.keys():
            str_u = lbl_d[key]
            pre  = str_u[0]
            ext  = str_u[1]
            dlbl = str_u[2]
            if hasattr(data, 'data_ds'):
                t_u = plasma.sig.t_ds
                d_u = data.data_ds.copy()
            else:
                t_u = data.time
                d_u = data.data.copy()

            sign = np.sign(np.average(d_u))

            if code == 'as':
                if key == 'bt':
                    d_u *= -1
                    pre = 'MAG'
                    ext = 'BTOR'
                    dlbl = 'Btor'.ljust(29) + 'T'
                elif key == 'ip':
                    d_u *= 1e-6
                    pre = 'MAG'
                    ext = 'IPL'
                    dlbl = 'Plasma Current'.ljust(20) + 'MA'
                elif key == 'wmhd':
                    pass
                else:
                    continue
            else:
                if key == 'bt':
                    d_u *= -R0_cm # major radius [cm]

            if code == 'tr' and key in ('ip', 'bt'):
                d_u = np.abs(d_u)

            d_u[0] = np.abs(d_u[0])
            comm = ' AUGD:%s:%d' %(data.diagsig, data.edition)
            uf = ufiles.UFILE()
            uf.pre = pre
            uf.ext = ext
            uf.shot = data.shot
            uf.X = {'label': tlbl, 'data': t_u}
            uf.f = {'label': dlbl, 'data': d_u} 
            uf.comment = comm
            uf.write(udir=udir)
            uf.comment += '\n Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
            uf.average()
            uf.write(udir=udir) # Time averaged

    uf.pre = 'Z'
    uf.ext = 'ZEF'
    dlbl  = 'Effective charge'.ljust(20)
    uf.comment = ''
    t_u = plasma.zef.t_ds
    d_u = plasma.zef.data_ds
    uf.X = {'label': tlbl, 'data': t_u}
    uf.f = {'label': dlbl, 'data': d_u} 
    uf.write(udir=udir)
    uf.comment += '\n Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
    uf.average()
    uf.write(udir=udir) # Time averaged

# NBI

    if hasattr(plasma.nbi, 'power_ds'):

        nsrc = plasma.nbi.power_ds.shape[1]

        xlbl = 'Channel number'
        dlbl = 'NBI power'.ljust(20) + 'W'
        x_nbi = 1. + np.arange(nsrc)
        t_u = plasma.nbi.t_ds
        d_u = plasma.nbi.power_ds
        if np.max(plasma.nbi.power_ds) > plasma.nbi.nbi_min:
            uf = ufiles.UFILE()
            uf.pre = 'P'
            uf.ext = 'NBI'
            uf.shot = plasma.nbi.shot
            uf.X = {'label': tlbl, 'data': t_u}
            uf.Y = {'label': xlbl, 'data': x_nbi}
            uf.f = {'label': dlbl, 'data': d_u}
            uf.write(udir=udir)
            uf.comment = ' Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
            uf.average()
            uf.write(udir=udir) # Time averaged

# ECRH

    if hasattr(plasma.ech, 'power_ds'):

        xlbl = 'Channel number'
        dlbl = 'ECRH power'.ljust(20) + 'W'
        ngy = len(plasma.ech.freq)
        x_ech = 1. + np.arange(ngy)
        t_u = plasma.ech.t_ds
        if code == 'tr':
            prephi = 'PHITR'
            prethe = 'THETR'
            phi_fac = -1
            the_fac = np.array([-1, -1, -1, -1, 1, 1, 1, 1])
        elif code == 'as':
            prephi = 'PHIAS'
            prethe = 'THEAS'
            phi_fac = 1
            the_fac = 1
            
        if np.max(plasma.ech.power_ds) > plasma.ech.ech_min:
            uf = ufiles.UFILE()
            uf.pre = 'P'
            uf.ext = 'ECH'
            uf.shot = plasma.ech.shot
            uf.X = {'label': tlbl, 'data': t_u}
            uf.Y = {'label': xlbl, 'data': x_ech}
            uf.f = {'label': dlbl, 'data': plasma.ech.power_ds}
            uf.write(udir=udir)
            uf.comment = ' Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
            uf.average()
            uf.write(udir=udir)

# Angles

        dlbl = 'ECRH tor. angle'.ljust(20) + 'deg'
        uf = ufiles.UFILE()
        uf.pre = prephi
        uf.ext = 'ECH'
        uf.shot = plasma.ech.shot
        uf.X = {'label': tlbl, 'data': t_u}
        uf.Y = {'label': xlbl, 'data': x_ech}
        uf.f = {'label': dlbl, 'data': phi_fac*plasma.ech.phi_ds}
        uf.write(udir=udir)
        uf.comment = ' Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
        uf.average()
        uf.write(udir=udir)

        dlbl = 'ECRH pol. angle'.ljust(20) + 'deg'
        uf.pre = prethe
        uf.ext = 'ECH'
        uf.shot = plasma.ech.shot
        uf.X = {'label': tlbl, 'data': t_u}
        uf.Y = {'label': xlbl, 'data': x_ech}
        uf.f = {'label': dlbl, 'data': the_fac*plasma.ech.theta_ds}
        uf.write(udir=udir)
        uf.comment = ' Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
        uf.average()
        uf.write(udir=udir)

# ICRF

    if hasattr(plasma.icr, 'power_ds'):

        nant = plasma.icr.power_ds.shape[1]
        xlbl = 'Channel number'
        dlbl = 'ICRH power'.ljust(20) + 'W'
        x_icr = 1. + np.arange(nant)
        t_u = plasma.icr.t_ds
        d_u = plasma.icr.power_ds
        if np.max(plasma.icr.power_ds) > plasma.icr.icr_min:
            uf = ufiles.UFILE()
            uf.pre = 'P'
            uf.ext = 'ICH'
            uf.shot = plasma.icr.shot
            uf.X = {'label': tlbl, 'data': t_u}
            uf.Y = {'label': xlbl, 'data': x_icr}
            uf.f = {'label': dlbl, 'data': d_u}
            uf.write(udir=udir)
            uf.comment = ' Time averaged %8.4f - %8.4f' %(t_u[0], t_u[-1])
            uf.average()
            uf.write(udir=udir)

    if hasattr(plasma.icr, 't_cxf'):

        dlbl = 'Minority concentration'

        if plasma.icr.minr is not None:
            uf = ufiles.UFILE()
            uf.pre = 'ICR'
            uf.ext = 'MINR'
            uf.shot = plasma.icr.shot
            uf.X = {'label': tlbl, 'data': plasma.icr.t_cxf}
            uf.f = {'label': dlbl, 'data': plasma.icr.minr}
            uf.write(udir=udir)

        if plasma.icr.minl is not None:
            uf = ufiles.UFILE()
            uf.pre = 'ICR'
            uf.ext = 'MINL'
            uf.shot = plasma.icr.shot
            uf.X = {'label': tlbl, 'data': plasma.icr.t_cxf}
            uf.f = {'label': dlbl, 'data': plasma.icr.minl}
            uf.write(udir=udir)

# Pellet

    if hasattr(plasma.pellet, 'rho_dep'):

        dlbl = 'Ablation rho_tor'
        uf = ufiles.UFILE()
        uf.pre = 'PEL'
        uf.ext = 'RHO_ABL'
        uf.shot = plasma.pellet.shot
        uf.X = {'label': tlbl, 'data': plasma.pellet.times}
        uf.f = {'label': dlbl, 'data': plasma.pellet.rho_dep}
        uf.write(udir=udir)

        dlbl = 'Ablation time'.ljust(20) + 'Seconds'
        uf.ext = 'TIM_ABL'
        uf.f = {'label': dlbl, 'data': plasma.pellet.ablation_time}
        uf.write(udir=udir)


def write_u2d(pr_obj, coord='rho_tor', udir=None):

    xfac = 1.

    for prefix in ('Te', 'Ti', 'Ne', 'Angf'):

        prof = getattr(pr_obj, prefix)
        t_u = pr_obj.time
        if coord == 'rho_tor':
            d_u = prof.yfit_rhot
        elif coord == 'rho_pol':
            d_u = prof.yfit_rhop
        if hasattr(prof, 'tnot'):
            t_u = np.delete(t_u, prof.tnot)
            d_u = np.delete(d_u, prof.tnot, axis=0)

        ext_str = ''
        for sflist in prof.sflists:
            diag = sflist.split(':')[1]
            ext_str += diag

# Find unit label and conversion factor
        f_labels = '%s/labels.dat' %os.path.dirname(os.path.realpath(__file__))
        prestr, dlab, tr_lbl, tr_unit, as_name, trfac, asfac = \
            np.genfromtxt(f_labels, dtype='str', skip_header=2, unpack=True)
        (j_pre, ) = np.where(prestr == prefix)
        if len(j_pre) > 0:
            jpre = j_pre[0]
            if prefix in ('Te', 'Ti', 'Ne'):
                d_u = np.maximum(d_u, 1e-6)
            if tr_unit[jpre] != 'none':
                dlbl = '%-20s%-10s' %(dlab[jpre], tr_unit[jpre])
            else:
                dlbl = '%-30s' %dlab[jpre]
            tr_fac = float(trfac[jpre])
            logger.debug('%s %s %s %s', t_u.shape, pr_obj.rho.shape, d_u.shape, prof.units)

            comm = 'Shotfiles:\n'
            for sfl in prof.sflists:
                comm += '  %s\n' %sfl
            comm += ' Mapping with equilibrium:\n'
            comm +=  '  exp=%s\n diag=%s\n ed=%s\n' %(pr_obj.eq_exp, pr_obj.eq_diag, pr_obj.eq_ed)
            if hasattr(prof, 'method'):
                comm += ' Method: %s\n' %prof.method
            if all(hasattr(prof, attr) for attr in ['fit_tol', 'sigmas']):
                comm += '  fit tolerance=%8.4f\n sigmas=%8.4f\n' %(prof.fit_tol, prof.sigmas)

            uf = ufiles.UFILE()
            uf.pre = prefix.upper()
            uf.ext = ext_str
            uf.shot = pr_obj.shot
            uf.X = {'label': tlbl       , 'data': t_u}
            uf.Y = {'label': pr_obj.xlbl, 'data': xfac*pr_obj.rho}
            uf.f = {'label': dlbl       , 'data': tr_fac*d_u}
            uf.comment = comm
            uf.write(udir=udir)

# Time averaged profile

            uf.comment = ' Time averaged %8.4f - %8.4f\n' %(t_u[0], t_u[-1])
            uf.comment += comm
            uf.average()
            uf.write(udir=udir)
