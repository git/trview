import os

import numpy as np
from trview import GPR1D

def gauss(x_in, y_in, err_in, x_fit, fit_tol=.01):

# Define kernels
    IG = GPR1D.IG_WarpingFunction(.3, .2, .1, 0.98, 0.85)

    kernel  = GPR1D.Gibbs_Kernel(1., wfunc=IG)
    nkernel = GPR1D.RQ_Kernel( 1., .1, 10.)

    kbounds  = np.atleast_2d([[1., .2, 7.e-2, 7.e-2], [5., 1., .3, .2]])
    nkbounds = np.atleast_2d([[.1, .1, 1.], [10., 1., 30.]])
#    nkbounds = np.atleast_2d([[.1, 1.e-2, 1., 1.e-3], [10., 1., 10, 1.e-2]])

# GPR fit accounting only for y-errors
# Define fit settings
    GPR = GPR1D.GaussianProcessRegression1D()

    GPR.set_kernel      (kernel=kernel , kbounds=kbounds , regpar=fit_tol)
    GPR.set_error_kernel(kernel=nkernel, kbounds=nkbounds, regpar=3.)

    GPR.set_search_parameters      (epsilon=1.e-2, method='adam', spars=[1.e-2, .4, .8])
    GPR.set_error_search_parameters(epsilon=1.e-1, method='adam', spars=[1.e-2, .4, .8])

    GPR.set_raw_data(xdata=x_in, ydata=y_in, \
        yerr=err_in, dxdata=[0.], dydata=[0.], dyerr=[0.])

# Do fit
    GPR.GPRFit(x_fit, nrestarts=5)
    (vgp,egp,vdgp,edgp) = GPR.get_gp_results()

    return vgp, egp
