import logging
import numpy as np
import aug_sfutils as sf
from aug_sfutils import read_imas
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.signals')


diagsigs = {'ip': 'FPC:IpiFP', 'bt': 'FPC:BTF', 'uloop': 'TOT:u_loop' , \
    'prad': 'BPD:Pradtot', 'nrate': 'ENR:NRATE_II', \
    'betpol': 'GQH:betpol' , 'wmhd': 'GQH:Wmhd', 'vol': 'GQH:Vol', \
    'perim': 'GQH:Circumf', 'R0': 'GQH:Rgeo', 'Rin': 'GQH:Rin', \
    'Rout': 'GQH:Raus', 'li': 'GQH:li'}


class mysig:
    pass


class SIGNALS:


    def __init__(self, nshot, readSF=True, imasRun=None, tbeg=0., tend=10.):

        self.shot    = nshot
        self.tbeg    = tbeg
        self.tend    = tend
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromShotfile(self):

        sfsig = {}
        for key, diagsig in diagsigs.items():
            tmp = type('', (), {})()
            diag, sig = diagsig.split(':')
            if diag not in sfsig.keys():
                sfsig[diag] = sf.SFREAD(self.shot, diag)
            if sfsig[diag].status:
                tim = sfsig[diag].gettimebase(sig)
                dat = sfsig[diag].getobject(sig).copy()
                if dat is None:
                    if key in ('prad', 'betpol'):
                        logger.warning('Signal %s of diag %s not found', sig, diag)
                        continue

                (index, ) = np.where((tim >= self.tbeg) & (tim <= self.tend))
                if len(index) < 1:
                    index = np.argmin(np.abs(tim - self.tbeg))
                tmp.time = np.atleast_1d(tim[index])
                tmp.data = np.atleast_1d(dat[index])
                tmp.unit = dat.phys_unit
                tmp.diagsig = diagsig
                tmp.shot = self.shot
                tmp.edition = sfsig[diag].ed
                self.__dict__[key] = tmp
            else:
                logger.warning('Signal %s:%s not found for #%d', diag, sig, self.shot)
                continue


    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        summ = imas.summary()
# Global quantities


        summ.ids_properties.homogeneous_time = 1
        summ.time = self.t_ds
        summ.global_quantities.beta_pol.value   = self.betpol.data_ds
        summ.global_quantities.ip.value         = self.ip.data_ds
        summ.global_quantities.b0.value         = self.bt.data_ds
        summ.global_quantities.v_loop.value     = self.uloop.data_ds
        if hasattr(self, 'li'):
            li3 = 2 * self.li.data_ds * self.vol.data_ds/(self.R0.data_ds * self.perim.data_ds**2)
            summ.global_quantities.li.value         = li3
        summ.global_quantities.energy_mhd.value = self.wmhd.data_ds
        if hasattr(self, 'prad'):
            summ.global_quantities.power_radiated.value = self.prad.data_ds
        if hasattr(self, 'nrate'):
            summ.fusion.neutron_rates.total.value = self.nrate.data_ds

        return summ


    def fromIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('summary')
        summ = ids.summary
        self.t_ds = summ.time
        self.ip    = mysig()
        self.bt    = mysig()
        self.uloop = mysig()
        self.ip.time    = self.t_ds
        self.bt.time    = self.t_ds
        self.uloop.time = self.t_ds
        self.ip.data_ds    = summ.global_quantities.ip.value
        self.bt.data_ds    = summ.global_quantities.b0.value
        self.uloop.data_ds = summ.global_quantities.v_loop.value
        self.ip.unit    = 'A'
        self.bt.unit    = 'T'
        self.uloop.unit = 'V'
        self.ip.diagsig    = 'IMAS:ip'
        self.bt.diagsig    = 'IMAS:bt'
        self.uloop.diagsig = 'IMAS:uloop'
        self.ip.edition    = self.run
        self.bt.edition    = self.run
        self.uloop.edition = self.run
        self.ip.shot    = self.shot
        self.bt.shot    = self.shot
        self.uloop.shot = self.shot


    def fromH5(self):

        summ_ids = read_imas.read_imas_h5(self.shot, self.run, branch='summary')
        summ = summ_ids['summary']
        self.t_ds = summ.time
        self.ip    = mysig()
        self.uloop = mysig()
        self.ip.t_ds    = self.t_ds
        self.bt.t_ds    = self.t_ds
        self.uloop.t_ds = self.t_ds
        self.ip.data_ds    = summ.global_quantities.ip.value
        self.bt.data_ds    = summ.global_quantities.b0.value
        self.uloop.data_ds = summ.global_quantities.v_loop.value
        self.ip.diagsig    = 'IMAS:ip'
        self.bt.diagsig    = 'IMAS:bt'
        self.uloop.diagsig = 'IMAS:uloop'
        self.ip.edition    = self.run
        self.bt.edition    = self.run
        self.uloop.edition = self.run
        self.ip.shot    = self.shot
        self.bt.shot    = self.shot
        self.uloop.shot = self.shot
