import logging
import numpy as np
from scipy.interpolate import interp1d

logger = logging.getLogger('trview.rabbit_pos')

def rabbit_pos(nbi):

    n_box1 = nbi.n_box[0]
    n_box2 = nbi.n_box[1]
    
    n_nbi = np.sum(nbi.n_box) #number of sources

# R(P):: Distance horizontal beam crossing to - torus axis [cm]
    R0 = np.array(nbi.n_box[0]*[284.2] + n_box2*[329.6], dtype=np.float32)

# PHI: angle between R and box [rad]
    phi_deg = np.array(nbi.n_box[0]*[15.] + n_box2*[18.9], dtype=np.float32)
    phi_rad = np.radians(phi_deg)

# THETA:   angle towards P (horizontal beam crossing) [rad]
    theta_deg = np.array(n_box1*[33.75] + n_box2*[29.], dtype=np.float32)
    theta_rad = np.radians(theta_deg)
    theta_rad[4:] += np.pi
# 3 sectors shift (new coordinate system)
    theta_rad -= np.pi/8.*3.

# ALPHA: horizontal angle between Box-axis and source [rad]
    alpha_deg = 4.1357*np.array(2*[1., -1., -1., 1.], dtype=np.float32)

# distance between P0 and Px!
    delta_x_cm = np.array(5*[-50.] + [50., 50, -50], dtype=np.float32)

# BETA = vertical angle between box-axis and source [rad]
    beta_deg = np.array([-4.8991, -4.8991, 4.8991, 4.8991, -4.8991, -6.6555, 6.6555, 4.8991])
    beta_nis_deg = nbi.beta_sf_deg*np.array(2*[-1., -1., 1., 1.], dtype=np.float32)
    logger.debug(beta_nis_deg)

# correct beta of Q6
    dbeta6 = 6.65 + beta_nis_deg[5]
    xdbeta = np.array([-0.3, 0. , 0.55], dtype=np.float32)
    ydbeta = np.array([0.03, 0.07, 0.2], dtype=np.float32)
    f = interp1d(xdbeta, ydbeta, fill_value='extrapolate')
    dbeta_corr6_deg = f(dbeta6)
    dbeta_corr6_rad = np.radians(dbeta_corr6_deg)
 
# correct beta of Q7
    dbeta7 = 6.65 - beta_nis_deg[6]
    xdbeta = np.array([-0.46, 0.  , 0.53], dtype=np.float32)
    ydbeta = np.array([0.14 , 0.14, 0.32], dtype=np.float32)
    f = interp1d(xdbeta, ydbeta, fill_value='extrapolate')
    dbeta_corr7_deg = f(dbeta7)
    dbeta_corr7_rad = np.radians(dbeta_corr7_deg)

    alpha_rad    = np.radians(alpha_deg)
    beta_rad     = np.radians(beta_deg)
    beta_nis_rad = np.radians(beta_nis_deg)
    beta_nis_rad[5] += -dbeta_corr6_rad
    beta_nis_rad[6] +=  dbeta_corr7_rad

# Ralph's correction
# move beams different toroidal injection geometry of Q3:

    alpha_rad[2] += np.radians(0.4)

    theta_rad[4] += np.radians(0.3)
    alpha_rad[4] += np.radians(0.3)

    theta_rad[5] += np.radians(-0.4)
    alpha_rad[5] += np.radians(0.65)

    theta_rad[6] += np.radians(-0.2)
    alpha_rad[6] += np.radians(0.4)

    beta_rad[7]  += np.radians(-0.1)
    theta_rad[7] += np.radians(0.0)
    alpha_rad[7] += np.radians(-0.20)

# get dbeta
    dbeta_rad = beta_nis_rad - beta_rad

# distance center of box and P0
    d_box_P0_cm = 650.0

# source elevation w.r.t. midplane [cm] estimate
    src_hv_cm = np.tan(beta_rad)*(delta_x_cm - d_box_P0_cm)

# the off-axis rotation axis of NBI Q6 and NBI Q7 is
# actually not at the source but somewhat distant
# so calculate the source-position depending on the rotation.
# the axis of rotation is 45 cm higher than the source axis. 
# consider this by dxr and dyr
    pivot = np.array([0., 0., 0., 0., 0., -45., 45., 0.], dtype=np.float32)
    dcos1 = np.cos(dbeta_rad) - 1.
    dz = 95.*np.sin(dbeta_rad) + pivot*dcos1
    dx = 95.*dcos1 - pivot*np.sin(dbeta_rad)
 
# new vertical source positions
    src_hv_cm += dz
 
# also the distance between P0 and the source changes in the
#  horizontal plane!
    d_box_P0_cm += - np.cos(alpha_rad)*np.cos(dbeta_rad)*dx

# distance source, P0
# src_hw_cm_homepage=[47., -47., -47., 47., 47., -47., -47., 47.]
    src_hw_cm = np.tan(alpha_rad)*d_box_P0_cm

# Phi_box: angle of the box-axi to the x-axis [rad]
    phi_box_rad = theta_rad - phi_rad

# P0 position as defined on the AUG homepage [cm]
    xyz_P0_cm = np.zeros((n_nbi, 3), dtype=np.float32)
    xyz_P0_cm[:, 0] = R0 * np.cos(theta_rad)
    xyz_P0_cm[:, 1] = R0 * np.sin(theta_rad)
    xyz_P0_cm[:, 2] = src_hv_cm + np.sqrt(d_box_P0_cm**2 + src_hw_cm**2 + src_hv_cm**2)*np.sin(beta_nis_rad)

# position of source in xyz coordinates
    xyz_src_cm = np.zeros_like(xyz_P0_cm)
    dist = np.hypot(d_box_P0_cm, src_hw_cm)
    xyz_src_cm[:, 0] = xyz_P0_cm[:, 0] + dist*np.cos(phi_box_rad + alpha_rad)
    xyz_src_cm[:, 1] = xyz_P0_cm[:, 1] + dist*np.sin(phi_box_rad + alpha_rad)
    xyz_src_cm[:, 2] = src_hv_cm
# Unit vector
    xyz_vec = xyz_P0_cm - xyz_src_cm
    for jnb in range(n_nbi):
        xyz_vec[jnb, :] /= np.linalg.norm(xyz_vec[jnb])

    xyz_src_m = 0.01*xyz_src_cm

    return xyz_src_m, xyz_vec
