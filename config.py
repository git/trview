import os

platform = os.uname().nodename

browser = '/usr/bin/firefox'

if platform[:3] == 'tok' or platform[:5] in ('cx-ld', 'CX-LD', 'hz-ld'):
    rabbitDir = '/shares/departments/AUG/users/git/ASTRA_LIBRARIES_EXT/rabbit'
    ecaDir = '/shares/departments/AUG/users/git/local/lib'
else:
    rabbitDir = '/afs/eufus.eu/user/g/g2gtardi/ASTRA_LIBRARIES_EXT/rabbit'
    ecaDir = '/afs/eufus.eu/user/g/g2gtardi/trview/lib'
    
rabbitTable   = '%s/tables_highRes' %rabbitDir
rabbitLimiter = '%s/limiters/limiter_aug.dat' %rabbitDir
rabbitNml_AUG = \
'''&species
   Aimp = 200.00
   Zimp = 100.00
/

&output_settings
   it_orbout = -1
/

&physics
   jumpcor = 2
   table_path   = '%s'
   limiter_file = '%s'
   Rlim = 2.20 !AUG defaults, overridden if limiter_file exists
   zlim = 0.20
   Rmax = 2.26 !AUG defaults
   Rmin = 1.08
   torqjxb_model = 3
/

&numerics
   norbits = 20
   orbit_dt_fac = 1.56
   distfun_nv = 200
   distfun_vmax = 4.5e6
/
''' %(rabbitTable, rabbitLimiter)

ecalib = '%s/libaug_ecrh_setmirrors.so' %ecaDir

