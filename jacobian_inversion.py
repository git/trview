import numpy as np
from scipy.linalg.lapack import dgbsv
from scipy.interpolate import interp1d

flt = np.float64


class jacobInv:


    def __init__(self, Rb, Zb, X0, Y0, lambda2d_in, lambda2dp_in, psin_grid, psin_gridp, psin2d, iplasma, psib, effprime, epprime, relambda_flag):

        nrho, ntheta = lambda2d_in.shape
        dXb0 = np.hypot(Rb - X0, Zb - Y0) # ntheta

# Theta grids
        thetap = np.arctan2(Zb - Y0, Rb - X0)
        thetap[thetap < 0] += 2.*np.pi
        if thetap[0] > thetap[-1]:
            (ind, ) = np.where(np.diff(thetap) < 0)
            thetap[: ind[0]+1] -= 2.*np.pi # make thetap monotonic
        thetap = np.append(thetap, thetap[0] + 2.*np.pi) # ntheta+1

        thetap_interm = 0.5*(thetap[1:] + thetap[:-1])
        thetap_interm = np.append(thetap_interm, thetap_interm[0] + 2.*np.pi) # ntheta+1

        indthe = np.arange(ntheta)
        indthe_left  = np.roll(indthe,  1)
        indthe_right = np.roll(indthe, -1)

# Coordinate differentials
        ddr   = np.diff(psin_grid)                                         # nrho-1
        ddr_i = np.append(0.5*psin_gridp[0], np.diff(psin_gridp))     # nrho-1
        dtp   = np.diff(thetap)                                            # ntheta
        dt_i  = thetap_interm[indthe_left+1] - thetap_interm[indthe_left]  # ntheta

        self.lambda2d  = lambda2d_in # (nrho, ntheta)
        self.lambda2dp = lambda2dp_in
        if relambda_flag:
            for jthe in range(ntheta):
                psin = psin2d[:, jthe]
                lambda2dold = self.lambda2d[:, jthe]
                fint  = interp1d(psin, lambda2dold)
                self.lambda2d[1:-1, jthe] = fint(psin_grid[1:-1])
                fintp = interp1d(psin_grid, self.lambda2d[:, jthe])
                self.lambda2dp[:-1, jthe] = fintp(psin_gridp)

        dXcos = dXb0*np.cos(thetap[:-1]) # ntheta
        dXsin = dXb0*np.sin(thetap[:-1])

        dXb0_i = 0.5*(dXb0 + dXb0[indthe_right])
        dXicos = dXb0_i*np.cos(thetap_interm[:-1])
        dXisin = dXb0_i*np.sin(thetap_interm[:-1])
        lambda2di  = 0.5*(self.lambda2d  + self.lambda2d [:, indthe_right])
        lambda2dpi = 0.5*(self.lambda2dp + self.lambda2dp[:, indthe_right])

        self.X = X0 + self.lambda2d *dXcos[None, :]
        self.Y = Y0 + self.lambda2d *dXsin[None, :]
        X2     = X0 + self.lambda2dp*dXcos[None, :]
        Y2     = Y0 + self.lambda2dp*dXsin[None, :]
        X_i1   = X0 + lambda2di *dXicos[None, :]
        Y_i1   = Y0 + lambda2di *dXisin[None, :]
        X_i    = X0 + lambda2dpi*dXicos[None, :]
        Y_i    = Y0 + lambda2dpi*dXisin[None, :]

# Jacobian matrices (nrho-1, ntheta)
        dArea = np.zeros((nrho-1, ntheta), dtype=flt)
        dtp1  = np.zeros_like(dArea)
        dtpr1 = np.zeros_like(dArea)
        for jrho in range(nrho-1):
            if jrho == 0:
                dXdr   = (X2 [jrho, :] - self.X[jrho, :]) / psin_gridp[jrho]
                dYdr   = (Y2 [jrho, :] - self.Y[jrho, :]) / psin_gridp[jrho]
                dXdri1 = (X_i[jrho, :] - self.X[jrho, :]) / psin_gridp[jrho]
                dYdri1 = (Y_i[jrho, :] - self.Y[jrho, :]) / psin_gridp[jrho]
                dXdh   = (X_i[jrho, :] - X_i[jrho, indthe_left]) / dt_i
                dYdh   = (Y_i[jrho, :] - Y_i[jrho, indthe_left]) / dt_i
                dXdhi1 = (X2[jrho, indthe_right] - X2[jrho, :]) / dtp
                dYdhi1 = (Y2[jrho, indthe_right] - Y2[jrho, :]) / dtp
            else:
                dXdr   = (X2  [jrho, :] - X2 [jrho-1, :]) / ddr_i[jrho]
                dYdr   = (Y2  [jrho, :] - Y2 [jrho-1, :]) / ddr_i[jrho]
                dXdri1 = (X_i [jrho, :] - X_i[jrho-1, :]) / ddr_i[jrho]
                dYdri1 = (Y_i [jrho, :] - Y_i[jrho-1, :]) / ddr_i[jrho]
                dXdh   = (X_i1[jrho, :] - X_i1[jrho, indthe_left]) / dt_i
                dYdh   = (Y_i1[jrho, :] - Y_i1[jrho, indthe_left]) / dt_i
                dXdhi1 = (self.X[jrho, indthe_right] - self.X[jrho, :]) / dtp
                dYdhi1 = (self.Y[jrho, indthe_right] - self.Y[jrho, :]) / dtp
            Jcbni1  = dXdri1*dYdhi1 - dXdhi1*dYdri1 # i, j+1/2
            Mdet_inv_i = 1./Jcbni1
            drdX =  dYdhi1*Mdet_inv_i
            dhdX = -dYdri1*Mdet_inv_i
            drdY = -dXdhi1*Mdet_inv_i
            dhdY =  dXdri1*Mdet_inv_i
            gradhi1 = dhdX**2   + dhdY**2
            grti1   = drdX*dhdX + drdY*dhdY   # i, j+1/2
            Jcbn    = dXdr*dYdh - dXdh*dYdr   # i, j
            mat_i1  = Jcbni1/X_i1[jrho, :]
            dArea[jrho, :] = Jcbn*ddr_i[jrho]*dt_i
            dtp1 [jrho, :] = mat_i1*gradhi1*ddr_i[jrho]/dtp
            dtpr1[jrho, :] = mat_i1*grti1

        dXdr2  = np.diff(self.X, axis=0)/ddr[:, None] # ntheta
        dYdr2  = np.diff(self.Y, axis=0)/ddr[:, None]
        dXdh2  = (X_i[:-1, :] - X_i[:-1, indthe_left])/dt_i[None, :]
        dYdh2  = (Y_i[:-1, :] - Y_i[:-1, indthe_left])/dt_i[None, :]
        Jcbn2  = dXdr2*dYdh2 - dXdh2*dYdr2  # i+1/2, j
        Mdet_inv = 1./Jcbn2
        drdX =  dYdh2*Mdet_inv
        dhdX = -dYdr2*Mdet_inv
        drdY = -dXdh2*Mdet_inv
        dhdY =  dXdr2*Mdet_inv
        gradr2 = drdX**2 + drdY**2
        grt2   = drdX*dhdX + drdY*dhdY      # i+1/2, j
        mat2 = Jcbn2/X2[:-1, :]
        self.dArea2_X2 = ddr*np.einsum('ij,j', mat2, dt_i) # nrho-1
        drp1  = mat2*gradr2*dt_i[None, :]/ddr[:, None]
        drpt1 = mat2*grt2

# Solver Inversion Matrix
# DBFSV taking 10x CPU than the rest, it's optimised

        known_term = dArea*(effprime[:-1, None]/self.X[:-1, :] + epprime[:-1, None]*self.X[:-1, :]) # nrho-1, ntheta
        cnorm = np.sum(known_term)/(iplasma*0.8*np.pi**2)
        known_term /= cnorm

#        NRHS   = 1
        KL     = 2*ntheta
        KU     = 2*ntheta
        Ndims  = 1 + (nrho - 2)*ntheta
        LDAB   = 1 + 2*KL + KU
        AB = np.zeros((LDAB, Ndims), dtype=flt)
        BB = np.zeros(Ndims, dtype=flt)
        BB[0] = np.sum(known_term[0, :])
        BB[1:] = known_term[1:, :].ravel()
        BB[-ntheta:] -= psib*(drp1[nrho-2, :] + 0.5*(dtpr1[nrho-2, :] - dtpr1[nrho-2, indthe_left]))

        jrho  = 0
        j00   = 0
        jp0 = 1 + indthe
        jpp = 1 + indthe_right
        jpm = 1 + indthe_left
        jrawm = KL + KU + j00
# jp0, jpp, jpm overlap; j00 not
        AB[jrawm-j00, j00] = -np.sum(drp1[jrho, :])
        AB[jrawm-jp0, jp0] = drp1[jrho, :]
        AB[jrawm-jpp, jpp] += 0.25*drpt1[jrho, :]
        AB[jrawm-jpm, jpm] -= 0.25*drpt1[jrho, :]

        for jrho in range(1, nrho-1):
            jm0 = 1 + indthe       + (jrho - 2)*ntheta
            j00 = 1 + indthe       + (jrho - 1)*ntheta
            jp0 = 1 + indthe       + jrho*ntheta
            jmm = 1 + indthe_left  + (jrho - 2)*ntheta
            j0m = 1 + indthe_left  + (jrho - 1)*ntheta
            jpm = 1 + indthe_left  + jrho*ntheta
            jmp = 1 + indthe_right + (jrho - 2)*ntheta
            j0p = 1 + indthe_right + (jrho - 1)*ntheta
            jpp = 1 + indthe_right + jrho*ntheta
            jrawm  = KL + KU + j00

            drho = 0.25*(drpt1[jrho, :] - drpt1[jrho-1, :])
            dthe = 0.25*(dtpr1[jrho, :] - dtpr1[jrho, indthe_left])
            AB[jrawm - j00, j00] = -(drp1[jrho, :] + drp1[jrho-1, :] + dtp1[jrho, :] + dtp1[jrho, indthe_left])
            AB[jrawm - j0p, j0p] = dtp1[jrho, :] + drho
            AB[jrawm - j0m, j0m] = dtp1[jrho, indthe_left] - drho
            if jrho < nrho - 2:
                AB[jrawm-jp0, jp0] = drp1[jrho, :] + dthe
                AB[jrawm-jpp, jpp] =  0.25*(dtpr1[jrho, :] + drpt1[jrho, :])
                AB[jrawm-jpm, jpm] = -0.25*(drpt1[jrho, :] + dtpr1[jrho, indthe_left])
            if jrho == 1:
                jm0 = 0
                AB[jrawm-jm0, jm0] = drp1[jrho-1, :] - 2.*dthe
            else:
                AB[jrawm-jmp, jmp] = -0.25*(drpt1[jrho-1, :] + dtpr1[jrho, :])
                AB[jrawm-jmm, jmm] =  0.25*(dtpr1[jrho, indthe_left] + drpt1[jrho-1, :])
                AB[jrawm-jm0, jm0] = drp1[jrho-1, :] - dthe

        _, _, bb_out, _ = dgbsv(KL, KU, AB, BB)

        self.psi = np.zeros((nrho, ntheta), dtype=flt)
        self.psi[ 0, :] = bb_out[0]
        self.psi[-1, :] = psib
        self.psi[1:-1, :] = bb_out[1:].reshape((nrho-2, ntheta))
