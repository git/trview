import os, sys, logging
import numpy as np
from trview import descur2d, mom2rz
from nemec import nemec_wrapper as nmwrap
import aug_sfutils as sf
from multiprocessing import Pool, cpu_count

fmt = logging.Formatter('%(asctime)s | %(name)s | %(levelname)s: %(message)s', '%H:%M:%S')

logger = logging.getLogger('pyNEMEC')

if len(logger.handlers) == 0:
    hnd = logging.StreamHandler()
    hnd.setFormatter(fmt)
    logger.addHandler(hnd)
#logger.setLevel(logging.INFO)
logger.setLevel(logging.DEBUG)

nemec_home = '%s/nemec' %os.getenv('SHARES_USER_DIR')
if not os.path.exists(nemec_home):
    os.mkdir(nemec_home)
nemec_out = '%s/output' %nemec_home
if not os.path.exists(nemec_out):
    os.mkdir(nemec_out)

descu  = descur2d.scrunch


def nemec_run(nmc_input):

    logger.info('Initialising NEMEC')

    jt, time, vmec_d, tf, iota, pres, rbnd, zbnd = nmc_input

    nmwrap.pynrho  = vmec_d['nrho']
    nmwrap.pyniter = vmec_d['niter']
    nmwrap.pynstep = vmec_d['nstep']
    nmwrap.pympol  = vmec_d['m_moms']
    nmwrap.pyntor  = 0 # No toroidal moments for tokamaks
    nmwrap.pynvacskip = 1
    nmwrap.pyntheta   = vmec_d['ntheta']
    nmwrap.pynzeta    = 1
    nmwrap.pyiasym    = 1
    nmwrap.pynfp      = 1
    nmwrap.pyncurr    = 0
    nmwrap.pycurtor   = 0.
    nmwrap.pydelt     = 0.5
    nmwrap.pyuser_fouraxis = ''.ljust(120)
    nmwrap.pyuser_pressure = ''.ljust(120)
    nmwrap.pyuser_itor     = ''.ljust(120)
    nmwrap.pyuser_iota     = ''.ljust(120)
    nmwrap.pyuser_fourlcms = ''.ljust(120)
    nmwrap.pymgrid_file    = 'none'.ljust(120)
    nmwrap.pyres_file      = 'none'.ljust(120)
    nmwrap.pypres_profile  = 'user'.ljust(120)
    nmwrap.pyiota_profile  = 'user'.ljust(120)
    nmwrap.pyitor_profile  = 'none'.ljust(120)
    nmwrap.pyout_path      = ('%s/output/' %nemec_home).ljust(120)
    nmwrap.pyvac_1_2   = 1
    nmwrap.pygamma     =     0.
    nmwrap.pybetascale =     1.
    nmwrap.pyraxis_co0 =  1.731
    nmwrap.pyzaxis_co0 =  0.108
    nmwrap.pywoutt     =   'red'.ljust(10)
    nmwrap.pywoutf     = 'ascii'.ljust(10)
    nmwrap.pyextension = '%-10d' %(jt+1)
    nmwrap.pylcont     = False
    nmwrap.pylfreeb    = False
    nmwrap.pylwr_res   = False
    nmwrap.pyns_array   = np.array([vmec_d['nrho']], dtype=np.int32)
    nmwrap.pyftol_array = np.array([1.e-14], dtype=np.float32)

    tfln = tf/tf[-1]
    nmwrap.pyphiedge = tf[-1]

# Profiles
    nmwrap.pyiotsto = tfln
    nmwrap.pyiotaus = iota
    nmwrap.pyniotau = len(iota)

    nmwrap.pypresto = tfln
    nmwrap.pypresus = pres - pres[-1]
    nmwrap.pynpresu = len(nmwrap.pypresus)

# Fourier moments for boundary
    moments = descu((rbnd, zbnd, vmec_d['m_moms'], time, 0))
    nmwrap.pyrbc = moments[:, 0]
    nmwrap.pyrbs = moments[:, 1]
    nmwrap.pyzbc = moments[:, 2]
    nmwrap.pyzbs = moments[:, 3]

# NEMEC calculation
    nmwrap.calc_nemec()

# Output (rho, jpol)
    rc = np.squeeze(nmwrap.rc)
    rs = np.squeeze(nmwrap.rs)
    zc = np.squeeze(nmwrap.zc)
    zs = np.squeeze(nmwrap.zs)

    return rc, rs, zc, zs


class NEMEC:


    def __init__(self, vmec_d, eqm=None, equ_d=None, t_in=None):

        vmec_dv = {}
        for key, val in vmec_d.items():
            setattr(self, key, val['value'])
            vmec_dv[key] = val['value']

        if equ_d is None:
            self.shot = eqm.shot
        else:
            for key, val in equ_d.items():
                setattr(self, key, val)

#=====================
# Read AUG equilibrium
#=====================

        if eqm is None:
            eqm = sf.EQU(equ_d['shot'], diag=equ_d['equ_dia'], \
                   exp=equ_d['equ_exp'], ed=equ_d['equ_ed'])

        if t_in is None:
            t_in = eqm.time

        self.time = np.atleast_1d(t_in)

        tim_ind = []
        for tim in self.time:
            j_t = np.argmin(np.abs(eqm.time - tim))
            tim_ind.append(j_t)

        if hasattr(eqm, 'lpfp'): # Shotfile
            nrho_equ = np.max(eqm.lpfp) + 1
        else: # IMAS
            nrho_equ = eqm.pfl.shape[1]

# Check rho-dimension in wrapper, consistency!!
        self.tfl  = eqm.tfl [tim_ind, :nrho_equ]
        self.q    = eqm.q   [tim_ind, :nrho_equ]
        self.pres = eqm.pres[tim_ind, :nrho_equ]

# Fourier moments of last closed surface
        if hasattr(eqm, 'lpfp'): # Shotfile
            rhop = 0.999
            self.rbnd, self.zbnd = sf.rho2rz(eqm, rhop, t_in=self.time, coord_in='rho_pol', all_lines=False)
        else:
            self.rbnd = eqm.sep.rscat
            self.zbnd = eqm.sep.zscat

# Time-dependent output

        nt = len(self.time)

        timeout_pool = 200
        pool = Pool(cpu_count())

# Execute NEMEC
        out = pool.map_async(nemec_run, [(jt, self.time[jt], vmec_dv, \
                self.tfl[jt], 1./self.q[jt], self.pres[jt], self.rbnd[jt][0], self.zbnd[jt][0]) for jt in range(nt)]).get(timeout_pool)
        pool.close()
        pool.join()

        out = np.array(out)
        self.rc = out[:, 0, :, :]
        self.rs = out[:, 1, :, :]
        self.zc = out[:, 2, :, :]
        self.zs = out[:, 3, :, :]

        self.rbnd, self.zbnd = mom2rz.mom2rz(self.rc, self.rs, self.zc, self.zs, endpoint=True)
        nrho = self.rbnd.shape[1]
        self.tfl_n = np.linspace(0, 1, nrho)
        self.tfgrid = np.outer(self.tfl[:, -1], self.tfl_n)
