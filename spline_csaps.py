import numpy as np
from csaps import csaps
from scipy.linalg import norm


def spline(x_exp, y_exp, x_out, fit_tol=1, y_err=None):

    smooth_fac = max(0, 1 - fit_tol)
    ind_fin = np.isfinite(y_exp)
    x_fin = x_exp[ind_fin]
    y_fin = y_exp[ind_fin]
    wei = np.ones(len(x_fin))
    ynorm = norm(y_fin)

    x_in, ind_uni = np.unique(np.append(-x_fin[::-1], x_fin), return_index=True)
    y_in = np.append( y_fin[::-1], y_fin)/ynorm
    y_in = y_in[ind_uni]

    if y_err is not None:
        err_fin = y_err[ind_fin]
        (ind_pos, ) = np.where(err_fin > 0)
        wei[ind_pos] = (ynorm/err_fin[ind_pos])**2
    weights = np.append( wei[::-1], wei)
    weights = weights[ind_uni]
    yspl = csaps(x_in, y_in, x_out, smooth=smooth_fac, weights=weights)

    return yspl*ynorm


def rec_spline(x_exp, y_exp, x_out, sigmas=1, fit_tol=1, y_err=None):


    if y_err is not None:
        tol = sigmas*y_err
    else:
        tol = sigmas*0.1*y_exp

    x = x_exp
    y = y_exp

    while(True):
        yspl = spline(x, y, x_out, fit_tol=fit_tol, y_err=y_err)
        yref = np.interp(x, x_out, yspl)
        ydiff = np.abs(y - yref)
        ydiff[np.isnan(y)] = 0
        (ind_bad, ) = np.where(ydiff > tol)
        if len(ind_bad) > 0:
            y[ind_bad] = np.nan
        else:
            break

    return yspl, y
