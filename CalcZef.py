import logging
import numpy as np
from scipy.interpolate import interp1d
from scipy import constants
import aug_sfutils as sf

logger = logging.getLogger('trview.CalcZef')


class Zeff_fml:


    def __init__(self, nshot, diag='DCN', sig='H-1', n_av=100):


        dcn = sf.SFREAD(nshot, diag)
        if dcn.status :
            tim = dcn.gettimebase(sig)
            dat = dcn.getobject(sig, cal=True)
        else:
            return

        dat  *= 1.e-19
        ntim = len(tim)

        max_zef = 4.5
#        n_av = 100 # corresponds to 10 ms
        n_t = int(ntim/n_av) - 1
        logger.info('Diag %s,  nt = %d', diag, n_t)

        ind = n_av*np.arange((n_t + 1), dtype=int)
        t_tmp = np.cumsum(tim, dtype=float)[ind]
        d_tmp = np.cumsum(dat, dtype=float)[ind]
        self.time = (t_tmp[1:] - t_tmp[:-1])/n_av
        ne_bar    = (d_tmp[1:] - d_tmp[:-1])/n_av
        self.zeff = np.zeros(n_t)
        ind_one = (ne_bar > 1)
        self.zeff[~ind_one] = max_zef
        self.zeff[ind_one] = np.minimum( 1. + 11.2/(ne_bar[ind_one] - 0.74), max_zef)


class CalcZeff:


    def __init__(self, equ, tbeg, tend, idz=False, diag='CEZ', exp='AUGD'):

        nshot = equ.shot

        ntim = 0
        nLOS = 0
        nr = 201
# Set boundary rho, from which bremsstrahlung is considered to be zero:
        self.rhoZero = 1.1

# Load data from diag

        cx = sf.SFREAD(nshot, diag, exp=exp)
        if cx.status:
            los = cx.getparset('LOSInfo')
            lin = cx.getparset('LineInfo')
            lam = lin['cxwavel']
            activ = np.bool_(lin['active'])
            self.lam = np.mean(lam[activ])
            R = los['R']
            Z = los['z']
            phi = np.radians(los['phi'])
            Rorig = los['Rorig']
            Zorig = los['zorig']
            phiorig = np.radians(los['phiorig'])
            print(nshot, diag, exp)
            base_name = 'baseline' if hasattr(cx, 'baseline') else 'base'
            logger.debug('%s, %s', diag, base_name)

            tvec     = cx.gettimebase(base_name)  
            Base     = cx.getobject(base_name)
            Base_err = Base.copy()
            fit_stat = cx.getobject('fit_stat')
            logger.debug(str(fit_stat.shape))
            if fit_stat.size == Base.size:
                Base_err[ (fit_stat != 1) | (Base==0) ] = np.infty
            self.status = True
        else:
            self.status = False
#            return

# Load Ne, Te from IDA
        sfidz = sf.SFREAD(nshot, 'IDZ')
        sfida = sf.SFREAD(nshot, 'IDA')
        if idz and sfidz.status:  #USE IDZ data to compare the models
            tvec = sfidz.getobject('time_cer')

            tvec_ida = sfidz.getobject( 'timeZeff')
            ne       = sfidz.getobject('Ne')
            Te       = sfidz.getobject('Te')
            rho_ida  = sfidz.getobject('rhop').T

            E = (constants.c*constants.h/(self.lam*1e-9))
            dlam = 5
            Base     = sfidz.getobject('cer_dat' )/E*dlam
            Base_err = sfidz.getobject('cer_datu')/E*dlam

        elif sfida.status:

            tvec_ida = sfida.getobject('time')
            ne       = sfida.getobject('ne')
            Te       = sfida.getobject('Te')
            rho_ida  = sfida.getobject('rhop')

        ind = R != 0
        R, Z, Zorig, Rorig = R[ind], Z[ind], Zorig[ind], Rorig[ind]
        Base, Base_err, phi, phiorig = Base[:,ind], Base_err[:,ind], phi[ind], phiorig[ind]

        tbeg = max(tbeg, tvec_ida[0])
        tend = min(tend, tvec_ida[-1])

        tred = (tvec >= tbeg) & (tvec <= tend)
        t_ind = tred & ( np.any(np.isfinite(Base_err), axis=1) )
        if not any(t_ind):
            return False

        self.Base = Base[t_ind, :]
        self.Base_err = Base_err[t_ind, :]
        self.tvec = tvec[t_ind]
        ntim = len(self.tvec)
        nLOS = sum(ind)

        nt = len(tvec_ida)
        dt = np.mean(np.diff(tvec_ida))

        if dt > 40e-3:
            reduct =1
        else:
            reduct = int(np.round(40e-3/np.mean(np.diff(tvec_ida))))

        nt = int(nt/reduct)*reduct
        tvec_ida = tvec_ida[:nt:reduct]

        ne = np.reshape(ne[:, :nt].T,(nt//reduct, reduct, ne.shape[0])).mean(1)
        f = interp1d(tvec_ida, ne, axis=0, fill_value=np.nan, bounds_error=False, copy=False)
        self.ne = f(self.tvec).T

        Te = np.reshape(Te[:, :nt].T,(nt//reduct, reduct, Te.shape[0])).mean(1)
        f = interp1d(tvec_ida, Te, axis=0, fill_value=np.nan, bounds_error=False, copy=False)
        self.Te = f(self.tvec).T

        rho_ida = np.reshape(rho_ida[:, :nt].T,(nt//reduct, reduct, rho_ida.shape[0])).mean(1)
        f = interp1d(tvec_ida, rho_ida, axis=0, fill_value=np.nan, bounds_error=False, copy=False)
        self.rho_ida = f(self.tvec).T

        self.R = R
        self.Z = Z

# Map LOS to mag. coordinates

        if diag == 'CMZ':
            tmax = 11
        else:
            tmax = 2
        t = np.linspace(0, tmax, nr)

        rin, zin, lin = [], [], []
        for r1, z1, p1, r2, z2, p2 in zip(R, Z, phi, Rorig, Zorig, phiorig):
            X1 = np.array((r1*np.cos(p1), r1*np.sin(p1), z1))
            X0 = np.array((r2*np.cos(p2), r2*np.sin(p2), z2))
            X  = np.outer(t, (X1 - X0)) + X0
            rin.append(np.hypot(X[:, 0], X[:, 1]))
            zin.append(X[:, 2])
            lin.append(np.sqrt(np.sum((X - X0)**2, axis=1)))

        rin = np.hstack(rin)
        zin = np.hstack(zin)
        L = np.vstack(lin)

        self.rhoLos = sf.rz2rho(equ, R, Z, t_in=self.tvec, coord_out='rho_pol')
        rho = sf.rz2rho(equ, rin, zin, t_in=self.tvec, coord_out='rho_pol')
        rho = np.reshape(rho, (ntim, nLOS, nr ))

        self.rho = rho
        self.rho_b = (rho[..., 1:] + rho[..., :-1])/2  #rho between
        self.dL = np.diff(L)

        self.rin = np.reshape(rin, (nLOS, nr))
        self.zin = np.reshape(zin, (nLOS, nr))

        logger.info(diag)


    def calc_Base_per_m(self, jt, ilos):

# Calculates the average baseout_per_m (average over ilos)

        if np.sum(ilos) == 0: #no LOS selected...
            return 0.

        mask = (self.rho_b[jt,ilos,:]>0.99)&(self.rho_b[jt,ilos,:]<self.rhoZero)
        L = np.sum(self.dL[ilos,:]*mask, axis=1)
        Base_per_m_all = self.Base[jt,ilos] / L
        if np.sum(self.Base_err[jt, ilos]**-2) > 0 & np.sum(self.Base_err[jt, ilos] > 0):
            Base_per_m = np.average(Base_per_m_all, weights=self.Base_err[jt, ilos]**-2)
        else:
            Base_per_m = 0.
        return Base_per_m


    def model_Baseout(self, jt, Baseout_av):

        dL = np.copy(self.dL)
        mask = (self.rho_b[jt,:,:]>0.99)&(self.rho_b[jt,:,:]<self.rhoZero)
        L = np.sum(self.dL*mask, axis=1)
        Baseout_per_los = Baseout_av * L       

        return Baseout_per_los


    def brems_function(self, jt, Baseout_av=0.):

# http://iopscience.iop.org/0741-3335/52/4/045006/pdf/0741-3335_52_4_045006.pdf
# Original formula:
# gaunt=5.542-(3.108-alog(te/1000.))*(0.6905-0.1323/zeff)
#  emisrho=10.*7.57e-9*gaunt*ne^2*zeff/(lambda*sqrt(te))*exp(-h_planck*c0/(lambda*te))

        te = np.interp(self.rho_b[jt,:,:],self.rho_ida[:,jt],self.Te[:,jt])
        ne = np.interp(self.rho_b[jt,:,:],self.rho_ida[:,jt],self.ne[:,jt])/1e6  #cm^-3

        ind = self.rho_b[jt,:,:]>0.99
        dL = np.copy(self.dL) 
        dL[ind] = 0  #out of the plasma

        h_planck=4.135667e-15         # [eV/s]
        c0 = constants.c              # [m/s]

        A = 7.57e-9*ne**2/(self.lam*np.sqrt(te))*np.exp(-h_planck*c0/(self.lam/1e9*te))
        B = 5.542 - (3.108-np.log(te/1e3))*0.6905
        C = (3.108-np.log(te/1e3))*0.1323

# gaunt =  B + C / zeff
# brems = A * gaunt * zeff = A * ( B + C / zeff) * zeff = 
#       = A*B*zeff + A*C
# (same result as em_profiles::bremsstrahlung_spec_optical, te, lambda)

        AB = np.sum(dL*A*B, axis=1)
        AC = np.sum(dL*A*C, axis=1)

        Baseout = self.model_Baseout(jt, Baseout_av)
        Base = self.Base[jt,:] - Baseout

        zeff_all = (Base-AC)/AB
        zeff_err =  Base/AB

        if np.isnan(zeff_err).any() or np.isinf(zeff_err).any():
            zeff=1.6
        else:
            zeff = np.average(zeff_all, weights=zeff_err**-2)

        zeff = max(1,min(5,zeff))
        LOS_rad=AB*zeff + AC + Baseout
 
        return LOS_rad,zeff


class Zeff:


    def __init__(self, equ, idz=False):

        nshot = equ.shot
        zeffCMZ = CalcZeff(equ, 0., 10., diag='CMZ', idz=idz)
        zeffCEZ = CalcZeff(equ, 0., 10., idz=idz)

        self.timeZeff = zeffCEZ.tvec
        self.Zeff  = self.timeZeff*0.
        self.Zeff0 = self.timeZeff*0.

        for jt, time in enumerate(self.timeZeff):

            #evaluation with CEZ only:
            itimein = (np.abs(zeffCEZ.tvec-time)).argmin()
            retro0, zef0 = zeffCEZ.brems_function(itimein, Baseout_av=0.)
            self.Zeff0[jt] = zef0

            #evaluation with CEZ+CMZ (for "outside radiation" estimation)
            if hasattr(zeffCMZ, 'tvec'):
                itime = (np.abs(zeffCMZ.tvec-time)).argmin()
                #use los outside of plasma and make sure values are finite:
                ilos = (zeffCMZ.rhoLos[itime,:] > 0.99) & (np.isfinite(zeffCMZ.Base_err[itime,:]))

                # Estimate average Baseline outside Plasma: (per length of LOS)
                Baseout = zeffCMZ.calc_Base_per_m(itime, ilos)

                retro,  zef  = zeffCEZ.brems_function(itimein, Baseout_av=Baseout)
                self.Zeff[jt]  = zef



if __name__ == "__main__":


    nshot = 39649
    equ = sf.EQU(nshot)

    zcl = Zeff(equ, idz=False)

    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.title("#"+str(nshot))
    plt.plot(zcl.timeZeff, zcl.Zeff , label='$Z_{eff}$, outside: 0.99-1.1')
    plt.plot(zcl.timeZeff, zcl.Zeff0, label='$Z_{eff}$, no outside substract.')
    plt.xlabel('Time [s]')
    plt.ylabel('$Z_{eff}$')
    plt.ylim([0, 3])
    plt.legend()
    plt.show()
