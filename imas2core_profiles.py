import os, sys
import numpy as np
from aug_sfutils import read_imas

key_prof = ['Ti', 'Te', 'Ne']
spec_lbl = {(1, 1): 'H', (1, 2): 'D', (2, 4): 'He', (6, 12): 'C'}

class PROFILE:
    pass


class profiles:


    def __init__(self, shot, run):

        self.shot = shot
        self.run = run
        self.fromIMAS()
#        self.fromH5()


    def fromIMAS(self):
        
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('core_profiles')
        cp = ids.core_profiles

        self.time = cp.time
        self.rho = cp.profiles_1d[0].grid.rho_tor_norm
        nt = len(self.time)
        nrho = len(self.rho)
        main_ion0 = cp.profiles_1d[0].ion[0]
        angf_flag = False
        if hasattr(main_ion0, 'rotation_frequency_tor'):
            if len(main_ion0.rotation_frequency_tor) > 0:
                angf_flag = True
        zef_flag = hasattr(cp.profiles_1d[0], 'zeff')
        self.zef1d = np.ones(nt)
        if angf_flag:
            vkey = 'Angf'
        else:
            vkey = 'Vtor'
        keys = key_prof + [vkey]
        for key in keys:
            self.__dict__[key] = PROFILE()
            self.__dict__[key].yfit_rhot = np.zeros((nt, nrho))
            self.__dict__[key].yerr_rhot = np.zeros((nt, nrho))
            self.__dict__[key].diags   = 'IMAS'
            self.__dict__[key].xlbl    = 'rho_tor'
            self.__dict__[key].sflists = ['IDS:IMAS:%d' %self.run]
        self.xlbl    = 'rho_tor'
        self.eq_exp  = 'IDS'
        self.eq_diag = 'IMAS'
        self.eq_ed   = self.run

        self.ion_d = {}
        self.ion_d['Zmain'] = cp.profiles_1d[0].ion[0].element[0].z_n
        self.ion_d['Amain'] = cp.profiles_1d[0].ion[0].element[0].a
        self.ion_d['Zimp' ] = cp.profiles_1d[0].ion[1].element[0].z_n
        self.ion_d['Aimp' ] = cp.profiles_1d[0].ion[1].element[0].a
        self.ion_d['zef1d'] = np.zeros(nt)
        for jt in range(nt):
            self.Te.yfit_rhot[jt, :] = cp.profiles_1d[jt].electrons.temperature
            self.Ne.yfit_rhot[jt, :] = cp.profiles_1d[jt].electrons.density_thermal
            self.Ti.yfit_rhot[jt, :] = cp.profiles_1d[jt].ion[0].temperature
            self.Te.yerr_rhot[jt, :] = cp.profiles_1d[jt].electrons.temperature_error_upper
            self.Ne.yerr_rhot[jt, :] = cp.profiles_1d[jt].electrons.density_thermal_error_upper
            self.Ti.yerr_rhot[jt, :] = cp.profiles_1d[jt].ion[0].temperature_error_upper
            self.Te.units = 'eV'
            self.Ti.units = 'eV'
            self.Ne.units = '1/m^3'
            if angf_flag:
                self.Angf.yfit_rhot[jt, :] = cp.profiles_1d[jt].ion[0].rotation_frequency_tor
                self.Angf.yerr_rhot[jt, :] = cp.profiles_1d[jt].ion[0].rotation_frequency_tor_error_upper
                self.Angf.units = 'rad/s'
            else:
                self.Vtor.yfit_rhot[jt, :] = cp.profiles_1d[jt].ion[0].velocity_tor
                self.Vtor.yerr_rhot[jt, :] = cp.profiles_1d[jt].ion[0].velocity_tor_error_upper
                self.Vtor.units = 'm/s'
            if zef_flag:
                zeff = np.unique(cp.profiles_1d[jt].zeff)
                if len(zeff) == 1:
                    self.zef1d[jt] = zeff
            self.ion_d['zef1d'][jt] = cp.profiles_1d[jt].zeff[0] # assuming constant Zeff profile


    def fromH5(self):
        
        cp_ids = read_imas.read_imas_h5(self.shot, self.run, branch='core_profiles')
        cp = cp_ids['core_profiles']

        self.time = cp['time'][:]
        nt = len(self.time)
        self.rho = cp['profiles_1d[]&grid&rho_tor_norm'][0, :]

        angf_flag = False
        if 'profiles_1d[]&ion[]&rotation_frequency_tor' in cp.keys():
            if len(cp['profiles_1d[]&ion[]&rotation_frequency_tor'][:]) > 0:
                angf_flag = True
        zef_flag = ('profiles_1d[]&zeff' in cp.keys())
        self.zef1d = np.ones(nt)

        if angf_flag:
            vkey = 'Angf'
        else:
            vkey = 'Vtor'
        keys = key_prof + [vkey]
        for key in keys:
            self.__dict__[key] = PROFILE() # empty instance
            self.__dict__[key].diags = 'IMAS'
            self.__dict__[key].sflists = ['IDS:IMAS:%d' %self.run]
        self.xlbl    = 'rho_tor'
        self.eq_exp  = 'IDS'
        self.eq_diag = 'IMAS'
        self.eq_ed   = self.run
            
        self.ion_d = {}
        self.ion_d['Zmain'] = cp['profiles_1d[]&ion[]&element[]&z_n'][0, 0, 0]
        self.ion_d['Amain'] = cp['profiles_1d[]&ion[]&element[]&a'  ][0, 0, 0]
        self.ion_d['Zimp' ] = cp['profiles_1d[]&ion[]&element[]&z_n'][0, 1, 0]
        self.ion_d['Aimp' ] = cp['profiles_1d[]&ion[]&element[]&a'  ][0, 1, 0]
        self.ion_d['zef1d'] = cp['profiles_1d[]&zeff'][:, 0] # assuming constant Zeff profile
        self.Te.yfit_rhot = cp['profiles_1d[]&electrons&temperature'][:]
        self.Ne.yfit_rhot = cp['profiles_1d[]&electrons&density'][:]
        self.Ti.yfit_rhot = cp['profiles_1d[]&ion[]&temperature'][:, 0, :]
        self.Te.units = 'eV'
        self.Ti.units = 'eV'
        self.Ne.units = '1/m^3'
        if angf_flag:
            self.Angf.yfit_rhot = cp['profiles_1d[]&ion[]&rotation_frequency_tor'][:, 0, :]
            self.Angf.units = 'rad/s'
        else:
            self.Vtor.yfit_rhot = cp['profiles_1d[]&ion[]&velocity_tor'][:, 0, :]
            self.Vtor.units = 'm/s'
        if zef_flag:
            zeff = np.unique(cp['profiles_1d[]&zeff'][:])
            if len(zeff) == 1:
                self.zef1d = zeff

        
if __name__ == '__main__':

    shot = 38384
    run = 6
    cp = profiles(shot, run)

    print(cp.Te.yfit_rhot[0, :])
