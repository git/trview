import os, logging
import numpy as np
from scipy.interpolate import interp1d
import aug_sfutils as sf

logger = logging.getLogger('trview.read_prof')
#logger.setLevel(logging.INFO)

def dd2read(sf_line, equ, gpar):


# Get input

    nshot = gpar.shot
    exp, diag, sig, ed = sf_line.split(':')
    ed = int(ed)

# Check diag features
    f_diags = '%s/diags.dat' %os.path.dirname(os.path.realpath(__file__))
    u_in = open(f_diags, 'rt')
    pre = None
    for line in u_in:
        if (diag in line) and (' '+sig+' ' in line):
            _, _, pre, rad, rlbl, zlbl, err_sig = line.split()
            break
    u_in.close()

    if pre is None:
        raise Exception('diag: %s sig: %s was not found in %s'%(diag, sig, f_in))

# Open shotfile
    dsig = '%s/%s' %(diag, sig)

    diasf = sf.SFREAD(nshot, diag, exp=exp, ed=ed)

    if diasf.status:

        tim = diasf.gettimebase(sig)
        if tim is None:
            logger.error('No TimeBase found for signal group %s for shot %d' %(dsig, nshot))
            return None
        logger.debug(sf.__version__)
        darr_sgr = diasf.getobject(sig).copy()
        if darr_sgr is None:
            logger.error('Signal group %s not found for shot %d' %(dsig, nshot) )
            return None

# Error bars

        if err_sig in ('', 'guess', 'None', 'none', None):
   
            err_sgr = np.abs(darr_sgr)*0.05     # universal guess of 5% error
            darr_sgr[~np.isfinite(darr_sgr)] = 0
        else:
            err_sgr = diasf.getobject(err_sig).copy()
            err_sgr [~np.isfinite(darr_sgr)] = np.infty
            darr_sgr[~np.isfinite(darr_sgr)] = np.nan
            err_sgr [~np.isfinite(err_sgr) ] = np.infty
#            err_sgr [err_sgr == 0] = 0.05*np.abs(darr_sgr[err_sgr == 0])
            err_sgr [err_sgr == 0] = np.infty # solved vtor bug for high t-sampling!!

        if diag in ('DCR',):
            lam = diasf.getobject('lambda')

    else:
        logger.error('Shotfile not found for diagnostic %s for shot %d' %(diag, nshot))
        return None

    (t_ind, ) = np.where((tim >= gpar.tbeg) & (tim <= gpar.tend))
    tarr = tim[t_ind]
    ntim = len(tarr)

    rels = diasf.sf.__dict__[sig].relations
    if (diasf.sf.__dict__[rels[0]].objectType == 13) and (diasf.sf.__dict__[rels[1]].objectType == 8):
            darr_sgr = darr_sgr.T
            err_sgr  = err_sgr.T
    nx = darr_sgr.shape[1]

    logger.info('Diag %s    nt=%d    #chn %d' %(diag, ntim, nx))

    darr_2d = darr_sgr[t_ind, :]
    if err_sgr is not None:
        darr_2d_err = err_sgr[t_ind, :]


# Radial coordinates

#---------
# R, z
#---------

    if rad == 'e':

        if diag in ('CEZ', 'CHZ', 'CMZ', 'CPZ', 'CES', 'CAS', 'CAZ', 'COZ', 'CUZ'):
# Get {R(#ch), z(#ch)}

            cx = sf.SFREAD(nshot, diag, exp=exp, ed=ed)
            if cx.status:

                rmaj = cx.getobject('R_time')
                if rmaj is None:
                    rdat = cx.getobject(rlbl)[:, 0]
                    zdat = cx.getobject(zlbl)[:, 0]
                    nt = len(tim)
                    nx = len(rdat)
                    rmaj = np.tile(rdat, nt).reshape((nt, nx))
                    zin  = np.tile(zdat, nt).reshape((nt, nx))
                else:
                    zin  = cx.getobject('z_time').T
                    rmaj = rmaj.T

            rmaj = rmaj[t_ind, :]
            zin  = zin[t_ind, :]
            ind = [j for j in range(rmaj.shape[1]) if np.max(rmaj[:, j]) > 0]     #problems with unused channels
            rmaj = rmaj[..., ind]
            zin = zin[..., ind]
            darr_2d = darr_2d[:, ind]
            darr_2d_err = darr_2d_err[:, ind]

            if sig == 'vrot' and gpar.angf:
                darr_2d     /= rmaj
                darr_2d_err /= rmaj
                darr_2d.phys_unit = 'rad/s'

        if diag == 'VTA' or diag == 'VTN':
            ind = darr_2d <= 0.1
            darr_2d[ind] = 0
            darr_2d_err[ind] = np.infty     #wrong points 
# Get {R(t), z(#ch)}
            ts = sf.SFREAD(nshot, diag, exp=exp, ed=ed)
            if ts.status:
                rmaj = ts.getobject(rlbl)
                zin  = ts.getobject(zlbl)

        if diag in ('CEC', 'RMD'):
 # Get {R(t, #ch), z(t, #ch)}
            ece = sf.SFREAD(nshot, diag, exp=exp, ed=ed)
            if ece.status:
                timr = ece.gettimebase(rlbl)
                rdat = ece.getobject(rlbl)
                zdat = ece.getobject(zlbl)

            interp_r = interp1d(timr, rdat, axis=0, copy=False, \
                                bounds_error=False, fill_value=0)
            interp_z = interp1d(timr, zdat, axis=0, copy=False, \
                                bounds_error=False, fill_value=0)

            darr_2d_err = darr_2d*0.05 # estimate 5% error
            rmaj = interp_r(tarr)
            zin  = interp_z(tarr)
            ntim = len(tarr)

            if err_sig == 'guess':
# Guess the errorbars
                mai = sf.SFREAD(nshot, 'MAI')
                if mai.status:
                    Bt = mai.getobject('BTF')
                    Rt_tvec = mai.gettimebase('BTF')[:len(Bt)]
                    Bt = np.mean(Bt[( Rt_tvec > tarr.min() ) &( Rt_tvec < tarr.max() )])

                R = np.median(rmaj, axis=0)

                from trview import CEC_correction

                Te_corr = CEC_correction.correction(darr_2d, None, Bt, R, nshot)
                darr_2d_err = np.hypot(Te_corr-darr_2d, 0.03*Te_corr)
                darr_2d_err[np.isnan(Te_corr)] = np.infty

        if ntim == 0:
            logger.error('No measurement within the time range')
            return None

        if diag == 'VTA' or diag == 'VTN':
            rmaj = np.tile(rmaj[t_ind], (len(zin), 1)).T

        logger.info('Mapping {R, z} -> rho')

        rho = sf.rz2rho(equ, rmaj, zin, t_in=tarr, coord_out='rho_pol')

#---------------------
# Already rho_pol base
#---------------------

    if rad == 'p':

        logger.info('Getting rho base')

        if sig == 'vt':
            if not gpar.angf:
                return None
            darr_2d.phys_unit = 'rad/s'

 # Get {rho(#ch, t)}
        if diag == 'DCR':
            ## BEN: set number of DCR points to 15!!
            rhop = np.linspace(0, 1.1, 15)
            darr_2d = np.polyval(darr_2d[:, ::-1].T, rhop[:, None]).T
            darr_2d[:, rhop > 1] *= np.exp( -np.outer(lam[t_ind], rhop[rhop>1]-1))
            ## BEN: set statisictal error to 10%!!
            darr_2d_err = np.abs(darr_2d*0.1)
            rho = np.tile(rhop, (ntim, 1))

        else:
            pol = sf.SFREAD(nshot, diag, ed=ed, exp=exp)
            if pol.status:
                if diag == 'FRS':
                    rhop = pol.getobject('rhop')
                else:
                    rhop = pol.getareabase(sig)

                if diag in ('DLP', 'DPR'): #, 'PED'):
                    rho = np.tile(rhop, (ntim, 1))
                else:
                    rhop = np.atleast_2d(rhop)
                    if diag in ('IDA', 'IDI'):
                        rho = rhop.T[t_ind, :]
                    else:
                        rho = rhop[t_ind, :]

            else:
                logger.error('Shotfile %s not found for shot %d' %(diag, nshot))
                return None
 
        if (diag == 'IDZ') and (sig == 'Zeff'):
            if darr_2d_err is not None:
                darr_2d_err[~np.isfinite(darr_2d_err)] = 1  #replace infinite errors by finite but large 

    if pre in ('Ti', 'Te', 'Ne'):
        ind = (darr_2d < 1e-6)
        if darr_2d_err is not None:
            darr_2d_err[ind] = np.infty     # faulty measurements

    out_d = {'tgrid': tarr, 'rhop': rho, 'data': darr_2d, \
             'data_err': darr_2d_err, 'ed': diasf.ed, 'unit': darr_2d.phys_unit}

    return out_d
