Repository of the TRVIEW GUI package.

TRVIEW is a comprehensive tool to read/manipulate plasma parameters, profiles and equilibria from AUG shotfiles or IMAS. It is coupled to RABBIT, TORBEAM, NEMEC and FEQIS, and it can store input files for ASTRA and TRANSP.

Clone:
```
  git clone https://gitlab.mpcdf.mpg.de/git/trview.git
```

Documentation:  
https://gitlab.mpcdf.mpg.de/git/trview/-/wikis/TRVIEW-manual


Reference:  
G. Tardini et al,  
"A package to bridge experimental tokamak data to modelling workflows for heating and transport"  
Nuclear Fusion 64 (2024)  
https://iopscience.iop.org/article/10.1088/1741-4326/ad346f
