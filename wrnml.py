import os, fnmatch, shutil, logging
import numpy as np
import aug_sfutils as sf
from trview import ufiles, tr_path, transp_nml

logger = logging.getLogger('trview.wrnml')


def wr_arr(lbl, arr, fmt='%6.1f'):

    logger.debug(lbl)
    arr_nml = '%-14s = ' %lbl
    for val in arr[:-1]:
        arr_nml += (fmt %val)
        arr_nml += ', '
    arr_nml += (fmt %arr[-1])
    arr_nml +='\n'

    return arr_nml


def sym_prof(uname):

    uf = ufiles.UFILE(fin=uname)
    rho_lbl = uf.Y['label'].strip()
    if rho_lbl == 'rho_pol':
        sym_prof = -7
    elif rho_lbl == 'rho_tor':
        sym_prof = -5
    return sym_prof


def cpuf(runid):

    tr = tr_path.TR_PATH(runid)
    nshot = int(runid[:-3])
    s5shot = '%05d' %nshot
    preext_io = [
            ('M'  , 'MRY'), 
            ('SM' , 'CUR'), 
            ('S'  , 'RBZ'), 
            ('U'  , 'VSF'), 
            ('P'  , 'NBI'), 
            ('P'  , 'ICH'), 
            ('P'  , 'ECH'), 
            ('ICR', 'MINL'), 
            ('ICR', 'MINR'), 
            ('PHITR', 'ECH'), 
            ('Q'  , 'QPR'),
            ('THETR', 'ECH'), 
            ('Z'  , 'ZEF')
        ]

    for pre, ext in preext_io:
        ufile = '%s%s.%s'     %(pre, s5shot, ext)
        source = '%s/%s'      %(tr.udb    , ufile)
        target = '%s/%s'      %(tr.run_dir, ufile)
        source2 = '%s/%s_AVG' %(tr.udb    , ufile)
        target2 = '%s/%s_AVG' %(tr.run_dir, ufile)
        if os.path.isfile(source):
            logger.info('cp %s %s' %(source, target))
            shutil.copy2(source, target)
        if os.path.isfile(source2):
            logger.info('cp %s %s' %(source2, target2))
            shutil.copy2(source2, target2)

# Profiles
    for fudb in os.listdir(tr.udb):
        for pre in ('TE', 'TI', 'NE', 'ANGF'):
            if fnmatch.fnmatch(fudb, '%s%s.*' %(pre, s5shot)):
                try:
                    a = int(fudb[-1])
                except:
                    logger.debug(fudb)
                    source = '%s/%s' %(tr.udb, fudb)
                    target = '%s/%s' %(tr.run_dir, fudb)
                    logger.info('cp %s %s' %(source, target))
                    shutil.copy2(source, target)


def w_nml(plasma, gtra):

    lev_geo = '6' # 6 computed, 8 prescribed
    mcrf  = gtra.mcrf
    nshot = plasma.gpar.shot
    s5shot = '%05d' %nshot
    lett = 'A'
    nrun = 1
    runid = '%sA01' %s5shot
    tr = tr_path.TR_PATH(runid)
    nml_out = tr.fnml
    while os.path.isfile(nml_out):
        nrun +=  1
        runid = '%sA%02d' %(s5shot, nrun)
        tr = tr_path.TR_PATH(runid)
        nml_out = tr.fnml

    os.system('mkdir -p %s' %tr.udb)
    os.system('mkdir -p %s' %tr.run_dir)

#--------------
# Fields' signs
#--------------

    ipl_ccw = 'T'
    btf_ccw = 'F'
    fpc = sf.SFREAD(nshot, 'FPC')
    if fpc.status:
        ipl = fpc.getobject('IpiFP')
        btf = fpc.getobject('BTF')
        if np.sum(ipl) < 0:
            ipl_ccw = 'F'
        if np.sum(btf) > 0:
            btf_ccw = 'T'

#---------
# Profiles
#---------

    if len(plasma.profiles.Te.sflist) == 0:
        raise Exception('Missing Te profile')
    if len(plasma.profiles.Ne.sflist) == 0:
        raise Exception('Missing ne profile')

    sym_txt = ''
    ext_txt = ''

# Te

    ext = plasma.profiles.Te.diags
    ext_txt += "preter = 'TE'\n"  
    ext_txt += "extter = '%s'\n" %ext
    uname = '%s/TE%s.%s' %(tr.udb, s5shot, ext)
    if os.path.isfile(uname):
        sym_txt += 'nriter = %d\n' %sym_prof(uname)
        sym_txt += 'nsyter = 0\n'

# ne

    ext = plasma.profiles.Ne.diags
    ext_txt += "prener = 'NE'\n"
    ext_txt += "extner = '%s'\n" %ext 
    uname = '%s/NE%s.%s' %(tr.udb, s5shot, ext)
    if os.path.isfile(uname):
        sym_txt += 'nriner = %d\n' %sym_prof(uname)
        sym_txt += 'nsyner = 0\n'
    
# Ti

    ti_txt = ''
    if len(plasma.profiles.Ti.sflist) > 0:
        ext = plasma.profiles.Ti.diags
        uname = '%s/TI%s.%s' %(tr.udb, s5shot, ext)
        if os.path.isfile(uname):
            ti_txt += 'nlti2 = T\n'
            ti_txt += 'fiefac = 1.0\n'
            ti_txt += 'giefac = 0.0\n'
            ext_txt += "preti2 = 'TI'\n"  
            ext_txt += "extti2 = '%s'\n" %ext 
            sym_txt += 'nriti2 = %d\n' %sym_prof(uname)
            sym_txt += 'nsyti2 = 0\n'
        else:    # Ti = Te
            ti_txt += 'nlti2 = F\n'
            ti_txt += 'fiefac = 0.0\n'
            ti_txt += 'giefac = 1.0\n'
    else:    # Ti = Te
        ti_txt += 'nlti2 = F\n'
        ti_txt += 'fiefac = 0.0\n'
        ti_txt += 'giefac = 1.0\n'
        
# vtor

    vt_txt = ''
    if len(plasma.profiles.Angf.sflist) > 0:
        ext = plasma.profiles.Angf.diags
        uname = '%s/ANGF%s.%s' %(tr.udb, s5shot, ext)
        if os.path.isfile(uname):
            vt_txt += 'nlvphi = T\n'
            vt_txt += 'nlomgvtr = T\n'
            ext_txt += "preomg = 'ANGF'\n"  
            ext_txt += "extomg = '%s'\n" %ext 
            sym_txt += 'nriomg = %d\n' %sym_prof(uname)
            sym_txt += 'nsyomg = 0\n'
    else:    # No rotation
        vt_txt += 'nlvphi = F\n'

#----
# FBM
#----

    fbm_str = ''
    if hasattr(gtra, 'avgtim'):
        avgtim = gtra.avgtim
        tbeg = plasma.gpar.tbeg
        tend = plasma.gpar.tend
        outtim = np.sort([float(x) for x in gtra.outtim.split()])
        (ind, ) = np.where((outtim > plasma.gpar.tbeg + gtra.avgtim) & (outtim <= plasma.gpar.tend))
        outtim = outtim[ind]
        if len(outtim) > 0:
            fbm_str += 'MTHDAVG=2\n'
            fbm_str += 'AVGTIM=%8.4f\n' %avgtim
            fbm_str += 'NLDEP0_GATHER=T\n'
            for jfb, tfbm in enumerate(outtim):
                fbm_str += 'OUTTIM(%d) =%6.3f\n' %(jfb + 1, tfbm)
    if mcrf:
        ebdmax = 5.e5
    else:
        ebdmax = 1.5e5

#----------------
# Namelist string
#----------------

    tres_in = min(gtra.tres, 0.01)
    nmlvars = [nshot, plasma.gpar.tbeg, plasma.gpar.tend, tres_in, tres_in, \
        tres_in, tres_in, btf_ccw, ipl_ccw, nshot, fbm_str, ti_txt, vt_txt, \
        plasma.ion_d['Amain'], plasma.ion_d['Zmain'], \
        plasma.ion_d['Aimp' ], plasma.ion_d['Zimp' ], \
        sym_txt, ext_txt, gtra.nmc, gtra.dtbeam, gtra.dtbeam, ebdmax]

    nmltuple = tuple(nmlvars)
    nml = transp_nml.main %nmltuple

#------------
# Docu EQU sf
#------------

    uname = '%s/M%s.MRY' %(tr.udb, s5shot)
    if os.path.isfile(uname):
        fu = open(uname, 'r')
        eq = {}
        for line in fu.readlines():
            for eqstr in ('exp', 'dia', 'ed'):
                if eqstr in line:
                    eq[eqstr] = line.split('=')[1].strip()
        for key, val in eq.items():
            nml = nml.replace('$eq_' + key, val)

#-----------------------
# Append NBI, ECRH, ICRH
#-----------------------

    pfile = '%s/P%s' %(tr.udb, s5shot)
    pnbi = pfile + '.NBI'
    pech = pfile + '.ECH'
    pich = pfile + '.ICH'

    if os.path.isfile(pnbi):
        tr_nbi_nml = nbi2nml(plasma.nbi, ipl_ccw=ipl_ccw)
        logger.info('Appending NBI to namelist')
        nml += '\n' + tr_nbi_nml
    else:
        logger.info('%s not found!!' %pnbi)
    if os.path.isfile(pech) and hasattr(plasma, 'ech'):
        tr_ech_nml = ech2nml(plasma.ech)
        logger.info('Appending ECRH to namelist')
        nml += '\n' + tr_ech_nml
    if os.path.isfile(pich) and hasattr(plasma, 'icr'):
        tr_ich_nml = ich2nml(plasma.icr, mcrf)
        logger.info('Appending ICRH to namelist')
        nml += '\n' + tr_ich_nml

# Write namelist

    u_out = open(nml_out, 'w')
    u_out.write(nml)
    u_out.close()

# Copy ufile to TRANSP working dir

    cpuf(runid)

    logger.info('Output namelist %s', nml_out)

    return runid


def nbi2nml(nbi, ipl_ccw='T'):

    if not hasattr(nbi, 'rtcena'):
        nbi.aug_tr()

    nnbi = len(nbi.einja)

    nbi_old_new = 14050
    if nbi.shot <= nbi_old_new:
        tr_nbi_nml = transp_nml.nbi_old
    else:
        tr_nbi_nml = transp_nml.nbi

    nbvars = []
    for jx in range(nnbi):
        nbvars.append(ipl_ccw)
        if jx in (5, 6) and nbi.shot > nbi_old_new: # Variable NBI geometry for sources 6, 7
            nbvars.append(nbi.xlbtna[jx])
            nbvars.append(nbi.xlbapa[jx])
            nbvars.append(nbi.xybapa[jx])
        nbvars.append(nbi.abeam[jx])
        nbvars.append(nbi.zbeam[jx])
        nbvars.append(nbi.einja[jx])
        nbvars.append(nbi.ffulla[jx])
        nbvars.append(nbi.fhalfa[jx])
    nbtuple = tuple(nbvars)

    return tr_nbi_nml %nbtuple


def ech2nml(ech):

    nshot = ech.shot

    if not hasattr(ech, 'p_ecs'):

        return '! No ECRH'

    ngy = len(ech.freq)

    tr_ech_nml = transp_nml.ech %ngy

    if hasattr(ech, 'freq'):    
        tr_ech_nml +=  wr_arr('FREQECH', ech.freq, fmt='%10.3e')
    else:
        tr_ech_nml += 'FREQECH error\n'

    tr_ech_nml += wr_arr('XECECH', 100.*ech.R)
    tr_ech_nml += wr_arr('ZECECH', 100.*ech.z)
    tr_ech_nml += wr_arr('ECB_WIDTH_HOR' , 100.*ech.width_y)
    tr_ech_nml += wr_arr('ECB_WIDTH_VERT', 100.*ech.width_z)
    tr_ech_nml += wr_arr('ECB_CURV_HOR'  , 100.*ech.curv_y)
    tr_ech_nml += wr_arr('ECB_CURV_VERT' , 100.*ech.curv_z)
    logger.debug(ech.rfmod)
    tr_ech_nml += wr_arr('RFMODECH', ech.rfmod)

    return tr_ech_nml


def ich2nml(icr, mcrf):

    freqicha_str = 'FRQicha = '
    for freq in icr.freq:
        freqicha_str += '%12.5e, ' %freq
    freqicha_str = freqicha_str[:-2]
    tr_ich_nml = transp_nml.ich %freqicha_str
    if mcrf: #MC kick-operator
        for target in ('NLFI_MCRF', 'NLFI_ORBRZ', 'EDBFAC', 'NZNBME'):
            source = '!' + target
            tr_ich_nml = tr_ich_nml.replace(source, target)
    return tr_ich_nml
