#!/usr/bin/env python

import os, sys, json, logging, traceback, shutil, webbrowser

from PyQt5 import QtWidgets as qtw
from PyQt5.QtWidgets import QToolTip
from PyQt5.QtGui import QPixmap, QIcon, QDoubleValidator, QFont
from PyQt5.QtCore import Qt, QRect, QSize, QLocale
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import numpy as np
from trview import config, prof_names, fconf, ufiles, \
    torbeam_exec, rabbit_exec, nemec_exec, feqis_pbe, \
    code_io, get_exp, eqdsk, mom2rz, rhop2rhot, \
    write_u, astra_nml_u, astra_exp, wrnml, fit_prof

try:
    import aug_sfutils as sf
except:
    pass

colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
col = ['#c00000', '#00c000', '#0000c0', '#b0b000', '#b000b0', '#00b0b0', '#000000']

titleFont = QFont()
titleFont.setPointSize(12)
titleHeight = 50
tabFont = QFont()
tabFont.setPointSize(10)

os.environ['BROWSER'] = config.browser

usLocale = QLocale('us')

encoder = '/mpcdf/soft/SLE_15/packages/x86_64/ffmpeg/4.4.0/bin/ffmpeg'
if not os.path.exists(encoder):
    encoder = '/usr/bin/ffmpeg'

docu_url = 'https://gitlab.mpcdf.mpg.de/git/trview/-/wikis/TRVIEW-manual'
logger = logging.getLogger('trview.gui')

try:
    from trview import trv2imas
except:
    traceback.print_exc()
    logger.warning('Missing IMAS output, do:')
    logger.warning('   on IPP-toki: module load trview')
    logger.warning('   on gateway : module load cineca trview')

# Font sizes

titleSize  = 22
titleSize2 = 18
textSize   = 18
fontSize   = 18
fontSize2  = 14
tickSize   = 18
tickSize2  = 14
lWid       = 2.5
mSize      = 7.0

# For the paper

titlePaper    = 32
fontSizePaper = 28
tickSizePaper = 28
lwPaper = 3.5
legPaper = 22
border_wid = 3

heat_dir = os.path.dirname(os.path.realpath(__file__))
heat_user_dir = '%s/trview' %os.getenv('SHARES_USER_DIR')
json_dir = '%s/settings' %heat_user_dir
if not os.path.isdir(heat_user_dir):
    os.mkdir(heat_user_dir)
if not os.path.isdir(json_dir):
    os.mkdir(json_dir)
json_default = '%s/default.json' %json_dir

sepFit_ErrorMessage = {1: 'Close to ELM', 2: 'max R > 2.2', 3: 'min z < -1.1', 4: 'max z > 1', \
    5: 'All moments <= 0', 6: 'min z-fit < -1.1'}


class GUI_TAB:
    pass


class Slider(qtw.QSlider):
    def mousePressEvent(self, event):
        super(Slider, self).mousePressEvent(event)
        if event.button() == Qt.LeftButton:
            val = self.pixelPosToRangeValue(event.pos())
            self.setValue(val)

    def pixelPosToRangeValue(self, pos):
        opt = qtw.QStyleOptionSlider()
        self.initStyleOption(opt)
        gr = self.style().subControlRect(qtw.QStyle.CC_Slider, opt, qtw.QStyle.SC_SliderGroove, self)
        sr = self.style().subControlRect(qtw.QStyle.CC_Slider, opt, qtw.QStyle.SC_SliderHandle, self)

        sliderLength = sr.width()
        sliderMin = gr.x()
        sliderMax = gr.right() - sliderLength + 1

        pr = pos - sr.center() + sr.topLeft()
        p = pr.x()
        return qtw.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), \
            p - sliderMin, sliderMax - sliderMin, opt.upsideDown)


class TRV_GUI(qtw.QMainWindow):


    def __init__(self, tok='AUGD', nml_init=config.rabbitNml_AUG):

        super().__init__()

        self.setLocale(usLocale)

        self.message()

        self.tok = tok
        if self.tok == 'AUGD':
            self.gc_d = sf.getgc()

        height = qtw.QDesktopWidget().screenGeometry(-1).height()

        ywin  = min(1100, int(0.95*height))
        xwin  = int(ywin*1.88)
        yhead = 44
        ypbar = 20
        ynavi = 45
        dynavi = int(1.6*ynavi)

        ybody = ywin-yhead-dynavi-ypbar
        yframes = ybody - 75

        head = qtw.QWidget(self)
        body = qtw.QWidget(self)
        navi = qtw.QWidget(self)
 
        head.setGeometry(QRect(0,           0, xwin,  yhead))
        body.setGeometry(QRect(0,       yhead, xwin,  ybody))
        navi.setGeometry(QRect(0, ywin-dynavi, xwin, dynavi))
        header_grid = qtw.QHBoxLayout(head) 
        navi_grid   = qtw.QHBoxLayout(navi)
        navi_exit = qtw.QWidget(self)
        navi_rab  = qtw.QWidget(self)
        navi_tbm  = qtw.QWidget(self)
        navi_view = qtw.QWidget(self)
        navi_grid.addWidget(navi_exit, alignment=Qt.AlignLeft)
        navi_grid.addWidget(navi_rab , alignment=Qt.AlignLeft)
        navi_grid.addWidget(navi_tbm , alignment=Qt.AlignLeft)
        navi_grid.addWidget(navi_view, alignment=Qt.AlignRight)

        self.tabs = qtw.QTabWidget(body)
        self.tabs.setGeometry(QRect(0, 0, xwin, ybody))


        set_tabs = qtw.QTabWidget()
        self.exp_tabs = qtw.QTabWidget()
        self.rab_tabs = qtw.QTabWidget()
        self.tbm_tabs = qtw.QTabWidget()
        self.vmc_tabs = qtw.QTabWidget()
        self.feq_tabs = qtw.QTabWidget()
        self.tabs.addTab(set_tabs, 'Settings')
        self.tabs.addTab(self.exp_tabs, 'Input')
        self.tabs.addTab(self.rab_tabs, 'RABBIT output')
        self.tabs.addTab(self.tbm_tabs, 'TORBEAM output')
        self.tabs.addTab(self.vmc_tabs, 'NEMEC output')
        self.tabs.addTab(self.feq_tabs, 'FEQIS output')

        css_tab = "QTabBar::tab {width: 140;}"
        self.tabs.setStyleSheet(css_tab)
        css_exp = "QTabBar::tab {width: 110;}"
        self.exp_tabs.setStyleSheet(css_exp)
        self.rab_tabs.setStyleSheet(css_tab)
        self.tbm_tabs.setStyleSheet(css_tab)
        self.vmc_tabs.setStyleSheet(css_tab)
        self.feq_tabs.setStyleSheet(css_tab)

        exp_tab = qtw.QWidget()
        tra_tab = qtw.QWidget()
        rab_tab = qtw.QWidget()
        tbm_tab = qtw.QWidget()
        vmc_tab = qtw.QWidget()
        feq_tab = qtw.QWidget()

        set_tabs.addTab(exp_tab, 'Exp settings')
        set_tabs.addTab(tra_tab, 'TRANSP settings')
        set_tabs.addTab(rab_tab, 'RABBIT settings')
        set_tabs.addTab(tbm_tab, 'TORBEAM settings')
        set_tabs.addTab(vmc_tab, 'NEMEC settings')
        set_tabs.addTab(feq_tab, 'FEQIS settings')

# Menubar

        menubar = self.menuBar()
        fileMenu   = qtw.QMenu('&File', self)
        jsonMenu   = qtw.QMenu('&Setup', self)
        rbsaveMenu = qtw.QMenu('&RABBIT output', self)
        tbsaveMenu = qtw.QMenu('&TORBEAM output', self)
        helpMenu   = qtw.QMenu('&Help', self)
        menubar.addMenu(fileMenu)
        menubar.addMenu(jsonMenu)
        menubar.addMenu(rbsaveMenu)
        menubar.addMenu(tbsaveMenu)
        menubar.addMenu(helpMenu)

        getSfAction   = qtw.QAction('&Read/fit AUG SF', fileMenu)
        getImasAction = qtw.QAction('&Read IMAS', fileMenu)
        rbRunAction   = qtw.QAction('&Run RABBIT', fileMenu)
        tbRunAction   = qtw.QAction('&Run TORBEAM', fileMenu)
        vmRunAction   = qtw.QAction('&Run NEMEC', fileMenu)
        fqRunAction   = qtw.QAction('&Run FEQIS', fileMenu)
        transpAction  = qtw.QAction('&Write TRANSP input', fileMenu)
        astraAction   = qtw.QAction('&Write ASTRA input', fileMenu)
        eqdskAction   = qtw.QAction('&Dump EQDSK', fileMenu)
        imasAction    = qtw.QAction('&Fill IDS tree', fileMenu)
        exitAction    = qtw.QAction('&Exit', fileMenu)
        getSfAction.triggered.connect(self.expFromShotfile)
        getImasAction.triggered.connect(self.expFromIMAS)
        rbRunAction.triggered.connect(self.exec_rabbit)
        tbRunAction.triggered.connect(self.exec_torbeam)
        vmRunAction.triggered.connect(self.exec_nemec)
        fqRunAction.triggered.connect(self.exec_feqis)
        transpAction.triggered.connect(self.writeTranspInput)
        astraAction.triggered.connect(self.writeAstraInput)
        eqdskAction.triggered.connect(self.writeEqdsk)
        imasAction.triggered.connect(self.fill_ids)
        exitAction.triggered.connect(sys.exit)

        fileMenu.addAction(getSfAction)
        if 'imas' in sys.modules:
            fileMenu.addAction(getImasAction)
            fileMenu.addAction(imasAction)
        fileMenu.addAction(rbRunAction)
        fileMenu.addAction(tbRunAction)
        fileMenu.addAction(vmRunAction)
        fileMenu.addAction(fqRunAction)
        fileMenu.addAction(transpAction)
        fileMenu.addAction(astraAction)
        fileMenu.addAction(eqdskAction)
        fileMenu.addSeparator()
        fileMenu.addAction(exitAction)

        rbcdfAction = qtw.QAction('&Write RABBIT NetCDF'    , rbsaveMenu)
        rbwsfAction = qtw.QAction('&Write RBT shotfile'     , rbsaveMenu)
        rbu1rAction = qtw.QAction('&Store 1D u-file(rho)'   , rbsaveMenu)
        rbu1tAction = qtw.QAction('&Store 1D u-file(t)'     , rbsaveMenu)
        rbu2dAction = qtw.QAction('&Store 2D u-file(t, rho)', rbsaveMenu)
        rbmovAction = qtw.QAction('&Store movie', rbsaveMenu)
        rbcdfAction.triggered.connect(self.write_rabbit_cdf)
        rbwsfAction.triggered.connect(self.write_rabbit_sf)
        rbu1rAction.triggered.connect(self.uf1r_rabbit)
        rbu1tAction.triggered.connect(self.uf1t_rabbit)
        rbu2dAction.triggered.connect(self.uf2_rabbit)
        rbmovAction.triggered.connect(self.smov_rabbit)

        rbsaveMenu.addAction(rbcdfAction)
        if self.tok == 'AUGD':
            rbsaveMenu.addAction(rbwsfAction)
        rbsaveMenu.addAction(rbu1rAction)
        rbsaveMenu.addAction(rbu1tAction)
        rbsaveMenu.addAction(rbu2dAction)
        rbsaveMenu.addAction(rbmovAction)

        tbcdfAction = qtw.QAction('&Write TORBEAM NetCDF', tbsaveMenu)
        tbsaveMenu.addAction(tbcdfAction)

        loadAction  = qtw.QAction('&Load Setup', jsonMenu)
        saveAction  = qtw.QAction('&Save Setup', jsonMenu)
        jsonMenu.addAction(loadAction)
        jsonMenu.addAction(saveAction)
        loadAction.triggered.connect(self.load_json)
        saveAction.triggered.connect(self.save_json)

        aboutAction = qtw.QAction('&Web docu', helpMenu)
        aboutAction.triggered.connect(self.about)
        helpMenu.addAction(aboutAction)

        header_grid.addWidget(menubar, 0, alignment=Qt.AlignLeft)

# Icons toolbar

        tool_map = {'rabbit': self.exec_rabbit, 
            'netcdf_rb': self.write_rabbit_cdf, 'save_sf': self.write_rabbit_sf, \
            'torbeam': self.exec_torbeam, 
            'netcdf_tb': self.write_torbeam_cdf, \
            'exit': sys.exit}
        tool_tips = {'rabbit': 'Collect input & execute RABBIT', \
            'torbeam': 'Collect input & execute TORBEAM', \
            'netcdf_rb': 'Store RABBIT results as NetCDF', \
            'netcdf_tb': 'Store TORBEAM results as NetCDF', \
            'save_sf': 'Store RBT shotfile', \
            'exit': 'Exit'}

        self.tool_but = {}

        lbl = 'exit'
        jpos = 0
        self.tool_but[lbl] = qtw.QPushButton()
        self.tool_but[lbl].setToolTip(tool_tips[lbl])
        self.tool_but[lbl].setFixedWidth(ynavi)
        self.tool_but[lbl].setFixedHeight(ynavi)
        self.tool_but[lbl].setIcon(QIcon('%s/%s.gif' %(heat_dir, lbl)))
        self.tool_but[lbl].setIconSize(QSize(ynavi, ynavi))
        self.tool_but[lbl].clicked.connect(tool_map[lbl])
        navi_exit_layout = qtw.QHBoxLayout(navi_exit)
        navi_exit_layout.addWidget(self.tool_but[lbl], jpos)

        navi_rab_layout = qtw.QHBoxLayout(navi_rab)
        for lbl in ['rabbit', 'netcdf_rb', 'save_sf']:
            jpos += 1
            self.tool_but[lbl] = qtw.QPushButton()
            self.tool_but[lbl].setFixedWidth(ynavi)
            self.tool_but[lbl].setFixedHeight(ynavi)
            self.tool_but[lbl].setToolTip(tool_tips[lbl])
            self.tool_but[lbl].setIcon(QIcon('%s/%s.gif' %(heat_dir, lbl)))
            self.tool_but[lbl].setIconSize(QSize(ynavi, ynavi))
#            self.tool_but[lbl].setScaledContents(True)
            self.tool_but[lbl].clicked.connect(tool_map[lbl])
            navi_rab_layout.addWidget(self.tool_but[lbl], jpos)

        navi_tbm_layout = qtw.QHBoxLayout(navi_tbm)
        for lbl in ['torbeam', 'netcdf_tb']:
            jpos += 1
            self.tool_but[lbl] = qtw.QPushButton()
            self.tool_but[lbl].setFixedWidth(ynavi)
            self.tool_but[lbl].setFixedHeight(ynavi)
            self.tool_but[lbl].setToolTip(tool_tips[lbl])
            self.tool_but[lbl].setIcon(QIcon('%s/%s.gif' %(heat_dir, lbl)))
            self.tool_but[lbl].setIconSize(QSize(ynavi, ynavi))
            self.tool_but[lbl].clicked.connect(tool_map[lbl])
            navi_tbm_layout.addWidget(self.tool_but[lbl], jpos)

# Progress bar

        self.progress = Slider(Qt.Horizontal, self)
        self.progress.setGeometry(int(0.05*xwin), int(ywin-dynavi-ypbar), \
            int(0.9*xwin), ypbar)
        self.progress.setValue(0)
        self.progress.valueChanged.connect(self.update_slider)
        self.progress.setVisible(False)

# Icons navigation bar

        navi_view_layout = qtw.QHBoxLayout(navi_view)
        navi_map = {'ButtonPlay': self.play, 'ButtonBack': self.rewind, \
            'ButtonPrevious': self.previous, 'ButtonNext': self.forward}

        self.navi_but = {}
        for jpos, lbl in enumerate(navi_map.keys()):
            self.navi_but[lbl] = qtw.QPushButton()
            self.navi_but[lbl].setFixedWidth(ynavi)
            self.navi_but[lbl].setFixedHeight(ynavi)
            self.navi_but[lbl].setToolTip(lbl.replace('Button', ''))
            self.navi_but[lbl].setIcon(QIcon('%s/%s.gif' %(heat_dir, lbl)))
            self.navi_but[lbl].setIconSize(QSize(ynavi, ynavi))
            self.navi_but[lbl].clicked.connect(navi_map[lbl])
            navi_view_layout.addWidget(self.navi_but[lbl], jpos)
            self.navi_but[lbl].setVisible(False)
        self.play_pause = self.navi_but['ButtonPlay']

# User options

        with open(json_default, 'r') as fjson:
            self.gui_init_d = json.load(fjson)

        self.guiQt_d = {}
        for node in self.gui_init_d.keys():
            self.guiQt_d[node] = {}

        user = os.getenv('USER')

#-------------
# Setting TABS
#-------------

        xnml = 60
        n_xwid = int(0.3*(xwin - xnml))

        exp_tab_layout = qtw.QHBoxLayout(exp_tab)
        tra_tab_layout = qtw.QVBoxLayout(tra_tab)
        rab_tab_layout = qtw.QHBoxLayout(rab_tab)
        tbm_tab_layout = qtw.QHBoxLayout(tbm_tab)
        vmc_tab_layout = qtw.QHBoxLayout(vmc_tab)
        feq_tab_layout = qtw.QHBoxLayout(feq_tab)

#--------
# EXP tab
#--------

        exp_entries  = qtw.QWidget()
        prof_entries = qtw.QWidget()
        exp_tab_layout.addWidget( exp_entries, alignment=Qt.AlignLeft)
        exp_tab_layout.addWidget(prof_entries, alignment=Qt.AlignLeft)
        exp_tab_layout.addStretch()
        exp_entries_grid  = qtw.QGridLayout(exp_entries)
        prof_entries_grid = qtw.QGridLayout(prof_entries)

        exp_entries.frame = qtw.QFrame(exp_entries)
        exp_entries.frame.setGeometry(exp_entries.geometry())
        exp_entries.frame.setFixedHeight(yframes)
        exp_entries.frame.setLineWidth(border_wid)
        exp_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        prof_entries.frame = qtw.QFrame(prof_entries)
        prof_entries.frame.setGeometry(prof_entries.geometry())
        prof_entries.frame.setFixedWidth(955)
        prof_entries.frame.setFixedHeight(yframes)
        prof_entries.frame.setLineWidth(border_wid)
        prof_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['shot', 'tbeg', 'tend', 'dt_in', \
            'eq_exp', 'eq_diag', 'eq_ed', \
            'nrho_in', 'nthe_in', 'nmom', 'pel_trig', 'ids_run']
        cb = ['no_elm']
        combos = {'IDSbackend': ['HDF5', 'MDS+', 'ASCII'], \
                  'logs': ['Silent', 'Info', 'Debug'] }
        self.fill_widget(exp_entries_grid, 'exp', title='Exp/fit options', entries=entries, checkbuts=cb, combos=combos)
        exp_entries_grid.setRowStretch(   exp_entries_grid.rowCount()  , 1)
        exp_entries_grid.setColumnStretch(exp_entries_grid.columnCount(), 1)

#-------------------------
# Diag choice + fit method
#-------------------------

        self.cbvar = {}
        self.fitTolerance = {}
        self.sigmaReject  = {}

        ne_widget = self.diag_choice('Ne')
        te_widget = self.diag_choice('Te')
        ti_widget = self.diag_choice('Ti')
        vt_widget = self.diag_choice('Angf')
        prof_entries_grid.addWidget(ne_widget, 0, 0)
        prof_entries_grid.addWidget(te_widget, 0, 1)
        prof_entries_grid.addWidget(ti_widget, 1, 0)
        prof_entries_grid.addWidget(vt_widget, 1, 1)

#-----------
# RABBIT tab
#-----------

        rab_entries  = qtw.QWidget()
        rab_namelist = qtw.QWidget()
        rab_tab_layout.addWidget(rab_entries , alignment=Qt.AlignLeft)
        rab_tab_layout.addWidget(rab_namelist, alignment=Qt.AlignLeft)
        rab_entries_grid  = qtw.QGridLayout(rab_entries)
        rab_namelist_grid = qtw.QGridLayout(rab_namelist)

        rab_entries.frame = qtw.QFrame(rab_entries)
        rab_entries.frame.setGeometry(rab_entries.geometry())
        rab_entries.frame.setFixedHeight(yframes)
        rab_entries.frame.setLineWidth(border_wid)
        rab_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['nrho_out_rb']
        cb = ['dump_rb_cdf', 'dump_rb_input']
        self.fill_widget(rab_entries_grid, 'rabbit', title='RABBIT options', entries=entries, checkbuts=cb)

# RABBIT namelist text fiels

        rab_namelist.frame = qtw.QFrame(rab_namelist)
        rab_namelist.frame.setGeometry(rab_namelist.geometry())
        rab_namelist.frame.setFixedHeight(yframes)
        rab_namelist.frame.setLineWidth(border_wid)
        rab_namelist.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        self.guiQt_d['rabbit']['nml'] = qtw.QPlainTextEdit(nml_init)
        self.guiQt_d['rabbit']['nml'].setFixedWidth(600)
        self.guiQt_d['rabbit']['nml'].setFixedHeight(450)
        nml_qlab = qtw.QLabel('Namelist')
        nml_qlab.setFont(titleFont)
        nml_qlab.setFixedHeight(titleHeight)
        rab_namelist_grid.addWidget(nml_qlab, 0, 2)
        rab_namelist_grid.addWidget(self.guiQt_d['rabbit']['nml'], 1, 2)
        rab_namelist_grid.setRowStretch(rab_namelist_grid.rowCount(), 1)

        rab_tab_layout.addStretch()

#------------
# TORBEAM tab
#------------

        tbm_entries   = qtw.QWidget()
        tbm_intinbeam = qtw.QWidget()
        tbm_fltinbeam = qtw.QWidget()
        tbm_tab_layout.addWidget(tbm_entries  , alignment=Qt.AlignLeft)
        tbm_tab_layout.addWidget(tbm_intinbeam, alignment=Qt.AlignLeft)
        tbm_tab_layout.addWidget(tbm_fltinbeam, alignment=Qt.AlignLeft)
        tbm_entries_grid   = qtw.QGridLayout(tbm_entries)
        tbm_intinbeam_grid = qtw.QGridLayout(tbm_intinbeam)
        tbm_fltinbeam_grid = qtw.QGridLayout(tbm_fltinbeam)

# TORBEAM general options

        tbm_entries.frame = qtw.QFrame(tbm_entries)
        tbm_entries.frame.setGeometry(tbm_entries.geometry())
        tbm_entries.frame.setFixedHeight(yframes)
        tbm_entries.frame.setLineWidth(border_wid)
        tbm_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['nrho_out_tb']
        cb = ['dump_tb_cdf']
        self.fill_widget(tbm_entries_grid, 'torbeam', title='TORBEAM options', entries=entries, checkbuts=cb)

# TORBEAM intinbeam

        title_qlbl = qtw.QLabel('Intinbeam')
        title_qlbl.setFont(titleFont)
        title_qlbl.setFixedHeight(titleHeight)

        tbm_intinbeam_grid.addWidget(title_qlbl, 0, 0, alignment=Qt.AlignLeft)

        tbm_intinbeam.frame = qtw.QFrame(tbm_intinbeam)
        tbm_intinbeam.frame.setGeometry(tbm_intinbeam.geometry())
        tbm_intinbeam.frame.setFixedHeight(yframes)
        tbm_intinbeam.frame.setLineWidth(border_wid)
        tbm_intinbeam.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        jrow = 1
        for key in torbeam_exec.int_in_list:
            label = qtw.QLabel('%2d   %s' %(self.gui_init_d['torbeam'][key]['pos'], key))
            self.guiQt_d['torbeam'][key] = qtw.QSpinBox()
            self.guiQt_d['torbeam'][key].setRange(-100, 100)
            self.guiQt_d['torbeam'][key].setValue(self.gui_init_d['torbeam'][key]['value'])
            self.guiQt_d['torbeam'][key].setFixedWidth(100)
            if key == 'nprofv':
                self.guiQt_d['torbeam'][key].setDisabled(True)
            tbm_intinbeam_grid.addWidget(label, jrow, 0)
            tbm_intinbeam_grid.addWidget(self.guiQt_d['torbeam'][key], jrow, 1)
            for qtobj in label, self.guiQt_d['torbeam'][key]:
                qtobj.setToolTip(self.gui_init_d['torbeam'][key]['toolTip'])
            jrow += 1

        tbm_intinbeam_grid.setRowStretch(tbm_intinbeam_grid.rowCount(), 1)

# TORBEAM floatinbeam

        title_qlbl = qtw.QLabel('Floatinbeam')
        title_qlbl.setFont(titleFont)
        title_qlbl.setFixedHeight(titleHeight)

        tbm_fltinbeam_grid.addWidget(title_qlbl, 0, 0, alignment=Qt.AlignLeft)
        tbm_fltinbeam.frame = qtw.QFrame(tbm_fltinbeam)
        tbm_fltinbeam.frame.setGeometry(tbm_fltinbeam.geometry())
        tbm_fltinbeam.frame.setFixedHeight(yframes)
        tbm_fltinbeam.frame.setLineWidth(border_wid)
        tbm_fltinbeam.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        jrow = 1
        for key in torbeam_exec.flt_in_list:
            label = qtw.QLabel('%2s   %s' %(self.gui_init_d['torbeam'][key]['pos'], key))
            self.guiQt_d['torbeam'][key] = qtw.QLineEdit()
            self.guiQt_d['torbeam'][key].setText('%12.4e' %self.gui_init_d['torbeam'][key]['value'])
#            valid = QDoubleValidator(-1.e2, 1.e2, 4, notation=QDoubleValidator.ScientificNotation)
            self.guiQt_d['torbeam'][key].setFixedWidth(100)
            tbm_fltinbeam_grid.addWidget(label, jrow, 0)
            tbm_fltinbeam_grid.addWidget(self.guiQt_d['torbeam'][key], jrow, 1)
            for qtobj in label, self.guiQt_d['torbeam'][key]:
                qtobj.setToolTip(self.gui_init_d['torbeam'][key]['toolTip'])
            jrow += 1


        tbm_fltinbeam_grid.setRowStretch(tbm_fltinbeam_grid.rowCount(), 1)

        tbm_tab_layout.addStretch()

#-----------
# NEMEC tab
#-----------

        vmc_entries = qtw.QWidget()
        vmc_tab_layout.addWidget(vmc_entries, alignment=Qt.AlignLeft)
        vmc_entries_grid = qtw.QGridLayout(vmc_entries)

        vmc_entries.frame = qtw.QFrame(vmc_entries)
        vmc_entries.frame.setGeometry(vmc_entries.geometry())
        vmc_entries.frame.setFixedHeight(yframes)
        vmc_entries.frame.setLineWidth(border_wid)
        vmc_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['ntheta', 'nrho', 'm_moms', 'niter', 'nstep']
        self.fill_widget(vmc_entries_grid, 'nemec', title='NEMEC options', entries=entries)

#-----------
# FEQIS tab
#-----------

        feq_entries = qtw.QWidget()
        feq_tab_layout.addWidget(feq_entries, alignment=Qt.AlignLeft)
        feq_entries_grid = qtw.QGridLayout(feq_entries)

        feq_entries.frame = qtw.QFrame(feq_entries)
        feq_entries.frame.setGeometry(feq_entries.geometry())
        feq_entries.frame.setFixedHeight(yframes)
        feq_entries.frame.setLineWidth(border_wid)
        feq_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['ntheta', 'nrho', 'm_moms']
        self.fill_widget(feq_entries_grid, 'feqis', title='FEQIS options', entries=entries)

#-----------
# TRANSP tab
#-----------

        tra_entries = qtw.QWidget()
        tra_scripts = qtw.QWidget()
        tra_tab_layout.addWidget(tra_entries, alignment=Qt.AlignLeft)
        tra_tab_layout.addWidget(tra_scripts, alignment=Qt.AlignLeft)
        tra_entries_grid = qtw.QGridLayout(tra_entries)
        tra_scripts_grid = qtw.QHBoxLayout(tra_scripts)

        tra_entries.frame = qtw.QFrame(tra_entries)
        tra_entries.frame.setGeometry(tra_entries.geometry())
        tra_entries.frame.setFixedHeight(yframes)
        tra_entries.frame.setLineWidth(border_wid)
        tra_entries.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        entries = ['tres', 'dtbeam', 'nmc', 'avgtim', 'outtim']
        cb = ['mcrf']
        self.fill_widget(tra_entries_grid, 'transp', title='TRANSP options', entries=entries, checkbuts=cb)

        tra_scripts.frame = qtw.QFrame(tra_scripts)
        tra_scripts.frame.setGeometry(tra_scripts.geometry())
        tra_scripts.frame.setFixedHeight(yframes)
        tra_scripts.frame.setLineWidth(border_wid)
        tra_scripts.frame.setFrameStyle(qtw.QFrame.Panel | qtw.QFrame.Raised)

        butWidth  = 70
        butHeight = 30
        _, gexp = self.get_gui_tab('exp')
        self.tra_runid = qtw.QLineEdit('%dA01' %gexp.shot)
        self.tra_runid.setFixedWidth(butWidth)
        self.tra_runid.setFixedHeight(butHeight)
        qlbl = qtw.QLabel('Runid')
        qlbl.setFixedWidth(butWidth)
        qlbl.setFixedHeight(butHeight)

        tra_scripts_grid.addWidget(qlbl, 0, alignment=Qt.AlignLeft)
        tra_scripts_grid.addWidget(self.tra_runid, 1)
        jpos = 2

        script_d = {'trdat': 'trdat', 'Submit': 'tr_submit', 'Fetch': 'tr_fetch', \
            'Unpack': 'tr_unpack', 'Abort': 'tr_clean', 'cdfplayer': '/shares/departments/AUG/users/git/python/cdfplayer/cdfviewer.py -rid'}
        for key, script in script_d.items():
            sbut = qtw.QPushButton(key)
            sbut.setFixedWidth(butWidth)
            sbut.setFixedHeight(butHeight)
            sbut.clicked.connect(lambda checked, a=script: self.script(a))
            tra_scripts_grid.addWidget(sbut, jpos)
            jpos += 1

#----------------
# Display GUI

#        self.setStyleSheet("QFrame {border-width: 1;"
#                                "border-radius: 3;"
#                                "border-style: solid;"
#                                "border-color: rgb(80, 80, 80)}")
#        self.setStyleSheet("QLabel { border-width: 0.5 }")
        self.setGeometry(10, 10, xwin, ywin)
        self.setWindowTitle('TRVIEW')

# GUi interaction (mouse, keys)

        self.stop  = True
        self.ctrl  = False

        self.show()


    def about(self):

        webbrowser.open(docu_url)


    def message(self):

        msg = \
'''Version 1.0.6<br>
Major revision!<br>
<UL>Full functionality is kept, including IMAS</UL>
<UL>Added coupling to RABBIT and TORBEAM!</UL>
<UL>Uniform time base for time traces, profiles, equilibria, code output</UL>
<UL>New class for full experimental input!</UL>
<UL>Fully new layout:
  <UL>using pyqt</UL>
  <UL>separate tabs for settings (exp, RABBIT, TORBEAM, TRANSP)</UL>
  <UL>Displaying codes' output</UL>
  <UL>unified time navigation bar</UL></UL>
<UL>Details at <A HREF="%s">TRVIEW home</A></UL>
''' %docu_url
        msgBox = qtw.QMessageBox(self)
        msgBox.setIcon(qtw.QMessageBox.Information)
        msgBox.setTextFormat(Qt.RichText)
        msgBox.setText(msg)
        json_source = '%s/settings/default.json' %heat_dir
        shutil.copy(json_source, json_default)


    def script(self, sscript):

        runid = self.tra_runid.text()
        command = '%s %s' %(sscript, runid)
        print(command)
        os.system(command)


    def diag_choice(self, profLabel):

        prof_widget = qtw.QWidget()

        prof_layout = qtw.QVBoxLayout(prof_widget)

        title_widget  = qtw.QWidget(self)
        diaSig_widget = qtw.QWidget(self)
        sflist_widget = qtw.QWidget(self)
        fitPar_widget = qtw.QWidget(self)

        prof_layout.addWidget( title_widget, 0)
        prof_layout.addWidget(diaSig_widget, 1)
        prof_layout.addWidget(sflist_widget, 2)
        prof_layout.addWidget(fitPar_widget, 3)

        prof_widget.setObjectName('diag_choice')

        title_widget.setObjectName('prof_title')
        diaSig_widget.setObjectName('diag_sigs')
        sflist_widget.setObjectName('sflist')
        fitPar_widget.setObjectName('fit')
        
        prof_widget.setStyleSheet("QWidget#diag_choice {border-width: 1; border-radius: 4; border-style: solid; border-color: #c0c0c0}")

        title_widget.setStyleSheet("QWidget#prof_title {margin: 0px, padding: 0px}")
        diaSig_widget.setStyleSheet("QWidget#diag_sigs {margin: 0px, padding: 0px}")
        sflist_widget.setStyleSheet("QWidget#sflist {margin: 0px, padding: 0px}")
        fitPar_widget.setStyleSheet("QWidget#sflist {margin: 0px, padding: 0px}")

        title_widget.setFixedHeight(34)
        diaSig_widget.setFixedHeight(92)
        sflist_widget.setFixedHeight(36)
        fitPar_widget.setFixedHeight(82)

        title_layout  = qtw.QHBoxLayout(title_widget)
        diaSig_layout = qtw.QGridLayout(diaSig_widget)
        sflist_layout = qtw.QGridLayout(sflist_widget)
        fitPar_layout = qtw.QGridLayout(fitPar_widget)

        tit_qlab = qtw.QLabel(profLabel)
        tit_qlab.setFont(titleFont)
        tit_qlab.setAlignment(Qt.AlignCenter)
        title_layout.addWidget(tit_qlab, alignment=Qt.AlignCenter)

        self.fit_methods = ['Rec. spline', 'Gaussian fit', 'Fit 2D']

        self.cbvar[profLabel] = {}

# Browse all diagnostics with a given profLabel

        f_diags = '%s/diags.dat' %os.path.dirname(os.path.realpath(__file__))
        diags, sigs, pre = \
            np.genfromtxt(f_diags, dtype='str', skip_header=2, unpack=True, usecols=(0, 1, 2))

        jrow = 0
        jcol = 0
        ncol = 6
        sf_diags = self.gui_init_d[profLabel]['sflist']['value'].split()
        sfdiags = []
        for sfd in sf_diags:
            tmp = sfd.split(':')
            if tmp[0] == 'AUGD' and tmp[3] == '0':
                sfdiags.append('%s:%s' %(tmp[1].strip(), tmp[2].strip()))
        for jdia, diag in enumerate(diags):
            if pre[jdia] == profLabel:
                sig = sigs[jdia]
                diag_sig = '%s:%s' %(diag, sig)
                self.cbvar[profLabel][diag_sig] = qtw.QCheckBox(diag_sig)
                if (diag_sig in sfdiags):
                    self.cbvar[profLabel][diag_sig].setChecked(True)
                self.cbvar[profLabel][diag_sig].stateChanged.connect(lambda: self.set_sflist(profLabel))
                diaSig_layout.addWidget(self.cbvar[profLabel][diag_sig], jrow, jcol)
                jcol += 1
                if jcol%ncol == ncol-1:
                    jrow += 1
                    jcol = 0

# Summary with selected diags

        self.fill_widget(sflist_layout, node=profLabel, entries=['sflist'], lbl_wid=0, ent_wid=400)

# Fit parameters

        fit_combos = {'method': ['Rec. spline', 'Gaussian fit', 'Fit 2D']}
        self.fill_widget(fitPar_layout, node=profLabel, combos=fit_combos, entries=['fit_tol', 'sigma'])

# General layout
        prof_layout.addStretch()

        return prof_widget


    def set_sflist(self, profLabel):

        sf_list = [sf for sf, chosen in self.cbvar[profLabel].items() if chosen.isChecked()]
        sflist = ''
        for jsig, dsig in enumerate(sf_list):
            tmp = 'AUGD:%s:0' %dsig
            sflist += '%s ' %tmp 

        self.guiQt_d[profLabel]['sflist'].setText(sflist)


    def fill_widget(self, layout, node, entries=[], checkbuts=[], radiobuts=[], combos={}, lbl_wid=240, ent_wid=100, wid_hei=25, stretch=True, title=None):
# Checkbutton

        jrow = 0
        if title is not None:
            qlbl = qtw.QLabel(title)
            qlbl.setFixedWidth(lbl_wid)
            qlbl.setFont(titleFont)
            qlbl.setFixedHeight(titleHeight)
            layout.addWidget(qlbl, jrow, 0, alignment=Qt.AlignLeft)
            jrow += 1

        for key in checkbuts:
            lbl = self.gui_init_d[node][key]['Qlabel']
            self.guiQt_d[node][key] = qtw.QCheckBox(lbl)
            self.guiQt_d[node][key].setFixedWidth(lbl_wid)
            layout.addWidget(self.guiQt_d[node][key], jrow, 0, 1, 2, alignment=Qt.AlignLeft)
            if self.gui_init_d[node][key]['value']:
                self.guiQt_d[node][key].setChecked(True)
            jrow += 1

        for key in entries:
            val = self.gui_init_d[node][key]['value']
            lbl = self.gui_init_d[node][key]['Qlabel']
            qlbl = qtw.QLabel(lbl)
            qlbl.setFixedWidth(lbl_wid)
            qlbl.setFixedHeight(wid_hei)
            if isinstance(val, int):
                self.guiQt_d[node][key] = qtw.QSpinBox()
                self.guiQt_d[node][key].setRange(-1000000, 1000000)
                self.guiQt_d[node][key].setValue(val)
            elif isinstance(val, float):
                self.guiQt_d[node][key] = qtw.QDoubleSpinBox()
                self.guiQt_d[node][key].setRange(-1000., 1000.)
                self.guiQt_d[node][key].setDecimals(4)
                self.guiQt_d[node][key].setValue(val)
            else:
                self.guiQt_d[node][key] = qtw.QLineEdit(str(val))
            self.guiQt_d[node][key].setFixedWidth(ent_wid)
            layout.addWidget(qlbl               , jrow, 0, alignment=Qt.AlignLeft)
            layout.addWidget(self.guiQt_d[node][key], jrow, 1, alignment=Qt.AlignLeft)
            jrow += 1

        for key, combs in combos.items():
            self.guiQt_d[node][key] = qtw.QComboBox()
            lbl = self.gui_init_d[node][key]['Qlabel']
            qlbl = qtw.QLabel(lbl)
            for comb in combs:
                self.guiQt_d[node][key].addItem(comb.strip())
            index = self.guiQt_d[node][key].findText(self.gui_init_d[node][key]['value'].strip())
            self.guiQt_d[node][key].setCurrentIndex(index)
            layout.addWidget(qlbl               , jrow, 0, alignment=Qt.AlignLeft)
            layout.addWidget(self.guiQt_d[node][key], jrow, 1, alignment=Qt.AlignLeft)
            jrow += 1

        if stretch:
            layout.setRowStretch(layout.rowCount(), 1)
            layout.setColumnStretch(layout.columnCount(), 1)


    def gui2json(self):

        json_d = {}
        for node, val in self.guiQt_d.items():
           json_d[node], _ = self.get_gui_tab(node)
        return json_d


    def get_gui_tab(self, node):

        node_dic = {}
        for key, val_d in self.gui_init_d[node].items():
            node_dic[key] = {}
            for attr, val in val_d.items():
                if attr != 'value':
                    node_dic[key][attr] = val
        for key, val in self.guiQt_d[node].items():
            if isinstance(val, qtw.QLineEdit):
                if isinstance(self.gui_init_d[node][key]['value'], float):
                    node_dic[key]['value'] = float(val.text())
                else:
                    node_dic[key]['value'] = val.text()
            elif isinstance(val, qtw.QSpinBox):
                node_dic[key]['value'] = val.value()
            elif isinstance(val, qtw.QDoubleSpinBox):
                node_dic[key]['value'] = val.value()
            elif isinstance(val, qtw.QCheckBox):
                node_dic[key]['value'] = val.isChecked()
            elif isinstance(val, qtw.QComboBox):
                node_dic[key]['value'] = val.currentText()
            elif isinstance(val, qtw.QButtonGroup):
                bid = val.checkedId()
                node_dic[key]['value'] = self.rblists[key][bid]

        gclass = GUI_TAB()
        for key, val in node_dic.items():
            setattr(gclass, key, val['value'])

        return node_dic, gclass


    def set_gui(self, json_d):

        for node, var_d in json_d.items():
            for varname, val_d in var_d.items():
                val = val_d['value']
                if varname not in self.guiQt_d[node].keys():
                    continue
                widget = self.guiQt_d[node][varname]
                if isinstance(widget, qtw.QCheckBox):
                    widget.setChecked(val)
                elif isinstance(widget, qtw.QButtonGroup):
                    for but in widget.buttons():
                        if but.text() == val:
                            but.setChecked(True)
                elif isinstance(widget, qtw.QLineEdit):
                    if val:
                        widget.setText(str(val))
                elif isinstance(widget, qtw.QSpinBox):
                    widget.setValue(val)
                elif isinstance(widget, qtw.QDoubleSpinBox):
                    widget.setValue(val)
                elif isinstance(widget, qtw.QComboBox):
                    for index in range(widget.count()):
                        if widget.itemText(index).strip() == val.strip():
                            widget.setCurrentIndex(index)
                            break


    def load_json(self):

        ftmp = qtw.QFileDialog.getOpenFileName(self, 'Open file', \
            '%s/settings' %heat_user_dir, "json files (*.json)")
        f_json = ftmp[0]
        with open(f_json) as fjson:
            setup_d = json.load(fjson)
        self.set_gui(setup_d)


    def save_json(self):

        out_dic = self.gui2json()
        ftmp = qtw.QFileDialog.getSaveFileName(self, 'Save file', \
            '%s/settings' %heat_user_dir, "json files (*.json)")
        f_json = ftmp[0]
        with open(f_json, 'w') as fjson:
            fjson.write(json.dumps(out_dic))


    def addPlot(self, title, figure, notebook=None):

        new_tab = qtw.QWidget()
        layout = qtw.QVBoxLayout()
        new_tab.setLayout(layout)

        new_canvas = FigureCanvas(figure)
        new_toolbar = NavigationToolbar(new_canvas, new_tab)
        new_canvas.mpl_connect('button_press_event', fconf.on_click)
        layout.addWidget(new_canvas)
        layout.addWidget(new_toolbar)
        if notebook is None:
            notebook = self.exp_tabs
        notebook.addTab(new_tab, title)


    def expFromShotfile(self):

        _, self.gexp = self.get_gui_tab('exp')
        self.gexp.angf = True
        if self.gexp.logs == 'Silent':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.ERROR)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.ERROR)
        elif self.gexp.logs == 'Info':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.INFO)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.INFO)
        elif self.gexp.logs == 'Debug':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.DEBUG)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.DEBUG)

        self.gprof = {}
        for profLabel in prof_names:
            _, self.gprof[profLabel] = self.get_gui_tab(profLabel)
        self.plasma = get_exp.INPUT(self.gexp, self.gprof)

        if hasattr(self.plasma.nbi, 'time'):
            self.n_nbi = len(self.plasma.nbi.einj_kev)
        if hasattr(self.plasma.ech, 'freq'):
            self.n_gy  = len(self.plasma.ech.freq)
        if hasattr(self.plasma.icr, 'freq'):
            self.n_ant = len(self.plasma.icr.freq)
        self.nt    = len(self.plasma.profiles.time)

        self.plot_input()


    def expFromIMAS(self):

        _, self.gexp = self.get_gui_tab('exp')
        self.gexp.angf = True
        if self.gexp.logs == 'Silent':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.ERROR)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.ERROR)
        elif self.gexp.logs == 'Info':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.INFO)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.INFO)
        elif self.gexp.logs == 'Debug':
            logger = logging.getLogger('aug_sfutils')
            logger.setLevel(logging.DEBUG)
            logger = logging.getLogger('trview')
            logger.setLevel(logging.DEBUG)

        self.gprof = {}
        for profLabel in prof_names:
            _, self.gprof[profLabel] = self.get_gui_tab(profLabel)
        self.plasma = get_exp.INPUT(self.gexp, self.gprof, readSF=False)

        if hasattr(self.plasma.nbi, 'einj_kev'):
            self.n_nbi = len(self.plasma.nbi.einj_kev)
        if hasattr(self.plasma.ech, 'freq'):
            self.n_gy  = len(self.plasma.ech.freq)
        if hasattr(self.plasma.icr, 'freq'):
            self.n_ant = len(self.plasma.icr.freq)
        self.nt    = len(self.plasma.profiles.time)

        self.plot_input()


    def exec_rabbit(self):

        if not hasattr(self, 'plasma'):
            self.expFromShotfile()
        _, grab = self.get_gui_tab('rabbit')
        grab.nml = self.guiQt_d['rabbit']['nml'].toPlainText()
        if hasattr(self.plasma.nbi, 't_ds'):
            self.rbb = rabbit_exec.RABBIT(self.plasma, grab)
            self.plot_rabbit()
        else:
            logger.error('No NBI for shot %d' %self.plasma.gpar.shot)
  

    def exec_torbeam(self):

        if not hasattr(self, 'plasma'):
            self.expFromShotfile()
        _, gtbm = self.get_gui_tab('torbeam')
        self.tbb = torbeam_exec.TORBEAM(self.plasma, gtbm)
        self.plot_torbeam()


    def exec_nemec(self):

        if not hasattr(self, 'plasma'):
            self.expFromShotfile()
        gvmc_d, _ = self.get_gui_tab('nemec')
        self.vmc = nemec_exec.NEMEC(gvmc_d, eqm=self.plasma.equ)
        self.plot_nemec()


    def exec_feqis(self):

        if not hasattr(self, 'plasma'):
            self.expFromShotfile()
        gfeq_d, _ = self.get_gui_tab('feqis')
        feq_d = {key: val['value'] for key, val in gfeq_d.items()}
        self.feq = feqis_pbe.FEQIS_PBE(feq_d, eqm=self.plasma.equ)
        self.plot_feqis()


    def plot_input(self):

        n_tabs = self.exp_tabs.count() 
        self.exp_tabs.setCurrentIndex(0)
        for jtab in range(n_tabs-1, -1, -1):
            self.exp_tabs.removeTab(jtab)

        for fig_lbl in ('fig_i_sep', 'fig_i_nbi', \
            'fig_i_ech', 'fig_i_phi', 'fig_i_the', 'fig_i_icr', 'fig_i_equ'):
            self.__dict__[fig_lbl] = Figure(figsize=(4., 1.5), dpi=100)
            self.__dict__[fig_lbl].subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_prof = {}

        for profLabel in prof_names:
            self.fig_prof[profLabel] = Figure(figsize=(4., 1.5), dpi=100)
            self.fig_prof[profLabel].subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_i_sig = Figure(figsize=(4., 1.4), dpi=100)
        self.fig_i_pel = Figure(figsize=(4., 1.4), dpi=100)
        self.fig_i_sig.subplots_adjust(left=0.1, bottom=0.1 , right=0.95, top=0.92, hspace=0)
        self.fig_i_pel.subplots_adjust(left=0.1, bottom=0.15, right=0.95, top=0.93, hspace=0)

        self.addPlot('Separatrix'   , self.fig_i_sep)
        self.addPlot('Time traces'  , self.fig_i_sig)
        self.addPlot('Input P_NBI'  , self.fig_i_nbi)
        self.addPlot('Input P_ECH'  , self.fig_i_ech)
        self.addPlot('Input tor_ECH', self.fig_i_phi)
        self.addPlot('Input pol_ECH', self.fig_i_the)
        self.addPlot('Input P_ICRF' , self.fig_i_icr)
        self.addPlot('Pellet'       , self.fig_i_pel)
        self.addPlot('Input equil.' , self.fig_i_equ)

        self.fplot  = {}
        self.ax_ctr = {}
        self.xplot  = {}
        self.ax1d   = {}
        self.ok = {}
        self.caplines = {}
        self.barlinecols = {}
        self.tline = {}
        self.rejPoints = {}
        self.delPoints = {}
        nt = len(self.plasma.profiles.time)

        for profLabel in prof_names:
            profObj = getattr(self.plasma.profiles, profLabel)
#            profObj = self.plasma.profiles.profLabel
            profObj.ind_del = [ [] for jt in range(nt) ]
            profObj.tnot = []

        self.xg, self.tg = np.meshgrid(self.plasma.profiles.rho, self.plasma.profiles.time)

        for profLabel in prof_names:
            self.prof_tab(profLabel)

        self.set_exp_plots()


    def prof_tab(self, profLabel):

        figure = self.fig_prof[profLabel]
        figure.clf()

        prof_tab = qtw.QWidget()
        prof_layout = qtw.QVBoxLayout()
        prof_tab.setLayout(prof_layout)
        ent_frame = qtw.QWidget()
        lbl_frame = qtw.QWidget()
        prof_canvas = FigureCanvas(figure)
        prof_canvas.setFixedHeight(500)
        prof_toolbar = NavigationToolbar(prof_canvas, prof_tab)

# Figures

# Buttons
        ent_layout = qtw.QHBoxLayout()
        ent_frame.setLayout(ent_layout)

        butWidth =  80
        lblWidth = 120

        fitButton = qtw.QPushButton('Re-fit')
        fitButton.setFixedWidth(butWidth)
        fitButton.clicked.connect(self.reFit)
        fitButton.setToolTip('Re-fit data with current fit tolerance, sigmas')

        fitLabel = qtw.QLabel('Fit tolerance')
        fitLabel.setFixedWidth(lblWidth)
        fitLabel.setToolTip('Multiplier for error bars, the higher, the smoother the fit')

        self.fitTolerance[profLabel] = qtw.QDoubleSpinBox()
        self.fitTolerance[profLabel].setRange(0., 1.)
        self.fitTolerance[profLabel].setValue(self.gprof[profLabel].fit_tol)
        self.fitTolerance[profLabel].setFixedWidth(butWidth)
        self.fitTolerance[profLabel].setToolTip('Multiplier for error bars, the higher, the smoother the fit')

        sigmaLabel = qtw.QLabel('# sigmas')
        sigmaLabel.setFixedWidth(lblWidth)
        sigmaLabel.setToolTip('# sigmas, out of which scatter data are deleted')

        self.sigmaReject[profLabel] = qtw.QDoubleSpinBox()
        self.sigmaReject[profLabel].setRange(0., 10.)
        self.sigmaReject[profLabel].setValue(self.gprof[profLabel].sigma)
        self.sigmaReject[profLabel].setFixedWidth(butWidth)
        self.sigmaReject[profLabel].setToolTip('# sigmas, out of which scatter data are deleted')

        ent_layout.addWidget(fitButton)
        ent_layout.addWidget(fitLabel, 1)
        ent_layout.addWidget(self.fitTolerance[profLabel], 2)
        ent_layout.addWidget(sigmaLabel, 3)
        ent_layout.addWidget(self.sigmaReject[profLabel], 4)
        ent_layout.addStretch()

# Instructions
        lbl_layout = qtw.QGridLayout()
        lbl_frame.setLayout(lbl_layout)

        scatLabel = qtw.QLabel('Left plot, scatter points:')
        scatBlue  = qtw.QLabel('Blue: hand-deleted')
        scatBlue.setStyleSheet("QLabel { color : blue; }");
        scatRed   = qtw.QLabel('Red: rejected by rc. spline')
        scatRed.setStyleSheet("QLabel { color : red; }");
        scatGreen = qtw.QLabel('Green: valid points')
        scatGreen.setStyleSheet("QLabel { color : green; }");
        for jrow, lbl in enumerate([scatLabel, scatBlue, scatRed, scatGreen]):
            lbl_layout.addWidget(lbl, jrow, 0)

        delLabel      = qtw.QLabel('Mouse-L: deletes points')
        delAllLabel   = qtw.QLabel('Ctrl+Mouse-L: deletes frame')
        UndeleteLabel = qtw.QLabel('Mouse-Mid: un-delete points for this frame')
        MoveLabel     = qtw.QLabel('Mouse-R: move to next frame plot')
        for jrow, lbl in enumerate([delLabel, delAllLabel, UndeleteLabel, MoveLabel]):
            lbl_layout.addWidget(lbl, jrow, 1)

        lbl_layout.setColumnStretch(lbl_layout.columnCount(), 1)

# Layout

        prof_layout.addWidget(prof_canvas)
        prof_layout.addWidget(prof_toolbar)
        prof_layout.addWidget(ent_frame)
        prof_layout.addWidget(lbl_frame)

# Profile figures

# 1d plot

        ylab = profLabel
        rho  = self.plasma.profiles.rho
        profObj = self.plasma.profiles.__dict__[profLabel]
        if hasattr(profObj, 'yfit_rhop'):
            fitarr = profObj.yfit_rhop
            xlab = r'$\rho_{pol}$'
        else:
            fitarr = profObj.yfit_rhot
            xlab = r'$\rho_{tor}$'
        if hasattr(profObj, 'units'):
            ylab = '%s [%s]' %(profLabel, profObj.units)
        ax_prof = figure.add_subplot(1, 3, 1)
        ax_prof.set_xlim([0, 1.1])
        ymax = 1.1*np.nanmax(fitarr)
# git
        ax_prof.set_ylim([0, ymax])
#        ax_prof.set_ylim([0, 3500])
        ax_prof.set_xlabel(xlab, fontsize=fontSize)
        ax_prof.set_ylabel(ylab, fontsize=fontSize)
        self.fplot[profLabel], = ax_prof.plot(rho, fitarr[0, :], 'b-', linewidth=lWid)
        if hasattr(profObj, 'xexp_rhop'):
            exprho = profObj.xexp_rhop
            exparr = profObj.yexp_rhop
            self.xplot[profLabel], = ax_prof.plot(exprho[0, :], exparr[0, :], 'go', markersize=mSize)
        ax_prof.plot([1, 1], [0, ymax], 'k--', linewidth=lWid)
        ax_prof.grid('on')
        ax_prof.tick_params(axis='both', labelsize=fontSize)
        ax_prof.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
        ax_prof.yaxis.major.formatter._useMathText = True
        self.ax1d[profLabel] = ax_prof
        
        self.ok[profLabel], self.caplines[profLabel], self.barlinecols[profLabel] = \
            self.ax1d[profLabel].errorbar(0, 0, 0, fmt='g.', capsize=3., linewidth=lWid)
        self.rejPoints[profLabel], = ax_prof.plot([], [], 'ro', markersize=mSize)
        self.delPoints[profLabel], = ax_prof.plot([], [], 'bo', markersize=mSize)

# Contour plot

        if self.tg.shape[0] > 1:
            self.ax_ctr[profLabel] = figure.add_subplot(1, 3, 2)
            if hasattr(profObj, 'yfit_rhop'):
                ctr = self.ax_ctr[profLabel].contourf(self.tg, self.xg, profObj.yfit_rhop)
            else:
                ctr = self.ax_ctr[profLabel].contourf(self.tg, self.xg, profObj.yfit_rhot)
            figure.colorbar(ctr, aspect=10, shrink=0.9)
            self.ax_ctr[profLabel].set_xlabel('Time [s]', fontsize=fontSize)
            self.ax_ctr[profLabel].set_ylabel(xlab , fontsize=fontSize)
            self.ax_ctr[profLabel].tick_params(axis='both', labelsize=fontSize)
            self.tline[profLabel], = self.ax_ctr[profLabel].plot([], [], 'k-')

        self.exp_tabs.addTab(prof_tab, profLabel)
        fcanv = figure.canvas
        fcanv.mpl_connect('button_press_event', self.MouseInteraction)
        fcanv.mpl_connect('key_press_event', self.on_key)
        fcanv.mpl_connect('key_release_event', self.off_key)
        fcanv.setFocusPolicy( Qt.ClickFocus )
        fcanv.setFocus()


    def reFit(self):

        profObj = self.plasma.profiles.__dict__[self.currentProfLabel]
        profObj.fit_tol = float(self.fitTolerance[self.currentProfLabel].text())
        profObj.sigmas  = float(self.sigmaReject[self.currentProfLabel].text())
        fit_prof.fit_prof(profObj, self.plasma.profiles.time, self.plasma.profiles.rho)
        self.update_exp_plot()

       
    def plot_rabbit(self):

        n_tabs = self.rab_tabs.count() 
        self.rab_tabs.setCurrentIndex(0)
        for jtab in range(n_tabs-1, -1, -1):
            self.rab_tabs.removeTab(jtab)

        if not hasattr(self, 'rbb'):
            logger.error('Run RABBIT first')
            return

        self.fig_o1d = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_o2d = Figure(figsize=(4., 1.5), dpi=100)

        self.fig_o1d.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_o2d.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)

        self.addPlot('RABBIT time traces', self.fig_o1d, notebook=self.rab_tabs)
        self.addPlot('RABBIT profiles'   , self.fig_o2d, notebook=self.rab_tabs)
        self.set_rabbit_plots()


    def plot_torbeam(self):

        n_tabs = self.tbm_tabs.count() 
        self.tbm_tabs.setCurrentIndex(0)
        for jtab in range(n_tabs-1, -1, -1):
            self.tbm_tabs.removeTab(jtab)

        if not hasattr(self, 'tbb'):
            logger.error('Run TORBEAM first')
            return

        self.fig_pech  = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_jeccd = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_beam  = Figure(figsize=(4., 1.5), dpi=100)

        self.fig_pech.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_jeccd.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_beam.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0., hspace=0.3)

        self.addPlot('TORBEAM P_ECRH', self.fig_pech, notebook=self.tbm_tabs)
        self.addPlot('TORBEAM j_ECCD', self.fig_jeccd, notebook=self.tbm_tabs)
        self.addPlot('Beam propagation', self.fig_beam, notebook=self.tbm_tabs)
        self.set_torbeam_plots()


    def plot_nemec(self):

        n_tabs = self.vmc_tabs.count() 
        self.vmc_tabs.setCurrentIndex(0)
        for jtab in range(n_tabs-1, -1, -1):
            self.vmc_tabs.removeTab(jtab)

        if not hasattr(self, 'vmc'):
            logger.error('Run NEMEC first')
            return

        self.fig_vmbnd  = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_vmfmom = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_vmprof = Figure(figsize=(4., 1.5), dpi=100)

        self.fig_vmbnd.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_vmfmom.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0)
        self.fig_vmprof.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)

        self.addPlot('NEMEC boundary'   , self.fig_vmbnd , notebook=self.vmc_tabs)
        self.addPlot('NEMEC Four. moms' , self.fig_vmfmom, notebook=self.vmc_tabs)
        self.addPlot('NEMEC input prof.', self.fig_vmprof, notebook=self.vmc_tabs)
        self.set_nemec_plots()


    def plot_feqis(self):

        n_tabs = self.feq_tabs.count() 
        self.feq_tabs.setCurrentIndex(0)
        for jtab in range(n_tabs-1, -1, -1):
            self.feq_tabs.removeTab(jtab)

        if not hasattr(self, 'feq'):
            logger.error('Run FEQIS first')
            return

        self.fig_fqbnd  = Figure(figsize=(4., 1.5), dpi=100)
        self.fig_fqprof = Figure(figsize=(4., 1.5), dpi=100)

        self.fig_fqbnd.subplots_adjust(left=0.07, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)
        self.fig_fqprof.subplots_adjust(left=0.1, bottom=0.15, right=0.97, top=0.9, \
            wspace=0.3, hspace=0.3)

        self.addPlot('FEQIS contours'      , self.fig_fqbnd , notebook=self.feq_tabs)
        self.addPlot('FEQIS input profiles', self.fig_fqprof, notebook=self.feq_tabs)
        self.set_feqis_plots()


    def set_exp_plots(self):

        self.jt = 0
        self.progress.setMaximum(self.nt-1)
        self.timeout = 1e-10
        self.exp_tabs.currentChanged.connect(self.update_exp_plot)

        self.marknbi = {}
        self.markech = {}
        self.markphi = {}
        self.markthe = {}
        self.markicr = {}

        self.fig_i_sep.clf()
        self.fig_i_sig.clf()
        self.fig_i_nbi.clf()
        self.fig_i_ech.clf()
        self.fig_i_phi.clf()
        self.fig_i_the.clf()
        self.fig_i_icr.clf()
        self.fig_i_pel.clf()
        self.fig_i_equ.clf()

        txt_init = '# %d  t=%5.3f s' %(self.plasma.gpar.shot, self.plasma.profiles.time[self.jt])
        self.txt_i_sep = self.fig_i_sep.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_sig = self.fig_i_sig.text(0.5, 0.95, txt_init, ha='center', fontsize=titlePaper)
        self.txt_i_nbi = self.fig_i_nbi.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_ech = self.fig_i_ech.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_phi = self.fig_i_phi.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_the = self.fig_i_the.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_icr = self.fig_i_icr.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)
        self.txt_i_pel = self.fig_i_pel.text(0.5, 0.95, txt_init, ha='center', fontsize=titlePaper)
        self.txt_i_equ = self.fig_i_equ.text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)

        self.txt_prof = {}
        for profLabel in prof_names:
            self.txt_prof[profLabel] = self.fig_prof[profLabel].text(0.5, 0.95, txt_init, ha='center', fontsize=titleSize)

# Separatrix

        ax_i_sep = self.fig_i_sep.add_subplot(1, 1, 1, aspect='equal')
        ax_i_sep.set_xlim((0.8, 3.0))
        ax_i_sep.set_ylim((-1.4, 1.4))
        ax_i_sep.set_xlabel('R [m]', fontsize=fontSize)
        ax_i_sep.set_ylabel('z [m]', fontsize=fontSize)
        ax_i_sep.tick_params(axis='both', labelsize=tickSize)
        self.errtxt = ax_i_sep.text(2.6, .7, '', ha='center', fontsize=textSize)

        if hasattr(self, 'gc_d'):
            for gc in self.gc_d.values():
                ax_i_sep.plot(gc.r, gc.z, 'b-')

        self.sepscat, = ax_i_sep.plot(self.plasma.equ.sep.rscat[0][0], self.plasma.equ.sep.zscat[0][0], 'k+', markersize=mSize)
        self.sepfit,  = ax_i_sep.plot(self.plasma.equ.sep.rfit [0], self.plasma.equ.sep.zfit [0], 'g-', linewidth=lWid)
        self.mag, = ax_i_sep.plot(self.plasma.equ.Rmag[0], self.plasma.equ.Zmag[0], 'ro', label='Mag. axis', markersize=mSize)
        if hasattr(self.plasma.equ, 'Rxpu'):
            self.xu,  = ax_i_sep.plot(self.plasma.equ.Rxpu[0], self.plasma.equ.Zxpu[0], 'bo', label='Lower X', markersize=mSize)
            self.xo,  = ax_i_sep.plot(self.plasma.equ.Rxpo[0], self.plasma.equ.Zxpo[0], 'bo', label='Upper X', markersize=mSize)
            self.u,   = ax_i_sep.plot(self.plasma.equ.Rzunt [0], self.plasma.equ.Zunt [0], 'ko', markersize=mSize)
            self.o,   = ax_i_sep.plot(self.plasma.equ.Rzoben[0], self.plasma.equ.Zoben[0], 'go', markersize=mSize)

        ax_i_sep.legend(numpoints=1, loc=1)

# Time traces

        n_ct = 1
        n_sub = 1 # Zeff
        for key, data in self.plasma.sig.__dict__.items():
            if hasattr(data, 'time'):
                n_sub += 1
        n_rows = (n_sub+1)//2
        self.marksig = {}
        for key, data in self.plasma.sig.__dict__.items():
            if not hasattr(data, 'time'):
                continue
            diag, sig = data.diagsig.split(':')
            logger.debug('%s: %s', diag, sig)
            if data.unit is None:
                ylab = key
            else:
                ylab = '%s [%s]' %(key, data.unit)
            ax_i_sig = self.fig_i_sig.add_subplot(n_rows, 2, n_ct)
            ax_i_sig.plot(self.plasma.sig.t_ds, data.data_ds, color=col[n_ct%len(col)], linewidth=lWid)
            self.marksig[key], = ax_i_sig.plot(self.plasma.sig.t_ds[0], data.data_ds[0], 'go', markersize=mSize)
            dmax = np.max(data.data_ds)
            dmin = np.min(data.data_ds)
            if dmin > 0:
                ax_i_sig.set_ylim([0, 1.1*dmax])
            elif dmax < 0:
                ax_i_sig.set_ylim([1.1*dmin, 0])
            ax_i_sig.set_ylabel(ylab, fontsize=fontSize2)
            ax_i_sig.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=tickSize2)
            ax_i_sig.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tickSize2)
            ax_i_sig.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
            ax_i_sig.yaxis.major.formatter._useMathText = True
            if n_ct%n_rows == n_rows - 1 and n_ct%2 == 1:
                ax_i_sig.tick_params(labelbottom=True)
                ax_i_sig.set_xlabel('Time [s]', fontsize=fontSize2)
            n_ct += 1

        ax_i_zef = self.fig_i_sig.add_subplot(n_rows, 2, n_ct)
        dmax = np.max(self.plasma.zef.data_ds)
        ax_i_zef.set_ylim([0, 1.1*dmax])
        ax_i_zef.set_ylabel(r'Z$_{eff}$', fontsize=fontSize2)
        ax_i_zef.plot(self.plasma.zef.t_ds, self.plasma.zef.data_ds, color=col[n_ct%len(col)], linewidth=lWid)
        self.zefplot, = ax_i_zef.plot(self.plasma.zef.t_ds[0], self.plasma.zef.data_ds[0], 'go')
        ax_i_zef.tick_params(labelbottom=True, left='on', right='on', bottom='on', top='on', direction='in', labelsize=tickSize2)
        ax_i_zef.set_xlabel('Time [s]', fontsize=fontSize2)

# PNBI time traces

        if hasattr(self.plasma.nbi, 'power_ds'):

            nrow = 2
            ncol = 4

            dmax = np.max(self.plasma.nbi.power_ds)
            for jnb in range(self.n_nbi):
                jplot = jnb + 1
                ylab = '# %d' %jplot
                ax_i_nbi = self.fig_i_nbi.add_subplot(nrow, ncol, jplot)
                ax_i_nbi.set_xlabel('t [s]', fontsize=fontSize)
                ax_i_nbi.set_ylabel(ylab, fontsize=fontSize)
                ax_i_nbi.plot(self.plasma.nbi.t_ds, self.plasma.nbi.power_ds[:, jnb], 'b-')
                ax_i_nbi.grid('on')
                self.marknbi[jnb], = ax_i_nbi.plot(self.plasma.nbi.t_ds[0], self.plasma.nbi.power_ds[0, jnb], 'go')
                jplot += 1
                ax_i_nbi.tick_params(labelsize=tickSize)
                ax_i_nbi.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
                ax_i_nbi.yaxis.major.formatter._useMathText = True
                ax_i_nbi.set_ylim([0, 1.1*dmax])

# PECH time traces

        if hasattr(self.plasma.ech, 'power_ds'):

            nrow = 2
            ncol = 4

            dmax = np.max(self.plasma.ech.power_ds)
            for jgy in range(self.n_gy):
                ylab = 'gy%d' %(jgy+1)
                ax_i_ech = self.fig_i_ech.add_subplot(nrow, ncol, jgy+1)
                ax_i_ech.set_xlabel('t [s]', fontsize=fontSize)
                ax_i_ech.set_ylabel(ylab, fontsize=fontSize)
                ax_i_ech.plot(self.plasma.ech.t_ds, self.plasma.ech.power_ds[:, jgy], 'b-')
                ax_i_ech.grid('on')
                self.markech[jgy], = ax_i_ech.plot(self.plasma.ech.t_ds[0], self.plasma.ech.power_ds[0, jgy], 'go')
                ax_i_ech.tick_params(labelsize=tickSize)
                ax_i_ech.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
                ax_i_ech.yaxis.major.formatter._useMathText = True
                ax_i_ech.set_ylim([0, 1.1*dmax])

# ECH tor. angle time traces

            dmax = np.max(self.plasma.ech.phi_ds)
            dmin = np.min(self.plasma.ech.phi_ds)
            for jgy in range(self.n_gy):
                ylab = r'$\phi_{ECH}$ [deg] gy%d' %(jgy+1)
                ax_i_phi = self.fig_i_phi.add_subplot(nrow, ncol, jgy+1)
                ax_i_phi.set_xlabel('t [s]', fontsize=fontSize)
                ax_i_phi.set_ylabel(ylab, fontsize=fontSize)
                ax_i_phi.plot(self.plasma.ech.t_ds, self.plasma.ech.phi_ds[:, jgy], 'b-')
                ax_i_phi.grid('on')
                self.markphi[jgy], = ax_i_phi.plot(self.plasma.ech.t_ds[0], self.plasma.ech.phi_ds[0, jgy], 'go')
                ax_i_phi.tick_params(labelsize=tickSize)
                ax_i_phi.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
                ax_i_phi.yaxis.major.formatter._useMathText = True
                if dmax > 0:
                    ax_i_phi.set_ylim(ymax=1.1*dmax)
                else:
                    ax_i_phi.set_ylim(ymax=0)
                if dmin < 0:
                    ax_i_phi.set_ylim(ymin=1.1*dmin)
                else:
                    ax_i_phi.set_ylim(ymin=0)

# ECH pol. angle time traces

            dmax = np.max(self.plasma.ech.theta_ds)
            dmin = np.min(self.plasma.ech.theta_ds)
            for jgy in range(self.n_gy):
                ylab = r'$\theta_{ECH}$ [deg] gy%d' %(jgy+1)
                ax_i_the = self.fig_i_the.add_subplot(nrow, ncol, jgy+1)
                ax_i_the.set_xlabel('t [s]', fontsize=fontSize)
                ax_i_the.set_ylabel(ylab, fontsize=fontSize)
                ax_i_the.plot(self.plasma.ech.t_ds, self.plasma.ech.theta_ds[:, jgy], 'b-')
                ax_i_the.grid('on')
                self.markthe[jgy], = ax_i_the.plot(self.plasma.ech.t_ds[0], self.plasma.ech.theta_ds[0, jgy], 'go')
                ax_i_the.tick_params(labelsize=tickSize)
                ax_i_the.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
                ax_i_the.yaxis.major.formatter._useMathText = True
                if dmax > 0:
                    ax_i_the.set_ylim(ymax=1.1*dmax)
                else:
                    ax_i_the.set_ylim(ymax=0)
                if dmin < 0:
                    ax_i_the.set_ylim(ymin=1.1*dmin)
                else:
                    ax_i_the.set_ylim(ymin=0)

# PICR time traces
        if hasattr(self.plasma.icr, 'power_ds'):

            nrow = 2
            ncol = 4

            dmax = np.max(self.plasma.icr.power_ds)
            for jant in range(self.n_ant):
                ylab = 'Ant%d' %(jant+1)
                ax_i_icr = self.fig_i_icr.add_subplot(nrow, ncol, jant+1)
                ax_i_icr.set_xlabel('t [s]', fontsize=fontSize)
                ax_i_icr.set_ylabel(ylab, fontsize=fontSize)
                ax_i_icr.plot(self.plasma.icr.t_ds, self.plasma.icr.power_ds[:, jant], 'b-')
                ax_i_icr.grid('on')
                self.markicr[jant], = ax_i_icr.plot(self.plasma.icr.t_ds[0], self.plasma.icr.power_ds[0, jant], 'go')
                ax_i_icr.tick_params(labelsize=tickSize)
                ax_i_icr.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
                ax_i_icr.yaxis.major.formatter._useMathText = True
                ax_i_icr.set_ylim([0, 1.1*dmax])

# Pellet

        tmin =  0.
        tmax = 10.

        fSize   = fontSizePaper
        lWidth  = lwPaper
        legSize = legPaper
        tSize   = tickSizePaper

        ax_rho = self.fig_i_pel.add_subplot(3, 1, 1)
        if hasattr(self.plasma.pellet, 'rho_dep'):
            tmin = self.plasma.pellet.times[0]
            tmax = self.plasma.pellet.times[-1]
            ax_rho.plot(self.plasma.pellet.times, self.plasma.pellet.rho_dep, linewidth=lWidth)
        ax_rho.set_ylim([0, 1])
        ax_rho.set_xlim([tmin, tmax])
        ax_rho.set_ylabel(r'$\rho_{tor|abl}$', fontsize=fSize)
        ax_rho.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=fSize)
        ax_rho.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=fSize)

        ax_t = self.fig_i_pel.add_subplot(3, 1, 2)
        if hasattr(self.plasma.pellet, 'ablation_time'):
            ax_t.plot(self.plasma.pellet.times, 1e3*self.plasma.pellet.ablation_time, linewidth=lWidth)
        ax_t.set_xlabel('Time [s]', fontsize=fSize)
        ax_t.set_ylabel(r'$\Delta t_{abl}$ [ms]', fontsize=fSize)
        ax_t.set_xlim([tmin, tmax])
        ax_t.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=tSize)
        ax_t.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)

        ax_5co = self.fig_i_pel.add_subplot(3, 1, 3)
        if hasattr(self.plasma.pellet, 'tpid'):
            ax_5co.plot(self.plasma.pellet.tpid, self.plasma.pellet.c5o, linewidth=lWidth)
            ax_5co.plot([tmin, tmax], [self.plasma.pellet.trigger, self.plasma.pellet.trigger], label='Threshold', linewidth=lWidth)
            ax_5co.set_ylabel('%s:5Co' %self.plasma.pellet.pel_diag, fontsize=fSize)
            ax_5co.legend(fontsize=legSize)
        ax_5co.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=True, direction='in', labelsize=tSize)
        ax_5co.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)
        ax_5co.set_xlabel('Time [s]', fontsize=fSize)
        ax_5co.set_xlim([tmin, tmax])
    
# Input profiles

        rho  = self.plasma.profiles.rho
        xlab = r'$\rho_{pol}$'

# Contours of input equil

        self.X, self.Y = np.meshgrid(self.plasma.equ.Rmesh, self.plasma.equ.Zmesh)
        self.tabs.setCurrentIndex(1)
        self.exp_tabs.setCurrentIndex(0)
        self.update_exp_plot()


    def set_rabbit_plots(self):

        self.jt = 0
        self.progress.setMaximum(self.nt-1)
        self.timeout = 1e-10
        self.rab_tabs.currentChanged.connect(self.update_rabbit_plot)

        self.mark1d  = {}
        self.line2d  = {}

        self.sig  = {}
        self.sgr  = {}

# Profiles

        for sig in ('powe', 'powi', 'bdens', 'bdep', 'jnbcd', 'jfi', 'press', \
               'torqe', 'torqi', 'wfi_par', 'wfi_perp', 'wfi_par_lab'):
# Summing over n_nbi
            self.sgr[sig] = np.sum(self.rbb.OUTPUT.__dict__[sig], axis=2)
        for sig in ('nrate', 'dvol', 'darea'):
# Summing over n_nbi
            self.sgr[sig] = np.nan_to_num(self.rbb.OUTPUT.__dict__[sig])

# Time traces
        for sig in ('prot', 'powe_tot', 'powi_tot', 'pshine', 'pcxloss', 'inbcd'):
# Summing over n_nbi
            self.sig[sig] = np.sum(self.rbb.OUTPUT.__dict__[sig], axis=1)

# Plots

        self.fig_o1d.clf()
        self.fig_o2d.clf()

        txt_init = '# %d  t=%5.3f s' %(self.rbb.shot, self.rbb.time[self.jt])
        self.txt_o1d = self.fig_o1d.text(0.5, 0.95, txt_init, ha='center', fontsize=fontSize)
        self.txt_o2d = self.fig_o2d.text(0.5, 0.95, txt_init, ha='center', fontsize=fontSize)

# Output scalars

        nrow = 2
        ncol = 4
        jplot = 1
        for trace, darr in self.sig.items():
            ylab = '%s %s' %(trace, darr.units)
            ax_o1d = self.fig_o1d.add_subplot(nrow, ncol, jplot)
            ax_o1d.set_xlabel('t [s]', fontsize=fontSize2)
            ax_o1d.set_ylabel(ylab, fontsize=fontSize2)
            ax_o1d.plot(self.rbb.time, darr, 'b-')
            ax_o1d.grid('on')
            ax_o1d.set_ylim([0, 1.1*np.max(darr) + 0.1])
            self.mark1d[trace], = ax_o1d.plot(self.rbb.time[0], darr[0], 'go')
            jplot += 1
            ax_o1d.tick_params(axis='both', labelsize=tickSize2)
            ax_o1d.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
            ax_o1d.yaxis.major.formatter._useMathText = True

# Output profiles

        nrow = 3
        ncol = 5
        jplot = 1
        for prof, darr in self.sgr.items():
            xlab = r'$\rho_{tor}$'
            ylab = '%s %s' %(prof, darr.units)

            ax_o2d = self.fig_o2d.add_subplot(nrow, ncol, jplot)
            ax_o2d.set_xlim([0, 1])
            if np.max(darr) > 0:
                ax_o2d.set_ylim([0, 1.1*np.max(darr)])
            ax_o2d.set_xlabel(xlab, fontsize=fontSize2)
            ax_o2d.set_ylabel(ylab, fontsize=fontSize2)
            self.line2d[prof], = ax_o2d.plot(self.rbb.OUTPUT.rhot_out, darr[0, :], 'b-')
            ax_o2d.grid('on')
            ax_o2d.tick_params(axis='both', labelsize=tickSize2)
            ax_o2d.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
            ax_o2d.yaxis.major.formatter._useMathText = True
            jplot += 1

        self.tabs.setCurrentIndex(2)
        self.rab_tabs.setCurrentIndex(0)
        self.update_rabbit_plot()


    def set_torbeam_plots(self):

        fSize   = fontSizePaper
        lWidth  = lWid
        tSize   = tickSizePaper
        legSize = 18

        self.jt = 0
        self.progress.setMaximum(self.nt-1)
        self.timeout = 1e-10
        self.tbm_tabs.currentChanged.connect(self.update_torbeam_plot)

        self.mark1d  = {}
        self.line2d  = {}

        self.sig  = {}
        self.sgr  = {}

# Plots

        self.fig_pech.clf()
        self.fig_jeccd.clf()
        self.fig_beam.clf()

        txt_init = '# %d  t=%5.3f s' %(self.tbb.shot, self.tbb.time[self.jt])
        self.txt_pech  = self.fig_pech.text( 0.5, 0.95, txt_init, ha='center', fontsize=fSize)
        self.txt_jeccd = self.fig_jeccd.text(0.5, 0.95, txt_init, ha='center', fontsize=fSize)
        self.txt_beam  = self.fig_beam.text( 0.5, 0.95, txt_init, ha='center', fontsize=fSize)

# Output PECRH

        nrow = 2
        ncol = 4
        darr = self.tbb.OUTPUT.power_out
        self.pech = {}
        for jgy in range(self.n_gy):
            xlab = r'$\rho_{pol}$'
            ylab = r'dP$_{ECH}$/dV gy%d %s' %(jgy+1, darr.units)

            ax_pech = self.fig_pech.add_subplot(nrow, ncol, jgy+1)
            ax_pech.set_xlim([0, 1])
            if np.max(darr) > 0:
                ax_pech.set_ylim([0, 1.1*np.max(darr)])
            ax_pech.set_xlabel(xlab, fontsize=fontSize)
            ax_pech.set_ylabel(ylab, fontsize=fontSize)
            self.pech[jgy], = ax_pech.plot(self.tbb.OUTPUT.rhop_out, darr[0, :, jgy], 'b-')
            ax_pech.grid('on')
            ax_pech.tick_params(axis='both', labelsize=tickSize)
            ax_pech.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
            ax_pech.yaxis.major.formatter._useMathText = True

# Output jECCD

        nrow = 2
        ncol = 4
        darr = self.tbb.OUTPUT.jeccd_out
        self.jeccd = {}
        for jgy in range(self.n_gy):
            xlab = r'$\rho_{pol}$'
            ylab = r'j$_{ECCD}$ gy%d %s' %(jgy+1, darr.units)

            ax_jec = self.fig_jeccd.add_subplot(nrow, ncol, jgy+1)
            ax_jec.set_xlim([0, 1])
            if np.max(darr) > 0:
                ax_jec.set_ylim([0, 1.1*np.max(darr)])
            ax_jec.set_xlabel(xlab, fontsize=fontSize)
            ax_jec.set_ylabel(ylab, fontsize=fontSize)
            self.jeccd[jgy], = ax_jec.plot(self.tbb.OUTPUT.rhop_out, darr[0, :, jgy], 'b-')
            ax_jec.grid('on')
            ax_jec.tick_params(axis='both', labelsize=tickSize)
            ax_jec.ticklabel_format(axis='y', style='sci', scilimits=(-4, 4))
            ax_jec.yaxis.major.formatter._useMathText = True

# Beam propagation

        ax_beam_pol = self.fig_beam.add_subplot(1, 2, 1, aspect='equal')
        if hasattr(self, 'gc_d'):
            for gc in self.gc_d.values():
                ax_beam_pol.plot(100*gc.r, 100*gc.z, 'b-', linewidth=lWidth)

        ax_beam_pol.set_xlabel('R [cm]', fontsize=fSize)
        ax_beam_pol.set_ylabel('z [cm]', fontsize=fSize)
        self.beam_pol_c = {}
        self.beam_pol_u = {}
        self.beam_pol_l = {}
        for jgy in range(self.n_gy):
            self.beam_pol_c[jgy], = ax_beam_pol.plot(self.tbb.OUTPUT.beam_pol_c_r[0][jgy], self.tbb.OUTPUT.beam_pol_c_z[0][jgy], color=colors[jgy], label='%d' %(jgy+1), linewidth=lWidth)
            self.beam_pol_u[jgy], = ax_beam_pol.plot(self.tbb.OUTPUT.beam_pol_u_r[0][jgy], self.tbb.OUTPUT.beam_pol_u_z[0][jgy], color=colors[jgy], linestyle='dashed', linewidth=lWidth)
            self.beam_pol_l[jgy], = ax_beam_pol.plot(self.tbb.OUTPUT.beam_pol_l_r[0][jgy], self.tbb.OUTPUT.beam_pol_l_z[0][jgy], color=colors[jgy], linestyle='dashed', linewidth=lWidth)
        ax_beam_pol.tick_params(axis='both', which='both', direction='in', labelsize=tSize, width=lWidth)
        ax_beam_pol.legend(fontsize=legSize)

        ax_beam_hor = self.fig_beam.add_subplot(1, 2, 2, aspect='equal')
        try:
            tor_d = sf.getgc_tor()
        except:
            pass
        if 'tor_d' in locals():
            for tor_pl in tor_d.values():
                ax_beam_hor.plot(100*tor_pl.x, 100*tor_pl.y, 'b-', linewidth=lWidth)

        ax_beam_hor.set_xlabel('x [cm]', fontsize=fSize)
        ax_beam_hor.set_ylabel('y [cm]', fontsize=fSize)
        self.beam_hor_c = {}
        self.beam_hor_u = {}
        self.beam_hor_l = {}
        for jgy in range(self.n_gy):
            self.beam_hor_c[jgy], = ax_beam_hor.plot(self.tbb.OUTPUT.beam_hor_c_r[0][jgy], self.tbb.OUTPUT.beam_hor_c_z[0][jgy], color=colors[jgy], label='%d' %(jgy+1), linewidth=lWidth)
            self.beam_hor_u[jgy], = ax_beam_hor.plot(self.tbb.OUTPUT.beam_hor_u_r[0][jgy], self.tbb.OUTPUT.beam_hor_u_z[0][jgy], color=colors[jgy], linestyle='dashed')
            self.beam_hor_l[jgy], = ax_beam_hor.plot(self.tbb.OUTPUT.beam_hor_l_r[0][jgy], self.tbb.OUTPUT.beam_hor_l_z[0][jgy], color=colors[jgy], linestyle='dashed', linewidth=lWidth)
        ax_beam_hor.tick_params(axis='both', which='both', direction='in', labelsize=tSize, width=lWidth)
        ax_beam_hor.legend(fontsize=legSize, loc=3)

        self.tabs.setCurrentIndex(3)
        self.tbm_tabs.setCurrentIndex(0)
        self.update_torbeam_plot()


    def set_nemec_plots(self):

        fSize = 18
        lWidth = lwPaper
        tSize = 15

        fSizeC = 18
        lWidthC = lWid
        tSizeC = 15

        self.jt = 0
        self.progress.setMaximum(self.nt-1)
        self.timeout = 1e-10
        self.vmc_tabs.currentChanged.connect(self.update_nemec_plot)
        txt_init = '# %d  t=%5.3f s' %(self.vmc.shot, self.vmc.time[0])
        self.txt_vmbnd  = self.fig_vmbnd.text( 0.5, 0.95, txt_init, ha='center', fontsize=fSizeC)
        self.fig_vmbnd.text( 0.60, 0.75, 'NEMEC', ha='center', fontsize=fSizeC)
        self.txt_vmfmom = self.fig_vmfmom.text(0.5, 0.95, txt_init, ha='center', fontsize=fSize)
        self.txt_vmprof = self.fig_vmprof.text(0.5, 0.95, txt_init, ha='center', fontsize=fSize)

# Surface contours
        ax_vmbnd = self.fig_vmbnd.add_subplot(1, 1, 1, aspect='equal')
        ax_vmbnd.set_xlabel('R [m]', fontsize=fSizeC)
        ax_vmbnd.set_ylabel('z [m]', fontsize=fSizeC)
        ax_vmbnd.tick_params(axis='both', labelsize=tSizeC)
        if hasattr(self, 'gc_d'):
            for gc in self.gc_d.values():
                ax_vmbnd.plot(gc.r, gc.z, 'b-', linewidth=lWidthC)
        nt, nrho, ntheta = self.vmc.rbnd.shape
        self.bnd = {}
        for jrho in range(nrho):
            self.bnd[jrho], = ax_vmbnd.plot(self.vmc.rbnd[0, jrho, :], self.vmc.zbnd[0, jrho, :], 'g-', linewidth=lWidthC)

# Fourier moments
        mom_type = 4
        self.rcplot = {}
        self.rsplot = {}
        self.zcplot = {}
        self.zsplot = {}
        jplot = 1
        for jmom in range(self.vmc.m_moms):
            ax_rc = self.fig_vmfmom.add_subplot(mom_type, self.vmc.m_moms, jplot)
            ax_rs = self.fig_vmfmom.add_subplot(mom_type, self.vmc.m_moms, jplot+  self.vmc.m_moms)
            ax_zc = self.fig_vmfmom.add_subplot(mom_type, self.vmc.m_moms, jplot+2*self.vmc.m_moms)
            ax_zs = self.fig_vmfmom.add_subplot(mom_type, self.vmc.m_moms, jplot+3*self.vmc.m_moms)
            ax_rc.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=tSize)
            ax_rs.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=tSize)
            ax_zc.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=False, direction='in', labelsize=tSize)
            ax_zs.tick_params(axis='x', which='both', bottom='on', top='on', labelbottom=True , direction='in', labelsize=tSize)
            ax_rc.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)
            ax_rs.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)
            ax_zc.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)
            ax_zs.tick_params(axis='y', which='both', left='on', right='on', direction='in', labelsize=tSize)
            ax_rc.set_xlim([0, 1])
            ax_rs.set_xlim([0, 1])
            ax_zc.set_xlim([0, 1])
            ax_zs.set_xlim([0, 1])
            if jmom == 0:
                ax_rc.set_ylabel(r'$R_{cos}$ [cm]', fontsize=fSize)
                ax_rs.set_ylabel(r'$R_{sin}$ [cm]', fontsize=fSize)
                ax_zc.set_ylabel(r'$z_{cos}$ [cm]', fontsize=fSize)
                ax_zs.set_ylabel(r'$z_{sin}$ [cm]', fontsize=fSize)
            ax_zs.set_xlabel(r'$\Phi_N$', fontsize=fSize)
            self.rcplot[jmom], = ax_rc.plot(self.vmc.tfl_n, 100*self.vmc.rc[0, :, jmom], 'g-', linewidth=lWidth)
            self.rsplot[jmom], = ax_rs.plot(self.vmc.tfl_n, 100*self.vmc.rs[0, :, jmom], 'g-', linewidth=lWidth)
            self.zcplot[jmom], = ax_zc.plot(self.vmc.tfl_n, 100*self.vmc.zc[0, :, jmom], 'g-', linewidth=lWidth)
            self.zsplot[jmom], = ax_zs.plot(self.vmc.tfl_n, 100*self.vmc.zs[0, :, jmom], 'g-', linewidth=lWidth)
            jplot += 1

# Input pres, q
        ax_pres = self.fig_vmprof.add_subplot(1, 2, 1)
        ax_q    = self.fig_vmprof.add_subplot(1, 2, 2)
        ax_pres.set_xlim([0, 1])
        ax_q.set_xlim(   [0, 1])
        ax_pres.set_xlabel(r'$\Phi_N$', fontsize=fSize)
        ax_pres.set_ylabel('Pressure [Pa]', fontsize=fSize)
        ax_q.set_xlabel(r'$\Phi_N$'   , fontsize=fSize)
        ax_q.set_ylabel(r'Safety factor'  , fontsize=fSize)
        ax_q.tick_params(   axis='both', labelsize=tSize)
        ax_pres.tick_params(axis='both', labelsize=tSize)
        tfl_n = self.vmc.tfl[0, :]/self.vmc.tfl[0, -1]
        self.pplot, = ax_pres.plot(tfl_n, self.vmc.pres[0, :], 'g-')
        self.qplot, = ax_q.plot(   tfl_n, self.vmc.q   [0, :], 'g-')

        self.tabs.setCurrentIndex(4)
        self.vmc_tabs.setCurrentIndex(0)
        self.update_nemec_plot()


    def set_feqis_plots(self):

        fSize  = fontSizePaper
        lWidth = lwPaper
        tSize  = tickSizePaper

        fSizeC = 18
        lWidthC = 2.7
        tSizeC = 15

        self.jt = 0
        self.progress.setMaximum(self.nt-1)
        self.timeout = 1e-10
        self.feq_tabs.currentChanged.connect(self.update_feqis_plot)
        txt_init = '# %d  t=%5.3f s' %(self.feq.shot, self.feq.time[0])
        self.txt_fqbnd  = self.fig_fqbnd.text( 0.5, 0.95, txt_init, ha='center', fontsize=fSizeC)
        self.fig_fqbnd.text( 0.60, 0.75, 'FEQIS', ha='center', fontsize=fSizeC)
        self.txt_fqprof = self.fig_fqprof.text(0.5, 0.95, txt_init, ha='center', fontsize=fSize)

# Surface contours
        ax_fqbnd = self.fig_fqbnd.add_subplot(1, 1, 1, aspect='equal')
        ax_fqbnd.set_xlabel('R [m]', fontsize=fSizeC)
        ax_fqbnd.set_ylabel('z [m]', fontsize=fSizeC)
        ax_fqbnd.tick_params(axis='both', labelsize=tSizeC)
        if hasattr(self, 'gc_d'):
            for gc in self.gc_d.values():
                ax_fqbnd.plot(gc.r, gc.z, 'b-', linewidth=lWidthC)
        nt, nrho, ntheta = self.feq.rsurf.shape
        self.bnd = {}
        ind_plot = np.append(np.arange(ntheta), 0)
        for jrho in range(nrho):
            self.bnd[jrho], = ax_fqbnd.plot(self.feq.rsurf[0, jrho, ind_plot], self.feq.zsurf[0, jrho, ind_plot], 'g-', linewidth=lWidthC)

# Input pprime, ffprime
        ax_pprime = self.fig_fqprof.add_subplot(1, 2, 1)
        ax_ffprim = self.fig_fqprof.add_subplot(1, 2, 2)
        ax_pprime.set_xlim([0, 1])
        ax_ffprim.set_xlim([0, 1])
        ax_pprime.set_xlabel(r'$\Psi_N$'  , fontsize=fSize)
        ax_pprime.set_ylabel(r'dP/d$\Psi$ [Pa/Web]', fontsize=fSize)
        ax_ffprim.set_xlabel(r'$\Psi_N$'  , fontsize=fSize)
        ax_ffprim.set_ylabel("FF'", fontsize=fSize)
        ax_pprime.tick_params(axis='both', labelsize=tSize)
        ax_ffprim.tick_params(axis='both', labelsize=tSize)
        nrho = self.feq.ffprime_in.shape[1]
        psin = np.linspace(0, 1, nrho)
        self.pprimeplot, = ax_pprime.plot(psin, self.feq.pprime_in [0, :], 'g-', linewidth=lWidth)
        self.ffprimplot, = ax_ffprim.plot(psin, self.feq.ffprime_in[0, :], 'g-', linewidth=lWidth)

        self.tabs.setCurrentIndex(5)
        self.feq_tabs.setCurrentIndex(0)
        self.update_feqis_plot()


    def uf1t_rabbit(self):

        self.ff = False
        self.rw = False

        uf = ufiles.UFILE()
        uf.shot = self.rbb.shot
        uf.X = {'label': 'Time:'.ljust(20) + 's', 'data': self.rbb.time}
        uf.pre = 'A'
        for trace, darr in self.sig.items():
            uf.ext = trace
            uf.f['label'] = trace.ljust(20) + darr.units
            uf.f['data'] = darr
            uf.write()


    def uf1r_rabbit(self):

        self.ff = False
        self.rw = False

        uf = ufiles.UFILE()
        uf.shot = self.rbb.shot
        uf.pre = 'A'
 
        uf.X = {'label': 'rho_tor', 'data': self.rbb.OUTPUT.rhot_out}

        for prof, darr in self.sgr.items():
            uf.ext = prof
            uf.f['label'] = prof.ljust(20) + darr.units
            uf.f['data']  = darr[self.jt, :]
            uf.scalar = [{ 'label': 'Time:'.ljust(20) + 's', 'data': self.rbb.time[self.jt] }]
            uf.write()


    def uf2_rabbit(self):

        self.ff = False
        self.rw = False

        uf = ufiles.UFILE()
        uf.shot = self.rbb.shot
        uf.pre = 'A'
        uf.X = {'label': 'Time'.ljust(20) + 'Seconds', 'data': self.rbb.time}
        uf.Y = {'label': 'rho_tor', 'data': self.rbb.OUTPUT.rhot_out}
        for prof, darr in self.sgr.items():
            uf.ext = prof + '2'
            uf.f['label'] = prof.ljust(20) + darr.units
            uf.f['data']  = darr
            uf.write()


    def write_rabbit_cdf(self):

        if hasattr(self, 'rbb'):
            code_io.code2cdf(self.rbb)
        else:
            logger.error('Run RABBIT first')


    def write_rabbit_sf(self):

        if hasattr(self, 'rbb'):
            code_io.rb2sf(self.rbb)
        else:
            logger.error('Run RABBIT first')


    def write_torbeam_cdf(self):

        if hasattr(self, 'tbb'):
            code_io.code2cdf(self.tbb, code='tb')
        else:
            logger.error('Run TORBEAM first')


    def smov_rabbit(self):

        self.ff = False
        self.rw = False

        jtab = self.tabs.currentIndex()
        figs = [self.fig_o1d, self.fig_o2d]
        self.viewfig = figs[jtab-1]

# Store png's
        order = int(np.log10(self.nt)) + 1 # To have nice alphabetic sorted files
        fmt = '%' + str(order) + 'd'
        for jtim in range(self.nt):
            self.jt = jtim
            self.update_plot()
            jstr = (fmt % self.jt).replace(' ', '0')
            fout = '%drb-%s.png' %(self.rbb.shot, jstr)
            logger.info('Stored %s', fout)
            self.viewfig.savefig(fout, dpi=100)

# Concatenate png's to avi
        out_mov = '%srb.avi' %self.rbb.shot
 
        command = ('ffmpeg', '-framerate', '10', '-i', '38384rb-%2d.png', '-vcodec', 'mpeg4', out_mov)
        os.spawnvp(os.P_WAIT, encoder, command)
        os.system('rm *.png')

        logger.info('Stored movie %s', out_mov)


    def update_slider(self, value):

        self.jt = int(value + 0.5)
        self.update_plot()


    def update_plot(self):

        jtab_main = self.tabs.currentIndex()
        if jtab_main == 0:
            self.progress.setVisible(False)
            for but in self.navi_but.values():
                but.setVisible(False)
        elif jtab_main == 1:
            self.code = 'exp'
            self.update_exp_plot()
        elif jtab_main == 2:
            self.code = 'rabbit'
            self.update_rabbit_plot()
        elif jtab_main == 3:
            self.code = 'torbeam'
            self.update_torbeam_plot()
        elif jtab_main == 4:
            self.code = 'nemec'
            self.update_nemec_plot()
        elif jtab_main == 5:
            self.code = 'feqis'
            self.update_feqis_plot()


    def update_exp_plot(self):

        for but in self.navi_but.values():
            but.setVisible(True)
        self.progress.setVisible(True)
        self.progress.setValue(self.jt)

        tim = self.plasma.profiles.time[self.jt]
        txt = '# %d  t=%5.3f s' %(self.plasma.gpar.shot, tim)

        jtab = self.exp_tabs.currentIndex()

        if jtab == 0:

            self.sepscat.set_data(self.plasma.equ.sep.rscat[self.jt][0], self.plasma.equ.sep.zscat[self.jt][0])

            fit_error = self.plasma.equ.sep.error[self.jt]
            if fit_error == 0:
                self.sepfit.set_visible(True)
                self.sepfit.set_data(self.plasma.equ.sep.rfit[self.jt], self.plasma.equ.sep.zfit[self.jt])
                self.errtxt.set_text('')
            else:
                self.sepfit.set_visible(False)
                self.errtxt.set_text(sepFit_ErrorMessage[fit_error])

            self.mag.set_data([self.plasma.equ.Rmag  [self.jt]], [self.plasma.equ.Zmag [self.jt]])
            if hasattr(self, 'xu'):
                if hasattr(self.plasma.equ, 'Rxpu'):
                    self.xu.set_data( [self.plasma.equ.Rxpu  [self.jt]], [self.plasma.equ.Zxpu [self.jt]])
                    self.xo.set_data( [self.plasma.equ.Rxpo  [self.jt]], [self.plasma.equ.Zxpo [self.jt]])
                    self.u.set_data(  [self.plasma.equ.Rzunt [self.jt]], [self.plasma.equ.Zunt [self.jt]])
                    self.o.set_data(  [self.plasma.equ.Rzoben[self.jt]], [self.plasma.equ.Zoben[self.jt]])

            self.txt_i_sep.set_text(txt)
            self.fig_i_sep.canvas.draw()

        elif jtab == 1:

            for key, sigplot in self.marksig.items():
                sigplot.set_data([self.plasma.sig.t_ds[self.jt]], [self.plasma.sig.__dict__[key].data_ds[self.jt]])
            self.zefplot.set_data([self.plasma.zef.t_ds[self.jt]], [self.plasma.zef.data_ds[self.jt]])
            self.txt_i_sig.set_text(txt)
            self.fig_i_sig.canvas.draw()

        elif jtab == 2:
            if hasattr(self, 'n_nbi'):
                for jnb in range(self.n_nbi):
                    self.marknbi[jnb].set_data([self.plasma.nbi.t_ds[self.jt]], [self.plasma.nbi.power_ds[self.jt, jnb]])
                self.txt_i_nbi.set_text(txt)
                self.fig_i_nbi.canvas.draw()

        elif jtab == 3:
            if hasattr(self, 'n_gy'):
                for jgy in range(self.n_gy):
                    self.markech[jgy].set_data([self.plasma.ech.t_ds[self.jt]], [self.plasma.ech.power_ds[self.jt, jgy]])
                self.txt_i_ech.set_text(txt)
                self.fig_i_ech.canvas.draw()

        elif jtab == 4:
            if hasattr(self, 'n_gy'):
                for jgy in range(self.n_gy):
                    self.markphi[jgy].set_data([self.plasma.ech.t_ds[self.jt]], [self.plasma.ech.phi_ds[self.jt, jgy]])
                self.txt_i_phi.set_text(txt)
                self.fig_i_phi.canvas.draw()

        elif jtab == 5:
            if hasattr(self, 'n_gy'):
                for jgy in range(self.n_gy):
                    self.markthe[jgy].set_data([self.plasma.ech.t_ds[self.jt]], [self.plasma.ech.theta_ds[self.jt, jgy]])
                self.txt_i_the.set_text(txt)
                self.fig_i_the.canvas.draw()

        elif jtab == 6:
            if hasattr(self, 'n_ant') and hasattr(self, 'power_ds'):
                for jant in range(self.n_ant):
                    self.markicr[jant].set_data([self.plasma.icr.t_ds[self.jt]], [self.plasma.icr.power_ds[self.jt, jant]])
                self.txt_i_icr.set_text(txt)
                self.fig_i_icr.canvas.draw()

# 7th tab: pellet

        elif jtab == 7:

            pass

        elif jtab == 8:

            self.fig_i_equ.clf()

            ax_pfm = self.fig_i_equ.add_subplot(1, 4, 1, aspect='equal')
            ax_pfm.set_xlabel('R [m]', fontsize=fontSize2)
            ax_pfm.set_ylabel('z [m]', fontsize=fontSize2)
            ax_pfm.set_title('Poloidal flux', fontsize=titleSize2)
            ax_pfm.tick_params(axis='both', labelsize=tickSize2)
            n_levels = 21
            psi2 = self.plasma.equ.pfm[:, :, self.jt].T
            levels = np.linspace(np.min(psi2), np.max(psi2), n_levels)
            pfm = ax_pfm.contourf(self.X, self.Y, psi2, levels=levels)
            self.fig_i_equ.colorbar(pfm, aspect=20, shrink=0.8)
# overplot separatrix
            ax_pfm.contour(self.X, self.Y, psi2, levels=[self.plasma.equ.psix[self.jt]], colors='r', linestyles='-', linewidths=2.)

            if hasattr(self.plasma.equ, 'Bt'):
                ax_bt = self.fig_i_equ.add_subplot(1, 4, 2, aspect='equal')
                ax_bt.set_xlabel('R [m]', fontsize=fontSize2)
                ax_bt.set_ylabel('z [m]', fontsize=fontSize2)
                ax_bt.set_title(r'B$_{tor}$', fontsize=titleSize2)
                ax_bt.tick_params(axis='both', labelsize=tickSize2)
                n_levels = 21
                bt2 = self.plasma.equ.Bt[self.jt].T
                levels = np.linspace(np.min(bt2), np.max(bt2), n_levels)
                bt = ax_bt.contourf(self.X, self.Y, bt2, levels=levels)
                self.fig_i_equ.colorbar(bt, aspect=20, shrink=0.8)
# overplot separatrix
                ax_bt.contour(self.X, self.Y, psi2, levels=[self.plasma.equ.psix[self.jt]], colors='r', linestyles='-', linewidths=2.)

                ax_br = self.fig_i_equ.add_subplot(1, 4, 3, aspect='equal')
                ax_br.set_xlabel('R [m]', fontsize=fontSize2)
                ax_br.set_ylabel('z [m]', fontsize=fontSize2)
                ax_br.set_title(r'B$_R$', fontsize=titleSize2)
                ax_br.tick_params(axis='both', labelsize=tickSize2)
                n_levels = 21
                br2 = self.plasma.equ.Br[self.jt].T
                levels = np.linspace(np.min(br2), np.max(br2), n_levels)
                br = ax_br.contourf(self.X, self.Y, br2, levels=levels)
                self.fig_i_equ.colorbar(br, aspect=20, shrink=0.8)
# overplot separatrix
                ax_br.contour(self.X, self.Y, psi2, levels=[self.plasma.equ.psix[self.jt]], colors='r', linestyles='-', linewidths=2.)

                ax_bz = self.fig_i_equ.add_subplot(1, 4, 4, aspect='equal')
                ax_bz.set_xlabel('R [m]', fontsize=fontSize2)
                ax_bz.set_ylabel('z [m]', fontsize=fontSize2)
                ax_bz.set_title(r'B$_z$', fontsize=titleSize2)
                ax_bz.tick_params(axis='both', labelsize=tickSize2)
                n_levels = 21
                bz2 = self.plasma.equ.Bz[self.jt].T
                levels = np.linspace(np.min(bz2), np.max(bz2), n_levels)
                bz = ax_bz.contourf(self.X, self.Y, bz2, levels=levels)
                self.fig_i_equ.colorbar(bz, aspect=20, shrink=0.8)
# overplot separatrix
                ax_bz.contour(self.X, self.Y, psi2, levels=[self.plasma.equ.psix[self.jt]], colors='r', linestyles='-', linewidths=2.)

            self.txt_i_equ = self.fig_i_equ.text(0.5, 0.95, txt, ha='center', fontsize=titleSize)
            self.fig_i_equ.canvas.draw()

# Profiles
        elif jtab in (9, 10, 11, 12):
            self.currentProfLabel = prof_names[jtab-9]
            rho = self.plasma.profiles.rho
            profLabel = self.currentProfLabel
            profObj = self.plasma.profiles.__dict__[profLabel]
            yfit = profObj.yfit_rhop[self.jt]
            if profObj.diags == 'IMAS':
                err  = profObj.yerr_rhop[self.jt]
# Update the caplines
                self.caplines[profLabel][0].set_data((rho, yfit - err))
                self.caplines[profLabel][1].set_data((rho, yfit + err))
#Update the error bars
                self.barlinecols[profLabel][0].set_segments(zip(zip(rho, yfit - err), zip(rho, yfit + err)))
                self.fig_prof[profLabel].canvas.draw()
 
            self.fplot[profLabel].set_ydata(yfit)
            if not hasattr(profObj, 'xexp_rhop'):
                return
            rho = profObj.xexp_rhop[self.jt]
            dat = profObj.yexp_rhop[self.jt]
            ind_del = profObj.ind_del[self.jt]
            ind_ok = (np.isfinite(profObj.dat_ok[self.jt]))           
            xexp = rho[ind_ok]
            yexp = dat[ind_ok]
            err = profObj.yerr_rhop[self.jt, ind_ok]

            if self.currentProfLabel in self.xplot.keys():
                self.xplot[self.currentProfLabel].set_data(rho, dat)
            self.ok[profLabel].set_data(xexp, yexp)

# For gaussian and rec.spline fit, fill confidence region
            if hasattr(profObj, 'confid'):
                sigma = profObj.confid[self.jt]
                if np.max(sigma) > 0:
                    if hasattr(self, 'fill'):
                        self.fill.remove()

                    self.fill = self.ax1d[profLabel].fill_between(self.plasma.profiles.rho, yfit - sigma, yfit + sigma, facecolor='r', edgecolor='None', alpha=0.3)


# Update the caplines
            self.caplines[profLabel][0].set_data((xexp, yexp - err))
            self.caplines[profLabel][1].set_data((xexp, yexp + err))

#Update the error bars
            self.barlinecols[profLabel][0].set_segments(zip(zip(xexp, yexp - err), zip(xexp, yexp + err)))
            self.delPoints[profLabel].set_data(rho[ind_del], profObj.yexp_rhop[self.jt, ind_del])
            self.rejPoints[profLabel].set_data(rho[~ind_ok], profObj.yexp_rhop[self.jt, ~ind_ok])
            self.txt_prof[profLabel].set_text(txt)
            if len(self.tline) > 0:
                self.tline[profLabel].set_data([tim, tim], [self.plasma.profiles.rho[0], self.plasma.profiles.rho[-1]])

# Update contour plot
            if hasattr(profObj, 'yfit_rhop'):
                ctr = self.ax_ctr[profLabel].contourf(self.tg, self.xg, profObj.yfit_rhop)
            else:
                ctr = self.ax_ctr[profLabel].contourf(self.tg, self.xg, profObj.yfit_rhot)

            self.fig_prof[profLabel].canvas.draw()


    def update_rabbit_plot(self):

        for but in self.navi_but.values():
            but.setVisible(True)
        self.progress.setVisible(True)
        self.progress.setValue(self.jt)

        txt = '# %d  t=%5.3f s' %(self.rbb.shot, self.rbb.time[self.jt])

# Tabs

        jtab = self.rab_tabs.currentIndex()

        if jtab == 0:
            for trace, darr in self.sig.items():
                self.mark1d[trace].set_data([self.rbb.time[self.jt]], [darr[self.jt]])
            self.txt_o1d.set_text(txt)
            self.fig_o1d.canvas.draw()

        elif jtab == 1:
            for prof, darr in self.sgr.items():
                self.line2d[prof].set_ydata(darr[self.jt])
            self.txt_o2d.set_text(txt)
            self.fig_o2d.canvas.draw()


    def update_torbeam_plot(self):

        for but in self.navi_but.values():
            but.setVisible(True)
        self.progress.setVisible(True)
        self.progress.setValue(self.jt)

        txt = '# %d  t=%5.3f s' %(self.tbb.shot, self.tbb.time[self.jt])

# Tabs

        jtab = self.tbm_tabs.currentIndex()

        if jtab == 0:
            for jgy in range(self.n_gy):
                self.pech[jgy].set_ydata(self.tbb.OUTPUT.power_out[self.jt, :, jgy])
            self.txt_pech.set_text(txt)
            self.fig_pech.canvas.draw()

        elif jtab == 1:
            for jgy in range(self.n_gy):
                self.jeccd[jgy].set_ydata(self.tbb.OUTPUT.jeccd_out[self.jt, :, jgy])
            self.txt_jeccd.set_text(txt)
            self.fig_jeccd.canvas.draw()

        elif jtab == 2:
            for jgy in range(self.n_gy):
                self.beam_pol_c[jgy].set_xdata(self.tbb.OUTPUT.beam_pol_c_r[self.jt][jgy])
                self.beam_pol_c[jgy].set_ydata(self.tbb.OUTPUT.beam_pol_c_z[self.jt][jgy])
                self.beam_pol_u[jgy].set_xdata(self.tbb.OUTPUT.beam_pol_u_r[self.jt][jgy])
                self.beam_pol_u[jgy].set_ydata(self.tbb.OUTPUT.beam_pol_u_z[self.jt][jgy])
                self.beam_pol_l[jgy].set_xdata(self.tbb.OUTPUT.beam_pol_l_r[self.jt][jgy])
                self.beam_pol_l[jgy].set_ydata(self.tbb.OUTPUT.beam_pol_l_z[self.jt][jgy])

                self.beam_hor_c[jgy].set_xdata(self.tbb.OUTPUT.beam_hor_c_r[self.jt][jgy])
                self.beam_hor_c[jgy].set_ydata(self.tbb.OUTPUT.beam_hor_c_z[self.jt][jgy])
                self.beam_hor_u[jgy].set_xdata(self.tbb.OUTPUT.beam_hor_u_r[self.jt][jgy])
                self.beam_hor_u[jgy].set_ydata(self.tbb.OUTPUT.beam_hor_u_z[self.jt][jgy])
                self.beam_hor_l[jgy].set_xdata(self.tbb.OUTPUT.beam_hor_l_r[self.jt][jgy])
                self.beam_hor_l[jgy].set_ydata(self.tbb.OUTPUT.beam_hor_l_z[self.jt][jgy])
            self.txt_beam.set_text(txt)
            self.fig_beam.canvas.draw()


    def update_nemec_plot(self):

        for but in self.navi_but.values():
            but.setVisible(True)
        self.progress.setVisible(True)
        self.progress.setValue(self.jt)

        txt = '# %d  t=%5.3f s' %(self.vmc.shot, self.vmc.time[self.jt])

# Tabs

        jtab = self.vmc_tabs.currentIndex()
        if jtab == 0:
            for jrho in range(self.vmc.nrho):
                self.bnd[jrho].set_data(self.vmc.rbnd[self.jt, jrho, :], self.vmc.zbnd[self.jt, jrho, :])
            self.txt_vmbnd.set_text(txt)
            self.fig_vmbnd.canvas.draw()
        elif jtab == 1:
            for jmom in range(self.vmc.m_moms):
                self.rcplot[jmom].set_ydata(100.*self.vmc.rc[self.jt, :, jmom])
                self.rsplot[jmom].set_ydata(100.*self.vmc.rs[self.jt, :, jmom])
                self.zcplot[jmom].set_ydata(100.*self.vmc.zc[self.jt, :, jmom])
                self.zsplot[jmom].set_ydata(100.*self.vmc.zs[self.jt, :, jmom])
            self.txt_vmfmom.set_text(txt)
            self.fig_vmfmom.canvas.draw()
        elif jtab == 2:
            tfl_n = self.vmc.tfl[self.jt, :]/self.vmc.tfl[self.jt, -1]
            self.pplot.set_data(tfl_n, self.vmc.pres[self.jt, :])
            self.qplot.set_data(tfl_n, self.vmc.q   [self.jt, :])
            self.txt_vmprof.set_text(txt)
            self.fig_vmprof.canvas.draw()


    def update_feqis_plot(self):

        for but in self.navi_but.values():
            but.setVisible(True)
        self.progress.setVisible(True)
        self.progress.setValue(self.jt)

        txt = '# %d  t=%5.3f s' %(self.feq.shot, self.feq.time[self.jt])

# Tabs

        jtab = self.feq_tabs.currentIndex()
        if jtab == 0:
            nrho, ntheta = self.feq.rsurf.shape[-2:]
            ind_plot = np.append(np.arange(ntheta), 0)
            for jrho in range(nrho):
                self.bnd[jrho].set_data(self.feq.rsurf[self.jt, jrho, ind_plot], self.feq.zsurf[self.jt, jrho, ind_plot])
            self.txt_fqbnd.set_text(txt)
            self.fig_fqbnd.canvas.draw()
        elif jtab == 1:
            self.pprimeplot.set_ydata(self.feq.pprime_in [self.jt, :])
            self.ffprimplot.set_ydata(self.feq.ffprime_in[self.jt, :])
            self.txt_fqprof.set_text(txt)
            self.fig_fqprof.canvas.draw()

#----------
# Animation
#----------

    def previous(self):

        self.ff = False
        self.rw = False
        if self.jt > 0:
            self.jt -= 1
            self.update_plot()
        if self.jt == 0:
            self.play_pause.setIcon(QIcon('%s/ButtonPlay.gif' %heat_dir))
            self.play_pause.clicked.disconnect()
            self.play_pause.clicked.connect(self.play)


    def forward(self):

        self.ff = False
        self.rw = False
        if self.jt < self.nt - 1:
            self.jt += 1
            self.update_plot()
        if self.jt == self.nt-1:
            self.play_pause.setIcon(QIcon('%s/ButtonPlay.gif' %heat_dir))
            self.play_pause.clicked.disconnect()
            self.play_pause.clicked.connect(self.play)


    def pause(self):

        self.ff = False
        self.rw = False
        self.update_plot()
        self.play_pause.setIcon(QIcon('%s/ButtonPlay.gif' %heat_dir))
        self.play_pause.clicked.disconnect()
        self.play_pause.clicked.connect(self.play)


    def play(self):

        self.ff = True
        self.rw = False
        self.play_pause.setIcon(QIcon('%s/ButtonPause.gif' %heat_dir))
        self.play_pause.disconnect()
        self.play_pause.clicked.connect(self.pause)
        while self.ff and self.jt < self.nt - 1:
            self.jt += 1
            self.update_plot()
            if self.code == 'rabbit':
                self.fig_o1d.canvas.start_event_loop(self.timeout)
            elif self.code == 'torbeam':
                self.fig_pech.canvas.start_event_loop(self.timeout)
            elif self.code == 'exp':
                self.fig_i_nbi.canvas.start_event_loop(self.timeout)
        if self.jt == self.nt-1:
            self.play_pause.setIcon(QIcon('%s/ButtonPlay.gif' %heat_dir))
            self.play_pause.clicked.disconnect()
            self.play_pause.clicked.connect(self.play)


    def rewind(self):

        self.ff = False
        self.rw = True
        while self.rw and self.jt > 0:
            self.jt -= 1
            self.update_plot()
            if self.code == 'rabbit':
                self.fig_o1d.canvas.start_event_loop(self.timeout)
            elif self.code == 'torbeam':
                self.fig_pech.canvas.start_event_loop(self.timeout)
            elif self.code == 'exp':
                self.fig_i_nbi.canvas.start_event_loop(self.timeout)
        if self.jt == 0:
            self.play_pause.setIcon(QIcon('%s/ButtonPlay.gif' %heat_dir))
            self.play_pause.clicked.disconnect()
            self.play_pause.clicked.connect(self.play)

#------------
# Interaction
#------------

    def MouseInteraction(self, event):

        if event.button == 1:
            if self.ctrl:
                self.delete_frame(event) 
            else:
                self.delete_point(event) 
        elif event.button == 2: # press mouse-middle
            self.undo(event)
        elif event.button == 3 and self.ctrl:
            self.stop = True
            self.previous()
        elif event.button == 3:
            self.stop = True
            self.forward() 


    def undo(self, event):

        profObj = self.plasma.profiles.__dict__[self.currentProfLabel]
        profObj.ind_del[self.jt] = []
        out = fit_prof.fit1d((profObj.xexp_rhop[self.jt], profObj.yexp_rhop[self.jt], \
            profObj.yerr_rhop[self.jt], self.plasma.profiles.rho, self.plasma.profiles.time[self.jt], \
            profObj.fit_tol, profObj.sigmas, profObj.fit_method ))
        nx_fit = len(self.plasma.profiles.rho)
        nx_in = profObj.xexp_rhop.shape[1]
        profObj.yfit_rhop[self.jt] = out[: nx_fit]
        profObj.dat_ok[self.jt] = out[nx_fit: nx_fit+nx_in]
        if profObj.fit_method in ('Rec. spline', 'Gaussian fit'):
            profObj.confid[self.jt] = out[nx_fit+nx_in: ]

        if self.jt in profObj.tnot:
            profObj.tnot.remove(self.jt)
#            self.del2d[self.jt].set_data([], [])
#            self.del2d[self.jt].set_3d_properties([])
        self.update_exp_plot()


    def del_frame(self, jt):

        profObj = self.plasma.profiles.__dict__[self.currentProfLabel]
        if jt not in profObj.tnot:
            profObj.tnot.append(jt)
        profObj.ind_del[jt] = range(len(profObj.xexp_rhop[jt, :]))
#        self.del2d[jt], = self.ax2d.plot(self.rho_spl, self.tg[jt], self.yfit_rhop[jt, :], 'b-')

        self.update_exp_plot()


    def delete_frame(self, event):

        if self.ax1d[self.currentProfLabel] != event.inaxes:
            return
        if not self.stop:
            return
        self.del_frame(self.jt)


    def delete_point(self, event):

        if self.ax1d[self.currentProfLabel] != event.inaxes:
            return

        if not self.stop:
            return

        profLabel = self.currentProfLabel
        profObj = self.plasma.profiles.__dict__[profLabel]

        sx = self.ax1d[profLabel].get_xlim()
        dx = sx[-1] - sx[0]
        sy = self.ax1d[profLabel].get_ylim()
        dy = sy[-1] - sy[0]

        x    = profObj.xexp_rhop[self.jt, :].copy()
        y    = profObj.yexp_rhop[self.jt, :].copy()
        yerr = profObj.yerr_rhop[self.jt, :].copy()

        y   [profObj.ind_del[self.jt]] = np.nan
        yerr[profObj.ind_del[self.jt]] = np.infty

        idel = np.nanargmin(np.hypot((x - event.xdata)/dx, (y - event.ydata)/dy))
        profObj.ind_del[self.jt].append(idel)

        y[idel] = np.nan
        if np.sum(np.isfinite(y)) > 1:
            rhop = self.plasma.profiles.rho
            time = self.plasma.profiles.time
            out = fit_prof.fit1d( (x, y, yerr, rhop, time, profObj.fit_tol, profObj.sigmas, profObj.fit_method) )
            nx_fit = len(rhop)
            nx_in  = len(x)
            profObj.yfit_rhop[self.jt] = out[: nx_fit]
            profObj.dat_ok[self.jt] = out[nx_fit: nx_fit+nx_in]
            if profObj.fit_method in ('Rec. spline', 'Gaussian fit'):
                profObj.confid[self.jt] = out[nx_fit+nx_in: ]
            self.update_exp_plot()
        else:
            self.del_frame(self.jt)


    def on_key(self, event):
        if 'control' == event.key:
            self.ctrl = True
        if ' ' == event.key:
            if self.stop:
                self.play()
            else:
                self.pause()


    def off_key(self, event):
        if event.key in ('ctrl+control', 'control') :
            self.ctrl = False

#----------------
# Dump code input
#----------------

    def writeEqdsk(self):

        if not hasattr(self, 'plasma'):
            self.expFromShotfile()
        tim = self.plasma.profiles.time[self.jt]
        geq = sf.to_geqdsk(self.plasma.equ, t_in=tim)
        f_eqdsk = '%d_%5.3f.eqdsk' %(self.plasma.gpar.shot, tim)
        eqd = eqdsk.EQDSK()
        eqd.write(f_eqdsk, geq=geq)


    def writeTranspInput(self):

        if not hasattr(self.plasma, 'nbi'):
            self.expFromShotfile()
        _, gtra = self.get_gui_tab('transp')

        write_u.tr_u_geo(self.plasma.equ.sep)
        write_u.write_u1d(self.plasma)
        write_u.write_q(self.plasma.equ)
        write_u.write_u2d(self.plasma.profiles)
        runid = wrnml.w_nml(self.plasma, gtra)


    def writeAstraInput(self):

        if not hasattr(self.plasma, 'nbi'):
            self.expFromShotfile()
        write_u.write_u2d(self.plasma.profiles)
        astra_nml_u.astra_nml_u(self.plasma)
        astra_exp.astra_exp(self.plasma)


    def fill_ids(self):

        if not hasattr(self.plasma, 'nbi'):
            logger.error('Fetch exp data first')
            return

        if 'imas' not in sys.modules:
            logger.error('No IMAS available')
            return

# Create IMAS object, initialise IDS structure
        if hasattr(self, 'tbb'):
            tbb = self.tbb
        else:
            tbb = None
        if hasattr(self, 'rbb'):
            rbb = self.rbb
        else:
            rbb = None
        _, self.plasma.gpar = self.get_gui_tab('exp')
        last_json = json.dumps(self.gui2json(), indent=1)
        with open(f"{json_dir}/last.json", 'w') as ljson:
            ljson.write(last_json)
        trv2imas.TRV2IMAS(self.plasma, tbb=tbb, rbb=rbb, write_ids=True, last_json=last_json)


if __name__ == '__main__':

    app = qtw.QApplication(sys.argv)
    app.setStyle('fusion')
    main = TRV_GUI()
    app.exec()
