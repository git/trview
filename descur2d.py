import logging
import numpy as np

logger = logging.getLogger('trview.descur2d')

def scrunch(tup_in, niter=4000, ftol=1e-6, pexp=4, sort=True):

    rin, zin, mpol, tim, error = tup_in
    if error > 0:
        return np.zeros((mpol, 4), dtype=np.float64)

    if rin[0] == rin[-1]:
        rin = rin[:-1]
        zin = zin[:-1]
    rin = np.float64(rin)
    zin = np.float64(zin)
    ntheta = len(rin)
    reset = True
    bmax = 0.15
    ftolsq = ftol**2

# call fixaray

    dnorm = np.float64(2)/np.float64(ntheta)
    dm1 = np.arange(mpol, dtype=np.float64)
    faccon = .125*dnorm/(1. + dm1)**pexp
    xmpq4 = dm1**pexp
    xmpq4[:2] = 0
    xmpq3 = np.sqrt(xmpq4)

# end fixaray

    raxis = np.average(rin)
    zaxis = np.average(zin)

# call inguess

# call order
    xc = rin - raxis
    yc = zin - zaxis

    if sort:
        pol_ang = np.arctan2(yc, xc)
        pol_ang[pol_ang<0] += 2*np.pi
        ind_sort = np.argsort(pol_ang)
        xc = xc[ind_sort]
        yc = yc[ind_sort]
        rin = rin[ind_sort]
        zin = zin[ind_sort]

# call getangle from inguess

    xangle = np.arange(ntheta, dtype=np.float64)*np.pi*dnorm

    max_iterate = 10
    for iterate in range(max_iterate):
        rcos = np.dot(xc, np.cos(xangle))
        rsin = np.dot(xc, np.sin(xangle))
        zcos = np.dot(yc, np.cos(xangle))
        zsin = np.dot(yc, np.sin(xangle))

        elongate = zsin/rcos
        phiangle = np.arctan2(elongate*zcos - rsin, elongate*zsin + rcos)
        xangle += phiangle # this is the iterative statement
        if np.abs(phiangle) < 0.02:
            break

# end getangle
# end inguess

# call amplitude
    arg  = np.outer(dm1, xangle)
    cosa = np.cos(arg)
    sina = np.sin(arg)
    xrc = dnorm*np.dot(cosa, xc)
    xzc = dnorm*np.dot(cosa, yc) 
    xrs = dnorm*np.dot(sina, xc)
    xzs = dnorm*np.dot(sina, yc) 
    xrc[0] = raxis
    xzc[0] = zaxis
    xrs[1] = 0.5*(xrs[1] + xzc[1])
    xzc[1] = xrs[1]
    xvec = np.hstack((xrc, xrs, xzc, xzs, xangle))
    xdot = np.zeros_like(xvec)
    r10 = .5*( np.abs(xrc[1]) + np.abs(xrs[1]) + \
               np.abs(xzc[1]) + np.abs(xzs[1]) )
    r10sq = 1./r10**2

# end amplitude

    delt = 1.
    xm_ratio = 1./np.sqrt(1. + xmpq3[-1])
    nresets = 0
    gmin = 1e8
    gnorm_old = 1

    for jiter in range(niter):

# call funct
        gr = xrc*cosa.T + xrs*sina.T
        gz = xzc*cosa.T + xzs*sina.T
        r1 = -rin + np.sum(gr, axis=1)
        z1 = -zin + np.sum(gz, axis=1)
        rcon = np.dot( gr, xmpq4)
        zcon = np.dot( gz, xmpq4)
        rt1 = np.dot(cosa.T, dm1*xrs) - np.dot(sina.T, dm1*xrc)
        zt1 = np.dot(cosa.T, dm1*xzs) - np.dot(sina.T, dm1*xzc)

        t1fac = 1./np.max(rt1**2 + zt1**2)
        gangle = t1fac*(r1*rt1 + z1*zt1)
        tcon = t1fac*xm_ratio  # scalar

# FILTER CONSTRAINT FORCE TO REMOVE ALIASING

        gcon = rcon*rt1 + zcon*zt1
        ygc = np.dot(cosa, gcon)*faccon
        ygs = np.dot(sina, gcon)*faccon
        gcon2 = np.dot(ygc[1:-1], cosa[1:-1, :]) + np.dot(ygs[1:-1], sina[1:-1, :])
        gcon2 *= tcon*xmpq3[-1]

# ADD CURVE AND CONSTRAINT FORCES

        rcon = r1 + gcon2*rt1
        zcon = z1 + gcon2*zt1

        grc = dnorm*np.dot(cosa, rcon)
        grs = dnorm*np.dot(sina, rcon)
        gzc = dnorm*np.dot(cosa, zcon)
        gzs = dnorm*np.dot(sina, zcon)

# COMPUTE m=1 CONSTRAINT (ZC(m=1)=RS(m=1)) to the Forces

        gzc[1] += grs[1]
        gzc[1] *= 0.5
        grs[1] = gzc[1]
        grc[0] *= 0.5
        gnorm = np.sum(grc**2 + grs**2 + gzc**2 + gzs**2)*r10sq + dnorm*np.sum(gangle**2)

        if jiter > 0:
            fac = 1./(1. + tcon*xmpq3) # array(mpol)
            grc *= fac
            grs *= fac
            gzc *= fac
            gzs *= fac
            gvec = np.hstack((grc, grs, gzc, gzs, gangle))
# end funct

# call evolve
            gmin = min(gmin, gnorm)
#            dtau = 0.5*min(bmax, np.abs(1. - gnorm/gnorm_old) + 0.001)
#            xdot = ((1. - dtau)*xdot - delt*gvec)/(1. + dtau)
            dtau = min(bmax, np.abs(1. - gnorm/gnorm_old) + 0.001)
            xdot *= (1. - dtau)
            xdot += -delt*gvec
            xvec += xdot*delt
# end evolve

# RUDIMENTARY TIME STEP CONTROL
            if gnorm/gmin > 1.e6:
                reset = False
            if (not reset) or (gmin == gnorm):
                if reset:
                    xstore = xvec
                else:
                    xdot = 0
                    xvec = xstore
                    delt *= 0.95
                    reset = True
                    nresets += 1
                    if nresets >= 100:
                        logger.debug('Time step reduced 100 times without convergence %8.4f %12.4e', tim, gnorm)
                        return np.zeros((mpol, 4), dtype=np.float64)

        if gnorm < ftolsq:
            break # convergence

        gnorm_old = gnorm
        xrc, xrs, xzc, xzs, xangle = np.split(xvec, [mpol, 2*mpol, 3*mpol, 4*mpol])
        arg  = np.outer(dm1, xangle)
        cosa = np.cos(arg)
        sina = np.sin(arg)

    return xvec[:4*mpol].reshape((4, mpol)).T
