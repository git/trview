'''
Created on Dec 10, 2015 by S. Denk
Rewritten on May 8th, 2019 by G. Tardini
'''
import os, logging, datetime

import numpy as np
import aug_sfutils as sf
from aug_sfutils import read_imas
from trview import libech_wrapper, ecn_angles
try:
    import imas
except:
    pass

logger = logging.getLogger('trview.ech')

n_gy = [4, 4]
ngy = np.sum(np.array(n_gy))
gy_pos_x = 2*[2.380] + 2*[2.311] + n_gy[1]*[2.361]

# The swap of launcher ECRH3 3-4 is included in "gyros" class
gy_pos_y = [0., 0., -0.075, 0.075, -0.115, 0.115, 0.115, -0.115]
gy_pos_z = n_gy[0]*[0.] + 2*[0.32025] + 2*[-0.32025]

gy_sect  = n_gy[0]*[7.] + n_gy[1]*[4.5]

# Curvature

gy_curv_y_105 = 2*[0.8793] + 2*[2.9664] + n_gy[1]*[1.158]
gy_curv_y_140 = 2*[0.8793] + 2*[2.9664] + n_gy[1]*[0.8551]

# Beam width

gy_width_y_105 = 2*[0.0364] + 2*[0.0329] + n_gy[1]*[0.0301]
gy_width_y_140 = 2*[0.0364] + 2*[0.0329] + n_gy[1]*[0.0255]

gy_name = np.array(['ECRH1_1', 'ECRH1_2', 'ECRH1_3', 'ECRH1_4', \
                    'ECRH2_1', 'ECRH2_2', 'ECRH2_3', 'ECRH2_4'])


class gyros:


    def __init__(self, nshot, readSF=True, imasRun=None, tbeg=0., tend=10., ech_min=5.e3):


        self.shot = nshot
        self.ech_min  = ech_min
        self.tbeg = tbeg
        self.tend = tend
        
        self.n_gy = n_gy
        self.rfmod = np.array(ngy*[0.]) # 1 for O-mode, 0 for X-mode

# SI units (i.e. m, W, Hz), angles in degrees

        y = np.array(gy_pos_y, dtype=np.float32)
        x = np.array(gy_pos_x, dtype=np.float32)
        self.name = gy_name
        self.sect = np.array(gy_sect, dtype=np.float32)

# Swap ECRH3-4
        if(nshot > 33725):
            y[2] = gy_pos_y[3]
            y[3] = gy_pos_y[2]
            self.name[2] = gy_name[3]
            self.name[3] = gy_name[2]

        phi_rad = np.arctan2(y, x) + np.pi*self.sect/8.
        self.z = np.array(gy_pos_z, dtype=np.float32)
        self.R = np.hypot(x, y)
        self.phi = np.degrees(phi_rad, dtype=np.float32)
        self.x   = np.cos(phi_rad) * self.R
        self.y   = np.sin(phi_rad) * self.R
        if imasRun is not None:
            self.run = imasRun
            if 'imas' in globals():
                self.fromIMAS()
            else:
                self.fromH5()
        if readSF:
            self.fromShotfile()


    def fromIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return
        ids = read_imas.IMASids(self.shot, self.run)
        ids.read_block('ec_launchers')
        laun = ids.ec_launchers.beam
        ngy = len(laun)

        self.t_ds = laun[0].power_launched.time
        nt = len(self.t_ds)

        self.freq = np.zeros(ngy)
        self.mode = np.zeros(ngy)
        self.R    = np.zeros(ngy)
        self.Z    = np.zeros(ngy)
        self.width_y = np.zeros(ngy)
        self.width_z = np.zeros(ngy)
        self.curv_y  = np.zeros(ngy)
        self.curv_z  = np.zeros(ngy)
        self.power_ds = np.zeros((nt, ngy))
        self.theta_ds = np.zeros((nt, ngy))
        self.phi_ds   = np.zeros((nt, ngy))

        labels = []
        for jgy in range(ngy):
            labels.append(laun[jgy].name)
            self.freq[jgy] = laun[jgy].frequency.data
            self.mode[jgy] = laun[jgy].mode
            self.R[jgy]    = laun[jgy].launching_position.r
            self.Z[jgy]    = laun[jgy].launching_position.z
            self.power_ds[:, jgy] = laun[jgy].power_launched.data
            self.theta_ds[:, jgy] =  np.degrees(laun[jgy].steering_angle_pol.data)
            self.phi_ds[:, jgy]   = -np.degrees(laun[jgy].steering_angle_tor.data)
            self.width_y[jgy], self.width_z[jgy] = np.squeeze(laun[jgy].spot.size.data)
            curv  = np.squeeze(laun[jgy].phase.curvature.data)
            self.curv_y[jgy] = 1./curv[0]
            self.curv_z[jgy] = 1./curv[1]

        self.labels = np.array(labels)
        self.n_gy = []
        for lbl in np.unique(self.labels):
            self.n_gy.append(np.count_nonzero(self.labels == lbl))


    def fromH5(self):
        
        ech_ids = read_imas.read_imas_h5(self.shot, self.run, branch='ec_launchers')
        gyro = ech_ids['ec_launchers']

        self.t_ds    = gyro['beam[]&power_launched&time'][:]
        self.labels  = gyro['beam[]&name'][:]
        self.freq    = gyro['beam[]&frequency&data'][:]
        self.mode    = gyro['beam[]&mode'][:]
        self.R       = np.squeeze(gyro['beam[]&launching_position&r'][:])
        self.Z       = np.squeeze(gyro['beam[]&launching_position&z'][:])
        self.power_ds = gyro['beam[]&power_launched&data'][:]
        self.theta_ds =  np.degrees(gyro['beam[]&steering_angle_pol'][:])
        self.phi_ds   = -np.degrees(gyro['beam[]&steering_angle_tor'][:])
        self.width_y  = gyro['beam[]&spot&size'][:, 0, 0]
        self.width_z  = gyro['beam[]&spot&size'][:, 0, 1]
        curv  = np.squeeze(gyro['beam[]&phase&curvature'][:, 0, :])
        self.curv_y = 1./curv[:, 0]
        self.curv_z = 1./curv[:, 1]

        self.n_gy = []
        for lbl in np.unique(self.labels):
            self.n_gy.append(np.count_nonzero(self.labels == lbl))


    def fromShotfile(self):

        ech_obj = libech_wrapper.libECRH_wrapper(self.shot)
        if not hasattr(ech_obj, '_date'):
            logger.error('Problem with libech_wrapper, skipping ECH object')
            return # ECN not existing
        self.avail = np.zeros(ngy, dtype=bool)
        self.error = np.zeros(ngy)

#----
# ECS
#----

        ecs = sf.SFREAD(self.shot, 'ECS')
        if not ecs.status:
            return

        tim = ecs.getobject('T-B')
        tbeg = max(self.tbeg, 1e-6)
        (index, ) = np.where((tim >= tbeg) & (tim <= self.tend))
        self.t_ecs = tim[index]
        nt_ecs = len(self.t_ecs)
        alpha_ps     = np.zeros(ngy)
        beta_ps      = np.zeros(ngy)
        self.the_ps  = np.zeros(ngy, dtype=np.float32)
        self.phi_ps  = np.zeros(ngy, dtype=np.float32)
        self.freq    = np.zeros(ngy)
        self.curv_y  = np.zeros(ngy)
        self.width_y = np.zeros(ngy)
        self.p_ecs   = np.zeros((nt_ecs, ngy))

# Time-dependent angles

        tmp1 = ecs.getobject('thpl-G1N')
        tmp2 = ecs.getobject('thpl-G5')
        if tmp1 is not None:
            nt_b = len(self.t_ecs)
            self.the_ecs = np.zeros((nt_b, ngy))
            self.phi_ecs = np.zeros((nt_b, ngy))
            for jgy in range(n_gy[0]):
                self.the_ecs[:, jgy] = -ecs.getobject('thpl-G%d' %(jgy + 1))[index] # TORBEAM sign
                self.phi_ecs[:, jgy] = -ecs.getobject('phtr-G%d' %(jgy + 1))[index] # TORBEAM sign
            for jgy in range(n_gy[1]):
                self.the_ecs[:, n_gy[0] + jgy] = -ecs.getobject('thpl-G%dN' %(jgy + 1))[index] # TORBEAM sign
                self.phi_ecs[:, n_gy[0] + jgy] = -ecs.getobject('phtr-G%dN' %(jgy + 1))[index] # TORBEAM sign
        elif tmp2 is not None: # shot > 32813
            t_c = ecs.getobject('T-C')
            tbeg = max(tbeg, 1e-6)
            (indtc, ) = np.where((t_c >= tbeg) & (t_c <= self.tend))
            self.t_c_ecs = t_c[indtc]
            nt_c = len(self.t_c_ecs)
            self.the_ecs = np.zeros((nt_c, ngy))
            self.phi_ecs = np.zeros((nt_c, ngy))
            for jgy in range(ngy):
                self.the_ecs[:, jgy] = -ecs.getobject('thpl-G%d' %(jgy + 1))[indtc] # TORBEAM sign
                self.phi_ecs[:, jgy] = -ecs.getobject('phtr-G%d' %(jgy + 1))[indtc] # TORBEAM sign
            

# Power (dat), fixed angles from ParameterSet

        for jgy in range(ngy):
            if jgy < n_gy[0]:
                N_2 = jgy + 1
                gynum = 100 + N_2
                dat = ecs.getobject('PG%d' %N_2)
                if(self.shot < 33725): # ECRH1
                    gy_lbl = 'P_sy1_g%d' %N_2
                    tmp = ecs.getparset(gy_lbl)
                    if tmp is None:
                        break
                    alpha_ps[jgy] = tmp['GPolPos']  * 1000.
                else:             # ECRH3
                    gy_lbl = 'P_sy3_g%d' %N_2
                    tmp = ecs.getparset(gy_lbl)
                    alpha_ps[jgy] = tmp['GPolPos']
            else:                 # ECRH2
                N_2 = jgy - n_gy[0] + 1
                gynum = 200 + N_2
                dat = ecs.getobject('PG%dN' %N_2)
                gy_lbl = 'P_sy2_g%d' %N_2
                alpha_ps[jgy] = ecs.getparset(gy_lbl)['GPolPos'] * 1000.
            if dat is not None:
                if dat.dtype == '>f4':
                    self.p_ecs[:, jgy] = dat[index]

# Gyrotron switched on and with P_max > 5 kW

            gyps = ecs.getparset(gy_lbl)
            if 'gy_on' in gyps.keys():
                ongy = gyps['gy_on']
            elif 'gy_HV_on' in gyps.keys():
                ongy = gyps['gy_HV_on']
            if np.max(self.p_ecs[:, jgy]) < self.ech_min:
                self.p_ecs[:, jgy] = 0.
            self.avail[jgy] = (ongy and np.max(self.p_ecs[:, jgy]) > self.ech_min)
 
            self.freq[jgy] = gyps['gyr_freq']
            beta_ps  [jgy] = gyps['GTorPos']

            if np.abs(self.freq[jgy] - 1.40e11) < 5.e9:
                self.curv_y [jgy] = gy_curv_y_140 [jgy]
                self.width_y[jgy] = gy_width_y_140[jgy]
            elif np.abs(self.freq[jgy] - 1.05e11) < 5.e9:
                self.curv_y [jgy] = gy_curv_y_105 [jgy]
                self.width_y[jgy] = gy_width_y_105[jgy]
            else:
                logger.error('Gyrotron %s has freq = %1.3e Hz, not supported' %(gy_lbl, self.freq[jgy]))
                self.error[jgy] = -1

            self.curv_z  = self.curv_y
            self.width_z = self.width_y

            if (self.shot < 33725) or (jgy >= n_gy[0]):
                self.error[jgy], self.the_ps[jgy], self.phi_ps[jgy] = \
                    ech_obj.setval2tp(gynum, alpha_ps[jgy], beta_ps[jgy])
            else:
                self.the_ps[jgy] = alpha_ps[jgy]
                self.phi_ps[jgy] = beta_ps [jgy]
        self.the_ps *= -1 # TORBEAM
        self.phi_ps *= -1

        if hasattr(self, 't_c_ecs'):
            self.time = self.t_c_ecs
            self.power, _, _ = sf.time_slice.map2tgrid(self.time, self.t_ecs, self.p_ecs)
            self.theta = self.the_ecs
            self.phi   = self.phi_ecs
        else:
            self.time = self.t_ecs
            self.power = self.p_ecs
            tmp = ecn_angles.ecn_angles(self.shot, alpha_ps, beta_ps, self.avail, self.time)
            if tmp is not None:
                self.theta, self.phi = tmp
 

    def toIMAS(self):

        if not 'imas' in globals():
            logger.error('Unable to import IMAS')
            return

        ec = imas.ec_launchers()
        ec.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
        ec.ids_properties.homogeneous_time = 0
#    ec.ids_properties.source = os.path.abspath(__file__)

        ngy = np.sum(self.n_gy)
        nt_ds = len(self.t_ds)
        ecl = ec.beam
        ecl.resize(ngy)
        tim = np.atleast_1d(np.average(self.t_ds))
        zero = np.zeros(nt_ds, dtype=np.float64)
        for jgy in range(ngy):
            ecl[jgy].time = self.t_ds
            if jgy < self.n_gy[0]:
                if self.shot < 33725:
                    ecl[jgy].name = 'ECRH1'
                else:
                    ecl[jgy].name = 'ECRH3'
            else:
                ecl[jgy].name = 'ECRH2'
            ecl[jgy].identifier = 'Gy%d' %(jgy+1)
            ecl[jgy].frequency.time = tim
            ecl[jgy].frequency.data = np.atleast_1d(self.freq[jgy])
            ecl[jgy].power_launched.time = self.t_ds
            ecl[jgy].power_launched.data = self.power_ds[:, jgy]
            ecl[jgy].mode = -1
            ecl[jgy].launching_position.r = self.R[jgy] + zero
            ecl[jgy].launching_position.z = self.z[jgy] + zero
            ecl[jgy].launching_position.phi = zero   # cm->m
            ecl[jgy].steering_angle_pol = np.radians(self.theta_ds[:, jgy])
            ecl[jgy].steering_angle_tor = - np.radians(self.phi_ds[:, jgy])  # flip the sign DPC, 2020-11-24

            logger.info('Spot size [m]   %d %8.4f', jgy, self.width_y[jgy])
            logger.info('Curvature [1/m] %d %8.4f', jgy, self.curv_y[jgy])
            ecl[jgy].spot.size = np.tile([self.width_y[jgy], self.width_z[jgy]], nt_ds).reshape((2, nt_ds))
            ecl[jgy].spot.angle = zero

            ecl[jgy].phase.curvature = np.tile([1./self.curv_y[jgy], 1./self.curv_z[jgy]], nt_ds).reshape((2, nt_ds))
            ecl[jgy].phase.angle = zero

        return ec


if __name__ == '__main__':


    import matplotlib.pylab as plt

#    nshot = 33697 # Written new ECS
    nshot = 36087 # Written new ECS thpl-G5
    nshot = 28053
    nshot = 39612
    gy = gyros(nshot)

    plt.figure('Time', (14,9))
    plt.subplots_adjust(left=0.08, bottom=0.06, right=0.98, top=0.95, wspace=0.15, hspace=0.)

    for jgy in range(n_gy[0], ngy):

        logger.debug(gy.name[jgy])
        plt.subplot(4, 1, jgy - n_gy[0] + 1)
        plt.xlabel('Time [s]')
        plt.ylabel('Theta [deg] %s' %gy.name[jgy])
        if hasattr(gy, 't_c_ecs'):
            plt.plot(gy.t_c_ecs, gy.the_ecs[:, jgy], 'g-', label='ECS theta')
        if hasattr(gy, 'theta_ds'):
            plt.plot(gy.t_ang_ds, gy.theta_ds[:, jgy], 'r-', label='ECN ds. theta')
        logger.debug(gy.the_ps[jgy], gy.phi_ps[jgy])
    plt.legend()
    fpdf = '%d_ECH_angles.pdf' %nshot
    plt.savefig(fpdf)
    logger.info('Stored %s' %fpdf)
    plt.show()
