MODULE RABBIT_WRAPPER

    implicit none

    integer, dimension(:), allocatable :: ierr
    integer :: nrho_in, nspc, nrho_out, n_nbi, NRrect, NZrect, n_leg
    double precision, allocatable, dimension(:, :) :: powe, powi, &
       press, bdep, bdens, jfi, jnbcd, wfi_par, wfi_perp, wfi_par_lab, &
       torqe, torqi, torqjxb, torqth, torqthcxloss, torqdepo
    double precision :: aplasma, zplasma, aimp, zimp, dt_in, output_timing, &
        psi_sep, psi_axis, rmag, zmag
    double precision, dimension(:), allocatable :: a_beam, z_beam, pnbi,  &
        einj, prot, powe_tot, powi_tot, pshine, porbloss, pcxloss, Inbcd
    double precision, dimension(:, :), allocatable :: start_pos, unit_vec, &
        width_poly, part_mix
    double precision, dimension(:, :), allocatable :: PSI_rect
    double precision, dimension(:), allocatable :: Rrect
    double precision, dimension(:), allocatable :: Zrect
    double precision, dimension(:), allocatable :: psi1d_n, rhot_in, &
        ti_in, te_in, ne_in, omg_in, zef_in, iota, &
        area, vol, ffp, psi_n
    double precision, dimension(:), allocatable :: rhot_out, bdens_in, &
        nrate, dvol, darea
    double precision, dimension(:, :, :), allocatable :: bdep3, vinj3, einj3
    double precision, dimension(:, :, :, :), allocatable :: bdep_legmoms
    character(len=120) :: rb_nml


    CONTAINS

!-----------------------------------------------------------------------------
    subroutine rabbit_init

        use mod_rabbit_lib, only: rabbit_lib_init

        write(6, *) 'Grids'
        write(6, *) nrho_in, nspc, nrho_out, n_nbi, NRrect, NZrect
        write(6, *) 'START_POS', SHAPE(start_pos)
        write(6, *) start_pos
        write(6, *) 'Some plasma pars', aplasma, zplasma, aimp, zimp
        write(6, *) 'Namelist for RABBIT:', trim(rb_nml)

        call rabbit_lib_init(aplasma, zplasma, aimp, zimp,      &
            a_beam, z_beam, start_pos, unit_vec, width_poly,    &
            nspc, n_nbi, nrho_out, Rrect, zrect, Nrrect, Nzrect, &
            nrho_in, nrho_in, TRIM(rb_nml), LEN_TRIM(rb_nml), ierr)

    return
    end subroutine rabbit_init

!-----------------------------------------------------------------------------
    subroutine rabbit_step

        use mod_rabbit_lib, only: rabbit_lib_step, rabbit_lib_get_dV_dArea, &
            rabbit_lib_get_Wfi, rabbit_lib_get_bdep

        write(6, *) 'Call rabbit_lib_step'

        call rabbit_lib_step(rhot_in, ne_in, te_in, ti_in, &
            zef_in, omg_in, nrho_in, PSI_rect, psi1d_n, vol, area, &
            rhot_in, iota, ffp, psi_sep, psi_axis, rmag, zmag,     &
            Nrrect, Nzrect, nrho_in, pnbi, einj, part_mix,         &
            nspc, n_nbi, bdens_in, dt_in, output_timing,           & ! Output
            powe, powi, press, bdep, bdens, jfi, jnbcd,            &
            torqe, torqi, torqjxb, torqth, torqthcxloss, torqdepo, &
            nrate, rhot_out, nrho_out,     &
            powe_tot, powi_tot, pshine, prot, porbloss, pcxloss, Inbcd, ierr)

        write(6, *) 'Call rabbit_get_dV_darea'
        call rabbit_lib_get_dV_dArea(dvol, darea, nrho_out)
        write(6, *) 'Call rabbit_lib_get_Wfi'
        call rabbit_lib_get_Wfi(wfi_par, wfi_perp, wfi_par_lab, n_nbi, nrho_out)
        write(6, *) 'Call rabbit_lib_get_bdep'
        call rabbit_lib_get_bdep(bdep3, vinj3, einj3, bdep_legmoms, nrho_out, nspc, n_nbi, n_leg)
    return
    end subroutine rabbit_step

!-----------------------------------------------------------------------------
    subroutine dealloc_profiles

    use mod_rabbit_lib, only: rabbit_lib_deinit

    if (allocated(powe)) then
        write(6, *) 'CALL rabbit_lib_deinit'
        call rabbit_lib_deinit(n_nbi, ierr)
    endif

    return
    end subroutine dealloc_profiles

!-----------------------------------------------------------------------------

end module rabbit_wrapper
