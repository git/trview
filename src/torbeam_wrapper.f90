module TORBEAM_WRAPPER

    implicit none

    integer :: nrho_in, Nrrect, Nzrect, nprofvw, ndat, npnt, iend ! dims
    integer, dimension(:), allocatable :: intinbeam
    double precision, dimension(:), allocatable :: rhoresult, floatinbeam, &
        eqdata, prdata
    double precision, dimension(:), allocatable :: beam_proj_pol, beam_proj_hor, prof_out

    CONTAINS

!-----------------------------------------------------------------------------
    subroutine calc_torbeam

    integer :: j, kend, icnt, ibgout, n_ne, n_te
    double precision, dimension(:), allocatable :: volprofvw, t2data, t1data, t1tdata

    if (.not. allocated(t2data)) then
        allocate(volprofvw(2*nprofvw))
        allocate(t2data(5*ndat))
        allocate(t1data(6*ndat))
        allocate(t1tdata(6*ndat))
    endif
    if (allocated(beam_proj_pol)) then
        deallocate(beam_proj_pol)
        deallocate(beam_proj_hor)
    endif
    n_ne = nrho_in
    n_te = nrho_in

! Indices shifted by one w.r.t TORBEAM definition (manual)

    iend = 0
    kend = 0
    icnt = 0
    ibgout = 0
    t1data = 0.d0
    t1tdata = 0.d0
    t2data = 0.d0
    rhoresult = 0.d0

    call beam(intinbeam, floatinbeam, Nrrect, Nzrect, &
        eqdata, n_ne, n_te, prdata, &
        rhoresult, iend, t1data, t1tdata, kend, t2data, prof_out, &
        icnt, ibgout, nprofvw, volprofvw)

    allocate(beam_proj_pol(6*iend), beam_proj_hor(6*iend))

    beam_proj_pol = t1data(1: 6*iend)
    beam_proj_hor = t1tdata(1: 6*iend)
    deallocate(volprofvw)
    deallocate(t1data)
    deallocate(t1tdata)
    deallocate(t2data)

    return
    end subroutine calc_torbeam

!-----------------------------------------------------------------------------

end module TORBEAM_WRAPPER
