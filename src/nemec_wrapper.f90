module nemec_wrapper

    implicit none

! Input
    logical :: pylcont, pylfreeb, pylwr_res
    integer :: pynrho, pympol, pyntor, pyniter, pynstep, pynvacskip, &
        pynzeta, pyntheta, pyiasym, pynfp, pyncurr, pyvac_1_2, &
        pynpresu, pyniotau, pynitoru
    integer, dimension(:), allocatable :: pyns_array
    real :: pycurtor, pydelt, pygamma, pybetascale, &
        pyraxis_co0, pyzaxis_co0, pyphiedge
    real, dimension(:), allocatable :: pyftol_array, pyiotsto, pyiotaus, &
         pypresto, pypresus, pyitosto, pyitorus, &
         pyrbc, pyrbs, pyzbc, pyzbs
    character(len=10)  :: pywoutf, pywoutt, pyextension
    character(len=120) :: pymgrid_file, pyres_file, pyout_path, &
        pypres_profile, pyiota_profile, pyitor_profile, &
        pyuser_fouraxis, pyuser_fourlcms, &
        pyuser_pressure, pyuser_iota, pyuser_itor

! Output
    real, dimension(:, :, :), allocatable :: rc, rs, zc, zs

    contains

!---------------------------------------------------------------------
    subroutine calc_nemec

    use mod_vmec_output, only: rcout, rsout, zcout, zsout
    use mod_vmec_main, only: fsqr, ftolv, ns, mpol1, multi_ns_grid, iequi, &
         cosmu, sinmu, cosnv, sinnv, cosmum, sinmum, cosmui, cosmumi, &
         sinmui, sinmumi, cosnvn, sinnvn, &
         xrz3, xrz4, xl3, xl4, xn, xm, timer, ixm, jmin3
    use mod_vmec_input, only: lfirst_run, l_moreit, res_file, curtor, betascale, &
        raxis_co, zaxis_co, ns_array, ftol_array, mpol, ntor, &
        iota_profile, itor_profile, pres_profile, user_fouraxis, mgrid_file, &
        out_path, deltact, delt, lwr_res
    use mod_user_profiles, only: &
        presto, presus, presco1, presco2, presco3, presco4, &
        iotsto, iotaus, iotaco1, iotaco2, iotaco3, iotaco4, &
        itosto, itorus, itorco1, itorco2, itorco3, itorco4
    use mod_vacmod, only: sinper, cosper, sinuv, cosuv, sinu, cosu, sinv, cosv, &
        sinu1, cosu1, sinv1, cosv1, sinui, cosui, &
        tanu, tanv, xmpot, xnpot, cmns, csign, imirr
    use mod_vparams, only: nthreed

    logical, parameter :: lreset_xc = .false.
    logical :: lfirst=.true., l_time, lreset, lreseta, interp_flag, lopen
    integer :: igrid0, igrid, ns_min, nsval, istat1, ireset, ier_flag, js, jtor, jpol
    real :: timeon, timeoff
    character(len=10) :: extension

    lreseta = .true.
    ier_flag = 0
    lfirst_run = .true.
    ireset = 0
    extension = pyextension

100 continue

! INDEX OF LOCAL VARIABLES
!
! ier_flag   specifies error condition if nonzero
!  interp_flag
!           = F, compute xc array from scaled boundary data
!           = T, compute xc array by interpolation
! lfirst
!           = T, initial call to runvmec
!           = F, runvmec has been previously called

    call second0(timeon)

    l_moreit = .false.
    if (ier_flag == 4) l_moreit = .true. ! new start for 'more iterations'
    l_time = .false.
    ier_flag = 0

! INITIALIZE PARAMETERS on 1st call

    lreset = .false.
    if (lfirst .or. lreseta) lreset = .true.
    call vsetup1(lreset)

    call readin2(extension, lfirst, ier_flag)

    if (ier_flag /= 0) then
        inquire(nthreed, opened=lopen)
        if (lopen) then
            goto 1100
        else
            goto 1200
        endif
    endif

! COMPUTE INVARIANT ARRAYS

    call fixaray(ier_flag)
    if (ier_flag == 5) goto 1200

! COMPUTE INITIAL SOLUTION ON COARSE GRID
! IF PREVIOUS SEQUENCE DID NOT CONVERGE, DO COARSE RESTART

    if (lreseta .or. lfirst .or. fsqr > ftolv) then
        igrid0 = 1
    else
        igrid0 = multi_ns_grid
    endif

    ns_min = 0

    do igrid = igrid0, multi_ns_grid
        nsval = ns_array(igrid)
        if (nsval <= ns_min) cycle
        if (lfirst_run) then
            lfirst_run = .false.
            deltact = delt(1)
        elseif (.not. l_moreit .and. lwr_res) then
            iequi = 1
            if (ier_flag == 0 .or. ier_flag == 4) call funct3d(ier_flag)
            if (ier_flag == 5) goto 1100
            iequi = 0
            call close_all_files
            call open_outfiles (extension, nsval, ier_flag)
        endif
        ns_min = nsval
        ftolv = ftol_array(igrid)
! --- neu: -------------------------
        deltact = min(deltact, delt(igrid))
! ---------------  -----------------
        if (igrid == igrid0) then
            interp_flag = .false.
        else
            interp_flag = .true.
        endif
        call eqsolve (nsval, interp_flag, ier_flag, lreset, l_time)

        if (l_time) exit
        if (ier_flag /= 0 .and. ier_flag /= 4) exit
    enddo

    call second0 (timeoff)
    timer(0) = timeoff - timeon

1100 call FILEOUT(extension, ier_flag)
1200 continue

    lfirst = .false.
    select case(ier_flag)
        case default
            lreseta = .false.
        case(1:3)
            ireset = ireset + 1
            lreseta = .true.
            if (ireset <= 2) goto 100
        case(4)                                !Try a few more iterations
            ireset = ireset + 1
            lreseta = .false.
            if (ireset <= 2) then
                goto 100
            else
                print *, 'DECREASE DELT OR INCREASE NITER'
            endif
        case(5)
            print *, 'Error reading namelist in VMEC'
            goto 1000
    end select

    call free_permem

    close(nthreed)

1000 continue

    if (allocated(rc)) deallocate(rc, rs, zc, zs)
    allocate(rc(ns, 2*ntor+1, mpol), rs(ns, 2*ntor+1, mpol), &
             zc(ns, 2*ntor+1, mpol), zs(ns, 2*ntor+1, mpol))

    do js=1, ns
        do jtor=1, 2*ntor+1
            do jpol=1, mpol
                rc(js, jtor, jpol) = rcout(js, jtor-ntor-1, jpol-1)
                rs(js, jtor, jpol) = rsout(js, jtor-ntor-1, jpol-1)
                zc(js, jtor, jpol) = zcout(js, jtor-ntor-1, jpol-1)
                zs(js, jtor, jpol) = zsout(js, jtor-ntor-1, jpol-1)
            enddo
        enddo
    enddo

    return
    end subroutine calc_nemec

!------------------------------------------------------------------------------
    subroutine readin2(extension, lfirst, ier_flag)
!-----------------------------------------------------------------------------!
! S. Hirshman                                                                 !
! E. Strumberger, C. Tichmann, E. Schwarz                last change 01/24/12 !
!-----------------------------------------------------------------------------!
    use mod_vparams, only: nsd, nthreed, nres, ntmax, zero, signgs, dmu0
    use mod_vmec_main, only: multi_ns_grid, iresidue, nsold, neqs2_old, &
        mpol1, ntor1, ntheta1, rmn_bdy, zmn_bdy, currv, timer
    use mod_vsvd, only: pfac, phifac
    use mod_vmec_input, only: lcont, lreset_res, lfreeb, &
        ns_array, nfp, ncurr, niter, nstep, nzeta, mpol, ntor, nvacskip, &
        delt, gamma, spres_ped, phiedge, curtor, tcon0, am, ai, ac, &
        ftol_array, raxis_co, zaxis_co, raxis_si, zaxis_si, &
        rbc, rbs, zbc, zbs, &
        threed1_file, res_file, mgrid_file 
    use mod_mgrid_bfield, only: bvac, brvac, bzvac, bpvac, btemp, &
        nr0b, np0b, nz0b, nbvac, &
        rminb, zminb, rmaxb, zmaxb

    implicit none

    logical, intent(in)  :: lfirst
    character(*), intent(in) :: extension
    integer, intent(out) :: ier_flag

    integer, parameter :: ns_default = 31

    logical :: testers
    integer :: iread, n, istat, i, n1, m, nsmin, igrid, isgn
    real :: rtest, ztest, treadon, treadoff, r00_fix, z00_fix, &
        delta, trc, tzc

    call second0(treadon)

! READ IN DATA FROM INDATA FILE

    ier_flag = 0
    call read_indata2(ier_flag)

    if (ier_flag /= 0) return

! parse ns_array

    multi_ns_grid = 1
    nsmin = 1
    do while (ns_array(multi_ns_grid) > nsmin .and. multi_ns_grid < 100)
        nsmin = max(nsmin, ns_array(multi_ns_grid))
        if (nsmin <= 3) then
            print *, 'NS_ARRAY ELEMENTS < 4'
            ier_flag=5
            return
        else
            if (nsmin <= nsd) then
                multi_ns_grid = multi_ns_grid + 1
            else              !!Optimizer, Boozer code overflows otherwise
                ns_array(multi_ns_grid) = nsd
                nsmin = nsd
                print *,' NS_ARRAY ELEMENTS CANNOT EXCEED ',nsd
                print *,' CHANGING NS_ARRAY(',multi_ns_grid,') to ', nsd
            endif
        endif
    enddo
    multi_ns_grid = multi_ns_grid - 1

    do i=2, multi_ns_grid
        if (delt(i) == zero) delt(i) = delt(i-1)
    enddo

! >>> open output file nthreed here: <<<

    if (lfirst) then
        threed1_file = 'threed1.'//extension
        write (*, '(33('' -''))')
        if (lcont) then
            open(unit=nthreed, file=threed1_file, status='unknown',     &
                 position='append',iostat=iread)
        else
            open(unit=nthreed, file=threed1_file, iostat=iread)
        endif
        if (iread /= 0) then
            print *,'STOP open error in threed1'
            ier_flag=5
        endif

        if (res_file == "none") then
            lreset_res = .false.
        else
            lreset_res = .true.
            open(nres,file=trim(res_file),form='unformatted',iostat=iread)
            if (iread /= 0) then
                print *,'STOP open  error in res_file'
                ier_flag=5
            endif
            read (nres) rtest,ztest, pfac, phifac
            neqs2_old = nint(rtest)
            nsold     = nint(ztest)
        endif
    endif

    if(ier_flag == 5) return

    call write_indata

! open output files here, print out heading to threed1 file

    call heading(extension,ier_flag)
    if(ier_flag == 5) return

! READ IN AND STORE (FOR SEQUENTIAL RUNNING) MAGNETIC FIELD DATA
! FROM MGRID_FILE FIRST TIME (ISEQ_COUNT=0) ONLY
! SET LOGICAL FLAGS FOR ALL SUBSEQUENT RUNS

    call read_mgrid(ier_flag)
    if (ier_flag == 5) return

    testers = .false.
!ers  if (testers) then
    if (lfreeb) then
        if (lfirst) then
            write(NTHREED,20) nr0b, nz0b, np0b, rminb, rmaxb, &
                zminb, zmaxb, mgrid_file
        endif
 20     format(//,' VACUUM FIELD PARAMETERS:',/,1x,24('-'),/,        &
        '  nr-grid  nz-grid  np-grid      rmin      rmax      zmin', &
        '      zmax',/,3i9,4f10.3,/,'  input-file',/,2x,a100)

        istat = 0
        if (.not.allocated(bvac)) then
             allocate (bvac(nbvac, 3), brvac(nr0b, nz0b, np0b),  &
                 bzvac(nr0b, nz0b, np0b), bpvac(nr0b, nz0b, np0b), stat=istat)
        endif
        if (istat /= 0) then
            print *,' bvac allocation failed'
            ier_flag=5
            return
        endif

        bvac(:nbvac, :3) = zero
        do i=1, 3
            bvac(:nbvac, i) = bvac(:nbvac, i) + btemp(:nbvac,i)
        enddo
    endif

! WRITE OUT DATA TO THREED1 FILE

    if (nvacskip <= 0) nvacskip = nfp
    if (lfirst) then
        write(NTHREED,100) ns_array(multi_ns_grid), ntheta1, nzeta, &
            mpol, ntor, nfp, gamma, spres_ped, phiedge, curtor
 100    format(/,' COMPUTATION PARAMETERS: (u = theta, v = zeta)'/,   &
        1x,45('-'),/,                                                 &
        '     ns     nu     nv     mu     mv',/,                      &
        5i7,//,' CONFIGURATION PARAMETERS:',/,1x,25('-'),/,4x,        &
        'nfp      gamma      spres_ped    phiedge(wb)     curtor(A)', &
        /,i7,1p,e11.3,1p,2e15.3,1p,e14.3,/)

        write(NTHREED,110)ncurr,niter,ns_array(1),nstep,nvacskip,     &
        ftol_array(multi_ns_grid),tcon0
 110    format(' RUN CONTROL PARAMETERS:',/,1x,23('-'),/,2x,          &
        'ncurr  niter   nsin  nstep  nvacskip      ftol     tcon0',/, &
        4i7,i10,1p,2e10.2,/)
        write(NTHREED,130)
 130    format(' MASS PROFILE EXPANSION COEFFICIENTS',      &
        ' (am - newton/m**2):',/,1x,35('-'))
        write(NTHREED,135)(am(i),i=0,10)
        if (ncurr == 0) then
            write(NTHREED, 140)
            write(NTHREED, 135) (ai(i),i=0, 10)
        else
            write(NTHREED, 145)
            write(NTHREED, 135) (ac(i), i=0, 10)
        endif
 135    format(1p, 6e12.3)
 140    format(/' IOTA PROFILE EXPANSION COEFFICIENTS (ai):', /, 1x, 35('-'))
 145    format(/,                                                      &
        ' TOROIDAL CURRENT DENSITY (*V'') EXPANSION COEFFICIENTS (ac):' &
        ,/,1x,38('-'))
        write(NTHREED,180)
 180    format(/,' R-Z FOURIER BOUNDARY COEFFICIENTS',/,               &
        ' R = RBC*cos(m*u - n*v) + RBS*sin(m*u - n*v),',               &
        ' Z = ZBC*cos(m*u - n*v) + ZBS*sin(m*u-n*v)'/1x,86('-'),       &
        /,'   nb  mb     rbc         rbs         zbc         zbs   ',  &
        '    raxis_co    raxis_si    zaxis_co    zaxis_si ')
    endif

! --- CONVERT TO REPRESENTATION WITH RBS(m=1) = ZBC(m=1)

    delta = atan( (rbs(0,1) - zbc(0,1))/(rbc(0,1) + zbs(0,1)) )
    if (delta /= zero)then
        do m=0, mpol1
            do n = -ntor, ntor
                trc = rbc(n, m)*cos(m*delta) + rbs(n, m)*sin(m*delta)
                rbs(n, m) = rbs(n, m)*cos(m*delta) - rbc(n, m)*sin(m*delta)
                rbc(n, m) = trc
                tzc = zbc(n, m)*cos(m*delta) + zbs(n, m)*sin(m*delta)
                zbs(n, m) = zbs(n, m)*cos(m*delta) - zbc(n, m)*sin(m*delta)
                zbc(n, m) = tzc
            enddo
        enddo
    endif

! --- ALLOCATE MEMORY FOR NU, NV, MPOL, NTOR SIZED ARRAYS

    call allocate_nunv(ier_flag)
    if(ier_flag == 5) return

! CONVERT TO INTERNAL REPRESENTATION OF MODES
!
! R = RBCC*COS(M*U)*COS(N*V) + RBSS*SIN(M*U)*SIN(N*V)
!   + RBCS*COS(M*U)*SIN(N*V) + RBSC*SIN(M*U)*COS(N*V)
! Z = ZBCS*COS(M*U)*SIN(N*V) + ZBSC*SIN(M*U)*COS(N*V)
!   + ZBCC*COS(M*U)*COS(N*V) + ZBSS*SIN(M*U)*SIN(N*V)

    rmn_bdy(0:ntor, 0:mpol1, :ntmax) = zero
    zmn_bdy(0:ntor, 0:mpol1, :ntmax) = zero

    do m=0, mpol1
        do n=-ntor, ntor
            n1 = abs(n)
            isgn = 1
            if (n  < 0) isgn = -1
            if (n == 0) isgn = 0
            rmn_bdy(n1, m, 1) = rmn_bdy(n1, m, 1) + rbc(n, m)      ! RBCC
            rmn_bdy(n1, m, 2) = rmn_bdy(n1, m, 2) + isgn*rbc(n, m) ! RBSS
            zmn_bdy(n1, m, 1) = zmn_bdy(n1, m, 1) - isgn*zbs(n, m) ! ZBCS
            zmn_bdy(n1, m, 2) = zmn_bdy(n1, m, 2) + zbs(n, m)      ! ZBSC

            rmn_bdy(n1, m, ntmax-1) = rmn_bdy(n1, m, ntmax-1) - isgn*rbs(n, m) ! RBCS
            rmn_bdy(n1, m, ntmax  ) = rmn_bdy(n1, m, ntmax  ) + rbs(n, m)      ! RBSC
            zmn_bdy(n1, m, ntmax-1) = zmn_bdy(n1, m, ntmax-1) + zbc(n, m)      ! ZBCC
            zmn_bdy(n1, m, ntmax  ) = zmn_bdy(n1, m, ntmax  ) + isgn*zbc(n, m) ! ZBSS

            if (m == 0) then
                zmn_bdy(n1, m, ntmax) = zero
                rmn_bdy(n1, m, ntmax) = zero
                rmn_bdy(n1, m, 2    ) = zero
                zmn_bdy(n1, m, 2    ) = zero
            endif

            if ((abs(rbc(n, m)) + abs(rbs(n, m)) + &
                 abs(zbc(n, m)) + abs(zbs(n, m))) == zero ) cycle
            if (lfirst) then
                if (m == 0 .and. n >= 0) then
                    write(NTHREED, 195) n, m, rbc(n, m), rbs(n, m), zbc(n, m),   &
                    zbs(n, m), raxis_co(n), raxis_si(n), zaxis_co(n), zaxis_si(n)
                endif
                if (m /= 0)                                            &
                    write(NTHREED, 195) n, m, rbc(n, m), rbs(n, m), zbc(n, m), zbs(n, m)
                endif
            enddo
        enddo
 195 format(i5, i4, 1p, 8e12.4)

! CHECK SIGN OF JACOBIAN (SHOULD BE SAME AS SIGNGS)

    rtest = sum(rmn_bdy(0:ntor1-1, 1, 1))
    ztest = sum(zmn_bdy(0:ntor1-1, 1, 2))

    if ((rtest*ztest*signgs) >= zero) then
        ier_flag = 3
        return
    endif

    iresidue = -1
    currv = dmu0*curtor              !Convert to Internal units

    call second0(treadoff)
    timer(2) = timer(2) + (treadoff-treadon)

    return
    end subroutine readin2

!-------------------------------------------------------------------------
    subroutine read_indata2(ier_flag)
!-----------------------------------------------------------------------------!
! S. Hirshman                                                                 !
! E. Strumberger, C. Tichmann, E. Schwarz                last change 01/31/12 !
!-----------------------------------------------------------------------------!
    use mod_vparams, only: ntord, mpold, cbig
    use mod_vmec_main, only: lthreed, mpol1, ntor1, nznt, mnmax, mnsize, &
        ntheta1, ntheta2, ntheta3
    use mod_vacmod, only: mnpd, mnpd2, mf, nf, mf1, nf1, nu, nu2, nu3, &
        nv, nvp, nuv, nuv2, nfper
    use mod_vmec_input, only: lwr_jxb, lwr_merc, lwr_jdot, lwr_res, lcont, &
        lreset_res, lfreeb, vac_1_2, ns_array, &
        niter, nstep, mpol, ntor, nvacskip, ntheta, nzeta, iasym, nfp, ncurr, &
        time_limit, curtor, delt, gamma, betascale, phiedge, raxis_co, zaxis_co, &
        rbc, rbs, zbc, zbs, rscale, ftol_array, ac, ai, &
        format_type, out_path, res_file, mgrid_file, woutt, woutf, &
        user_fouraxis, user_fourlcms, user_pressure, user_iota, user_itor, &
        pres_profile, iota_profile, itor_profile
    use mod_user_profiles, only: npresu, nitoru, niotau, &
        presus, itorus, iotaus, presto, itosto, iotsto, &
        presco1, presco2, presco3, presco4, &
        itorco1, itorco2, itorco3, itorco4, &
        iotaco1, iotaco2, iotaco3, iotaco4

    implicit none

    integer, intent(out) :: ier_flag

    logical :: ltime, time_stop
    integer :: jpol, presspli, itorspli, iotaspli
    real :: r00_fix,z00_fix, dpres1, dpres2, diota1, diota2, ditor1, ditor2

    format_type = 'binary'
    lwr_jxb  = .false.
    lwr_merc = .false.
    lwr_jdot = .false.
    out_path = ""
    lwr_res = .false.
    lcont = .false.
    res_file = "none"
    time_limit = 0
    lreset_res = .false.

! Overwriting namelist values with python-interface values

    niter    = pyniter
    nstep    = pynstep
    mpol     = pympol
    ntor     = pyntor
    nvacskip = pynvacskip
    ntheta   = pyntheta
    nzeta    = pynzeta
    iasym    = pyiasym
    nfp      = pynfp
    ncurr    = pyncurr
    curtor      = pycurtor
    delt        = pydelt
    vac_1_2     = pyvac_1_2
    gamma       = pygamma
    betascale   = pybetascale
    phiedge     = pyphiedge
    raxis_co(0) = pyraxis_co0
    zaxis_co(0) = pyzaxis_co0
    user_fouraxis = pyuser_fouraxis
    user_fourlcms = pyuser_fourlcms
    user_pressure = pyuser_pressure
    user_iota     = pyuser_iota
    user_itor     = pyuser_itor
    mgrid_file    = pymgrid_file
    res_file      = pyres_file
    pres_profile  = pypres_profile
    iota_profile  = pyiota_profile
    itor_profile  = pyitor_profile
    out_path      = pyout_path
    woutt         = pywoutt
    woutf         = pywoutf
    lcont   = pylcont
    lfreeb  = pylfreeb
    lwr_res = pylwr_res
    ns_array(1:4)   = pyns_array
    ftol_array(1:4) = pyftol_array

    ! start time count
    ltime = time_stop(time_limit)

    if (mgrid_file == 'none') lfreeb = .false.

    if (pres_profile == 'user') then
        npresu = pynpresu
        allocate(presto( npresu), presus( npresu), presco1(npresu))
        allocate(presco2(npresu), presco3(npresu), presco4(npresu))
        presto = pypresto
        presus = pypresus
        presco1 = 0.; presco2 = 0.; presco3 = 0.; presco4 = 0.
        dpres1 = 0.
        dpres2 = 0.
        presspli = 0
        call math_spline(npresu, presto, presus, dpres1, dpres2, presspli, &
                         presco1, presco2, presco3, presco4, ier_flag)
    endif

    if (iota_profile == 'user') then
        niotau = pyniotau
        allocate(iotsto( niotau), iotaus( niotau), iotaco1(niotau))
        allocate(iotaco2(niotau), iotaco3(niotau), iotaco4(niotau))
        iotsto = pyiotsto
        iotaus = pyiotaus
        iotaco1 = 0.; iotaco2 = 0.; iotaco3 = 0.; iotaco4 = 0.
        diota1 = 0.
        diota2 = 0.
        iotaspli = 0
        call math_spline(niotau, iotsto, iotaus, diota1, diota2, iotaspli, &
                         iotaco1, iotaco2, iotaco3, iotaco4, ier_flag)
    endif

    if (itor_profile == 'user') then
        nitoru = pynitoru
        allocate(itosto( nitoru), itorus( nitoru), itorco1(nitoru))
        allocate(itorco2(nitoru), itorco3(nitoru), itorco4(nitoru))
        itosto = pyitosto
        itorus = pyitorus
        itorco1 = 0.; itorco2 = 0.; itorco3 = 0.; itorco4 = 0.
        ditor1 = 0.
        ditor2 = 0.
        itorspli = 0
        call math_spline(nitoru, iotsto, itorus, ditor1, ditor2, itorspli, &
                         itorco1, itorco2, itorco3, itorco4, ier_flag)
    endif

    do jpol=1, mpol
       rbc(0, jpol-1) = pyrbc(jpol) 
       rbs(0, jpol-1) = pyrbs(jpol) 
       zbc(0, jpol-1) = pyzbc(jpol) 
       zbs(0, jpol-1) = pyzbs(jpol) 
    enddo

! --- scaling of the plasma boundary  -ntord:ntord,0:mpol1d
    r00_fix = rbc(0, 0)
    z00_fix = zbc(0, 0)

    rbc = rscale*rbc
    rbs = rscale*rbs
    zbc = rscale*zbc
    zbs = rscale*zbs

    rbc(0, 0) = r00_fix
    zbc(0, 0) = z00_fix

! COMPUTE NTHETA, NZETA VALUES

    mpol = abs(mpol)
    ntor = abs(ntor)
    if (mpol > mpold) stop 'mpol>mpold: lower mpol'
    if (ntor > ntord) stop 'ntor>ntord: lower ntor'
    mpol1 = mpol - 1
    ntor1 = ntor + 1
! --- number of theta grid points (>=2*mpol+6)
    if (ntheta == 0) ntheta = 2*mpol + 6
    ntheta1 = 2*(ntheta/2)
    ntheta2 = 1 + ntheta1/2
    if (ntor == 0) lthreed = .false.
    if (ntor  > 0) lthreed = .true.

    if (iasym == 1) then
        ntheta3 = ntheta1
        mnpd2 = 2.*(mpol+2)*(2*ntor+1)
    else
        ntheta3 = ntheta2
        mnpd2 = (mpol+2)*(2*ntor+1)
    endif

! --- number of zeta grid points (=1 if ntor=0)
    if (nzeta == 0) nzeta = 2*ntor + 4
    if (ntor  == 0) nzeta = 1
    nznt = nzeta*ntheta3
    mnmax = ntor1 + mpol1*(1 + 2*ntor)      !size of rmnc,  rmns,  ...
    mnsize = mpol*ntor1                     !size of rmncc, rmnss, ...

    mf = mpol + 1
    nf = ntor
    nu = ntheta1
    nv = nzeta
    mf1 = 1 + mf
    nf1 = 2*nf + 1
    mnpd = mf1*nf1
    nfper = nfp
! NEED FOR INTEGRATION IN BELICU FOR AXISYMMETRIC PLASMA
!    if (nf == 0 .and. nv == 1) nfper = 64

    nvp = nv*nfper
    nuv = nu*nv
    nu2 = nu/2 + 1
    nu3 = ntheta3
    nuv2 = nznt
    if (nuv2 < mnpd) then
        print *, ' nuv2 < mnpd: not enough integration points'
        ier_flag=5
        return
    endif

! --- Old format: may not be reding in ac
    if (ncurr == 1 .and. ac(1) > cbig) ac = ai

    return
    end subroutine read_indata2

end module nemec_wrapper
