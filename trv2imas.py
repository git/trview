import os, datetime, subprocess, logging
from scipy.interpolate import CubicSpline, interp1d, PchipInterpolator
import numpy as np
import identifiers.poloidal_plane_coordinates_identifier
import imas
import aug_sfutils as sf
import trview 
from trview import rhop2rhot

logger = logging.getLogger('trview.trv2imas')


spec_lbl = {(1, 1): 'H', (1, 2): 'D', (2, 4): 'He', (6, 12): 'C'}


def TRV2IMAS(plasma, tbb=None, rbb=None, write_ids=False, last_json=None):

    gpar = plasma.gpar
    logger.info('Creating IDS structure')
    if gpar.IDSbackend == 'HDF5':
        db = imas.DBEntry(imas.imasdef.HDF5_BACKEND, 'aug', gpar.shot, gpar.ids_run, os.getenv('IMASDB'), '3')
    elif gpar.IDSbackend == 'MDS+':
        db = imas.DBEntry(imas.imasdef.MDSPLUS_BACKEND, 'aug', gpar.shot, gpar.ids_run, os.getenv('IMASDB'), '3')
    elif gpar.IDSbackend == 'ASCII':
        db = imas.DBEntry(imas.imasdef.ASCII_BACKEND, 'aug', gpar.shot, gpar.ids_run, os.getenv('IMASDB'), '3')
    status, _ = db.create()

    logger.info('Filling IDS structure')
    ds = fill_dataset_description(gpar.shot, last_json)
    wall = fill_wall(gpar.shot)
    tf = fill_tf(plasma.equ)
    if plasma.nbi.status and np.max(plasma.nbi.power_ds) > 0:
        nb = plasma.nbi.toIMAS()
    if hasattr(plasma.ech, 't_ds'):
        ec = plasma.ech.toIMAS()
    if hasattr(plasma.icr, 't_ds'):
        ic = plasma.icr.toIMAS()
    if hasattr(plasma.pellet, 'rho_dep'):
        pel = plasma.pellet.toIMAS()
    if tbb is not None :
        wav = fill_waves(tbb, equ=plasma.equ)
    if rbb is not None:
        dist = fill_distributions(rbb, equ=plasma.equ)

    eq  = plasma.equ.toIMAS()
    ece = plasma.ece.toIMAS()
    ps  = fill_pulse_schedule(plasma)
# Core profiles
    summ = plasma.sig.toIMAS()
    if rbb is not None:
        cp = fill_core_profiles(plasma,dist=dist)
    else:
        #dist = imas.distributions()   # create empty dist IDS -  not necessary
        cp = fill_core_profiles(plasma)
      
    if (rbb is not None) or (tbb is not None):
        csrc = fill_core_sources(equ=plasma.equ, rbb=rbb, tbb=tbb)

    if write_ids:
        logger.info('Dumping IDS file %s', gpar.IDSbackend)
        imasdb = os.getenv('IMASDB')
        logger.info('in dir %s', imasdb)
        os.system('mkdir -p %s' %imasdb)
        db.put(ds)
        logger.info('Putting tf')
        db.put(tf)
        logger.info('Putting wall')
        db.put(wall)
        logger.info('Putting ECE')
        db.put(ece)
        if 'nb' in locals():
            logger.info('Putting NBI')
            db.put(nb)
        if 'ec' in locals():
            logger.info('Putting ECH')
            db.put(ec)
        if 'ic' in locals():
            logger.info('Putting ICRF')
            db.put(ic)
        if 'pel' in locals():
            logger.info('Putting pellet')
            db.put(pel)
        if 'wav' in locals():
            logger.info('Putting waves')
            db.put(wav)
        if 'csrc' in locals():
            logger.info('Putting core_sources')
            db.put(csrc)
        if 'dist' in locals():
            logger.info('Putting distributions')
            db.put(dist)
        logger.info('Putting equilibrium')
        db.put(eq)
        logger.info('Putting summary')
        db.put(summ)
        logger.info('Putting core_profiles')
        db.put(cp)
        logger.info('Putting pulse_schedule')
        db.put(ps)
        db.close()
        logger.info('Closed IMAS file')


def fill_dataset_description(shot, last_json=None):

    ds = imas.dataset_description()
    ds.ids_properties.homogeneous_time = 2
    ds.ids_properties.comment = "trview (https://www.aug.ipp.mpg.de/aug/manuals/transp/trview, version %s)" %trview.__version__
#    ds.ids_properties.source = os.path.abspath(__file__)
    ds.ids_properties.provider = "Giovanni Tardini (giovanni.tardini@ipp.mpg.de)"
    ds.ids_properties.creation_date = datetime.datetime.today().strftime("%Y/%m/%d")
    ds.data_entry.user = os.getenv('USER')
    ds.data_entry.machine = "ASDEX Upgrade"
    ds.data_entry.pulse_type = "pulse"
    ds.data_entry.pulse = shot
    if last_json is not None:
        ds.simulation.workflow = last_json

    return ds


def fill_wall(shot):

    wall = imas.wall()
    gc_d = sf.getgc(shot)

    wall.ids_properties.comment = 'Based on YGC'
    wall.ids_properties.homogeneous_time = 1
#    wall.ids_properties.source = 'AUG Shotfile'
    wall.ids_properties.provider = os.getenv("USER")
    wall.ids_properties.creation_date = datetime.datetime.today().strftime("%Y-%m-%d")
    wall.code.name= "getgc"
    wall.output_flag = 0
    wall.time = np.array([0.0])
    w2d = wall.description_2d
    w2d.resize(2)

    w2d[0].type.name = 'wall'
    w2d[0].type.index = 1    # for gas-tight walls (disjoint PFCs with inner vessel as last limiter_unit; no vessel structure);
    w2d[0].type.description = 'AUG wall (gas-tight)'
    w2d[0].limiter.unit.resize(1)
    for index, key in enumerate(gc_d.keys()):
        gcr = np.array(gc_d[key].r)
        gcz = np.array(gc_d[key].z)

        if (gcr.min() < 1.65) & ( gcr.max() > 1.65) & (gcz.min() < 0.00) & ( gcz.max() > 0.00):

            w2d[0].limiter.unit[0].name = key
            if (gcr[0] == gcr[-1]) & (gcz[0] == gcz[-1]):
                w2d[0].limiter.unit[0].closed = 1
                w2d[0].limiter.unit[0].outline.r = gcr[:-1]
                w2d[0].limiter.unit[0].outline.z = gcz[:-1]
            else:
                w2d[0].limiter.unit[0].closed = 0
                w2d[0].limiter.unit[0].outline.r = gcr
                w2d[0].limiter.unit[0].outline.z = gcz

    w2d[1].type.name = 'wall'
    w2d[1].type.index = 2    # for free boundary codes (disjoint PFCs and vessel) 
    w2d[1].type.description = 'AUG wall (complete)'
    w2d[1].limiter.unit.resize(len(gc_d.keys()))
    for index, key in enumerate(gc_d.keys()):
        gcr = np.array(gc_d[key].r)
        gcz = np.array(gc_d[key].z)
        w2d[1].limiter.unit[index].name = key
        if (gcr[0] == gcr[-1]) & (gcz[0] == gcz[-1]):
            w2d[1].limiter.unit[index].closed = 1
            w2d[1].limiter.unit[index].outline.r = gcr[:-1]
            w2d[1].limiter.unit[index].outline.z = gcz[:-1]
        else:
            w2d[1].limiter.unit[index].closed = 0
            w2d[1].limiter.unit[index].outline.r = gcr
            w2d[1].limiter.unit[index].outline.z = gcz

    return wall


def fill_tf(equ):

    tf = imas.tf()
    tf.ids_properties.comment = 'Based on jpol from EQI'
    tf.ids_properties.homogeneous_time = 1
#    tf.ids_properties.source = 'AUG Shotfile'
    tf.ids_properties.provider = os.getenv("USER")
    tf.ids_properties.creation_date = datetime.datetime.today().strftime("%Y-%m-%d")

    tf.r0 = equ.R0
    B0 = np.array(equ.B0)

    tf.b_field_tor_vacuum_r.data = B0 * equ.R0
    tf.time = np.array(equ.time)

    return tf


def fill_distributions(rbb, equ):

    from identifiers.distribution_source_identifier import distribution_source_identifier

    dist = imas.distributions()
    dist.ids_properties.provider = os.getenv('USER')
    dist.ids_properties.homogeneous_time = 1
    dist.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
    dist.code.name = 'RABBIT'
    dist.time = rbb.time

    nnbi  = len(rbb.INPUT.einj)
    nt_nb = len(dist.time)

    dd = dist.distribution
    dd.resize(nnbi)
    
    interp_vol = CubicSpline(rbb.INPUT.rhot_in, rbb.INPUT.vol , bc_type='natural', axis=1)
    area_vol = rbb.INPUT.area[0, 0]/rbb.INPUT.vol[0, 0] # 1/(2*pi*R0)
    vol_out = interp_vol(rbb.OUTPUT.rhot_out)
    interp_area = CubicSpline(rbb.INPUT.rhot_in, rbb.INPUT.area, bc_type='natural', axis=1)
    area_out = interp_area(rbb.OUTPUT.rhot_out)

    for jnb in range(nnbi):
        dd[jnb].process.resize(1)
        dd[jnb].process[0].type.name = 'NBI'
        dd[jnb].process[0].type.index = distribution_source_identifier['NBI']['index']
        dd[jnb].process[0].type.description = distribution_source_identifier['NBI']['description']
        dd[jnb].profiles_1d.resize(nt_nb)
        dd[jnb].global_quantities.resize(nt_nb)
        spec_label = spec_lbl[(rbb.INPUT.z_beam[jnb], rbb.INPUT.a_beam[jnb])]
        for jt in range(nt_nb):
            dp1d = dd[jnb].profiles_1d[jt]
            dp1d.grid.rho_tor_norm = np.array(rbb.OUTPUT.rhot_out)
# Filling
            dp1d.grid.volume       = vol_out[jt]
            #dp1d.grid.area         = vol_out[jt] * area_vol
            dp1d.grid.area         = area_out[jt]
            dp1d.density_fast      = np.array(rbb.OUTPUT.bdens[jt, :, jnb])
            dp1d.pressure          = np.array(rbb.OUTPUT.press[jt, :, jnb]) 
# PWL: not sure we should use lab-frame parallel pressure here: modelling is typically carried out in plasma reference frame; difference is 10-30% for typical AUG NBI case 
            #dp1d.pressure_fast_parallel = np.array(rbb.OUTPUT.wfi_par_lab[jt, :, jnb])
            dp1d.pressure_fast_parallel = 2.0 * np.array(rbb.OUTPUT.wfi_par[jt, :, jnb]) # press_parallel=2*wfi_par (Rabbit definition according to M. Weiland)
            dp1d.pressure_fast_perpendicular     = np.array(rbb.OUTPUT.wfi_perp[jt, :, jnb]) # Pperp = wfiperp  (Rabbit definition according to M. Weiland)
            dp1d.pressure_fast     = (dp1d.pressure_fast_parallel +(2.0 * dp1d.pressure_fast_perpendicular))/3.0 # trace of pressure tensor
            
            dp1d.current_tor         = np.array(rbb.OUTPUT.jnbcd[jt, :, jnb])
            dp1d.current_phi         = np.array(rbb.OUTPUT.jnbcd[jt, :, jnb])
            dp1d.current_fast_tor    = np.array(rbb.OUTPUT.jfi[jt, :, jnb])
            dp1d.current_fast_phi    = np.array(rbb.OUTPUT.jfi[jt, :, jnb])
            dp1d.torque_tor_j_radial = np.array(rbb.OUTPUT.torqjxb[jt, :, jnb])
            dp1d.torque_phi_j_radial = np.array(rbb.OUTPUT.torqjxb[jt, :, jnb])
            dp1d.thermalisation.momentum_tor = np.array(rbb.OUTPUT.torqth[jt, :, jnb])
            dp1d.collisions.electrons.power_thermal = np.array(rbb.OUTPUT.powe[jt, :, jnb])
# Ion species
            dp1d.collisions.ion.resize(1)
            dp1d.collisions.ion[0].element.resize(1)
            dp1d.collisions.ion[0].element[0].a   = float(rbb.INPUT.a_beam[0])
            dp1d.collisions.ion[0].element[0].z_n = rbb.INPUT.z_beam[0]
            dp1d.collisions.ion[0].element[0].atoms_n = 1 # Number of atoms of this element in the molecule
            dp1d.collisions.ion[0].z_ion     = rbb.INPUT.z_beam[0]
            dp1d.collisions.ion[0].label     = spec_label
            dp1d.collisions.ion[0].power_thermal = np.array(rbb.OUTPUT.powi[jt, :, jnb])
            dp1d.collisions.ion[0].torque_thermal_tor = np.array(rbb.OUTPUT.torqi[jt, :, jnb])
            dp1d.collisions.ion[0].torque_thermal_phi = np.array(rbb.OUTPUT.torqi[jt, :, jnb])
            dp1d.source.resize(1)
            dp1d.source[0].particles = np.array(rbb.OUTPUT.bdep[jt, :, jnb])
            dp1d.source[0].energy = np.array(rbb.OUTPUT.powe[jt, :, jnb]) + np.array(rbb.OUTPUT.powi[jt, :, jnb]) # ???
            dp1d.source[0].momentum_tor = np.array(rbb.OUTPUT.torqdepo[jt, :, jnb])
            dp1d.source[0].momentum_phi = np.array(rbb.OUTPUT.torqdepo[jt, :, jnb])

            dgq = dd[jnb].global_quantities[jt]
            dgq.particles_n = np.trapz(dp1d.density_fast, dp1d.grid.volume)
            dgq.particles_fast_n = np.trapz(dp1d.density_fast, dp1d.grid.volume)
            #dgq.energy = 
            #dgq.energy_fast = 
            #dgq.energy_fast_parallel = 
            #dgq.torque_tor_j_radial = 
            #dgq.torque_phi_j_radial = 
            dgq.current_tor = np.trapz(dp1d.current_tor, dp1d.grid.area)
            dgq.current_phi = np.trapz(dp1d.current_phi, dp1d.grid.area)
            dgq.collisions.electrons.power_thermal = np.trapz(dp1d.collisions.electrons.power_thermal, dp1d.grid.volume)
            dgq.collisions.ion.resize(1)
            dgq.collisions.ion[0].element.resize(1)
            dgq.collisions.ion[0].element[0].a   = float(rbb.INPUT.a_beam[0])
            dgq.collisions.ion[0].element[0].z_n = rbb.INPUT.z_beam[0]
            dgq.collisions.ion[0].element[0].atoms_n = 1 # Number of atoms of this element in the molecule
            dgq.collisions.ion[0].z_ion     = rbb.INPUT.z_beam[0]
            dgq.collisions.ion[0].label     = spec_label
            dgq.collisions.ion[0].power_thermal = np.trapz(dp1d.collisions.ion[0].power_thermal, dp1d.grid.volume)
            dgq.collisions.ion[0].torque_thermal_tor = np.trapz(dp1d.collisions.ion[0].torque_thermal_tor, dp1d.grid.volume)
            dgq.collisions.ion[0].torque_thermal_phi = np.trapz(dp1d.collisions.ion[0].torque_thermal_phi, dp1d.grid.volume)
            dgq.source.resize(1)
            dgq.source[0].particles = np.trapz(dp1d.source[0].particles, dp1d.grid.volume)
            dgq.source[0].power = np.trapz(dp1d.source[0].energy, dp1d.grid.volume)
            dgq.source[0].torque_tor = np.trapz(dp1d.source[0].momentum_tor, dp1d.grid.volume)
            dgq.source[0].torque_phi = np.trapz(dp1d.source[0].momentum_phi, dp1d.grid.volume)
            
    return dist


def fill_waves(tbb, equ=None):

    wav = imas.waves()
    wav.ids_properties.provider = os.getenv('USER')
    wav.ids_properties.homogeneous_time = 1
    wav.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
    wav.code.name = 'TORBEAM'
    wav.time = tbb.time

    nt_ec = len(wav.time)
    ngy = len(tbb.INPUT.freq)
    nrays = 3

    # DPC -- get volume (similar to code in fill_core_profiles)
    nt_equ, nx_equ = equ.rho_tor.shape
    tfln_wt = np.zeros([nt_ec, nx_equ])
    psin_wt = np.zeros([nt_ec, nx_equ])
    vol_wt  = np.zeros([nt_ec, nx_equ])
    area_wt = np.zeros([nt_ec, nx_equ])
    q_wt    = np.zeros([nt_ec, nx_equ])
    for jrho in range(nx_equ):
        tfln_wt[:, jrho] = np.interp(wav.time, equ.time, equ.tfln[:, jrho])
        psin_wt[:, jrho] = np.interp(wav.time, equ.time, equ.psiN[:, jrho])
        area_wt[:, jrho] = np.interp(wav.time, equ.time, equ.area[:, jrho])
        vol_wt [:, jrho] = np.interp(wav.time, equ.time, equ.vol [:, jrho])
        q_wt   [:, jrho] = np.interp(wav.time, equ.time, equ.q   [:, jrho])

    cw = wav.coherent_wave
    cw.resize(ngy)
    power_out =  np.array(tbb.OUTPUT.power_out)
    jeccd_out = -np.array(tbb.OUTPUT.jeccd_out)

    for jgy in range(ngy):
        cw[jgy].identifier.type.description = 'TORBEAM'
#        cw[jgy].identifier.antenna_name =
# if statement needed to filter empty sources for hcd2core_sources
        if np.mean(tbb.OUTPUT.pecrh) > 0:
            cw[jgy].identifier.type.name   = 'EC'
            cw[jgy].identifier.type.index  = 1
            cw[jgy].wave_solver_type.index = 1 # BEAM/RAY TRACING
# Profiles
        cw[jgy].profiles_1d.resize(nt_ec)
        cw[jgy].global_quantities.resize(nt_ec)
        cw[jgy].beam_tracing.resize(nt_ec)
        for jt in range(nt_ec):
# Time traces
# Map area, vol to profiles# rho-grid
            rhot = np.sqrt(tfln_wt[jt, :])
            rhop = np.sqrt(psin_wt[jt, :])
            rho_pol_norm = np.array(tbb.OUTPUT.rhop_out[:])
            rho_tor_norm = np.interp(rho_pol_norm, rhop, rhot)
            area = np.interp(rho_pol_norm, rhop, area_wt[jt, :])
            vol  = np.interp(rho_pol_norm, rhop,  vol_wt[jt, :])
            q    = np.interp(rho_pol_norm, rhop,    q_wt[jt, :])
# Filling
            cw[jgy].global_quantities[jt].frequency               = tbb.INPUT.freq[jgy]
            cw[jgy].global_quantities[jt].electrons.power_thermal = float(tbb.OUTPUT.pecrh[jt, jgy])
            cw[jgy].global_quantities[jt].power                   = float(tbb.OUTPUT.pecrh[jt, jgy])
            cw[jgy].global_quantities[jt].current_tor             = float(tbb.OUTPUT.ieccd[jt, jgy])*1e-3
            cw[jgy].global_quantities[jt].current_phi             = float(tbb.OUTPUT.ieccd[jt, jgy])*1e-3
            cw[jgy].profiles_1d[jt].grid.rho_pol_norm             = rho_pol_norm
            cw[jgy].profiles_1d[jt].grid.rho_tor_norm             = rho_tor_norm
            cw[jgy].profiles_1d[jt].power_density                 = power_out[jt, :, jgy]
            cw[jgy].profiles_1d[jt].current_parallel_density      = jeccd_out[jt, :, jgy]
            cw[jgy].beam_tracing[jt].beam.resize(nrays)
# Central ray
            cw[jgy].beam_tracing[jt].beam[0].position.r = tbb.OUTPUT.beam_pol_c_r[jt][jgy]*1e-2
            cw[jgy].beam_tracing[jt].beam[0].position.z = tbb.OUTPUT.beam_pol_c_z[jt][jgy]*1e-2
            phi = np.arctan2(tbb.OUTPUT.beam_hor_c_z[jt][jgy], tbb.OUTPUT.beam_hor_c_r[jt][jgy])
            cw[jgy].beam_tracing[jt].beam[0].position.phi = phi
# Lower ray
            cw[jgy].beam_tracing[jt].beam[1].position.r = tbb.OUTPUT.beam_pol_l_r[jt][jgy]*1e-2
            cw[jgy].beam_tracing[jt].beam[1].position.z = tbb.OUTPUT.beam_pol_l_z[jt][jgy]*1e-2
            phi = np.arctan2(tbb.OUTPUT.beam_hor_l_z[jt][jgy], tbb.OUTPUT.beam_hor_l_r[jt][jgy])
            cw[jgy].beam_tracing[jt].beam[1].position.phi = phi
# Upper ray
            cw[jgy].beam_tracing[jt].beam[2].position.r = tbb.OUTPUT.beam_pol_u_r[jt][jgy]*1e-2
            cw[jgy].beam_tracing[jt].beam[2].position.z = tbb.OUTPUT.beam_pol_u_z[jt][jgy]*1e-2
            phi = np.arctan2(tbb.OUTPUT.beam_hor_u_z[jt][jgy], tbb.OUTPUT.beam_hor_u_r[jt][jgy])
            cw[jgy].beam_tracing[jt].beam[2].position.phi = phi

    return wav


def fill_pulse_schedule(plasma):

    ps = imas.pulse_schedule()
    ps.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
    ps.ids_properties.homogeneous_time = 0
#    ps.ids_properties.source = os.path.abspath(__file__)

# TF

    tf = ps.tf

    tf.r0 = plasma.equ.R0
    B0 = np.array(plasma.equ.B0)

    tf.time = np.array(plasma.equ.time)
    tf.b_field_tor_vacuum_r.reference.data = B0 * plasma.equ.R0

# Flux control

    fc = ps.flux_control
    fc.time = np.array(plasma.equ.time)
    fc.i_plasma.reference.data = np.array(plasma.equ.ip)

# NBI

    if hasattr(plasma.nbi, 't_ds'):
        nbu = ps.nbi.unit
        ps.nbi.time = plasma.nbi.t_ds
        nt_nb = len(ps.nbi.time)
        n_nbi = np.sum(plasma.nbi.n_box)
        nbu.resize(n_nbi)

        Rsrc = np.hypot(plasma.nbi.xsrc, plasma.nbi.ysrc)
        zsrc = plasma.nbi.xybsca
        phi_src = np.arctan2(plasma.nbi.ysrc, plasma.nbi.xsrc)
        for jnb in range(n_nbi):
            pow_frac  = plasma.nbi.power_mix[jnb, :].reshape((3, 1)) # energy_mix, t
            part_frac = plasma.nbi.part_mix[jnb, :].reshape((3, 1)) # energy_mix, t
            if jnb < 4:
                nbu[jnb].name = 'INJ1-%s' % (jnb+1)
            else:
                nbu[jnb].name = 'INJ2-%s' % (jnb-3)
            nbu[jnb].species.a   = np.float64(plasma.nbi.abeam[jnb])
            nbu[jnb].species.z_n = np.float64(plasma.nbi.zbeam[jnb])
            nbu[jnb].species.label = spec_lbl[(plasma.nbi.zbeam[jnb], plasma.nbi.abeam[jnb])]
            nbu[jnb].power.reference.data  = np.array(plasma.nbi.power_ds[:, jnb])
            nbu[jnb].energy.reference.data = np.repeat(1e3*plasma.nbi.einj_kev[jnb], nt_nb) #keV->eV

# ECRH
    if hasattr(plasma.ech, 't_ds'):

        ddic_vers = [int(x) for x in imas.__package__.split('ual_')[1].split('_')]
        new_ech = ddic_vers[0] >= 4 and ddic_vers[1] >= 11 and ddic_vers[2] >= 10
        if new_ech:
            ecl = ps.ec.beam
        else:
            ecl = ps.ec.launcher
        ps.ec.time = plasma.ech.t_ds
        nt_ec = len(ps.ec.time)
        ngy = np.sum(plasma.ech.n_gy)
        ecl.resize(ngy)

        for jgy in range(ngy):
            if jgy < plasma.ech.n_gy[0]:
                if plasma.ech.shot < 33725:
                    ecl[jgy].name = 'ECRH1'
                else:
                    ecl[jgy].name = 'ECRH3'
            else:
                ecl[jgy].name = 'ECRH2'
            ecl[jgy].identifier = 'Gy%d' %(jgy+1)
            ecl[jgy].frequency.reference.data = np.repeat(plasma.ech.freq[jgy], nt_ec)
            if new_ech:
                ecl[jgy].power_launched.reference.data = np.array(plasma.ech.power_ds[:, jgy])
            else:
                ecl[jgy].power.reference.data = np.array(plasma.ech.power_ds[:, jgy])
            ecl[jgy].steering_angle_pol.reference.data = np.radians(plasma.ech.theta_ds[:, jgy])
            ecl[jgy].steering_angle_tor.reference.data = - np.radians(plasma.ech.phi_ds[:, jgy])  # flip the sign DPC, 2020-11-24

# ICRF

    if hasattr(plasma.icr, 't_ds'):
        ica = ps.ic.antenna
        ps.ic.time = plasma.icr.t_ds
        nt_ic = len(ps.ic.time)
        nant = plasma.icr.power_ds.shape[1]
        ica.resize(nant)

        for jant in range(nant):
            ica[jant].name = str(jant+1)
            ica[jant].identifier = str(jant+1)
            ica[jant].frequency.data = np.repeat(plasma.icr.freq[jant], nt_ic)
            ica[jant].power.reference.data = np.array(plasma.icr.power_ds[:, jant])

    return ps


def fill_core_sources(equ=None, rbb=None, tbb=None, n_rho=101):

    from identifiers.core_source_identifier import core_source_identifier

    cs = imas.core_sources()
    cs.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
    cs.ids_properties.homogeneous_time = 1
    nsrc = 0
    if rbb is not None:
        nsrc += 1
    if tbb is not None:
        nsrc += 1
    cs.source.resize(nsrc)

    rho_grid = np.linspace(0., 1., n_rho)

    jsrc = 0

    if rbb is not None:
        tmp  = np.sum(np.array(rbb.OUTPUT.powi), axis=2)
        powi = interp1d(rbb.OUTPUT.rhot_out, tmp, fill_value='extrapolate', axis=1)
        tmp  = np.sum(np.array(rbb.OUTPUT.powe), axis=2)
        powe = interp1d(rbb.OUTPUT.rhot_out, tmp, fill_value='extrapolate', axis=1)
        jpar = np.zeros_like(powi)
        tmp  = np.sum(np.array(rbb.OUTPUT.torqdepo), axis=2)
        torq = interp1d(rbb.OUTPUT.rhot_out, tmp, fill_value='extrapolate', axis=1)
        tmp  = np.sum(np.array(rbb.OUTPUT.bdep), axis=2)
        bdep = interp1d(rbb.OUTPUT.rhot_out, tmp, fill_value='extrapolate', axis=1)
        powi_tot = np.sum(np.array(rbb.OUTPUT.powi_tot), axis=1)
        powe_tot = np.sum(np.array(rbb.OUTPUT.powe_tot), axis=1)
        inbcd = np.sum(np.array(rbb.OUTPUT.inbcd), axis=1)

        cs.time = rbb.time
        cs.source[jsrc].identifier.name = 'nbi'
        cs.source[jsrc].identifier.index = core_source_identifier['nbi']['index']
        cs.source[jsrc].identifier.description = core_source_identifier['nbi']['description']

        nt_nb = len(rbb.time)

        # DPC -- get volume (similar to code in fill_core_profiles)
        nt_equ, nx_equ = equ.rho_tor.shape
        tfln_cst = np.zeros([nt_nb, nx_equ])
        vol_cst  = np.zeros([nt_nb, nx_equ])
        area_cst = np.zeros([nt_nb, nx_equ])
        q_cst    = np.zeros([nt_nb, nx_equ])
        for jrho in range(nx_equ):
            tfln_cst[:, jrho] = np.interp(cs.time, equ.time, equ.tfln[:, jrho])
            area_cst[:, jrho] = np.interp(cs.time, equ.time, equ.area[:, jrho])
            vol_cst [:, jrho] = np.interp(cs.time, equ.time, equ.vol [:, jrho])
            q_cst   [:, jrho] = np.interp(cs.time, equ.time, equ.q   [:, jrho])

        cs_nb = cs.source[jsrc].profiles_1d
        cs_nb.resize(nt_nb)
        nnbi  = len(rbb.INPUT.einj) #I assume nr. injectors
        spec_label = spec_lbl[(rbb.INPUT.z_beam[0], rbb.INPUT.a_beam[0])]
        for jt in range(nt_nb):
# Map area, vol to profiles# rho-grid
            rho_cst = np.sqrt(tfln_cst[jt, :])
            area = np.interp(rho_grid, rho_cst, area_cst[jt, :])
            vol  = np.interp(rho_grid, rho_cst,  vol_cst[jt, :])
            q    = np.interp(rho_grid, rho_cst,    q_cst[jt, :])
# Filling
            cs_nb[jt].grid.rho_tor_norm = rho_grid
            cs_nb[jt].grid.volume       = vol
            cs_nb[jt].grid.area         = area
            cs_nb[jt].total_ion_energy = powi(rho_grid)[jt, :]
            #Rabbit provides toroidal current, not parallel current
            cs_nb[jt].j_parallel = np.zeros_like(cs_nb[jt].total_ion_energy)
            cs_nb[jt].momentum_tor        = torq(rho_grid)[jt, :]
            cs_nb[jt].electrons.particles = bdep(rho_grid)[jt, :]
            cs_nb[jt].electrons.energy    = powe(rho_grid)[jt, :]
            cs_nb[jt].ion.resize(1)
            cs_nb[jt].ion[0].element.resize(1)
            cs_nb[jt].ion[0].element[0].a   = float(rbb.INPUT.a_beam[0])
            cs_nb[jt].ion[0].element[0].z_n = rbb.INPUT.z_beam[0]
            cs_nb[jt].ion[0].element[0].atoms_n = 1 # Number of atoms of this element in the molecule
            cs_nb[jt].ion[0].z_ion     = rbb.INPUT.z_beam[0]
            cs_nb[jt].ion[0].label     = spec_label
            cs_nb[jt].ion[0].particles = bdep(rho_grid)[jt, :]
            cs_nb[jt].ion[0].energy = powi(rho_grid)[jt, :]
            cs_nb[jt].total_ion_energy = powi(rho_grid)[jt, :]
            
               
        cs_nb_gp =  cs.source[jsrc].global_quantities
        cs_nb_gp.resize(nt_nb)
        for jt in range(nt_nb):
            cs_nb_gp[jt].power = np.float64(powe_tot[jt] + powi_tot[jt])
            cs_nb_gp[jt].electrons.power = np.float64(powe_tot[jt])
            #             cs_nb_gp[jt].electrons.particles = 
            cs_nb_gp[jt].total_ion_power = np.float64(powi_tot[jt])
            #             cs_nb_gp[jt].total_ion_particles
            cs_nb_gp[jt].current_parallel = np.float64(inbcd[jt])

        jsrc += 1

    if tbb is not None:
        cs.time = tbb.time
        cs.source[jsrc].identifier.name = 'ec'
        cs.source[jsrc].identifier.index = core_source_identifier['ec']['index']
        cs.source[jsrc].identifier.description = core_source_identifier['ec']['description']

        #Filling IDS needed for JINTRAC [Guillermo]
        nt_ec = len(tbb.time)
        cs_ec = cs.source[jsrc].profiles_1d
        cs_ec.resize(nt_ec)
        ngy = len(tbb.INPUT.freq) #I assume nr. beams
        pecrh =  np.sum(np.array(tbb.OUTPUT.power_out), axis=2)
        jeccd = -np.sum(np.array(tbb.OUTPUT.jeccd_out), axis=2)
        rhop = np.array(tbb.OUTPUT.rhop_out)

        # DPC -- get volume (similar to code in fill_core_profiles)
        nt_equ, nx_equ = equ.rho_tor.shape
        tfln_cst = np.zeros([nt_nb, nx_equ])
        vol_cst  = np.zeros([nt_nb, nx_equ])
        area_cst = np.zeros([nt_nb, nx_equ])
        q_cst    = np.zeros([nt_nb, nx_equ])
        for jrho in range(nx_equ):
            tfln_cst[:, jrho] = np.interp(cs.time, equ.time, equ.tfln[:, jrho])
            area_cst[:, jrho] = np.interp(cs.time, equ.time, equ.area[:, jrho])
            vol_cst [:, jrho] = np.interp(cs.time, equ.time, equ.vol [:, jrho])
            q_cst   [:, jrho] = np.interp(cs.time, equ.time, equ.q   [:, jrho])
       
        for jt in range(nt_ec):
# Map area, vol to profiles# rho-grid
            rho_cst = np.sqrt(tfln_cst[jt, :])
            area = np.interp(rho_grid, rho_cst, area_cst[jt, :])
            vol  = np.interp(rho_grid, rho_cst,  vol_cst[jt, :])
            q    = np.interp(rho_grid, rho_cst,    q_cst[jt, :])
# Filling
            rhot = np.squeeze(sf.rho2rho(equ, rhop, t_in=tbb.time[jt], coord_in='rho_pol', coord_out='rho_tor'))
            cs_ec[jt].grid.rho_tor_norm = rho_grid
            cs_ec[jt].grid.volume       = vol
            cs_ec[jt].grid.area         = area
            ptmp = interp1d(rhot, pecrh[jt, :] , fill_value='extrapolate')
            jtmp = interp1d(rhot, jeccd[jt, :] , fill_value='extrapolate')
            cs_ec[jt].j_parallel       = jtmp(rho_grid) #[A/m2]
            cs_ec[jt].electrons.energy = ptmp(rho_grid) #[MW/m3]
            cs_ec[jt].total_ion_energy = np.zeros_like(cs_ec[jt].electrons.energy)

        cs_ec_gp =  cs.source[jsrc].global_quantities
        cs_ec_gp.resize(nt_ec)
        for jt in range(nt_ec):
            cs_ec_gp[jt].power = np.float64(np.array(tbb.OUTPUT.pecrh[jt]).sum())
            cs_ec_gp[jt].electrons.power = np.float64(np.array(tbb.OUTPUT.pecrh[jt]).sum())
            cs_ec_gp[jt].total_ion_power = np.float64(0.0)
            cs_ec_gp[jt].current_parallel = -np.float64(np.array(tbb.OUTPUT.ieccd[jt]).sum()) ### DPC: ToDo this is probably the toroidal current; minus sign to match jeccd above
        
    return cs


def fill_core_profiles(plasma,dist=None):
    cp = imas.core_profiles()

    if dist is not None:
        print("dist available for being added to core profiles:")
        dd = dist.distribution 
        nnbi  = len(dd)
        nt_nb = len(dist.time)
        
    pr_obj = plasma.profiles
    if hasattr(pr_obj, 'time'):
        if not hasattr(pr_obj, 'psi'):
            rhop2rhot.rhop2rhot(equ, pr_obj)
        cp.time = pr_obj.time
        if hasattr(plasma.sig, 'zef'):
            plasma.ion_d['zef1d'] = plasma.zef.data_ds
        else:
            plasma.ion_d['zef1d'] = cp.time*0 + 1.5      # in the absence of any Zeff data, assume 1.5
        logger.warning('Zeff forced to 1.5 in the absence of any other data')

    nt_cp = len(cp.time)
    nx_cp = len(pr_obj.rho)
 
    nt_equ, nx_equ = plasma.equ.rho_tor.shape

    rho_tor_n = pr_obj.rho
    sgn_psi = -1 # COCO 17 to 11

    cp1d = cp.profiles_1d
    cp1d.resize(nt_cp)

    cp.ids_properties.homogeneous_time = 1   # same timebase for core_profiles IDS
    cp.ids_properties.creation_date = datetime.datetime.today().strftime("%d/%m/%y")
#    cp.ids_properties.source = os.path.abspath(__file__)

    eV_m3_to_Pa = 1.602*1e-19

# Interpolate equilibrium quantities on profiles' time grid

    tfb = np.interp(cp.time, plasma.equ.time, plasma.equ.tfl[:, -1])
    B0  = np.interp(cp.time, plasma.equ.time, plasma.equ.B0)

    cp.vacuum_toroidal_field.r0 = plasma.equ.R0 # R0
    cp.vacuum_toroidal_field.b0 = B0

# profiles_1d

    rho_tor_b = np.sqrt(tfb/(np.pi*B0))
    rho_tor = np.outer(rho_tor_b, rho_tor_n) # (nt_cp)x(nx_cp)

    tfln_cpt = np.zeros([nt_cp, nx_equ])
    vol_cpt  = np.zeros([nt_cp, nx_equ])
    area_cpt = np.zeros([nt_cp, nx_equ])
    q_cpt    = np.zeros([nt_cp, nx_equ])
    psi_cpt  = np.zeros([nt_cp, nx_equ])
    jav_cpt  = np.zeros([nt_cp, nx_equ])
    for jrho in range(nx_equ):
        tfln_cpt[:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.tfln[:, jrho])
        area_cpt[:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.area[:, jrho])
        vol_cpt [:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.vol [:, jrho])
        q_cpt   [:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.q   [:, jrho])
        psi_cpt [:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.pfl [:, jrho])
        jav_cpt [:, jrho] = np.interp(cp.time, plasma.equ.time, plasma.equ.jav [:, jrho])

    zimp = np.float64(plasma.ion_d['Zimp'])
    if len(pr_obj.Ti.diags) == 0:
        logger.warning('No Ti data --- setting Ti = Te')
    if len(pr_obj.Angf.diags) == 0:
        logger.warning('No vtor data --- setting vtor = 0')

    for jt in range(nt_cp):
        te   = pr_obj.Te.yfit_rhot[jt, :]
        ne   = pr_obj.Ne.yfit_rhot[jt, :]
        nimp = (plasma.ion_d['zef1d'][jt] - 1.)/(zimp**2 - zimp)*ne
        nd = ne - zimp*nimp
        if len(pr_obj.Ti.diags) > 0:
            ti = pr_obj.Ti.yfit_rhot[jt, :]
        else:
            ti = te


# Map area, vol to profiles# rho-grid
        rho_cpt = np.sqrt(tfln_cpt[jt, :])
        area = np.interp(rho_tor_n, rho_cpt, area_cpt[jt, :])
        vol  = np.interp(rho_tor_n, rho_cpt,  vol_cpt[jt, :])
        q    = np.interp(rho_tor_n, rho_cpt,    q_cpt[jt, :])
        psi  = np.interp(rho_tor_n, rho_cpt,  psi_cpt[jt, :])
        jav  = np.interp(rho_tor_n, rho_cpt,  jav_cpt[jt, :])
# Filling

        cp1d[jt].grid.rho_tor      = rho_tor[jt, :]  
        #cp1d[jt].grid.psi          = pr_obj.psi[jt, :]*sgn_psi
        cp1d[jt].grid.psi          = psi
        #cp1d[jt].grid.psi_magnetic_axis = psi[0]     -- seems to cause problems later in ETS
        #cp1d[jt].grid.psi_boundary = psi[-1]     -- seems to cause problems later in ETS
        #cp1d[jt].grid.rho_pol_norm = np.sqrt((psi-psi[0])/(psi[-1]-psi[0]))     -- seems to cause problems later in ETS
        cp1d[jt].grid.rho_tor_norm = rho_tor_n
        cp1d[jt].grid.area         = area
        cp1d[jt].grid.volume       = vol
        cp1d[jt].electrons.temperature     = te
        cp1d[jt].electrons.temperature_error_upper = pr_obj.Te.yerr_rhot[jt, :]
        cp1d[jt].electrons.temperature_error_lower = pr_obj.Te.yerr_rhot[jt, :]
        cp1d[jt].electrons.density         = ne
        cp1d[jt].electrons.density_thermal = ne
        cp1d[jt].electrons.density_thermal_error_upper = pr_obj.Ne.yerr_rhot[jt, :]
        cp1d[jt].electrons.density_thermal_error_lower = pr_obj.Ne.yerr_rhot[jt, :]
        cp1d[jt].electrons.pressure = eV_m3_to_Pa*ne*te
        cp1d[jt].electrons.pressure_thermal = eV_m3_to_Pa*ne*te

        cp1d[jt].q = q
        cp1d[jt].j_total = jav            # DPC: not really sure whether jav is j_total, j_phi or something else
        cp1d[jt].j_tor = jav
        cp1d[jt].j_phi = jav

        cp1d[jt].t_i_average = ti
        cp1d[jt].ion.resize(2)

        cp1d[jt].ion[0].z_ion = np.float64(plasma.ion_d['Zmain'])
        cp1d[jt].ion[0].temperature = ti
        cp1d[jt].ion[0].temperature_error_upper = pr_obj.Ti.yerr_rhot[jt, :]
        cp1d[jt].ion[0].temperature_error_lower = pr_obj.Ti.yerr_rhot[jt, :]
        cp1d[jt].ion[0].label       = spec_lbl[(plasma.ion_d['Zmain'], plasma.ion_d['Amain'])]
        if len(pr_obj.Angf.diags) > 0:
            vt = pr_obj.Angf.yfit_rhot[jt, :]
            cp1d[jt].ion[0].rotation_frequency_tor = vt
            cp1d[jt].ion[0].rotation_frequency_tor_sonic = vt
            cp1d[jt].ion[0].rotation_frequency_tor_error_upper = pr_obj.Angf.yerr_rhot[jt, :]
            cp1d[jt].ion[0].rotation_frequency_tor_error_lower = pr_obj.Angf.yerr_rhot[jt, :]
        else:
            logger.debug('angf=zero')
            vt = np.zeros_like(te)
            cp1d[jt].ion[0].rotation_frequency_tor = vt
            cp1d[jt].ion[0].rotation_frequency_tor_sonic = vt
        cp1d[jt].ion[0].density          = nd
        cp1d[jt].ion[0].density_thermal  = nd
        cp1d[jt].ion[0].pressure         = eV_m3_to_Pa*nd*ti
        cp1d[jt].ion[0].pressure_thermal = eV_m3_to_Pa*nd*ti
       
        if dist is not None:
            cp1d[jt].ion[0].density_fast=np.float64(0.0) # needs to be initialised: sum
            cp1d[jt].ion[0].pressure_fast=np.float64(0.0) 
            cp1d[jt].ion[0].pressure_fast_perpendicular=np.float64(0.0) 
            cp1d[jt].ion[0].pressure_fast_parallel=np.float64(0.0)
            
            # define temporary arrays to sum up contributions of fast density/pressure:
            #see above: the Rabbit radial grid is: dd[0].profiles_1d[jt].grid.rho_tor_norm = np.array(rbb.OUTPUT.rhot_out)
            sum_density_fast=len(dd[0].profiles_1d[jt].grid.rho_tor_norm) #assuming the grid in dsitributions is the same for all sources
            sum_density_fast=np.float64(0.0)
            sum_pressure_fast=len(dd[0].profiles_1d[jt].grid.rho_tor_norm)
            sum_pressure_fast=np.float64(0.0)
            sum_pressure_fast_perp=len(dd[0].profiles_1d[jt].grid.rho_tor_norm)
            sum_pressure_fast_prep=np.float64(0.0)
            sum_pressure_fast_parallel=len(dd[0].profiles_1d[jt].grid.rho_tor_norm)
            sum_pressure_fast_parallel=np.float64(0.0)
                              
            for jnb in range(nnbi):   # sum up dens/press in distribution IDS
                dp1d = dd[jnb].profiles_1d[jt]
                sum_density_fast=sum_density_fast+dp1d.density_fast
                sum_pressure_fast=sum_pressure_fast+dp1d.pressure_fast
                sum_pressure_fast_perp=sum_pressure_fast_perp+dp1d.pressure_fast_perpendicular
                sum_pressure_fast_parallel=sum_pressure_fast_parallel+dp1d.pressure_fast_parallel             

            #interpolate to core_profile rho_tor_norm: (monotonicity conserving splines)
            pchip_dens_fast=PchipInterpolator(dd[0].profiles_1d[jt].grid.rho_tor_norm, sum_density_fast, axis=0)
            pchip_press_fast=PchipInterpolator(dd[0].profiles_1d[jt].grid.rho_tor_norm, sum_pressure_fast, axis=0)
            pchip_press_fast_perp=PchipInterpolator(dd[0].profiles_1d[jt].grid.rho_tor_norm, sum_pressure_fast_perp, axis=0)
            pchip_press_fast_par=PchipInterpolator(dd[0].profiles_1d[jt].grid.rho_tor_norm, sum_pressure_fast_parallel, axis=0)

            #make sure the total particle number and the overall integrated pressure remain constant after interpolation:
            n_fast= np.trapz(sum_density_fast, dp1d.grid.volume)
            pchip_dens_fast_vol=PchipInterpolator(dd[0].profiles_1d[jt].grid.volume, sum_density_fast, axis=0)
            n_fast_sp= np.trapz(pchip_dens_fast_vol(cp1d[jt].grid.volume), cp1d[jt].grid.volume)
            if abs(n_fast) < 1.0E-16 or abs(n_fast_sp) < 1.0E-16: # avoid possible division by 0
                 n_fast=1.0
                 n_fast_sp=1.0
            #print("n_fast, n_fast_sp: ", n_fast,n_fast_sp,n_fast/n_fast_sp

            p_fast= np.trapz(sum_pressure_fast, dp1d.grid.volume)
            pchip_press_fast_vol=PchipInterpolator(dd[0].profiles_1d[jt].grid.volume, sum_pressure_fast, axis=0)
            p_fast_sp= np.trapz(pchip_press_fast_vol(cp1d[jt].grid.volume), cp1d[jt].grid.volume)          
            if abs(p_fast) < 1.0E-16 or abs(p_fast_sp) < 1.0E-16: # avoid possible division by 0
                 p_fast=1.0
                 p_fast_sp=1.0
            #print("p_fast, p_fast_sp: ", p_fast,p_fast_sp,p_fast/p_fast_sp)

            p_fast_perp= np.trapz(sum_pressure_fast_perp, dp1d.grid.volume)
            pchip_press_fast_perp_vol=PchipInterpolator(dd[0].profiles_1d[jt].grid.volume, sum_pressure_fast_perp, axis=0)
            p_fast_perp_sp= np.trapz(pchip_press_fast_perp_vol(cp1d[jt].grid.volume), cp1d[jt].grid.volume)          
            if abs(p_fast_perp) < 1.0E-16 or abs(p_fast_perp_sp) < 1.0E-16: # avoid possible division by 0
                 p_fast_perp=1.0
                 p_fast_perp_sp=1.0
            #print("p_fast_perp, p_fast_perp_sp: ", p_fast_perp,p_fast_perp_sp,p_fast_perp/p_fast_perp_sp)

            p_fast_par= np.trapz(sum_pressure_fast_parallel, dp1d.grid.volume)
            pchip_press_fast_par_vol=PchipInterpolator(dd[0].profiles_1d[jt].grid.volume, sum_pressure_fast_parallel, axis=0)
            p_fast_par_sp= np.trapz(pchip_press_fast_par_vol(cp1d[jt].grid.volume), cp1d[jt].grid.volume)          
            if abs(p_fast_par) < 1.0E-16 or abs(p_fast_par_sp) < 1.0E-16: # avoid possible division by 0
                 p_fast_par=1.0
                 p_fast_par_sp=1.0
            #print("p_fast_par, p_fast_par_sp: ", p_fast_par,p_fast_par_sp,p_fast_par/p_fast_par_sp)
                 
            cp1d[jt].ion[0].density_fast=pchip_dens_fast(cp1d[jt].grid.rho_tor_norm)*n_fast/n_fast_sp
            cp1d[jt].ion[0].density=cp1d[jt].ion[0].density_thermal+(pchip_dens_fast(cp1d[jt].grid.rho_tor_norm)*n_fast/n_fast_sp) # add thermal and fast densities
            cp1d[jt].ion[0].pressure=cp1d[jt].ion[0].pressure_thermal+(pchip_press_fast_perp(cp1d[jt].grid.rho_tor_norm)*p_fast_perp/p_fast_perp) # add thermal and fast pressure_perp
            cp1d[jt].ion[0].pressure_fast_perpendicular=pchip_press_fast_perp(cp1d[jt].grid.rho_tor_norm)*p_fast_perp/p_fast_perp
            cp1d[jt].ion[0].pressure_fast_parallel=pchip_press_fast_par(cp1d[jt].grid.rho_tor_norm)*p_fast_par/p_fast_par
                
            #print('density_fast at time jt: ', cp1d[jt].ion[0].density_fast)
            #print('pressure_thermal at time jt: ', cp1d[jt].ion[0].pressure_thermal)
            #print('pressure_fast at time jt: ', cp1d[jt].ion[0].pressure_fast)
            #print('pressure_fast_perp at time jt: ', cp1d[jt].ion[0].pressure_fast_perp)
            #print('pressure_fast_parallel at time jt: ', cp1d[jt].ion[0].pressure_fast_parallel)
            
        cp1d[jt].ion[0].element.resize(1)
        cp1d[jt].ion[0].element[0].a   = np.float64(plasma.ion_d['Amain'])
        cp1d[jt].ion[0].element[0].z_n = np.float64(plasma.ion_d['Zmain'])
        cp1d[jt].ion[0].element[0].atoms_n = 1
        cp1d[jt].ion[0].multiple_states_flag = 0
    
        cp1d[jt].ion[1].z_ion = zimp
        cp1d[jt].ion[1].temperature = ti
        cp1d[jt].ion[1].label = spec_lbl[(plasma.ion_d['Zimp'], plasma.ion_d['Aimp'])]
        cp1d[jt].ion[1].rotation_frequency_tor = vt
        cp1d[jt].ion[1].rotation_frequency_tor_sonic = vt
        cp1d[jt].ion[1].density          = nimp
        cp1d[jt].ion[1].density_thermal  = nimp
        cp1d[jt].ion[1].pressure         = eV_m3_to_Pa*nimp*ti
        cp1d[jt].ion[1].pressure_thermal = eV_m3_to_Pa*nimp*ti
        cp1d[jt].ion[1].element.resize(1)
        cp1d[jt].ion[1].element[0].a   = np.float64(plasma.ion_d['Aimp'])
        cp1d[jt].ion[1].element[0].z_n = zimp
        cp1d[jt].ion[1].element[0].atoms_n = 1
        cp1d[jt].ion[1].multiple_states_flag = 0
    
        cp1d[jt].zeff = np.repeat(plasma.ion_d['zef1d'][jt], nx_cp)

    cp.global_quantities.ip = np.interp(cp.time, plasma.equ.time, plasma.equ.ip)


    return cp
