import numpy as np

class SFARR(np.ndarray):
    """
    Subclass of numpy.ndarray adding a few metadata,
    such as physical unit, description and relations
    """

    def __new__(cls, input_array, info=None):

        obj = np.asarray(input_array).view(cls)
        for attr in ('units', 'dims', 'long_name'):
            if hasattr(info, attr):
                obj.__dict__[attr] = info.__dict__[attr]
            else:
                obj.__dict__[attr] = None

        return obj

    def __array_finalize__(self, obj):

        if obj is None: return
        for attr in ('units', 'dims', 'long_name'):
            self.__dict__[attr] = getattr(obj, attr, None)
